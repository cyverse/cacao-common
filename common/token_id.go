package common

import (
	"crypto/rand"
	"fmt"
	"github.com/rs/xid"
	"golang.org/x/crypto/sha3"
	"strings"
	"time"
)

// TokenID is an id for Cacao Tokens, with format '<prefix>-<SHA3-hash>-<xid>'
type TokenID string

// TokenIDPrefix is the prefix for different types of tokens
type TokenIDPrefix string

const (
	// PersonalTokenPrefix is the personal token type prefix
	PersonalTokenPrefix TokenIDPrefix = "cptoken"
	// DelegatedTokenPrefix is the delegated token type prefix
	DelegatedTokenPrefix TokenIDPrefix = "cdtoken"
	// InternalTokenPrefix is the internal token type prefix
	InternalTokenPrefix TokenIDPrefix = "citoken"
)

// Define default size for random bytes in our tokens
const randomByteSize = 64

// Define default salt size for tokens
const saltSize = 16

// generateRandomBytes gives us size bytes of cryptographically secure randomness
func generateRandomBytes(size int) ([]byte, error) {
	var salt = make([]byte, size)

	_, err := rand.Read(salt[:])

	if err != nil {
		return []byte{}, err
	}

	return salt, nil
}

// NewTokenID creates a new TokenID for the given type, owner, + created time
func NewTokenID(typeString, owner string, createdAt time.Time, tokenXid xid.ID) (id TokenID, hash string, salt string, err error) {
	prefix := ""
	switch typeString {
	case "personal":
		prefix = string(PersonalTokenPrefix)
	case "delegated":
		prefix = string(DelegatedTokenPrefix)
	case "internal-deploy":
		prefix = string(InternalTokenPrefix)
	default:
		return TokenID(""), "", "", fmt.Errorf("token type must be provided")
	}
	if owner == "" {
		return TokenID(""), "", "", fmt.Errorf("owner must be provided")
	}
	randomBytes, err := generateRandomBytes(randomByteSize)
	if err != nil {
		return TokenID(""), "", "", err
	}
	saltBytes, err := generateRandomBytes(saltSize)
	if err != nil {
		return TokenID(""), "", "", err
	}
	timeBytes, err := createdAt.GobEncode()
	if err != nil {
		return TokenID(""), "", "", err
	}
	tokenString := prefix + fmt.Sprintf("_%x_%x_", randomBytes, timeBytes) + tokenXid.String()
	hashBytes := []byte(tokenString)
	hashBytes = append(hashBytes, saltBytes...)
	h := sha3.New256()
	h.Write(hashBytes)
	return TokenID(tokenString), fmt.Sprintf("%x", h.Sum(nil)), fmt.Sprintf("%x", saltBytes), nil
}

// String returns the string representation of a TokenID
func (tokenID TokenID) String() string {
	return string(tokenID)
}

// Prefix returns the token type prefix portion of a TokenID
func (tokenID TokenID) Prefix() string {
	index := strings.IndexRune(string(tokenID), '_')
	if index <= 0 {
		return ""
	}
	prefix := string(tokenID[:index])
	if prefix == string(PersonalTokenPrefix) || prefix == string(DelegatedTokenPrefix) || prefix == string(InternalTokenPrefix) {
		return prefix
	}
	return ""
}

// XID returns the XID portion of a TokenID
func (tokenID TokenID) XID() xid.ID {
	index := strings.LastIndex(string(tokenID), "_")
	if index < 0 {
		return xid.NilID()
	}
	id, err := xid.FromString(string(tokenID)[index+1:])
	if err != nil {
		return xid.NilID()
	}
	return id
}

// Validate returns true if the token is considered valid, false otherwise
func (tokenID TokenID) Validate() bool {
	prefix := tokenID.Prefix()
	if prefix == "" {
		return false
	}
	if tokenID.XID().IsNil() {
		return false
	}
	return true
}

func isHex(testString string) bool {
	for _, r := range testString {
		if !isRuneHex(r) {
			return false
		}
	}
	return true
}

func isRuneHex(testRune rune) bool {
	if testRune > 'f' {
		return false
	}
	if testRune < '0' {
		return false
	}
	return true
}
