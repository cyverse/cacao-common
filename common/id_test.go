package common

import (
	"testing"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
)

func TestNewID(t *testing.T) {
	prefix := "user"
	id := NewID(prefix)
	assert.Equal(t, string(id[:len(prefix)+1]), prefix+"-")
	_, err := xid.FromString(string(id)[len(prefix)+1:])
	assert.NoError(t, err)
}

func TestNewIDFromXID(t *testing.T) {
	prefix := "prefix"
	newXID := xid.New()
	id := NewIDFromXID(prefix, newXID)
	assert.Equal(t, prefix+"-"+newXID.String(), id.String())
}

func TestID_Prefix(t *testing.T) {
	id0 := NewID("")
	assert.Equal(t, "", id0.FullPrefix())

	id1 := NewID("prefix")
	assert.Equal(t, "prefix", id1.FullPrefix())

	id2 := NewID("primary-sub")
	assert.Equal(t, "primary-sub", id2.FullPrefix())

	id3 := NewID("primary-sub-third")
	assert.Equal(t, "primary-sub-third", id3.FullPrefix())

	empty := ID("")
	assert.Equal(t, "", empty.FullPrefix())

	noHyphen := ID(xid.New().String())
	assert.Equal(t, "", noHyphen.FullPrefix())

	consecutiveHyphen := NewID("f---")
	assert.Equal(t, "f---", consecutiveHyphen.FullPrefix())
}

func TestIDToXID(t *testing.T) {
	earlierXID := xid.New()
	id := NewID("prefix")
	xid1, err := id.XID()
	assert.NoError(t, err)

	xid2 := id.XIDMustParse()
	assert.Equal(t, 0, xid1.Compare(xid2))
	assert.True(t, earlierXID.Compare(xid1) < 0)
	assert.True(t, xid1.Compare(earlierXID) > 0)

	newerXID := xid.New()
	assert.True(t, newerXID.Compare(xid1) > 0)
	assert.True(t, xid1.Compare(newerXID) < 0)

	id3 := ID("")
	_, err = id3.XID()
	assert.Error(t, err)

	id4 := ID("foo")
	_, err = id4.XID()
	assert.Error(t, err)
}

func TestID_Validate(t *testing.T) {
	tests := []struct {
		name string
		id   ID
		want bool
	}{
		{"normal", ID(NewID("foobar")), true},
		{"w/ subtype", ID(NewID("foo-bar")), true},
		{"consecutive hyphen in prefix", ID(NewID("f---")), true},
		{"empty_prefix", ID(NewID("")), false},
		{"uppercase_in_prefix", ID(NewID("FooBar")), false},
		{"number_in_prefix", ID(NewID("foo123")), false},
		{"symbol in prefix", ID(NewID("foo.")), false},
		{"prefix start with digit", ID(NewID("1abc")), false},
		{"bad_xid1", ID("foo-c2iq0rg-oikfkguiqb90"), false},
		{"bad_xid2", ID("foo-c2iq0rg8oikfkg=iqb90"), false},
		{"bad_xid3", ID("foo-c2iq0rg8oikfkguiqb90aaa"), false},
		{"leading hyphen", ID("-c2iq0rg8oikfkguiqb90"), false},
		{"just prefix", ID("foo-"), false},
		{"trailing hyphen", ID("foo-c2iq0rg8oikfkguiqb90-"), false},
		{"random string", ID("randomstring"), false},
		{"just xid", ID("c2iq0rg8oikfkguiqb90"), false},
		{"really long prefix", ID("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa-c2iq0rg8oikfkguiqb90"), false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.id.Validate(); got != tt.want {
				t.Errorf("ID.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestID_PrimaryType(t *testing.T) {
	tests := []struct {
		name string
		id   ID
		want string
	}{
		{
			name: "empty",
			id:   "",
			want: "",
		},
		{
			name: "no hyphen",
			id:   "foobar",
			want: "",
		},
		{
			name: "just hyphen",
			id:   "-",
			want: "",
		},
		{
			name: "just xid",
			id:   "c2iq0rg8oikfkguiqb90",
			want: "",
		},
		{
			name: "consecutive hyphen",
			id:   "f------c2iq0rg8oikfkguiqb90",
			want: "f",
		},
		{
			name: "1 trailing hyphen",
			id:   "foobar-",
			want: "foobar",
		},
		{
			name: "2 hyphen",
			id:   "foo-bar-c2iq0rg8oikfkguiqb90",
			want: "foo",
		},
		{
			name: "3 hyphen",
			id:   "foo-bar-baz-c2iq0rg8oikfkguiqb90",
			want: "foo",
		},
		{
			name: "provider-openstack",
			id:   "provider-openstack-c2iq0rg8oikfkguiqb90",
			want: "provider",
		},
		{
			name: "provider-aws",
			id:   "provider-aws-c2iq0rg8oikfkguiqb90",
			want: "provider",
		},
		{
			name: "provider",
			id:   "provider-c2iq0rg8oikfkguiqb90",
			want: "provider",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.id.PrimaryType(), "PrimaryType()")
		})
	}
}

func TestID_SubType(t *testing.T) {
	tests := []struct {
		name string
		id   ID
		want string
	}{
		{
			name: "empty",
			id:   "",
			want: "",
		},
		{
			name: "short string, not xid",
			id:   "abc",
			want: "",
		},
		{
			name: "just xid",
			id:   "c2iq0rg8oikfkguiqb90",
			want: "",
		},
		{
			name: "leading hyphen",
			id:   "-c2iq0rg8oikfkguiqb90",
			want: "",
		},
		{
			name: "trailing hyphen",
			id:   "foo-",
			want: "",
		},
		{
			name: "consecutive hyphen",
			id:   "f------c2iq0rg8oikfkguiqb90",
			want: "----",
		},
		{
			name: "1 hyphen",
			id:   "foo-c2iq0rg8oikfkguiqb90",
			want: "",
		},
		{
			name: "2 hyphen",
			id:   "foo-bar-c2iq0rg8oikfkguiqb90",
			want: "bar",
		},
		{
			name: "3 hyphen",
			id:   "foo-bar-baz-c2iq0rg8oikfkguiqb90",
			want: "bar-baz",
		},
		{
			name: "provider-openstack",
			id:   "provider-openstack-c2iq0rg8oikfkguiqb90",
			want: "openstack",
		},
		{
			name: "provider-aws",
			id:   "provider-aws-c2iq0rg8oikfkguiqb90",
			want: "aws",
		},
		{
			name: "provider",
			id:   "provider-c2iq0rg8oikfkguiqb90",
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.id.SubType(), "SubType()")
		})
	}
}
