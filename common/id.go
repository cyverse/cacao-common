package common

import (
	"github.com/rs/xid"
	"strings"
	"unicode"
)

// ID for entities, xid with a prefix. <prefix>-<xid>
type ID string

// NewID creates a new ID with a prefix.
// Prefix must be lowercase letters, No special characters or numbers may be used.
func NewID(prefix string) ID {
	return ID(prefix + "-" + xid.New().String())
}

// NewIDFromXID creates a new ID from a prefix and an existing xid.
func NewIDFromXID(prefix string, id xid.ID) ID {
	return ID(prefix + "-" + id.String())
}

// String ...
func (id ID) String() string {
	return string(id)
}

// Prefix is the same as PrimaryType.
//
// Deprecated: Even though this method is called Prefix, it only returns the primary type, not the full prefix.
// This is meant to maintain the behavior for existing usages of this method.
// For full prefix (everything before the last hyphen), use FullPrefix() instead.
func (id ID) Prefix() string {
	return id.PrimaryType()
}

// FullPrefix returns the entire prefix which is anything before the last hyphen (the hyphen before the xid string).
func (id ID) FullPrefix() string {
	lastIndex := strings.LastIndex(string(id), "-")
	if lastIndex < 0 {
		return ""
	}
	return string(id[:lastIndex])
}

// PrimaryType return the first hyphen separated field in the prefix, or anything before 1st hyphen.
// Behavior of this method on invalid ID should not be relied on.
func (id ID) PrimaryType() string {
	index := strings.IndexRune(string(id), '-')
	if index <= 0 {
		return ""
	}
	return string(id[:index])
}

// SubType return the substring between first hyphen and last hyphen (the hyphen before xid string).
// Behavior of this method on invalid ID should not be relied on.
func (id ID) SubType() string {
	startIndex := strings.IndexRune(string(id), '-')
	if startIndex <= 0 {
		return ""
	}
	// last hyphen is the character before xid string (which is 20 characters long)
	endIndex := len(id) - 20
	if endIndex < 0 {
		return ""
	}
	if startIndex+1 > endIndex-1 {
		return ""
	}
	return string(id[startIndex+1 : endIndex-1])
}

// XID convert to XID
func (id ID) XID() (xid.ID, error) {
	startIndex := len(id) - 20
	if startIndex < 0 {
		startIndex = 0
	}
	return xid.FromString(string(id[startIndex:]))
}

// XIDMustParse convert to XID, panic if error
func (id ID) XIDMustParse() xid.ID {
	newXID, err := id.XID()
	if err != nil {
		panic(err)
	}
	return newXID
}

// Validate return true if id is validate.
func (id ID) Validate() bool {
	if len(id) < 22 {
		// 20+1+1
		// 20 characters for xid, 1 for '-', 1 for prefix (non-empty prefix)
		return false
	}
	if len(id) > 100 {
		// id should not be too long
		return false
	}
	if id[0] < 'a' || id[0] > 'z' {
		// must start with letter
		return false
	}
	lastHyphenIndex := strings.LastIndex(string(id), "-")
	if lastHyphenIndex <= 1 {
		// there must be at least 1 hyphen that is not the 1st or 2nd character.
		return false
	}
	if lastHyphenIndex != len(id)-21 {
		// last hyphen must be the one before the xid string
		return false
	}
	for _, r := range id[:lastHyphenIndex] {
		// prefix must be only lower case letter or hyphen
		if !unicode.IsLower(r) && r != '-' {
			return false
		}
	}
	if _, err := xid.FromString(string(id)[len(id)-20:]); err != nil {
		// last 20 character must be a valid xid
		return false
	}
	return true
}
