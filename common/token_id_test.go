package common

import (
	"encoding/hex"
	"fmt"
	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/sha3"
	"testing"
	"time"
)

func TestTokenID_Prefix(t *testing.T) {
	tests := []struct {
		name    string
		tokenID TokenID
		want    string
	}{
		{name: "Personal token", tokenID: TokenID("cptoken_9bcdeb662b49fd9d3830ea145bf007bd69e4399b5bcbe38225e9f64dff25f15e571620a61456195ba5b5561aec5a6acd3500493f6acd757d51818a5f0d2ba99d_010000000edcdce18711fe56c8fe5c_cl5el1u2au5ud19qunog"), want: "cptoken"},
		{name: "Delegated token", tokenID: TokenID("cdtoken_9266ceca2d1d1721b45b5c0b26360f28c6b169203b67d5e50f9d82139f44e9ab9e3cf209b2f7a61f81644a6889b6a792481a19818573d84a793f9fc651bacffb_010000000edcdce1de13675648fe5c_cl5elnm2au5uvki4ic3g"), want: "cdtoken"},
		{name: "Internal token", tokenID: TokenID("citoken_06428c7b16a9e5995211f81d950152657624efe2ce1a0b3c9abe0dd9ffc1fcc8ddac6615dbcc386494a905b9827b9c5716098c237842395d7f2e10fce6819772_010000000edcdce1f825dc0483fe5c_cl5elu62au5up6r0h78g"), want: "citoken"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.tokenID.Prefix(), "Prefix()")
		})
	}
}

func TestTokenID_String(t *testing.T) {
	tests := []struct {
		name    string
		tokenID TokenID
		want    string
	}{
		{name: "TokenID String", tokenID: TokenID("cptoken_9bcdeb662b49fd9d3830ea145bf007bd69e4399b5bcbe38225e9f64dff25f15e571620a61456195ba5b5561aec5a6acd3500493f6acd757d51818a5f0d2ba99d_010000000edcdce18711fe56c8fe5c_cl5el1u2au5ud19qunog"), want: "cptoken_9bcdeb662b49fd9d3830ea145bf007bd69e4399b5bcbe38225e9f64dff25f15e571620a61456195ba5b5561aec5a6acd3500493f6acd757d51818a5f0d2ba99d_010000000edcdce18711fe56c8fe5c_cl5el1u2au5ud19qunog"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.tokenID.String(), "String()")
		})
	}
}

func TestTokenID_Validate(t *testing.T) {
	tests := []struct {
		name    string
		tokenID TokenID
		want    bool
	}{
		{name: "Valid personal token", tokenID: TokenID("cptoken_9bcdeb662b49fd9d3830ea145bf007bd69e4399b5bcbe38225e9f64dff25f15e571620a61456195ba5b5561aec5a6acd3500493f6acd757d51818a5f0d2ba99d_010000000edcdce18711fe56c8fe5c_cl5el1u2au5ud19qunog"), want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.tokenID.Validate(), "Validate()")
		})
	}
}

func TestTokenID_XID(t *testing.T) {
	wantedXid, _ := xid.FromString("cl5el1u2au5ud19qunog")
	tests := []struct {
		name    string
		tokenID TokenID
		want    xid.ID
	}{
		{name: "TokenID XID", tokenID: TokenID("cptoken_9bcdeb662b49fd9d3830ea145bf007bd69e4399b5bcbe38225e9f64dff25f15e571620a61456195ba5b5561aec5a6acd3500493f6acd757d51818a5f0d2ba99d_010000000edcdce18711fe56c8fe5c_cl5el1u2au5ud19qunog"), want: wantedXid},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, tt.tokenID.XID(), "XID()")
		})
	}
}

func Test_isHex(t *testing.T) {
	type args struct {
		testString string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "valid hex chars", args: args{testString: "0123456789abcdef"}, want: true},
		{name: "invalid hex chars", args: args{testString: "literally anything not hex!"}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, isHex(tt.args.testString), "isHex(%v)", tt.args.testString)
		})
	}
}

func Test_isRuneHex(t *testing.T) {
	type args struct {
		testRune rune
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "valid hex rune 0", args: args{testRune: '0'}, want: true},
		{name: "valid hex rune 1", args: args{testRune: '1'}, want: true},
		{name: "valid hex rune 2", args: args{testRune: '2'}, want: true},
		{name: "valid hex rune 3", args: args{testRune: '3'}, want: true},
		{name: "valid hex rune 4", args: args{testRune: '4'}, want: true},
		{name: "valid hex rune 5", args: args{testRune: '5'}, want: true},
		{name: "valid hex rune 6", args: args{testRune: '6'}, want: true},
		{name: "valid hex rune 7", args: args{testRune: '7'}, want: true},
		{name: "valid hex rune 8", args: args{testRune: '8'}, want: true},
		{name: "valid hex rune 9", args: args{testRune: '9'}, want: true},
		{name: "valid hex rune a", args: args{testRune: 'a'}, want: true},
		{name: "valid hex rune b", args: args{testRune: 'b'}, want: true},
		{name: "valid hex rune c", args: args{testRune: 'c'}, want: true},
		{name: "valid hex rune d", args: args{testRune: 'd'}, want: true},
		{name: "valid hex rune e", args: args{testRune: 'e'}, want: true},
		{name: "valid hex rune f", args: args{testRune: 'f'}, want: true},
		{name: "invalid hex rune g", args: args{testRune: 'g'}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, isRuneHex(tt.args.testRune), "isRuneHex(%v)", tt.args.testRune)
		})
	}
}

func TestNewTokenID(t *testing.T) {
	owner := "testuser1"
	tokenType := "personal"
	presetCreatedAt := time.Now()
	newXid := xid.New()
	precomputedToken, tokenHash, presetSalt, _ := NewTokenID(tokenType, owner, presetCreatedAt, newXid)
	// if we know the username, created date, and salt, we can validate the SHA3 hash.
	hashBytes := []byte(precomputedToken)
	saltBytes, _ := hex.DecodeString(presetSalt)
	hashBytes = append(hashBytes, saltBytes...)
	h := sha3.New256()
	h.Write(hashBytes)
	hash := fmt.Sprintf("%x", h.Sum(nil))
	if hash != tokenHash {
		t.Errorf("Hash couldn't be reconstructed from values. Got %s, expected %s", hash, tokenHash)
	}
}
