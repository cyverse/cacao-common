package common

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewEventTypeSet(t *testing.T) {
	type args struct {
		eventTypes []EventType
	}
	tests := []struct {
		name string
		args args
		want EventTypeSet
	}{
		{
			name: "empty",
			args: args{
				eventTypes: []EventType{},
			},
			want: EventTypeSet{},
		},
		{
			name: "one",
			args: args{
				eventTypes: []EventType{"foo"},
			},
			want: EventTypeSet{"foo": {}},
		},
		{
			name: "two",
			args: args{
				eventTypes: []EventType{"foo", "bar"},
			},
			want: EventTypeSet{"foo": {}, "bar": {}},
		},
		{
			name: "duplicate",
			args: args{
				eventTypes: []EventType{"foobar", "bar", "bar"},
			},
			want: EventTypeSet{"foobar": {}, "bar": {}},
		},
		{
			name: "nil",
			args: args{
				eventTypes: nil,
			},
			want: EventTypeSet{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, NewEventTypeSet(tt.args.eventTypes...), "NewEventTypeSet(%v)", tt.args.eventTypes)
		})
	}
}
