package common

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

// CancelWhenSignaled calls the cancel function when receiving SIGINT or SIGTERM.
//
// This is useful for implementing graceful termination in service, create a
// context for the entire service, and then pass the cancel function to this
// function. The service will need to stop its different components upon context
// cancellation.
func CancelWhenSignaled(cancelFunc context.CancelFunc) {
	var channel = make(chan os.Signal)
	// os.Interrupt (SIGINT) for Ctrl-C
	// syscall.SIGTERM for k8s, https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-terminating-with-grace
	signal.Notify(channel, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-channel
		cancelFunc()
	}()
}
