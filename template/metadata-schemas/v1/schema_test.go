package v1

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/template"
)

func TestValidateMetadataV1(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "compute"
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"name": "single image openstack instances",
		"author": "Edwin Skidmore"
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)
}

func TestValidateMetadataVersionString(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"schema_url": "v1",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "compute"
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"schema_url": "v1",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore"
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)
}
