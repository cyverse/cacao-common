# Cacao Template Metadata V2

## Changelog

- Add a new field `schema_url` for semantic versioning: This field is used to determine which schema the metadata will use. Accepted values are URLs to schema, or version strings, like `v1` and `v2`.  
- Rename and expand `purpose` field values: Must have one of `openstack_compute`, `openstack_storage`, `general`, `aws_compute`, `aws_storage`, `bare_system`, `nextflow`, `kubernetes`, or `multi_cloud`.
- Add a new `help_text` to show help text in the UI.
- Change `type` field values in `cacao_post_task` object.
- Rename `location_type` field in `cacao_post_task` object to `location`.
- Add a new `execute_on` field in `cacao_post_task` object.
- Add a new `args` field in `cacao_post_task` object.
- Add a new `env` field in `cacao_post_task` object.
- Add a new `run_action` field in `cacao_post_task` object.
- Add a new `run_params` field in `cacao_post_task` object.
- Add a new `filters` field in metadata object.
- Add two new `images_allowed` and `images_blocked` fields in `filters` object.
- Add a new `schema_version` field in metadata object.
- Add a new `doc_url` field in metadata object.

## Schema

### `cacao_post_task` Object

| `Property` | Value Type | Required | Values |
| `type` | string | yes | `cacao_atmosphere_legacy`, `ansible`, `script_remote`, `script_local`, `docker`, or `deployment_run_action` |
| `location` | string | yes | for `script_remote` or `ansible`, this would be a uri. <br/> for `script_local`, command to execute or local path to script. |
| `execute_on` | string | no | `all` to execute on all instance resources. <br/> an index to declare that the tasks will only run on the resource at that index e.g. 0 will run task on the first instance resource. <br/> `all_but_first`, will execute the task on all but the first instance. |
| `args` | array of strings | no | only valid for `script_remote`, `script_local`, and `docker`. <br/> each argument is passed into the script or container on execution. |
| `env` | array of strings (`key=val` format) | no | only valid for `script_remote`, `script_local`, and `docker`. <br/> each list item is set as an environment variable. <br/> Must have `key=val` format. |
| `run_action` | string | yes (when `type` has `deployment_run_action`) | `run_delete` deletes run at the step. <br/> `run_param_update` updates parameters of a run at the step. |
| `run_params` | array of strings (`key=val` format) | yes (when `run_action` has `run_param_update`) | Must have `key=val` format. | 