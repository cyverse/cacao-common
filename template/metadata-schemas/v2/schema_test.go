package v2

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/template"
)

func TestValidateMetadataV2(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"schema_url" : "v2",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute"
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"name": "single image openstack instances",
		"author": "Edwin Skidmore"
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)
}

func TestValidateMetadataV2WithURL(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute"
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore"
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)
}

func TestValidateMetadataV2WithCacaoPreTask(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"cacao_pre_tasks": [
			{
				"type": "ansible",
				"location": "node1"
			}
		]
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// more complex one
	testMetadataJsonRequiredFields := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"cacao_pre_tasks": [
			{
				"type": "deployment_run_action",
				"location": "node1",
				"args": [],
				"env": ["key=val"],
				"run_action": "run_param_update",
				"run_params": ["pkey=pval"]
			}
		]
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonRequiredFields))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore"
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)

	// must fail
	testMetadataJsonRequiredFieldsBroken := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"cacao_pre_tasks": [
			{
				"type": "deployment_run_action",
				"location": "node1",
				"args": [],
				"env": ["key=val"]
			}
		]
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonRequiredFieldsBroken))
	assert.Error(t, err)
}

func TestValidateMetadataV2WithCacaoPostTask(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"cacao_post_tasks": [
			{
				"type": "ansible",
				"location": "node1"
			}
		]
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// more complex one
	testMetadataJsonRequiredFields := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"cacao_post_tasks": [
			{
				"type": "deployment_run_action",
				"location": "node1",
				"args": [],
				"env": ["key=val"],
				"run_action": "run_param_update",
				"run_params": ["pkey=pval"]
			}
		]
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonRequiredFields))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore"
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)

	// must fail
	testMetadataJsonRequiredFieldsBroken := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"cacao_post_tasks": [
			{
				"type": "deployment_run_action",
				"location": "node1",
				"args": [],
				"env": ["key=val"]
			}
		]
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonRequiredFieldsBroken))
	assert.Error(t, err)
}

func TestValidateMetadataV2WithParameters(t *testing.T) {
	testMetadataJsonMinimal := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"parameters": [
			{
				"name": "username",
				"type": "caaco_username",
				"default": "username",
				"ui_label": "CACAO username"
			},
			{
				"name": "password",
				"type": "caaco_password",
				"ui_label": "CACAO user password"
			}
		]
	}
	`

	err := template.ValidateMetadata([]byte(testMetadataJsonMinimal))
	assert.NoError(t, err)

	// must fail
	testMetadataJsonMinimalBroken := `
	{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v2/schema.json",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"parameters": [
			{
				"name": "username",
				"type": "caaco_username",
				"default": "username",
				"ui_label": "CACAO username"
			},
			{
				"name": "password"
			}
		]
	}
	`

	err = template.ValidateMetadata([]byte(testMetadataJsonMinimalBroken))
	assert.Error(t, err)
}
