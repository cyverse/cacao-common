# Cacao Template Metadata V3

## Changelog
- Add new fields `required` and `editable` to parameter data types
- Move fields `ui_label` and `help_text` in parameter data types to ui metadata
- Add ui metadata schema that will be used to render deployment wizard in Ionosphere
- Add new `purpose` field values: `openstack_share_storage`, `openstack_volume_storage`, `aws_ebs_storage`, and `aws_s3_storage`.