package template

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadMetadataSchema(t *testing.T) {
	testURL := defaultMetadataSchemaUrl
	_, schemaBytes, err := LoadMetadataSchema(testURL)
	assert.NoError(t, err)
	assert.NotEmpty(t, schemaBytes)

	// schema must be a valid json
	var schema map[string]interface{}
	err = json.Unmarshal(schemaBytes, &schema)
	assert.NoError(t, err)
}

func TestLoadMetadataSchemaByVersionString(t *testing.T) {
	testVersion := "v3"
	_, schemaBytes, err := LoadMetadataSchema(testVersion)
	assert.NoError(t, err)
	assert.NotEmpty(t, schemaBytes)

	// schema must be a valid json
	var schema map[string]interface{}
	err = json.Unmarshal(schemaBytes, &schema)
	assert.NoError(t, err)

	// test non-existing version
	nonexistingTestVersion := "v99999"
	_, schemaBytes, err = LoadMetadataSchema(nonexistingTestVersion)
	assert.Error(t, err)
	assert.Empty(t, schemaBytes)
}
