package template

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadUIMetadataSchema(t *testing.T) {
	testURL := defaultUIMetadataSchemaUrl
	_, schemaBytes, err := LoadUIMetadataSchema(testURL)
	assert.NoError(t, err)
	assert.NotEmpty(t, schemaBytes)

	// schema must be a valid json
	var schema map[string]interface{}
	err = json.Unmarshal(schemaBytes, &schema)
	assert.NoError(t, err)
}

func TestLoadUIMetadataSchemaByVersionString(t *testing.T) {
	testVersion := "v1"
	_, schemaBytes, err := LoadUIMetadataSchema(testVersion)
	assert.NoError(t, err)
	assert.NotEmpty(t, schemaBytes)

	// schema must be a valid json
	var schema map[string]interface{}
	err = json.Unmarshal(schemaBytes, &schema)
	assert.NoError(t, err)

	// test non-existing version
	nonexistingTestVersion := "v99999"
	_, schemaBytes, err = LoadUIMetadataSchema(nonexistingTestVersion)
	assert.Error(t, err)
	assert.Empty(t, schemaBytes)
}
