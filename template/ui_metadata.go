package template

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/cyverse/cacao-common/format"
)

const (
	uiMetadataSchemaRootUrl    = "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/ui-schemas"
	uiMetadataSchemaFilename   = "schema.json"
	uiMetadataSchemaV1Url      = uiMetadataSchemaRootUrl + "/v1/" + uiMetadataSchemaFilename
	defaultUIMetadataSchemaUrl = uiMetadataSchemaV1Url
)

// GetUIMetadataSchemaURL returns URL of the ui metadata schema for given version string or url
func GetUIMetadataSchemaURL(val string) (string, error) {
	// if empty, return default
	if strings.TrimSpace(val) == "" {
		return defaultUIMetadataSchemaUrl, nil
	}

	// URL
	valLower := strings.ToLower(val)
	if strings.HasPrefix(valLower, "http://") || strings.HasPrefix(valLower, "https://") {
		// url
		// try parse to validate URL format
		_, err := url.Parse(val)
		if err != nil {
			return "", err
		}

		return val, nil
	}

	// possibly a version string, like v1, v2...
	// also allows v1.0, v2.0
	if strings.HasPrefix(valLower, "v") {
		version := val[1:]
		matched, err := regexp.MatchString("^\\d+(\\.\\d+)*$", version)
		if err != nil {
			return "", err
		}

		if matched {
			// build url
			return fmt.Sprintf("%s/v%s/%s", uiMetadataSchemaRootUrl, version, uiMetadataSchemaFilename), nil
		}
	}

	return "", fmt.Errorf("unknown ui schema url value - %s", val)
}

// LoadUIMetadataSchema loads ui metadata schema for given URL or version
func LoadUIMetadataSchema(schemaURL string) (string, []byte, error) {
	schemaURL, err := GetUIMetadataSchemaURL(schemaURL)
	if err != nil {
		return "", nil, err
	}

	// read from URL
	response, err := http.Get(schemaURL)
	if err != nil {
		return "", nil, err
	}

	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return "", nil, err
	}

	response.Body.Close()

	// check if status is ok
	if response.StatusCode != http.StatusOK {
		// error
		return "", nil, fmt.Errorf("failed to read schema %s, status %s (%d)", schemaURL, response.Status, response.StatusCode)
	}

	return schemaURL, responseBody, nil
}

// ValidateUIMetadata validates Template UI Metadata bytes and returns error if error is raised
func ValidateUIMetadata(uiMetadataBytes []byte) error {
	// convert ui metadata json file into map[string]interface{}
	var meta map[string]interface{}

	err := json.Unmarshal(uiMetadataBytes, &meta)
	if err != nil {
		return err
	}

	schemaURL := ""
	if url, ok := meta["schema_url"]; ok {
		// has `schema_url` field
		urlString, ok2 := url.(string)
		if !ok2 {
			return fmt.Errorf("failed to read string value from schema_url field")
		}

		schemaURL = urlString
	}

	schemaURL, schemaBytes, err := LoadUIMetadataSchema(schemaURL)
	if err != nil {
		return err
	}

	// validate json using the schema
	return format.ValidateJSONSchemaWithResourceURL(uiMetadataBytes, schemaURL, schemaBytes)
}

// ValidateUIMetadataWithVersion validates Template UI Metadata bytes with specified version, and returns error if error is raised
func ValidateUIMetadataWithVersion(uiMetadataBytes []byte, versionString string) error {
	versionString = strings.TrimPrefix(versionString, "V")

	if !strings.HasPrefix(versionString, "v") {
		versionString = fmt.Sprintf("v%s", versionString)
	}

	schemaURL, schemaBytes, err := LoadUIMetadataSchema(versionString)
	if err != nil {
		return err
	}

	// validate json using the schema
	return format.ValidateJSONSchemaWithResourceURL(uiMetadataBytes, schemaURL, schemaBytes)
}
