package template

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/cyverse/cacao-common/format"
)

const (
	metadataSchemaRootUrl    = "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas"
	metadataSchemaFilename   = "schema.json"
	metadataSchemaV3Url      = metadataSchemaRootUrl + "/v3/" + metadataSchemaFilename
	defaultMetadataSchemaUrl = metadataSchemaV3Url
)

// GetMetadataSchemaURL returns URL of the metadata schema for given version string or url
func GetMetadataSchemaURL(val string) (string, error) {
	// if empty, return default
	if strings.TrimSpace(val) == "" {
		return defaultMetadataSchemaUrl, nil
	}

	// URL
	valLower := strings.ToLower(val)
	if strings.HasPrefix(valLower, "http://") || strings.HasPrefix(valLower, "https://") {
		// url
		// try parse to validate URL format
		_, err := url.Parse(val)
		if err != nil {
			return "", err
		}

		return val, nil
	}

	// possibly a version string, like v1, v2...
	// also allows v1.0, v2.0
	if strings.HasPrefix(valLower, "v") {
		version := val[1:]
		matched, err := regexp.MatchString("^\\d+(\\.\\d+)*$", version)
		if err != nil {
			return "", err
		}

		if matched {
			// build url
			return fmt.Sprintf("%s/v%s/%s", metadataSchemaRootUrl, version, metadataSchemaFilename), nil
		}
	}

	return "", fmt.Errorf("unknown metadata schema url value - %s", val)
}

// LoadMetadataSchema loads metadata schema for given URL or version
func LoadMetadataSchema(schemaURL string) (string, []byte, error) {
	schemaURL, err := GetMetadataSchemaURL(schemaURL)
	if err != nil {
		return "", nil, err
	}

	// read from URL
	response, err := http.Get(schemaURL)
	if err != nil {
		return "", nil, err
	}

	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return "", nil, err
	}

	response.Body.Close()

	// check if status is ok
	if response.StatusCode != http.StatusOK {
		// error
		return "", nil, fmt.Errorf("failed to read schema %s, status %s (%d)", schemaURL, response.Status, response.StatusCode)
	}

	return schemaURL, responseBody, nil
}

// ValidateMetadata validates Template Metadata bytes and returns error if error is raised
func ValidateMetadata(metadataBytes []byte) error {
	// convert metadata json file into map[string]interface{}
	var meta map[string]interface{}

	err := json.Unmarshal(metadataBytes, &meta)
	if err != nil {
		return err
	}

	schemaURL := ""
	if url, ok := meta["schema_url"]; ok {
		// has `schema_url` field
		urlString, ok2 := url.(string)
		if !ok2 {
			return fmt.Errorf("failed to read string value from schema_url field")
		}

		schemaURL = urlString
	}

	schemaURL, schemaBytes, err := LoadMetadataSchema(schemaURL)
	if err != nil {
		return err
	}

	// validate json using the schema
	return format.ValidateJSONSchemaWithResourceURL(metadataBytes, schemaURL, schemaBytes)
}

// ValidateMetadataWithVersion validates Template Metadata bytes with specified version, and returns error if error is raised
func ValidateMetadataWithVersion(metadataBytes []byte, versionString string) error {
	versionString = strings.TrimPrefix(versionString, "V")

	if !strings.HasPrefix(versionString, "v") {
		versionString = fmt.Sprintf("v%s", versionString)
	}

	schemaURL, schemaBytes, err := LoadMetadataSchema(versionString)
	if err != nil {
		return err
	}

	// validate json using the schema
	return format.ValidateJSONSchemaWithResourceURL(metadataBytes, schemaURL, schemaBytes)
}
