//go:build mongodb_integration

package db

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestMongoDBSchema(t *testing.T) {
	var config = MongoDBConfig{
		URL:    os.Getenv("MONGODB_URL"),
		DBName: os.Getenv("MONGODB_DB_NAME"),
	}
	conn, err := NewMongoDBConnection(&config)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, conn) {
		return
	}
	defer conn.Disconnect()

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
	defer cancelFunc()

	const collectionName = "collectionName123"
	defer conn.Database.Collection(collectionName).Drop(ctx)

	schema := bson.M{
		"$jsonSchema": bson.M{
			"bsonType": "object",
			"required": bson.A{"_id", "name", "created_at"},
			"properties": bson.M{
				"_id": bson.M{
					"bsonType":    "string",
					"description": "xid of object",
					"minLength":   int32(22),
				},
				"name": bson.M{
					"bsonType":    "string",
					"description": "name of the object",
					"minLength":   int32(1),
				},
				"updated_at": bson.M{
					"bsonType": "date",
				},
				"created_at": bson.M{
					"bsonType": "date",
				},
			},
		},
	}
	err = CreateCollectionWithSchema(ctx, conn, collectionName, schema)
	if !assert.NoError(t, err) {
		return
	}
	// fetch the current schema to verify it has been set
	listResult, err := conn.Database.ListCollections(ctx, bson.M{"name": collectionName})
	if !assert.NoError(t, err) {
		return
	}
	if !listResult.Next(ctx) {
		return
	}
	var collectionList []struct {
		Name    string `bson:"name"`
		Type    string `bson:"type"`
		Options struct {
			Validator        bson.M `bson:"validator"`
			ValidationAction string `bson:"validationAction"`
		} `bson:"options"`
	}
	err = listResult.All(ctx, &collectionList)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.Len(t, collectionList, 1) {
		return
	}
	assert.Equal(t, schema, collectionList[0].Options.Validator)
	assert.Truef(t, reflect.DeepEqual(collectionList[0].Options.Validator, schema), "reflect.DeepEqual should return true")
}

func TestMongoDBErrorHelper(t *testing.T) {
	var config = MongoDBConfig{
		URL:    os.Getenv("MONGODB_URL"),
		DBName: os.Getenv("MONGODB_DB_NAME"),
	}
	conn, err := NewMongoDBConnection(&config)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, conn) {
		return
	}
	defer conn.Disconnect()

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
	defer cancelFunc()

	const collectionName = "collectionName123"
	defer conn.Database.Collection(collectionName).Drop(ctx)

	t.Run("IsDuplicateError", func(t *testing.T) {
		_, err = conn.Database.Collection(collectionName).InsertOne(ctx, bson.M{"_id": "same_id"})
		if !assert.NoError(t, err) {
			return
		}
		_, err = conn.Database.Collection(collectionName).InsertOne(ctx, bson.M{"_id": "same_id"})
		if !assert.Error(t, err) {
			return
		}
		assert.Truef(t, IsDuplicateError(err), "IsDuplicateError() should return true")
		assert.False(t, IsNoDocumentError(err))
		assert.False(t, IsDocumentValidationFailure(err))

		err = conn.Insert(collectionName, bson.M{"_id": "same_id"})
		if !assert.Error(t, err) {
			return
		}
		assert.Truef(t, IsDuplicateError(err), "IsDuplicateError() should return true")
		assert.False(t, IsNoDocumentError(err))
		assert.False(t, IsDocumentValidationFailure(err))
	})

	t.Run("IsNoDocumentError", func(t *testing.T) {
		findOneResult := conn.Database.Collection(collectionName).FindOne(ctx, bson.M{"_id": "not_exists123"})
		err = findOneResult.Err()
		if !assert.Error(t, err) {
			return
		}
		assert.Truef(t, IsNoDocumentError(err), "IsNoDocumentError() should return true")
		assert.False(t, IsDuplicateError(err))
		assert.False(t, IsDocumentValidationFailure(err))

		var getResult bson.M
		err = conn.Get(collectionName, bson.M{"_id": "not_exists123"}, &getResult)
		if !assert.Error(t, err) {
			return
		}
		assert.Truef(t, IsNoDocumentError(err), "IsNoDocumentError() should return true")
		assert.False(t, IsDuplicateError(err))
		assert.False(t, IsDocumentValidationFailure(err))

		findResult, err := conn.Database.Collection(collectionName).Find(ctx, bson.M{"_id": "not_exists123"})
		if !assert.NoError(t, err) {
			return
		}
		assert.NoError(t, findResult.Err())
		var listResult []bson.M
		err = conn.List(collectionName, bson.M{"_id": "not_exists123"}, &listResult)
		if !assert.NoError(t, err) {
			return
		}
	})

	t.Run("IsDocumentValidationFailure", func(t *testing.T) {
		err = setCollectionValidationSchema(ctx, conn, collectionName, bson.M{
			"$jsonSchema": bson.M{
				"bsonType": "object",
				"required": bson.A{"_id"},
				"properties": bson.M{
					"_id": bson.M{
						"bsonType": "string",
					},
				},
			},
		})
		if !assert.NoError(t, err) {
			return
		}
		_, err = conn.Database.Collection(collectionName).InsertOne(ctx, bson.M{"_id": "123"})
		if !assert.NoError(t, err) {
			return
		}
		err = conn.Insert(collectionName, bson.M{"_id": "12345"})
		if !assert.NoError(t, err) {
			return
		}

		_, err = conn.Database.Collection(collectionName).InsertOne(ctx, bson.M{"_id": int32(123)})
		if !assert.Error(t, err) {
			return
		}
		assert.Truef(t, IsDocumentValidationFailure(err), "IsDocumentValidationFailure() should return true")
		assert.False(t, IsNoDocumentError(err))
		assert.False(t, IsDuplicateError(err))

		err = conn.Insert(collectionName, bson.M{"_id": int32(12345)})
		if !assert.Error(t, err) {
			return
		}
		assert.Truef(t, IsDocumentValidationFailure(err), "IsDocumentValidationFailure() should return true")
		assert.False(t, IsNoDocumentError(err))
		assert.False(t, IsDuplicateError(err))
	})
}
