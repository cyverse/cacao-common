// Package db contains utility functions for dealing with database
package db

import (
	"fmt"
	"reflect"
	"strings"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
)

// KeyValue is a struct for holding key-value data type
type KeyValue struct {
	Key   string
	Value interface{}
}

// ObjectStore is an interface for object-level database I/O
type ObjectStore interface {
	Release() error
	List(collection string, results interface{}) error
	ListConditional(collection string, conditions map[string]interface{}, results interface{}) error
	ListConditionalWithSort(collection string, conditions map[string]interface{}, sort []KeyValue, results interface{}) error
	ListForUser(collection string, owner string, results interface{}) error
	Get(collection string, id string, result interface{}) error
	GetConditional(collection string, conditions map[string]interface{}, result interface{}) error
	GetConditionalWithSort(collection string, conditions map[string]interface{}, sort []KeyValue, result interface{}) error
	Insert(collection string, document interface{}) error
	InsertMany(collection string, documents []interface{}) error
	Update(collection string, id string, updateDocument interface{}, updateFieldNames []string) (bool, error)
	UpdateConditional(collection string, conditions map[string]interface{}, updateDocument interface{}, updateFieldNames []string) (int64, error)
	UpdateWithMap(collection string, id string, updateDocument map[string]interface{}) (bool, error)
	UpdateConditionalWithMap(collection string, conditions map[string]interface{}, updateDocument map[string]interface{}) (int64, error)
	Replace(collection string, id string, replaceDocument interface{}) (bool, error)
	Delete(collection string, id string) (bool, error)
	DeleteConditional(collection string, conditions map[string]interface{}) (int64, error)
}

// validateDocumentType checks docuement type if it has mandatory fields, such as _id
// The document must contain following bson fields
// _id: the ID of a document
func validateDocumentType(document interface{}) bool {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "validateDocumentType",
	})

	documentType := reflect.TypeOf(document)
	if documentType.Kind() != reflect.Struct {
		return false
	}

	hasIDField := false

	for i := 0; i < documentType.NumField(); i++ {
		fieldTag := documentType.Field(i).Tag
		if tagValue, ok := fieldTag.Lookup("bson"); ok {
			tagValues := strings.Split(tagValue, ",")
			bsonFieldName := ""
			if len(tagValues) > 0 {
				bsonFieldName = tagValues[0]
			}

			if bsonFieldName == "_id" {
				// _id exists
				hasIDField = true
			}

			switch bsonFieldName {
			case "_id":
				// _id exists
				hasIDField = true
			default:
				// do nothing
			}
		}
	}

	if !hasIDField {
		logger.Tracef("cannot find _id field in a docuemnt %s", documentType.Name())
	}

	return hasIDField
}

// GetBSONRegistry returns bson registry with JSONFallbackStructTagParser
func GetBSONRegistry() (*bsoncodec.Registry, error) {
	// use JSON Fallback struct tag parser
	// this parser tries to use BSON tag first then JSON tag if BSON tag is not available
	structCodec, err := bsoncodec.NewStructCodec(bsoncodec.JSONFallbackStructTagParser)
	if err != nil {
		return nil, err
	}

	registryBuilder := bson.NewRegistryBuilder()
	registryBuilder.RegisterDefaultEncoder(reflect.Struct, structCodec)
	registryBuilder.RegisterDefaultDecoder(reflect.Struct, structCodec)

	return registryBuilder.Build(), nil
}

// extractFieldsToUpdate extracts fields to update from a docuement
func extractFieldsToUpdate(document interface{}, updateFieldNames []string, updateAllFields bool, filterIDField bool) (bson.M, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "extractFieldsToUpdate",
	})

	registry, err := GetBSONRegistry()
	if err != nil {
		logger.Error(err)
		return nil, fmt.Errorf("failed to get bson registry")
	}

	// cast to bson first
	bsonBytes, err := bson.MarshalWithRegistry(registry, document)
	if err != nil {
		logger.Error(err)
		return nil, fmt.Errorf("failed to marshal document to bson")
	}

	var documentMap map[string]interface{}
	err = bson.UnmarshalWithRegistry(registry, bsonBytes, &documentMap)
	if err != nil {
		logger.Error(err)
		return nil, fmt.Errorf("failed to unmarshal bson to document map")
	}

	updateFields := bson.M{}

	updateFieldNamesMap := map[string]bool{}
	for _, name := range updateFieldNames {
		updateFieldNamesMap[name] = true
	}

	for k, v := range documentMap {
		if filterIDField && k == "_id" {
			// filter
			continue
		}

		_, updateField := updateFieldNamesMap[k]
		if updateField || updateAllFields {
			updateFields[k] = v
		}
	}

	return updateFields, nil
}
