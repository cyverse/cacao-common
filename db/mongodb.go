// Package db contains utility functions for dealing with database
package db

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	// DefaultMongoDBURL is a default mongodb URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
)

// SortDirection is the direction to sort the result
type SortDirection int

// https://www.mongodb.com/docs/manual/reference/method/cursor.sort/#ascending-descending-sort
const (
	// AscendingSort sorts result in ascending order, this is a constants used by MongoDB for sorting order/index order
	AscendingSort SortDirection = 1 // default
	// DescendingSort sorts result in descending order, this is a constants used by MongoDB for sorting order/index order
	DescendingSort SortDirection = -1
)

// MongoDBConfig stores configurations used by mongodb
type MongoDBConfig struct {
	URL               string `envconfig:"MONGODB_URL" default:"mongodb://localhost:27017"`
	DBName            string `envconfig:"MONGODB_DB_NAME"` // no default, every microservice should ensure they use a different database name (and not cacao)
	ConnectionTimeout uint   `envconfig:"MONGODB_CONN_TIMEOUT" default:"10"`
}

// MongoDBConnection contains MongoDB connection info
type MongoDBConnection struct {
	Config   *MongoDBConfig
	Client   *mongo.Client
	Database *mongo.Database
}

// NewMongoDBConnection connects to MongoDB
func NewMongoDBConnection(config *MongoDBConfig) (*MongoDBConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "ConnectMongoDB",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	// use JSON Fallback struct tag parser
	// this parser tries to use BSON tag first then JSON tag if BSON tag is not available
	registry, err := GetBSONRegistry()
	if err != nil {
		return nil, err
	}

	clientOptions := options.Client().ApplyURI(config.URL).SetRegistry(registry)

	connTimeout := time.Duration(config.ConnectionTimeout) * time.Second
	if connTimeout <= 0 {
		connTimeout = time.Second * 10
	}
	ctx, cancel := context.WithTimeout(context.Background(), connTimeout)
	defer cancel()

	// Connect to MongoDB
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	// Check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		logger.WithError(err).Errorf("failed to check the connection to %s", config.URL)
		return nil, err
	}

	database := client.Database(config.DBName)

	logger.Tracef("established a connection to %s", config.URL)

	return &MongoDBConnection{
		Config:   config,
		Client:   client,
		Database: database,
	}, nil
}

// NewMongoDBConnectionWithCtx connects to MongoDB
func NewMongoDBConnectionWithCtx(ctx context.Context, config *MongoDBConfig) (*MongoDBConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "NewMongoDBConnectionWithCtx",
	})

	logger.Tracef("trying to connect to %s", config.URL)

	// use JSON Fallback struct tag parser
	// this parser tries to use BSON tag first then JSON tag if BSON tag is not available
	registry, err := GetBSONRegistry()
	if err != nil {
		return nil, err
	}

	clientOptions := options.Client().ApplyURI(config.URL).SetRegistry(registry)

	// Connect to MongoDB
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		logger.WithError(err).Errorf("failed to connect to %s", config.URL)
		return nil, err
	}

	// Check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		logger.WithError(err).Errorf("failed to check the connection to %s", config.URL)
		return nil, err
	}

	database := client.Database(config.DBName)

	logger.Tracef("established a connection to %s", config.URL)

	return &MongoDBConnection{
		Config:   config,
		Client:   client,
		Database: database,
	}, nil
}

// Disconnect disconnects MongoDB connection
func (conn *MongoDBConnection) Disconnect() error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Disconnect",
	})

	logger.Tracef("trying to disconnect from %s", conn.Config.URL)

	if conn.Client == nil {
		err := fmt.Errorf("client is not connected")
		logger.Error(err)
		return err
	}

	err := conn.Client.Disconnect(context.TODO())
	if err != nil {
		logger.WithError(err).Errorf("failed to disconnect from %s", conn.Config.URL)
		return err
	}

	logger.Tracef("disconnected from %s", conn.Config.URL)

	conn.Client = nil
	conn.Database = nil
	return nil
}

// List lists filtered documents in the given collection
// results must be a pointer to a struct array (i.e., *[]ExampleStruct)
func (conn *MongoDBConnection) List(collection string, filter map[string]interface{}, results interface{}, opts ...*options.FindOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.List",
	})

	logger.Tracef("trying to list documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// find documents
	// Passing bson.M{{}} as the filter
	cursor, err := coll.Find(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to find documents in a collection %s", collection)
		return err
	}
	defer cursor.Close(context.TODO())

	err = cursor.All(context.TODO(), results)
	if err != nil {
		logger.WithError(err).Error("failed to read documents from a cursor")
		return err
	}

	logger.Tracef("listed documents in a collection %s", collection)

	return nil
}

// ListAndGetCursor lists filtered documents in the given collection, and returns a cursor
func (conn *MongoDBConnection) ListAndGetCursor(collection string, filter map[string]interface{}, opts ...*options.FindOptions) (*mongo.Cursor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.ListAndGetCursor",
	})

	logger.Tracef("trying to list documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// find documents
	// Passing bson.M{{}} as the filter
	cursor, err := coll.Find(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to find documents in a collection %s", collection)
		return nil, err
	}

	logger.Tracef("listed documents in a collection %s", collection)

	return cursor, nil
}

// Get returns a document in the given collection
// results must be a pointer to a struct (i.e., *ExampleStruct)
func (conn *MongoDBConnection) Get(collection string, filter map[string]interface{}, result interface{}, opts ...*options.FindOneOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Get",
	})

	logger.Tracef("trying to get a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// find a document
	// Passing bson.M{{}} as the filter
	singleResult := coll.FindOne(context.TODO(), filter, opts...)

	if singleResult.Err() != nil {
		err := singleResult.Err()
		logger.WithError(err).Errorf("failed to find a document in a collection %s", collection)
		return err
	}

	err := singleResult.Decode(result)
	if err != nil {
		logger.WithError(err).Error("failed to decode a document from a result")
		return err
	}

	logger.Tracef("got a document in a collection %s", collection)

	return nil
}

// Insert inserts a document into the given collection
func (conn *MongoDBConnection) Insert(collection string, document interface{}, opts ...*options.InsertOneOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Insert",
	})

	logger.Tracef("trying to insert a document into a collection %s", collection)

	// get collection
	coll := conn.Database.Collection(collection)

	// insert a document
	_, err := coll.InsertOne(context.TODO(), document, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to insert a document into a collection %s", collection)
		return err
	}

	logger.Tracef("inserted a document into a collection %s", collection)

	return nil
}

// InsertMany inserts multiple documents into the given collection
func (conn *MongoDBConnection) InsertMany(collection string, documents []interface{}, opts ...*options.InsertManyOptions) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.InsertMany",
	})

	logger.Tracef("trying to insert %d documents into a collection %s", len(documents), collection)

	// get collection
	coll := conn.Database.Collection(collection)

	// insert documents
	result, err := coll.InsertMany(context.TODO(), documents, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to insert documents into a collection %s", collection)
		return err
	}

	logger.Tracef("inserted %d documents into a collection %s", len(result.InsertedIDs), collection)

	return nil
}

// Update updates a document in the given collection, returns true if updated successfully
func (conn *MongoDBConnection) Update(collection string, filter map[string]interface{}, updateValues map[string]interface{}, opts ...*options.UpdateOptions) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Update",
	})

	logger.Tracef("trying to update a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// update a document
	result, err := coll.UpdateOne(context.TODO(), filter, updateValues, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to update a document in a collection %s", collection)
		return false, err
	}

	if result.MatchedCount == 0 {
		logger.WithError(err).Errorf("failed to find a matching document in a collection %s", collection)
		return false, nil
	}

	logger.Tracef("updated a document in a collection %s", collection)

	return true, nil
}

// UpdateMany updates multiple document in the given collection, returns the number of documents updated as the first result
func (conn *MongoDBConnection) UpdateMany(collection string, filter map[string]interface{}, updateValues map[string]interface{}, opts ...*options.UpdateOptions) (int64, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.UpdateMany",
	})

	logger.Tracef("trying to update documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// update documents
	result, err := coll.UpdateMany(context.TODO(), filter, updateValues, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to update documents in a collection %s", collection)
		return 0, err
	}

	logger.Tracef("updated %d documents in a collection %s", result.MatchedCount, collection)

	return result.MatchedCount, nil
}

// Replace replaces a document in the given collection, returns true if replaced successfully
// Replace is different from Update. It updates an entire document, while Update updates some fields in a document.
func (conn *MongoDBConnection) Replace(collection string, filter map[string]interface{}, replaceDocument interface{}, opts ...*options.ReplaceOptions) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Replace",
	})

	logger.Tracef("trying to replace a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// replace a document
	result, err := coll.ReplaceOne(context.TODO(), filter, replaceDocument, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to replace a document in a collection %s", collection)
		return false, err
	}

	if result.MatchedCount == 0 {
		logger.WithError(err).Errorf("failed to find a matching document in a collection %s", collection)
		return false, nil
	}

	logger.Tracef("replaced a document in a collection %s", collection)

	return true, nil
}

// Delete deletes a document in the given collection, returns true if deleted successfully
func (conn *MongoDBConnection) Delete(collection string, filter map[string]interface{}, opts ...*options.DeleteOptions) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.Delete",
	})

	logger.Tracef("trying to delete a document in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// delete a document
	result, err := coll.DeleteOne(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to delete a document in a collection %s", collection)
		return false, err
	}

	if result.DeletedCount == 0 {
		logger.WithError(err).Errorf("failed to find a matching document in a collection %s", collection)
		return false, nil
	}

	logger.Tracef("deleted a document in a collection %s", collection)

	return true, nil
}

// DeleteMany deletes multiple document in the given collection, returns the number of documents deleted as the first result
func (conn *MongoDBConnection) DeleteMany(collection string, filter map[string]interface{}, opts ...*options.DeleteOptions) (int64, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.db",
		"function": "MongoDBConnection.DeleteMany",
	})

	logger.Tracef("trying to delete documents in a collection %s with a filter %v", collection, filter)

	// get collection
	coll := conn.Database.Collection(collection)

	// delete documents
	result, err := coll.DeleteMany(context.TODO(), filter, opts...)
	if err != nil {
		logger.WithError(err).Errorf("failed to delete documents in a collection %s", collection)
		return 0, err
	}

	logger.Tracef("deleted %d documents in a collection %s", result.DeletedCount, collection)

	return result.DeletedCount, nil
}

// IsDuplicateError returns true, if the mongodb error received is a duplicate entry on _id
// copied from https://stackoverflow.com/questions/56916969/with-mongodb-go-driver-how-do-i-get-the-inner-exceptions
func IsDuplicateError(err error) bool {
	var e mongo.WriteException
	if errors.As(err, &e) {
		for _, we := range e.WriteErrors {
			if we.Code == 11000 {
				return true
			}
		}
	}
	return false
}

// IsNoDocumentError returns true, if the mongodb error received is for no document found
func IsNoDocumentError(err error) bool {
	if err == mongo.ErrNoDocuments {
		return true
	}
	return false
}

// IsDocumentValidationFailure checks if an error is due to document validation
// (document does not match the validation schema).
func IsDocumentValidationFailure(err error) bool {
	werr := mongo.WriteException{}
	if errors.As(err, &werr) {
		if werr.HasErrorCode(121) { // 121 is DocumentValidationFailure
			return true
		}
	}
	return false
}

// CreateCollectionWithSchema creates a collection and sets validation schema on
// the collection. If the collection already exists, then we will attempt to set
// the validation schema on the existing collection. This function will check for
// any existing documents in the collection that does NOT match the new schema,
// if there is any such documents, this function will return an error without
// setting the validation schema.
//
// See exampleCreateCollectionWithSchema for an example of using this function.
func CreateCollectionWithSchema(ctx context.Context, conn *MongoDBConnection, collectionName string, schema bson.M) error {
	if _, ok := schema["$jsonSchema"]; !ok {
		return fmt.Errorf("'$jsonSchema' key is missing from the validation schema object")
	}
	if len(schema) > 1 {
		return fmt.Errorf("validation schema object should only have '$jsonSchema' key")
	}
	collections, err := conn.Database.ListCollections(ctx, bson.M{"name": collectionName})
	if err != nil {
		return err
	}
	defer collections.Close(context.Background())
	if !collections.Next(ctx) {
		// if collection does not exist, then create the collection with validator.
		collectionOpts := options.CreateCollection().SetValidationAction("error").SetValidationLevel("strict").SetValidator(schema)
		err = conn.Database.CreateCollection(ctx, collectionName, collectionOpts)
		if err != nil {
			return err
		}
		return nil
	}

	// check if existing collection has the same validator schema
	var collectionList []struct {
		Name    string `bson:"name"`
		Options struct {
			Validator        bson.M `bson:"validator"`
			ValidationAction string `bson:"validationAction"`
		} `bson:"options"`
	}
	err = collections.All(ctx, &collectionList)
	if err != nil {
		return err
	}
	if reflect.DeepEqual(collectionList[0].Options.Validator, schema) {
		// if the collection has the same validation schema as the one that we are trying
		// to set, then there is nothing more to do
		return nil
	}

	// if the collection exists and the current validation schema is different, check
	// existing documents in collection before we update the schema.

	// TODO: we should use a transaction to wrap the checking of existing documents
	// and set the new schema, BUT transaction is only supported on replica sets or
	// shared cluster, and NOT on standalone node, so we will need to adjust the
	// infra for this.

	// find documents that does not match the new schema, https://www.mongodb.com/docs/manual/core/schema-validation/use-json-schema-query-conditions/#find-documents-that-don-t-match-the-schema
	findResult, err := conn.Database.Collection(collectionName).Find(ctx, bson.M{"$nor": bson.A{schema}}, options.Find().SetProjection(bson.M{"_id": 1}))
	if err != nil {
		return err
	}
	var noMatchList []struct {
		ID string `bson:"_id"`
	}
	err = findResult.All(ctx, &noMatchList)
	if err != nil {
		return err
	}
	if len(noMatchList) > 0 {
		return fmt.Errorf("there are documents in the collection that does not match the new schema, fix them before update the validation schema, IDs: %v", noMatchList)
	}

	return setCollectionValidationSchema(ctx, conn, collectionName, schema)
}

func setCollectionValidationSchema(ctx context.Context, conn *MongoDBConnection, collectionName string, schema bson.M) error {
	// set the validator with "collMod" command
	cmd := bson.D{
		{"collMod", collectionName},
		{"validator", schema},
		{"validationLevel", "strict"},
		{"validationAction", "error"},
	}
	cmdResult := conn.Database.RunCommand(ctx, cmd)
	return cmdResult.Err()
}

func exampleCreateCollectionWithSchema() {
	schema := bson.M{
		"$jsonSchema": bson.M{
			"bsonType": "object",
			"required": []string{"_id", "name", "created_at"},
			"properties": bson.M{
				"_id": bson.M{
					"bsonType":    "string",
					"description": "xid of object",
					"minLength":   22,
				},
				"name": bson.M{
					"bsonType":    "string",
					"description": "name of the object",
					"minLength":   1,
				},
				"updated_at": bson.M{
					"bsonType": "date",
				},
				"created_at": bson.M{
					"bsonType": "date",
				},
			},
		},
	}
	conn, err := NewMongoDBConnection(&MongoDBConfig{
		URL:    "mongodb://localhost:27017",
		DBName: "cacao",
	})
	if err != nil {
		panic(err)
	}
	const collectName = "collectioName123"
	err = CreateCollectionWithSchema(context.Background(), conn, collectName, schema)
	if err != nil {
		panic(err)
	}
	err = conn.Insert(collectName, bson.M{
		"_id":  "object-aaaaaaaaaaaaaaaaaaaa",
		"name": "foobar",
		// created_at is missing
	})
	if err != nil {
		// should error here because created_at is missing
		fmt.Println(err.Error())
	}
	// Output: write exception: write errors: [Document failed validation: {"failingDocumentId": "object-aaaaaaaaaaaaaaaaaaaa","details": {"operatorName": "$jsonSchema","schemaRulesNotSatisfied": [{"operatorName": "required","specifiedAs": {"required": ["_id","name","created_at"]},"missingProperties": ["created_at"]}]}}]
}
