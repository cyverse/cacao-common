package messaging2

import (
	"context"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
)

// defaultQueryWorkerCount is the default value of workerCount for QueryConnection.ListenWithConcurrentWorkers()
const defaultQueryWorkerCount = 10

// QueryConnection ...
type QueryConnection interface {
	Request(ctx context.Context, requestCe cloudevents.Event) (reply cloudevents.Event, err error)
	// RequestStream request for multiple replies.
	// An example of the implementation is Scatter-Gather pattern.
	// https://docs.nats.io/using-nats/developer/sending/request_reply#scatter-gather
	RequestStream(ctx context.Context, requestCe cloudevents.Event) (ReplyStream, error)
	// Listen will start listening for queries, and call corresponding handlers if
	// query type matches what is provided in the map.
	//
	// Caller can stop listening by cancel the context passed to Listen(). WaitGroup
	// is for waiting for the Listen to stop, since stop listening is not immediate.
	//
	// Note that this is a non-blocking call, as in it does not block until the
	// listen stopped, use the WaitGroup to wait for Listen to be stopped.
	Listen(context.Context, map[common.QueryOp]QueryHandlerFunc, *sync.WaitGroup) error
	// ListenWithConcurrentWorkers will start listening for queries, just like
	// Listen(), but this will spawn worker go-routines that consume from an internal
	// channel. This allows for bounded concurrency. Channel buffer and worker count
	// need to be passed when calling this function.
	ListenWithConcurrentWorkers(ctx context.Context, m map[common.QueryOp]QueryHandlerFunc, wg *sync.WaitGroup, channelBufferLen uint, workerCount uint) error
}

// ReplyStream is for getting back potentially multiple replies.
type ReplyStream interface {
	ExpectNext() bool
	Next() (cloudevents.Event, error)
}

// QueryHandlerFunc is handler function for query, used by QueryConnection.Listen()
type QueryHandlerFunc func(ctx context.Context, request cloudevents.Event, writer ReplyWriter)

// ReplyWriter ...
type ReplyWriter interface {
	Write(cloudevents.Event) error
}

// ReplyStreamWriter ...
type ReplyStreamWriter struct {
	writer    ReplyWriter
	eventList []cloudevents.Event
}

// NewReplyStreamWriter ...
func NewReplyStreamWriter(writer ReplyWriter) *ReplyStreamWriter {
	return &ReplyStreamWriter{
		writer:    writer,
		eventList: nil,
	}
}

// Write implements ReplyWriter, events are not sent until calling Flush().
func (r *ReplyStreamWriter) Write(event cloudevents.Event) error {
	r.eventList = append(r.eventList, event)
	return nil
}

// Flush flushes all cloudevent
func (r *ReplyStreamWriter) Flush() error {
	for _, event := range r.eventList {
		event.SetExtension(natsReplyCountCloudEventExtension, len(r.eventList))
		err := r.writer.Write(event)
		if err != nil {
			return err
		}
	}
	return nil
}

// make a copy of the mapping, this is so that caller of Listen() or
// ListenWithConcurrentWorkers() cannot modify the mapping used by the function.
func copyQueryHandlerMappings(m map[common.QueryOp]QueryHandlerFunc) map[common.QueryOp]QueryHandlerFunc {
	m1 := make(map[common.QueryOp]QueryHandlerFunc)
	for op, h := range m {
		m1[op] = h
	}
	return m1
}
