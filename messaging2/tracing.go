package messaging2

// This file contains utility functions for adding support for OpenTelemetry to cloudevent

/*
import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/propagation"
)

// CloudEventTextMapCarrier ...
type CloudEventTextMapCarrier struct {
	ce *cloudevents.Event
}

var _ propagation.TextMapCarrier = CloudEventTextMapCarrier{}

// NewCloudEventTextMapCarrier ...
func NewCloudEventTextMapCarrier(ce *cloudevents.Event) CloudEventTextMapCarrier {
	return CloudEventTextMapCarrier{ce: ce}
}

// Get ...
func (c CloudEventTextMapCarrier) Get(key string) string {
	log.WithField("key", key).Info("CloudEventTextMapCarrier.Get")
	raw, ok := c.ce.Extensions()[key]
	if !ok {
		return ""
	}
	// FIXME log if raw is not string
	s1 := raw.(string)
	log.WithField("key", key).Debug(s1)
	return s1
}

// Set ...
func (c CloudEventTextMapCarrier) Set(key string, value string) {
	log.WithField("key", key).Info("CloudEventTextMapCarrier.Set")
	c.ce.SetExtension(key, value)
}

// Keys ...
func (c CloudEventTextMapCarrier) Keys() []string {
	var result []string
	for k := range c.ce.Extensions() {
		// Note that not all extensions are keys for open-telemetry
		result = append(result, k)
	}
	return result
}


*/
