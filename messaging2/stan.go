package messaging2

import (
	"context"
	"fmt"
	"sync"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

// NewStanConnectionFromConfig ...
func NewStanConnectionFromConfig(conf NatsStanMsgConfig) (StanConnection, error) {
	sc, err := stan.Connect(conf.ClusterID, conf.ClientID, stan.NatsURL(conf.URL), stan.SetConnectionLostHandler(func(conn stan.Conn, err error) {
		log.WithError(err).Panic("lost contact with STAN server")
	}))
	if err != nil {
		return StanConnection{}, err
	}

	conn := StanConnection{
		cloudeventSource:   conf.ClientID,
		nc:                 nil,
		sc:                 sc,
		stanSub:            nil,
		stanSubQueueName:   conf.QueueGroup,
		stanSubDurableName: conf.DurableName,
		eventSource:        nil,
		eventSourceSub:     nil,
	}
	eventSource, err := NewEventSourceFromStanConnection(&conn)
	if err != nil {
		sc.Close()
		return StanConnection{}, err
	}
	conn.eventSource = eventSource
	return conn, nil
}

// NewStanConnectionFromConfigWithoutEventSource will create a STAN connection that does not have EventSource.
// StanConnection created by this function should not use StanConnection.Request() or StanConnection.WaitForResponse()
func NewStanConnectionFromConfigWithoutEventSource(conf NatsStanMsgConfig) (StanConnection, error) {
	sc, err := stan.Connect(conf.ClusterID, conf.ClientID, stan.NatsURL(conf.URL), stan.SetConnectionLostHandler(func(conn stan.Conn, err error) {
		log.WithError(err).Panic("lost contact with STAN server")
	}))
	if err != nil {
		return StanConnection{}, err
	}

	conn := StanConnection{
		cloudeventSource:   conf.ClientID,
		nc:                 nil,
		sc:                 sc,
		stanSub:            nil,
		stanSubQueueName:   conf.QueueGroup,
		stanSubDurableName: conf.DurableName,
		eventSource:        nil,
		eventSourceSub:     nil,
	}
	return conn, nil
}

// StanConnection implements EventConnection
type StanConnection struct {
	cloudeventSource   string
	nc                 *NatsConnection
	sc                 stan.Conn
	stanSub            stan.Subscription
	stanSubQueueName   string
	stanSubDurableName string
	eventSource        *EventSource
	eventSourceSub     stan.Subscription
}

var _ EventConnection = &StanConnection{}

func (conn *StanConnection) stanConnect(natsConfig NatsConfig, stanConfig StanConfig) error {
	if conn.nc == nil {
		err := conn.stanConnectWithNewNatsConn(natsConfig, stanConfig)
		if err != nil {
			return err
		}
	} else {
		err := conn.stanConnectWithExistingNatsConn(natsConfig, stanConfig)
		if err != nil {
			return err
		}
	}
	return nil
}

func (conn *StanConnection) stanConnectWithNewNatsConn(natsConfig NatsConfig, stanConfig StanConfig) error {
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsURL(natsConfig.URL))
	if err != nil {
		return err
	}
	conn.sc = sc
	return nil
}

func (conn *StanConnection) stanConnectWithExistingNatsConn(natsConfig NatsConfig, stanConfig StanConfig) error {
	sc, err := stan.Connect(stanConfig.ClusterID, natsConfig.ClientID, stan.NatsConn(conn.nc.nc))
	if err != nil {
		return err
	}
	conn.sc = sc
	return nil
}

// Listen ...
func (conn *StanConnection) Listen(ctx context.Context, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanConnection.Listen",
	})
	if conn.stanSubQueueName == "" {
		return fmt.Errorf("queue group name for STAN is empty")
	}
	if conn.stanSubDurableName == "" {
		return fmt.Errorf("durable name for STAN is empty")
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to StanConnection.Listen")
	}
	m = copyEventHandlerMappings(m)
	sub, err := conn.sc.QueueSubscribe(common.EventsSubject, conn.stanSubQueueName, func(msg *stan.Msg) {
		ce, err := ConvertStan(msg)
		if err != nil {
			logger.WithError(err).Error("fail to convert STAN msg to cloudevent")
			return
		}
		handler, ok := m[common.EventType(ce.Type())]
		if !ok {
			logger.WithField("ceType", ce.Type()).Trace("skip event")
			return
		}
		err = handler(ctx, ce, StanResponseWriter{conn: conn, tid: GetTransactionID(&ce)})
		if err != nil {
			logger.WithField("ceType", ce.Type()).WithError(err).Error("handler fail to handle event")
			return
		}
	}, stan.DurableName(conn.stanSubDurableName), stan.MaxInflight(10))
	if err != nil {
		return err
	}
	conn.stanSub = sub
	go conn.watchContextCancellation(ctx, wg)
	return nil
}

func (conn *StanConnection) watchContextCancellation(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanConnection.watchContextCancellation",
	})
	<-ctx.Done()
	if conn.stanSub == nil {
		logger.Warn("STAN subscription is nil")
		return
	}
	err := conn.stanSub.Close()
	if err != nil {
		logger.WithError(err).Info("fail to close STAN subscription")
		return
	}
	logger.Info("STAN subscription is closed due to context")
}

// ListenWithConcurrentWorkers ...
func (conn *StanConnection) ListenWithConcurrentWorkers(ctx context.Context, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup, channelBufferLen uint, workerCount uint) error {
	if conn.stanSubQueueName == "" {
		return fmt.Errorf("queue group name for STAN is empty")
	}
	if conn.stanSubDurableName == "" {
		return fmt.Errorf("durable name for STAN is empty")
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to StanConnection.ListenWithConcurrentWorkers")
	}
	if wg == nil {
		return fmt.Errorf("cannot pass nil WaitGroup to StanConnection.ListenWithConcurrentWorkers")
	}
	if workerCount == 0 || workerCount > 1000 {
		workerCount = defaultEventWorkerCount
	}

	var ceChan = make(chan cloudevents.Event, channelBufferLen)
	// this WaitGroup is to help to avoid closing channel before all callback
	// finishes, callback may still be running even after the subscription is closed
	var callbackWG sync.WaitGroup
	sub, err := conn.sc.QueueSubscribe(common.EventsSubject, conn.stanSubQueueName, func(msg *stan.Msg) {
		callbackWG.Add(1)
		defer callbackWG.Done()
		ce, err := ConvertStan(msg)
		if err != nil {
			log.WithError(err).Error()
			_ = msg.Ack()
			return
		}
		_, ok := m[common.EventType(ce.Type())]
		if !ok {
			_ = msg.Ack()
			return
		}
		select {
		case ceChan <- ce:
			_ = msg.Ack()
		case <-time.After(time.Second * 9):
		}
	}, stan.DurableName(conn.stanSubDurableName), stan.MaxInflight(10), stan.AckWait(time.Second*10), stan.SetManualAckMode())
	if err != nil {
		return err
	}
	conn.stanSub = sub
	var workerWG sync.WaitGroup
	go func() {
		defer wg.Done()
		logger := log.WithFields(log.Fields{
			"package":  "adapters",
			"function": "StanConnection.watchContextCancellation",
		})
		<-ctx.Done()
		err := conn.stanSub.Close()
		if err != nil {
			logger.WithError(err).Info("fail to close STAN subscription")
			return
		}
		callbackWG.Wait() // wait for callback to finish
		close(ceChan)     // close channel to stop worker go-routines
		workerWG.Wait()   // wait for workers to stop
		logger.Info("STAN subscription is closed due to context")
	}()
	conn.spawnEventWorkers(ctx, ceChan, m, workerCount, &workerWG)
	return nil
}

func (conn *StanConnection) spawnEventWorkers(ctx context.Context, ceChan <-chan cloudevents.Event, m map[common.EventType]EventHandlerFunc, workerCount uint, wg *sync.WaitGroup) {
	for i := uint(0); i < workerCount; i++ {
		wg.Add(1)
		mCopy := copyEventHandlerMappings(m)
		go conn.eventWorker(ctx, ceChan, mCopy, wg)
	}
}

func (conn *StanConnection) eventWorker(ctx context.Context, ceChan <-chan cloudevents.Event, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup) {
	defer wg.Done()
	for ce := range ceChan {
		handler, ok := m[common.EventType(ce.Type())]
		if !ok {
			// should not happen, since we filter out the events with no handler in the subscription callback
			continue
		}
		err := handler(ctx, ce, StanResponseWriter{conn: conn, tid: GetTransactionID(&ce)})
		if err != nil {
			log.WithField("ceType", ce.Type()).WithError(err).Error("handler fail to handle event")
			continue
		}
	}
	// this function will break out the for-loop and return when ceChan is closed
}

// Publish ...
func (conn *StanConnection) Publish(ce cloudevents.Event) error {
	if ce.Source() == "" || ce.Source() == AutoPopulateCloudEventSource {
		ce.SetSource(conn.cloudeventSource)
	}
	marshal, err := ce.MarshalJSON()
	if err != nil {
		return err
	}
	err = conn.sc.Publish(common.EventsSubject, marshal)
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"ceType": ce.Type(),
		"tid":    GetTransactionID(&ce),
	}).Debug("published event")
	return nil
}

// Request ...
func (conn *StanConnection) Request(requestCe cloudevents.Event, responseEventTypes []common.EventType) (EventResponsePromise, error) {
	if conn.eventSource == nil {
		panic("EventSource is nil, StanConnection is not properly created")
	}
	var promise = &StanEventResponsePromise{
		ceChan:      make(chan cloudevents.Event),
		eventSource: conn.eventSource,
	}
	transactionID := GetTransactionID(&requestCe)
	if transactionID == "" {
		return nil, fmt.Errorf("missing transaction ID in STAN request")
	}

	listenerID, err := conn.eventSource.AddListenerMultiEventType(responseEventTypes, transactionID, Listener{
		Callback: func(ev common.EventType, ce cloudevents.Event) {
			promise.ceChan <- ce
		}, ListenOnce: true,
	})
	if err != nil {
		return nil, err
	}
	promise.listenerID = listenerID

	err = conn.Publish(requestCe)
	if err != nil {
		conn.eventSource.RemoveListenerByID(listenerID)
		return nil, err
	}
	// listener should automatically be removed upon receiving a matching cloudevent, since it is ListenOnce

	return promise, nil
}

// WaitForResponse ...
func (conn *StanConnection) WaitForResponse(transactionID common.TransactionID, responseEventTypes []common.EventType) (EventResponsePromise, error) {
	if conn.eventSource == nil {
		panic("EventSource is nil, StanConnection is not properly created")
	}
	var promise = &StanEventResponsePromise{
		ceChan:      make(chan cloudevents.Event),
		eventSource: conn.eventSource,
	}
	listenerID, err := conn.eventSource.AddListenerMultiEventType(responseEventTypes, transactionID, Listener{
		Callback: func(ev common.EventType, ce cloudevents.Event) {
			promise.ceChan <- ce
		}, ListenOnce: true,
	})
	if err != nil {
		return nil, err
	}
	promise.listenerID = listenerID

	// listener should automatically be removed upon receiving a matching cloudevent, since it is ListenOnce
	return promise, nil
}

// Close ...
func (conn *StanConnection) Close() error {
	return conn.sc.Close()
}

// GetStanConn ...
func (conn *StanConnection) GetStanConn() stan.Conn {
	return conn.sc
}

// StanEventResponsePromise ...
type StanEventResponsePromise struct {
	ceChan      chan cloudevents.Event
	eventSource *EventSource
	listenerID  ListenerID
}

var _ EventResponsePromise = &StanEventResponsePromise{}

// Response ...
func (rp *StanEventResponsePromise) Response(ctx context.Context) (cloudevents.Event, error) {
	defer rp.eventSource.RemoveListenerByID(rp.listenerID)
	select {
	case <-ctx.Done():
		return cloudevents.Event{}, ctx.Err()
	case ce := <-rp.ceChan:
		return ce, nil
	}
}

// ResponseChan ...
func (rp *StanEventResponsePromise) ResponseChan(context.Context) <-chan cloudevents.Event {
	return rp.ceChan
}

// Close ...
func (rp *StanEventResponsePromise) Close() error {
	rp.eventSource.RemoveListenerByID(rp.listenerID)
	return nil
}

// StanResponseWriter ...
type StanResponseWriter struct {
	conn *StanConnection
	tid  common.TransactionID
}

var _ EventResponseWriter = StanResponseWriter{}

// Write publish cloudevent, only set transaction ID when cloudevent does not have one
func (rw StanResponseWriter) Write(ce cloudevents.Event) error {
	if tid := GetTransactionID(&ce); tid == "" {
		SetTransactionID(&ce, rw.tid)
	}
	return rw.conn.Publish(ce)
}

// NoResponse ...
func (rw StanResponseWriter) NoResponse() {
}
