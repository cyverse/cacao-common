package messaging2

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	cetypes "github.com/cloudevents/sdk-go/v2/types"
	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// NewNatsConnectionFromConfig ...
func NewNatsConnectionFromConfig(conf NatsStanMsgConfig) (NatsConnection, error) {
	nc, err := connectNats(conf)
	if err != nil {
		return NatsConnection{}, err
	}
	return NewNatsConnection(conf, nc)
}

// NewNatsConnection creates NatsConnection from an existing nats.Conn, this is useful if one uses different
// connection options to establish the connection.
func NewNatsConnection(conf NatsStanMsgConfig, nc *nats.Conn) (NatsConnection, error) {
	var timeout time.Duration
	if conf.RequestTimeout <= 0 {
		timeout = DefaultNatsRequestTimeout
	} else if time.Duration(conf.RequestTimeout)*time.Second > MaxNatsRequestTimeout {
		timeout = MaxNatsRequestTimeout
	} else {
		timeout = time.Duration(conf.ReconnectWait) * time.Second
	}
	return NatsConnection{
		listenSubject:    conf.WildcardSubject,
		cloudeventSource: conf.ClientID,
		nc:               nc,
		nsub:             nil,
		requestTimeout:   timeout,
	}, nil
}

func connectNats(conf NatsStanMsgConfig) (*nats.Conn, error) {
	var reconnWait time.Duration
	if conf.ReconnectWait <= 0 {
		reconnWait = DefaultNatsReconnectWait
	} else {
		reconnWait = time.Duration(conf.ReconnectWait) * time.Second
	}
	if conf.MaxReconnects <= 0 {
		conf.MaxReconnects = DefaultNatsMaxReconnect
	}
	nc, err := nats.Connect(conf.URL, nats.Name(conf.ClientID), nats.ReconnectWait(reconnWait), nats.MaxReconnects(conf.MaxReconnects))
	if err != nil {
		return nil, err
	}
	return nc, nil
}

// NatsConnection implements QueryConnection
type NatsConnection struct {
	listenSubject string
	// cloudeventSource will be used as queue name, durable name
	cloudeventSource string
	nc               *nats.Conn
	nsub             *nats.Subscription
	requestTimeout   time.Duration
}

var _ QueryConnection = &NatsConnection{}

// Listen ...
func (conn *NatsConnection) Listen(ctx context.Context, m map[common.QueryOp]QueryHandlerFunc, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{"function": "NatsConnection.Listen"})
	if ctx == nil {
		return fmt.Errorf("cannot pass nil Context to NatsConnection.Listen")
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to NatsConnection.Listen")
	}
	m = copyQueryHandlerMappings(m)
	sub, err := conn.nc.QueueSubscribe(conn.listenSubject, conn.cloudeventSource, func(msg *nats.Msg) {
		handler, ok := m[common.QueryOp(msg.Subject)]
		if !ok {
			logger.WithField("subject", msg.Subject).Trace("skipped")
			return
		}
		logger.WithField("subject", msg.Subject).Trace("received msg")
		ce, err := ConvertNats(msg)
		if err != nil {
			logger.WithError(err).Error("fail to convert to cloudevent")
			return
		}
		newCtx, cancel := context.WithTimeout(ctx, conn.requestTimeout)
		handler(newCtx, ce, NatsReplyWriter{replySubject: msg.Reply, conn: conn})
		cancel()
	})
	if err != nil {
		logger.WithError(err).Error("fail to create subscription")
		return err
	}
	logger.WithFields(log.Fields{"subject": sub.Subject, "queueGroup": sub.Queue}).Info("subscribed")
	conn.nsub = sub
	go drainNatsSubscriptionOnContextDone(ctx, sub, wg)
	return nil
}

func drainNatsSubscriptionOnContextDone(ctx context.Context, sub *nats.Subscription, wg *sync.WaitGroup) {
	if wg != nil {
		defer wg.Done()
	}
	<-ctx.Done()
	err := sub.Drain()
	if err != nil {
		log.WithError(err).Error("fail to drain Core NATS subscription")
		return
	}
}

// ListenWithConcurrentWorkers will create an internal channels to subscribe to
// nats.Msg. Multiple concurrent worker go-routine will consume msg from the
// channels. This allows for bounded concurrency.
func (conn *NatsConnection) ListenWithConcurrentWorkers(ctx context.Context, m map[common.QueryOp]QueryHandlerFunc, wg *sync.WaitGroup, channelBufferLen uint, workerCount uint) error {
	logger := log.WithFields(log.Fields{"function": "NatsConnection.ListenWithConcurrentWorkers"})
	if ctx == nil {
		return fmt.Errorf("cannot pass nil Context to NatsConnection.ListenWithConcurrentWorkers")
	}
	if wg == nil {
		return fmt.Errorf("cannot pass nil WaitGroup to NatsConnection.ListenWithConcurrentWorkers")
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to NatsConnection.ListenWithConcurrentWorkers")
	}
	if workerCount == 0 || workerCount > 1000 {
		workerCount = defaultQueryWorkerCount
	}
	var msgChan = make(chan *nats.Msg, channelBufferLen)
	sub, err := conn.nc.ChanQueueSubscribe(conn.listenSubject, conn.cloudeventSource, msgChan)
	if err != nil {
		logger.WithError(err).Error("fail to create subscription")
		return err
	}
	logger.WithFields(log.Fields{"subject": sub.Subject, "queueGroup": sub.Queue}).Info("subscribed")
	conn.nsub = sub
	var workerWG sync.WaitGroup
	go func() {
		<-ctx.Done()
		err := sub.Drain()
		if err != nil {
			log.WithError(err).Error("fail to drain Core NATS subscription")
		}
		close(msgChan) // close the channel to shut down the worker go-routine
		workerWG.Wait()
		wg.Done() // only notify caller via WaitGroup after workers have stopped
	}()
	conn.spawnQueryWorkers(msgChan, workerCount, m, &workerWG)
	return nil
}

func (conn *NatsConnection) spawnQueryWorkers(msgChan <-chan *nats.Msg, workerCount uint, m map[common.QueryOp]QueryHandlerFunc, wg *sync.WaitGroup) {
	for i := uint(0); i < workerCount; i++ {
		wg.Add(1)
		mCopy := copyQueryHandlerMappings(m)
		go conn.queryWorker(msgChan, mCopy, wg)
	}
}

func (conn *NatsConnection) queryWorker(msgChan <-chan *nats.Msg, m map[common.QueryOp]QueryHandlerFunc, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{"function": "NatsConnection.queryWorker"})
	defer wg.Done()
	for msg := range msgChan {
		handler, ok := m[common.QueryOp(msg.Subject)]
		if !ok {
			logger.WithField("subject", msg.Subject).Trace("skipped")
			continue
		}
		logger.WithField("subject", msg.Subject).Trace("received msg")
		ce, err := ConvertNats(msg)
		if err != nil {
			logger.WithError(err).Error("fail to convert to cloudevent")
			continue
		}
		newCtx, cancel := context.WithTimeout(context.Background(), conn.requestTimeout)
		handler(newCtx, ce, NatsReplyWriter{replySubject: msg.Reply, conn: conn})
		cancel()
	}
	// this function will break out the for-loop and return when msgChan is closed
}

// Request ...
func (conn *NatsConnection) Request(ctx context.Context, request cloudevents.Event) (reply cloudevents.Event, err error) {
	if _, ok := ctx.Deadline(); !ok {
		ctx, _ = context.WithTimeout(ctx, conn.requestTimeout)
	}
	if request.Source() == "" || request.Source() == AutoPopulateCloudEventSource {
		request.SetSource(conn.cloudeventSource)
	}
	marshal, err := request.MarshalJSON()
	if err != nil {
		return cloudevents.Event{}, err
	}
	replyMsg, err := conn.nc.RequestWithContext(ctx, request.Type(), marshal)
	if err != nil {
		return cloudevents.Event{}, err
	}
	replyCe, err := ConvertNats(replyMsg)
	if err != nil {
		return cloudevents.Event{}, err
	}
	return replyCe, nil
}

type natsReplyStream struct {
	index    int
	allReply []cloudevents.Event
}

// ExpectNext ...
func (nrs *natsReplyStream) ExpectNext() bool {
	return nrs.index < len(nrs.allReply)
}

// Next ...
func (nrs *natsReplyStream) Next() (cloudevents.Event, error) {
	nrs.index++
	return nrs.allReply[nrs.index-1], nil
}

// RequestStream ...
func (conn *NatsConnection) RequestStream(parentCtx context.Context, request cloudevents.Event) (ReplyStream, error) {
	marshal, err := request.MarshalJSON()
	if err != nil {
		return nil, err
	}
	replySubj := conn.nc.NewRespInbox()

	sub, err := conn.nc.SubscribeSync(replySubj)
	if err != nil {
		return nil, err
	}
	defer sub.Unsubscribe()

	err = conn.nc.PublishRequest(request.Type(), replySubj, marshal)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(parentCtx, time.Second*5)
	defer cancel()
	getNextReply := func(sub *nats.Subscription) (*cloudevents.Event, error) {
		msg, err := sub.NextMsgWithContext(ctx)
		if err != nil {
			errorMessage := "unable to get next message"
			log.WithError(err).Error(errorMessage)
			return nil, errors.New(errorMessage)
		}

		// convert the message to a cloud event
		ce, err := messaging.ConvertNats(msg)
		if err != nil {
			errorMessage := "unable to convert a NATS message to a cloud event"
			log.WithError(err).Error(errorMessage)
			return nil, errors.New(errorMessage)
		}
		return &ce, err
	}

	// 1st reply
	replyEvent, err := getNextReply(sub)
	if err != nil {
		return nil, err
	}

	totalReplyCount := replyCountExtractor(replyEvent) // get total number of replies from 1st reply
	if totalReplyCount < 1 {
		errorMessage := "bad totalReplyCount"
		log.WithField("totalReplyCount", totalReplyCount).Error(errorMessage)
		return nil, errors.New(errorMessage)
	}
	var stream = natsReplyStream{
		index:    0,
		allReply: []cloudevents.Event{*replyEvent},
	}

	// rest of replies
	for i := 1; i < totalReplyCount; i++ {
		replyEvent, err = getNextReply(sub)
		if err != nil {
			return nil, err
		}
		stream.allReply = append(stream.allReply, *replyEvent)
	}
	return &stream, nil
}

// use replyCount to indicate how many replies there is for a request, this is to mimic the behavior of deployment service client.
const natsReplyCountCloudEventExtension = "ReplyCount"

// TODO consider using sequence extension for streaming multiple replies for one request.
// https://github.com/cloudevents/spec/blob/main/cloudevents/extensions/sequence.md
const natsSequenceCloudEventExtension = "sequence"

func replyCountExtractor(ce *cloudevents.Event) int {
	replyCountRaw, err := ce.Context.GetExtension(natsReplyCountCloudEventExtension)
	if err != nil {
		return 0
	}
	c, err := cetypes.ToInteger(replyCountRaw)
	if err != nil {
		return 0
	}
	return int(c)
}

// Close ...
func (conn *NatsConnection) Close() error {
	conn.nc.Close()
	return nil
}

// GetNatsConn returns the underlying nats.Conn (could be nil if not initialized), this is useful if one wants to
// implement custom NATS messaging.
func (conn *NatsConnection) GetNatsConn() *nats.Conn {
	return conn.nc
}

// NatsReplyWriter implements ReplyWriter
type NatsReplyWriter struct {
	replySubject string
	conn         *NatsConnection
}

var _ ReplyWriter = NatsReplyWriter{}

// Write a cloudevent as reply to the request. This can be called multiple time.
func (n NatsReplyWriter) Write(event cloudevents.Event) error {
	if event.Source() == "" || event.Source() == AutoPopulateCloudEventSource {
		event.SetSource(n.conn.cloudeventSource)
	}
	marshal, err := event.MarshalJSON()
	if err != nil {
		return err
	}
	err = n.conn.nc.Publish(n.replySubject, marshal)
	if err != nil {
		return err
	}
	return nil
}
