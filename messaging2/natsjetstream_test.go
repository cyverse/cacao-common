//go:build natsjs_integration

package messaging2

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
)

func TestNatsJetConnection_computeSubscribeSubjectFromEventTypes(t *testing.T) {
	t.Skip()
	return
	type args struct {
		responseEventTypes []common.EventType
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{
				responseEventTypes: []common.EventType{},
			},
			want: ">",
		},
		{
			name: "",
			args: args{
				responseEventTypes: []common.EventType{"org.cyverse.events"},
			},
			want: "org.cyverse.events",
		},
		{
			name: "",
			args: args{
				responseEventTypes: []common.EventType{"org.cyverse.events", "org.cyverse.events.Foo"},
			},
			want: "org.cyverse.events.>",
		},
		{
			name: "",
			args: args{
				responseEventTypes: []common.EventType{"org.cyverse.events.Foo", "org.cyverse.events.Bar"},
			},
			want: "org.cyverse.events.*",
		},
		{
			name: "",
			args: args{
				responseEventTypes: []common.EventType{"org.cyverse.events.Foo", "org.cyverse.events.Foo.Baz"},
			},
			want: "org.cyverse.events.Foo>",
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, computeSubscribeSubjectFromEventTypes(tt.args.responseEventTypes), "computeSubscribeSubjectFromEventTypes(%v)", tt.args.responseEventTypes)
		})
	}
}
