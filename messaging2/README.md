# messaging2 package
This package contains the messaging abstraction (`QueryConnection` & `EventConnection`) that service and service client
(e.g. those in `cacao-common/services`) can use to publish and receive messages.
This package also contains the implementations of the abstraction based on NATS (`QueryConnection`), NATS Streaming (`EventConnection`)
and NATS JetStream (`EventConnection`).

Note that NATS Streaming is deprecated, its usage should be replaced by NATS JetStream.

# Example
For an example of service client that uses the messaging abstraction, which publishes query request and events, checkout `natsWorkspaceClient` in `cacao-common/service/workspace_nats_client.go`.

For an example of service that uses the messaging abstraction, check out workspace service in `cacao/workspace-service` or user service (gitlab.com/cyverse/users-microservice).

# Semantics

There are 2 types of messages, query and event.
The difference between them is:
- query generally corresponds to read operations.
- query can withstand losses (we can safely retry), therefore does not need persistence.
- event are operations that have side effect (make changes to the system).
- event should be persisted by the underlying messaging bus.

# Pattern
## request/reply (query)
client sends a request to a service, service get back with a reply.

## request/response (event)
client sends a request to a service, service get back with a response event.

The difference between this and request/reply for query is that the response event is still an event, which means other parties can listen for the response event, whereas reply cannot be consumed by other service directly.

e.g.
```
client published CredentialCreationRequested => service A received CredentialCreationRequested => service A respond with CredentialCreated.
service B received CredentialCreated => service B published CacheUpdated.
```


## source/triggered
e.g. UserLoggedIn => PopulateCache

# Graceful termination
We want to be able to terminate service without losing messages, **especially events**.
When service exit gracefully, there is a grace period for the service to clean things up.
- Stop all incoming
- Incoming stopped
  - Need a way to be notified that incoming has stopped
  - This is necessary because stop incoming msg is not immediate, need to coordinate with message bus server.
- Allow messages that are currently being processed to finish
    - This means we need some degree of separation between stopping the incoming and ongoing msg handlers
      - stop incoming (e.g. close subscription) should not stop ongoing msg handlers
    - Need a timeout, cannot wait indefinitely
    - Need to know when this finishes, so that service can proceeds to exit
      - This suggests that we need to keep track of messages that are being processed
- Close connection and other resources (e.g. DB connection)
- Exit
  - Main thread needs to wait for all of the above to happen and finish, need a way to wait for that

# Integration tests
use [docker-compose.yaml](./docker-compose.yaml) for running integration tests locally.
```shell
docker-compose up -d
```

for integration tests that needs NATS Streaming:
```shell
export NATS_URL=localhost:5222
go test -v --tags=stan_integration .
```

for integration tests that needs NATS Core or NATS JetStream:
```shell
export NATS_URL=localhost:4222
go test -v --tags=nats_integration,natsjs_integration .
```
