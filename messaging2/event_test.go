package messaging2

import (
	"context"
	"sync"
	"testing"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

func fullEventType(postfix string) common.EventType {
	return common.EventType(common.EventTypePrefix + postfix)
}

func testEventConnection(t *testing.T, constructor func() EventConnection, cleanup func(EventConnection)) {
	log.SetLevel(log.TraceLevel)
	conn := constructor()
	defer cleanup(conn)

	listenCtx, cancelListenCtx := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancelListenCtx()
	var wg sync.WaitGroup

	var FooReceived = make(chan bool, 1)
	var BarReceived = make(chan bool, 1)
	wg.Add(1)
	err := conn.Listen(listenCtx, map[common.EventType]EventHandlerFunc{
		common.EventTypePrefix + "Foo": func(ctx context.Context, event cloudevents.Event, writer EventResponseWriter) error {
			log.Info(event.Type())
			assert.Equal(t, common.EventTypePrefix+"Foo", event.Type())
			FooReceived <- true
			return nil
		},
		common.EventTypePrefix + "Bar": func(ctx context.Context, event cloudevents.Event, writer EventResponseWriter) error {
			log.Info(event.Type())
			assert.Equal(t, common.EventTypePrefix+"Bar", event.Type())
			tid := GetTransactionID(&event)
			ce, err := CreateCloudEventWithTransactionID(map[string]string{"foo": "bar"}, fullEventType("Baz"), "test-sub", tid)
			if !assert.NoError(t, err) {
				return nil
			}
			err = writer.Write(ce)
			if !assert.NoError(t, err) {
				return nil
			}
			BarReceived <- true
			return nil
		},
	}, &wg)
	if !assert.NoError(t, err) {
		return
	}
	event, err := CreateCloudEvent(map[string]string{}, fullEventType("Foo"), "test-pub")
	if !assert.NoError(t, err) {
		return
	}
	err = conn.Publish(event)
	if !assert.NoError(t, err) {
		return
	}
	log.Infof("published %s", event.Type())

	select {
	case <-FooReceived:
	case <-time.After(time.Second * 2):
		assert.Fail(t, "did not recevive msg", common.EventTypePrefix+"Foo")
	}

	event, err = CreateCloudEventWithTransactionID(map[string]string{}, fullEventType("Bar"), "test-pub", messaging.NewTransactionID())
	if !assert.NoError(t, err) {
		return
	}
	response, err := conn.Request(event, []common.EventType{common.EventTypePrefix + "Baz"})
	if !assert.NoError(t, err) {
		return
	}
	log.Infof("published %s", common.EventTypePrefix+"Bar")
	select {
	case <-BarReceived:
	case <-time.After(time.Second * 2):
		assert.Fail(t, "did not recevive", common.EventTypePrefix+"Bar")
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
	responseCe, err := response.Response(ctx)
	cancelFunc()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, common.EventTypePrefix+"Baz", responseCe.Type())
	cancelListenCtx()
	wg.Wait()
}
