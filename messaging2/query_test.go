package messaging2

import (
	"context"
	"encoding/json"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
)

func testQueryConnection(t *testing.T, constructor func() QueryConnection, cleanup func(QueryConnection)) {
	conn := constructor()
	if conn == nil {
		return
	}
	defer cleanup(conn)

	var wg sync.WaitGroup

	var queryCounter uint32

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancelFunc()
	wg.Add(1)
	err := conn.Listen(ctx,
		map[common.QueryOp]QueryHandlerFunc{
			"cyverse.bar.get": func(ctx2 context.Context, request cloudevents.Event, writer ReplyWriter) {
				assert.Failf(t, request.Type(), "should not receive on this query handler")
			},
			"cyverse.foo.get": func(ctx2 context.Context, request cloudevents.Event, writer ReplyWriter) {
				assert.Equal(t, "cyverse.foo.get", request.Type())
				ce, err := CreateCloudEventWithAutoSource(map[string]string{"foo": "get"}, common.QueryOp("cyverse.foo.get"))
				if !assert.NoError(t, err) {
					return
				}
				err = writer.Write(ce)
				if !assert.NoError(t, err) {
					return
				}
				atomic.AddUint32(&queryCounter, 1)
			},
		},
		&wg)
	if !assert.NoError(t, err) {
		return
	}

	// send N concurrent queries
	const totalQueryCount = 1000
	var clientWG sync.WaitGroup
	for i := 0; i < totalQueryCount; i++ {
		clientWG.Add(1)
		go func() {
			defer clientWG.Done()
			ce, err := CreateCloudEventWithAutoSource(map[string]string{}, common.QueryOp("cyverse.foo.get"))
			if !assert.NoError(t, err) {
				return
			}
			reply, err := conn.Request(ctx, ce)
			if !assert.NoError(t, err) {
				return
			}
			assert.Equal(t, "cyverse.foo.get", reply.Type())
			var replyData map[string]string
			err = json.Unmarshal(reply.Data(), &replyData)
			if !assert.NoError(t, err) {
				return
			}
			assert.Len(t, replyData, 1)
			_, ok := replyData["foo"]
			assert.Truef(t, ok, "replyData does not contain 'foo'")
			assert.Equal(t, "get", replyData["foo"])
		}()
	}
	clientWG.Wait()

	cancelFunc()
	wg.Wait()
	assert.Equal(t, uint32(totalQueryCount), atomic.LoadUint32(&queryCounter))
}

func TestReplyCountExtractor(t *testing.T) {
	t.Run("no extension", func(t *testing.T) {
		ce, err := CreateCloudEventWithAutoSource(map[string]string{}, common.QueryOp("foo"))
		assert.NoError(t, err)
		result := replyCountExtractor(&ce)
		assert.Equal(t, 0, result)
	})
	t.Run("set via SetExtension()", func(t *testing.T) {
		ce, err := CreateCloudEventWithAutoSource(map[string]string{}, common.QueryOp("foo"))
		assert.NoError(t, err)
		ce.SetExtension(natsReplyCountCloudEventExtension, 123)
		result := replyCountExtractor(&ce)
		assert.Equal(t, 123, result)
	})
	t.Run("set via SetExtension() zero", func(t *testing.T) {
		ce, err := CreateCloudEventWithAutoSource(map[string]string{}, common.QueryOp("foo"))
		assert.NoError(t, err)
		ce.SetExtension(natsReplyCountCloudEventExtension, 0)
		result := replyCountExtractor(&ce)
		assert.Equal(t, 0, result)
	})
	t.Run("set via SetExtension() negative", func(t *testing.T) {
		ce, err := CreateCloudEventWithAutoSource(map[string]string{}, common.QueryOp("foo"))
		assert.NoError(t, err)
		ce.SetExtension(natsReplyCountCloudEventExtension, -1)
		result := replyCountExtractor(&ce)
		assert.Equal(t, -1, result)
	})
}
