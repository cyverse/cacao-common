//go:build nats_integration

package messaging2

import (
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestNatsConnection(t *testing.T) {
	log.SetLevel(log.TraceLevel)
	natsURL, ok := os.LookupEnv("NATS_URL")
	if !ok {
		natsURL = "localhost:4222"
	}
	config := NatsStanMsgConfig{
		NatsConfig: NatsConfig{
			URL:             natsURL,
			QueueGroup:      "cacao-common-test",
			WildcardSubject: "cyverse.foo.*",
			ClientID:        "cacao-common-test",
			MaxReconnects:   0,
			ReconnectWait:   0,
			RequestTimeout:  0,
		},
		StanConfig: StanConfig{
			ClusterID:     "",
			DurableName:   "cacao-common-test",
			EventsTimeout: 0,
		},
	}
	testQueryConnection(t,
		func() QueryConnection {
			conn, err := config.ConnectNats()
			if !assert.NoError(t, err) {
				return nil
			}
			return &conn
		},
		func(conn QueryConnection) {
			connection := conn.(*NatsConnection)
			connection.Close()
		},
	)
}
