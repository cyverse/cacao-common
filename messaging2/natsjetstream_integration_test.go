//go:build natsjs_integration

package messaging2

import (
	"context"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// this is an example of push consumer
func TestNatsJetStream(t *testing.T) {
	log.SetLevel(log.TraceLevel)
	natsURL, ok := os.LookupEnv("NATS_URL")
	if !ok {
		natsURL = "localhost:4222"
	}
	config := NatsStanMsgConfig{
		NatsConfig: NatsConfig{
			URL:             natsURL,
			QueueGroup:      "cacao-common-push-test",
			WildcardSubject: "",
			ClientID:        "cacao-common-push-test",
			MaxReconnects:   0,
			ReconnectWait:   0,
			RequestTimeout:  0,
		},
		StanConfig: StanConfig{
			ClusterID:     "",
			DurableName:   "cacao-common-push-test",
			EventsTimeout: 0,
		},
	}
	err := InitCyVerseStream(config)
	if err != nil {
		panic(err)
	}
	natsConn, err := config.ConnectNats()
	if err != nil {
		panic(err)
	}
	js, err := natsConn.nc.JetStream()
	if !assert.NoError(t, err) {
		return
	}

	var wg sync.WaitGroup
	wg.Add(1)
	// push subscription but sync delivery, note this is not really durable, since
	// Unsubscribe will remove the consumer, because consumer is created implicitly
	// by QueueSubscribeSync() call
	sub, err := js.QueueSubscribeSync(common.EventTypePrefix+">", config.ClientID, nats.BindStream(CyVerseStreamName), nats.Durable(config.DurableName))
	if !assert.NoError(t, err) {
		return
	}
	defer sub.Unsubscribe()

	ce, err := CreateCloudEventWithTransactionID(map[string]string{}, fullEventType("Bar"), config.ClientID, messaging.NewTransactionID())
	if !assert.NoError(t, err) {
		return
	}
	marshal, err := ce.MarshalJSON()
	if !assert.NoError(t, err) {
		return
	}
	_, err = js.Publish(ce.Type(), marshal)
	if !assert.NoError(t, err) {
		return
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*10)
	msg, err := sub.NextMsgWithContext(ctx) // push subscription works with NextMsgWithContext()
	cancelFunc()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, common.EventTypePrefix+"Bar", msg.Subject)
}

func TestNatsJetConnection(t *testing.T) {
	log.SetLevel(log.TraceLevel)
	natsURL, ok := os.LookupEnv("NATS_URL")
	if !ok {
		natsURL = "localhost:4222"
	}
	config := NatsStanMsgConfig{
		NatsConfig: NatsConfig{
			URL:             natsURL,
			QueueGroup:      "cacao-common-test",
			WildcardSubject: "",
			ClientID:        "cacao-common-test",
			MaxReconnects:   0,
			ReconnectWait:   0,
			RequestTimeout:  0,
		},
		StanConfig: StanConfig{
			ClusterID:     "",
			DurableName:   "cacao-common-test",
			EventsTimeout: 0,
		},
	}
	err := InitCyVerseStream(config)
	if err != nil {
		panic(err)
	}
	testEventConnection(t, func() EventConnection {
		conn, err := NewNatsJetConnectionFromConfig(config)
		if !assert.NoError(t, err) {
			panic(err)
		}
		return &conn

	}, func(c EventConnection) {
		conn := c.(*NatsJetConnection)
		conn.listenCtxCancelFunc()
		conn.Close()
	},
	)
}
