package messaging2

import (
	"fmt"
	"testing"

	v2 "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
)

// stanConn is for mocking Publish()
type stanConn struct {
	t              *testing.T
	PublishSubject string
	PublishData    []byte
}

var _ stan.Conn = (*stanConn)(nil)

func (s stanConn) Publish(subject string, data []byte) error {
	assert.Equal(s.t, s.PublishSubject, subject)
	assert.Equal(s.t, string(s.PublishData), string(data))
	return nil
}

func (s stanConn) PublishAsync(subject string, data []byte, ah stan.AckHandler) (string, error) {
	panic("not implemented")
}

func (s stanConn) Subscribe(subject string, cb stan.MsgHandler, opts ...stan.SubscriptionOption) (stan.Subscription, error) {
	panic("not implemented")
}

func (s stanConn) QueueSubscribe(subject, qgroup string, cb stan.MsgHandler, opts ...stan.SubscriptionOption) (stan.Subscription, error) {
	panic("not implemented")
}

func (s stanConn) Close() error {
	panic("not implemented")
}

func (s stanConn) NatsConn() *nats.Conn {
	panic("not implemented")
}

func TestStanResponseWriter_Write(t *testing.T) {
	ceNoTID, _ := CreateCloudEvent(map[string]string{}, common.EventType("Foo"), AutoPopulateCloudEventSource)
	ceWithTID, _ := CreateCloudEventWithTransactionID(map[string]string{}, common.EventType("Foo"), AutoPopulateCloudEventSource, NewTransactionID())
	type fields struct {
		conn func(t *testing.T) *StanConnection
		tid  common.TransactionID
	}
	type args struct {
		ce v2.Event
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "ce has no tid",
			fields: fields{
				conn: func(t *testing.T) *StanConnection {
					ceCopy := ceNoTID
					ceCopy.SetSource("eventsource-123")
					SetTransactionID(&ceCopy, "tid-bbbbbbbbbbbbbbbbbbbb")
					jsonMarshal, err := ceCopy.MarshalJSON()
					if !assert.NoError(t, err) {
						return nil
					}
					return &StanConnection{
						sc: stanConn{
							t:              t,
							PublishSubject: common.EventsSubject,
							PublishData:    jsonMarshal,
						},
						cloudeventSource: "eventsource-123",
					}
				},
				tid: "tid-bbbbbbbbbbbbbbbbbbbb",
			},
			args: args{
				ce: func() v2.Event {
					return ceNoTID
				}(),
			},
			wantErr: assert.NoError,
		},
		{
			name: "ce has tid",
			fields: fields{
				conn: func(t *testing.T) *StanConnection {
					ceCopy := ceWithTID
					ceCopy.SetSource("eventsource-123")
					jsonMarshal, err := ceCopy.MarshalJSON()
					if !assert.NoError(t, err) {
						return nil
					}
					return &StanConnection{
						sc: stanConn{
							t:              t,
							PublishSubject: common.EventsSubject,
							PublishData:    jsonMarshal,
						},
						cloudeventSource: "eventsource-123",
					}
				},
				tid: "tid-bbbbbbbbbbbbbbbbbbbb",
			},
			args: args{
				ce: func() v2.Event {
					return ceWithTID
				}(),
			},
			wantErr: assert.NoError,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rw := StanResponseWriter{
				conn: tt.fields.conn(t),
				tid:  tt.fields.tid,
			}
			tt.wantErr(t, rw.Write(tt.args.ce), fmt.Sprintf("Write(%v)", tt.args.ce))
		})
	}
}
