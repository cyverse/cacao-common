//go:build stan_integration

package messaging2

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStanConnection(t *testing.T) {
	natsURL, ok := os.LookupEnv("NATS_URL")
	if !ok {
		natsURL = "localhost:4222"
	}
	stanClusterID, ok := os.LookupEnv("NATS_CLUSTER_ID")
	if !ok {
		stanClusterID = "test-cluster"
	}
	config := NatsStanMsgConfig{
		NatsConfig: NatsConfig{
			URL:             natsURL,
			QueueGroup:      "cacao-common-test",
			WildcardSubject: "",
			ClientID:        "cacao-common-test",
			MaxReconnects:   0,
			ReconnectWait:   0,
			RequestTimeout:  0,
		},
		StanConfig: StanConfig{
			ClusterID:     stanClusterID,
			DurableName:   "cacao-common-test",
			EventsTimeout: 0,
		},
	}
	testEventConnection(t, func() EventConnection {
		conn, err := config.ConnectStan()
		if !assert.NoError(t, err) {
			return nil
		}
		return &conn
	}, func(conn EventConnection) {
		stanConn := conn.(*StanConnection)
		stanConn.stanSub.Unsubscribe() // unsubscribe to remove the durable subscription
		stanConn.Close()
	},
	)
}
