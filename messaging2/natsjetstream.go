package messaging2

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	nats "github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

// CyVerseStreamName is the stream name of the NATS JetStream.
// This is used to create JetStream Consumer and bind durable Subscription to.
const CyVerseStreamName = "CYVERSE"

// InitCyVerseStream creates the Streams that is used for event messaging for CACAO.
// This needs to be called outside of normal cacao microservices, since normal service should not manage stream itself.
func InitCyVerseStream(conf NatsStanMsgConfig) error {
	logger := log.WithField("function", "InitCyVerseStream")
	nc, err := connectNats(conf)
	if err != nil {
		return err
	}
	defer nc.Close()
	logger.Info("connected to nats")

	js, err := nc.JetStream()
	if err != nil {
		return err
	}
	logger.Trace("got jetstream context")

	exists, err := jetStreamNameExists(js, CyVerseStreamName)
	if err != nil {
		return err
	}
	if exists {
		logger.Info("CyVerse JetStream already exists")
		return nil
	}

	_, err = js.AddStream(&nats.StreamConfig{
		Name:     CyVerseStreamName,
		Subjects: []string{common.EventTypePrefix + ">"},
		MaxBytes: 1 << 30, //  1GB
	})
	if err != nil {
		return err
	}
	logger.Info("created CyVerse stream")

	return nil
}

// return true if there already exists a stream by the same name.
func jetStreamNameExists(js nats.JetStreamContext, checkStreamName string) (bool, error) {
	namesChan := js.StreamNames()
	var streamNameList = make([]string, 0)
forLoop:
	for {
		select {
		case streamName := <-namesChan:
			if streamName == "" {
				break forLoop
			}
			streamNameList = append(streamNameList, streamName)
			log.WithField("streamName", streamName).Trace("got 1 stream name")
		case <-time.After(time.Second * 5):
			break forLoop
		}
	}
	log.Trace("fetched all stream names")
	for _, name := range streamNameList {
		if name == checkStreamName {
			return true, nil
		}
	}
	return false, nil
}

// NewNatsJetConnectionFromConfig creates a new NATS JetStream connection.
func NewNatsJetConnectionFromConfig(config NatsStanMsgConfig) (NatsJetConnection, error) {
	var ackWait time.Duration
	if config.AckWaitSec <= 0 {
		ackWait = DefaultAckWait
	} else {
		ackWait = time.Duration(config.AckWaitSec) * time.Second
	}
	conn, err := config.ConnectNats()
	if err != nil {
		return NatsJetConnection{}, err
	}
	stream, err := conn.nc.JetStream()
	if err != nil {
		err2 := conn.Close()
		if err2 != nil {
			return NatsJetConnection{}, fmt.Errorf("%w, %s", err, err2.Error())
		}
		return NatsJetConnection{}, err
	}
	return NatsJetConnection{
		nc:                  conn,
		durableName:         config.DurableName,
		jet:                 stream,
		wildcardSubject:     config.EventWildcardSubject,
		ackWait:             ackWait,
		listenSub:           nil,
		listenCtx:           nil,
		listenCtxCancelFunc: nil,
	}, err
}

// NewNatsJetConnection creates a new NATS JetStream connection from existing nats.Conn.
// This is useful when one creates nats.Conn with different connection options.
func NewNatsJetConnection(config NatsStanMsgConfig, nc *nats.Conn) (NatsJetConnection, error) {
	var ackWait time.Duration
	if config.AckWaitSec <= 0 {
		ackWait = DefaultAckWait
	} else {
		ackWait = time.Duration(config.AckWaitSec) * time.Second
	}
	conn, err := NewNatsConnection(config, nc)
	if err != nil {
		return NatsJetConnection{}, err
	}
	stream, err := conn.nc.JetStream()
	if err != nil {
		err2 := conn.Close()
		if err2 != nil {
			return NatsJetConnection{}, fmt.Errorf("%w, %s", err, err2.Error())
		}
		return NatsJetConnection{}, err
	}
	return NatsJetConnection{
		nc:                  conn,
		durableName:         config.DurableName,
		jet:                 stream,
		wildcardSubject:     config.EventWildcardSubject,
		ackWait:             ackWait,
		listenSub:           nil,
		listenCtx:           nil,
		listenCtxCancelFunc: nil,
	}, err
}

// NatsJetConnection is a EventConnection that builds on NATS JetStream
type NatsJetConnection struct {
	nc                  NatsConnection
	durableName         string
	jet                 nats.JetStreamContext
	wildcardSubject     string
	ackWait             time.Duration
	listenSub           *nats.Subscription
	listenCtx           context.Context
	listenCtxCancelFunc context.CancelFunc
}

var _ EventConnection = &NatsJetConnection{}

// Close ...
func (conn *NatsJetConnection) Close() error {
	if conn.listenCtxCancelFunc != nil {
		conn.listenCtxCancelFunc()
	}
	if conn.listenSub != nil {
		conn.listenSub.Drain()
	}
	return conn.nc.Close()
}

// GetJetStreamContext returns the underlying nats.JetStreamContext, this could be useful if one wants to implement some
// custom messaging not covered in this implementation.
func (conn *NatsJetConnection) GetJetStreamContext() nats.JetStreamContext {
	return conn.jet
}

// Listen ...
func (conn *NatsJetConnection) Listen(ctx context.Context, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.Listen"})

	if conn.nc.cloudeventSource == "" {
		return fmt.Errorf("cloudevent source cannot be empty, need to use it to configure consumer")
	}
	if conn.durableName == "" {
		return fmt.Errorf("durable name cannot be empty, need to use it to configure consumer")
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to NatsJetConnection.Listen")
	}
	m = copyEventHandlerMappings(m)

	var subject = conn.wildcardSubject
	if subject == "" {
		subject = common.EventTypePrefix + ">"
	}

	// create consumer separately from subscription so that we can use Unsubscribe()
	// and Drain() on subscription without removing consumer.
	consumer, err := conn.jet.AddConsumer(CyVerseStreamName, &nats.ConsumerConfig{
		Durable:   conn.durableName,
		AckPolicy: nats.AckExplicitPolicy,
		AckWait:   conn.ackWait,
	})
	if err != nil {
		logger.WithError(err).Error("fail to create consumer")
		return err
	}
	logger.WithFields(log.Fields{"consumerName": consumer.Name, "durable": conn.durableName, "deliverySubject": consumer.Config.DeliverSubject}).Info("consumer created")

	// pull consumer
	sub, err := conn.jet.PullSubscribe(subject, conn.durableName, nats.BindStream(CyVerseStreamName))
	if err != nil {
		logger.WithError(err).Error("fail to create subscription")
		return err
	}
	conn.listenSub = sub
	logger.WithFields(log.Fields{"consumerName": consumer.Name, "queue": sub.Queue, "subject": sub.Subject, "subType": sub.Type()}).Info("subscription created")

	conn.listenCtx, conn.listenCtxCancelFunc = context.WithCancel(ctx)
	go conn.pullFromSub(conn.listenCtx, sub, m)
	go conn.watchContextCancellation(ctx, wg)
	return nil
}

func (conn *NatsJetConnection) watchContextCancellation(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "NatsJetConnection.watchContextCancellation",
	})
	<-ctx.Done()
	if conn.listenSub == nil {
		logger.Warn("NATS JS subscription is nil")
		return
	}
	err := conn.listenSub.Drain()
	if err != nil {
		logger.WithError(err).Error("fail to drain NATS JS subscription")
		return
	}
	logger.Info("NATS JS subscription is closed due to context")
}

func (conn *NatsJetConnection) pullFromSub(ctx context.Context, sub *nats.Subscription, m map[common.EventType]EventHandlerFunc) {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.pullFromSub"})
	logger.Debugf("start")
	defer logger.Warn("stop listening")
	for {
		msgList, err := sub.Fetch(1, nats.Context(ctx)) // pull subscription does not work with NextMsg(), only Fetch()
		if errors.Is(err, context.DeadlineExceeded) {
			continue
		}
		if errors.Is(err, context.Canceled) {
			return
		}
		if err != nil {
			logger.WithError(err).Panic("fail to get next msg")
			return
		}
		log.WithField("count", len(msgList)).Trace("fetched msgs from pull subscription")
		for _, msg := range msgList {
			conn.handleMsg(ctx, msg, m)
		}
	}
}

// ListenWithConcurrentWorkers ...
func (conn *NatsJetConnection) ListenWithConcurrentWorkers(ctx context.Context, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup, channelBufferLen uint, workerCount uint) error {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.ListenWithConcurrentWorkers"})

	if conn.nc.cloudeventSource == "" {
		return fmt.Errorf("cloudevent source cannot be empty, need to use it to configure consumer")
	}
	if conn.durableName == "" {
		return fmt.Errorf("durable name cannot be empty, need to use it to configure consumer")
	}
	if wg == nil {
		return fmt.Errorf("cannot pass nil WaitGroup to NatsJetConnection.ListenWithConcurrentWorkers")
	}
	if workerCount == 0 || workerCount > 1000 {
		workerCount = defaultEventWorkerCount
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to NatsJetConnection.ListenWithConcurrentWorkers")
	}

	var subject = conn.wildcardSubject
	if subject == "" {
		subject = common.EventTypePrefix + ">"
	}

	// create consumer separately from subscription so that we can use Unsubscribe()
	// and Drain() on subscription without removing consumer.
	consumer, err := conn.jet.AddConsumer(CyVerseStreamName, &nats.ConsumerConfig{
		Durable:       conn.durableName,
		AckPolicy:     nats.AckExplicitPolicy,
		AckWait:       conn.ackWait,
		MaxAckPending: 100,
	})
	if err != nil {
		logger.WithError(err).Error("fail to create consumer")
		return err
	}
	logger.WithFields(log.Fields{"consumerName": consumer.Name, "durable": conn.durableName, "deliverySubject": consumer.Config.DeliverSubject}).Info("consumer created")

	// pull consumer
	sub, err := conn.jet.PullSubscribe(subject, conn.durableName, nats.BindStream(CyVerseStreamName))
	if err != nil {
		logger.WithError(err).Error("fail to create subscription")
		return err
	}
	conn.listenSub = sub
	logger.WithFields(log.Fields{"consumerName": consumer.Name, "queue": sub.Queue, "subject": sub.Subject, "subType": sub.Type()}).Info("subscription created")

	conn.listenCtx, conn.listenCtxCancelFunc = context.WithCancel(ctx)
	var msgChan = make(chan *nats.Msg, channelBufferLen)

	var workerWG sync.WaitGroup
	conn.spawnEventWorkers(conn.listenCtx, msgChan, m, workerCount, &workerWG)
	go conn.pullFromSubToChannel(conn.listenCtx, sub, msgChan, channelBufferLen)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		err := sub.Drain()
		if err != nil {
			logger.WithError(err).Error("fail to drain NATS JS subscription")
			return
		}
		logger.Info("NATS JS subscription is closed due to context")
		workerWG.Wait() // wait for workers to stop
	}()
	return nil
}

func (conn *NatsJetConnection) pullFromSubToChannel(ctx context.Context, sub *nats.Subscription, msgChan chan *nats.Msg, channelBufferLen uint) {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.pullFromSubToChannel"})
	logger.Debugf("start")
	defer close(msgChan) // close channel to stop worker go-routines
	defer logger.Warn("stop listening")
	for {
		if channelBufferLen > 0 && len(msgChan) > 0 {
			// try not fetch when channel is buffering, we do not want to fetch excess msg.
			// do note that len(msgChan) is thread-safe but not necessarily accurate, which is fine in this case.
			continue
		}
		msgList, err := sub.Fetch(1, nats.Context(ctx)) // pull subscription does not work with NextMsg(), only Fetch()
		if errors.Is(err, context.DeadlineExceeded) {
			continue
		}
		if errors.Is(err, context.Canceled) {
			logger.Warn("context cancel")
			// return and trigger the deferred calls, which will close the channel to stop the workers
			return
		}
		if err != nil {
			// error could be nats server shutting down
			logger.WithError(err).Panic("fail to get next msg")
			return
		}
		log.WithField("count", len(msgList)).Trace("fetched msgs from pull subscription")
		for _, msg := range msgList {
			msgChan <- msg
		}
	}
}

// ListenWithConcurrentWorkersWithPush is ListenWithConcurrentWorkers but with push consumer
func (conn *NatsJetConnection) ListenWithConcurrentWorkersWithPush(ctx context.Context, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup, channelBufferLen uint, workerCount uint) error {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.ListenWithConcurrentWorkers"})

	if conn.nc.cloudeventSource == "" {
		return fmt.Errorf("cloudevent source cannot be empty, need to use it to configure consumer")
	}
	if conn.durableName == "" {
		return fmt.Errorf("durable name cannot be empty, need to use it to configure consumer")
	}
	if wg == nil {
		return fmt.Errorf("cannot pass nil WaitGroup to NatsJetConnection.ListenWithConcurrentWorkers")
	}
	if workerCount == 0 || workerCount > 1000 {
		workerCount = defaultEventWorkerCount
	}
	if len(m) == 0 {
		return fmt.Errorf("cannot pass empty or nil map to NatsJetConnection.ListenWithConcurrentWorkers")
	}

	var subject = conn.wildcardSubject
	if subject == "" {
		subject = common.EventTypePrefix + ">"
	}

	// create consumer separately from subscription so that we can use Unsubscribe()
	// and Drain() on subscription without removing consumer.
	consumer, err := conn.jet.AddConsumer(CyVerseStreamName, &nats.ConsumerConfig{
		Durable:        conn.durableName,
		AckPolicy:      nats.AckExplicitPolicy,
		AckWait:        conn.ackWait,
		MaxAckPending:  100,
		FlowControl:    true,
		DeliverSubject: nats.NewInbox(),  // think of this as an intermediate subject that NATS push all matched msg to, and push consumer subscribe from.
		DeliverGroup:   conn.durableName, // queue group name
	})
	if err != nil {
		logger.WithError(err).Error("fail to create consumer")
		return err
	}
	logger.WithFields(log.Fields{"consumerName": consumer.Name, "durable": conn.durableName, "deliverySubject": consumer.Config.DeliverSubject}).Info("consumer created")

	// push consumer
	var msgChan = make(chan *nats.Msg, channelBufferLen)
	sub, err := conn.jet.ChanQueueSubscribe(subject, conn.durableName, msgChan, nats.Durable(conn.durableName))
	if err != nil {
		logger.WithError(err).Error("fail to create subscription")
		return err
	}
	conn.listenSub = sub
	logger.WithFields(log.Fields{"consumerName": consumer.Name, "queue": sub.Queue, "subject": sub.Subject, "subType": sub.Type()}).Info("subscription created")
	conn.listenCtx, conn.listenCtxCancelFunc = context.WithCancel(ctx)

	var workerWG sync.WaitGroup
	conn.spawnEventWorkers(conn.listenCtx, msgChan, m, workerCount, &workerWG)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		err := sub.Unsubscribe()
		if err != nil {
			logger.WithError(err).Error("fail to drain NATS JS subscription")
			return
		}
		logger.Info("NATS JS subscription is closed due to context")
		close(msgChan)  // close channel to stop worker go-routines
		workerWG.Wait() // wait for workers to stop
	}()
	return nil
}

func (conn *NatsJetConnection) spawnEventWorkers(ctx context.Context, msgChan <-chan *nats.Msg, m map[common.EventType]EventHandlerFunc, workerCount uint, wg *sync.WaitGroup) {
	for i := uint(0); i < workerCount; i++ {
		wg.Add(1)
		m = copyEventHandlerMappings(m)
		go conn.eventWorker(ctx, msgChan, m, wg)
	}
}

func (conn *NatsJetConnection) eventWorker(ctx context.Context, msgChan <-chan *nats.Msg, m map[common.EventType]EventHandlerFunc, wg *sync.WaitGroup) {
	defer wg.Done()
	for msg := range msgChan {
		conn.handleMsg(ctx, msg, m)
	}
	// this function will break out the for-loop and return when msgChan is closed
}

func (conn *NatsJetConnection) handleMsg(ctx context.Context, msg *nats.Msg, m map[common.EventType]EventHandlerFunc) {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.handleMsg"})
	logger.WithField("subject", msg.Subject).Trace("received msg")
	handler, ok := m[common.EventType(msg.Subject)]
	if !ok {
		// if we have no handler for the event, just ack and ignore.
		conn.ack(logger, msg)
		return
	}
	//conn.inProgress(logger, msg)
	ce, err := ConvertNats(msg)
	if err != nil {
		logger.WithError(err).Error("fail to convert NATS-jet msg to cloudevent")
		conn.term(logger, msg)
		return
	}
	//conn.ack(logger, msg)
	logger.WithField("ceType", ce.Type()).Debug("converted to cloudevent")
	err = handler(ctx, ce, NatsJetResponseWriter{conn: conn, tid: GetTransactionID(&ce)})
	if err != nil {
		logger.WithError(err).Error("fail to handle a cloudevent")
	}
	conn.ack(logger, msg)
}

func (conn *NatsJetConnection) inProgress(logger *log.Entry, msg *nats.Msg) {
	err := msg.InProgress()
	if err != nil {
		logger.WithError(err).Error("fail to notify msg is in progress")
	}
}
func (conn *NatsJetConnection) ack(logger *log.Entry, msg *nats.Msg) {
	err := msg.AckSync()
	if err != nil {
		logger.WithError(err).Error("fail to ack msg")
	}
	logger.Info("acked")
}
func (conn *NatsJetConnection) term(logger *log.Entry, msg *nats.Msg) {
	err := msg.Term()
	if err != nil {
		logger.WithError(err).Error("fail to term msg")
	}
}
func (conn *NatsJetConnection) nack(logger *log.Entry, msg *nats.Msg) {
	err := msg.Nak()
	if err != nil {
		logger.WithError(err).Error("fail to nack msg")
	}
}

// Publish ...
func (conn *NatsJetConnection) Publish(ce cloudevents.Event) error {
	if ce.Source() == "" || ce.Source() == AutoPopulateCloudEventSource {
		ce.SetSource(conn.nc.cloudeventSource)
	}
	marshaled, err := ce.MarshalJSON()
	if err != nil {
		return err
	}
	ack, err := conn.jet.Publish(ce.Type(), marshaled, nats.MsgId(ce.ID()))
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"seq":    ack.Sequence,
		"stream": ack.Stream,
		"ceType": ce.Type(),
		"tid":    GetTransactionID(&ce),
	}).Debug("published event")
	return nil
}

// TODO compute subscription subject from responseEventTypes, so that we receive fewer events that are not relevant.
// This is useful for NatsJetConnection.Request().
func computeSubscribeSubjectFromEventTypes(responseEventTypes []common.EventType) string {
	if len(responseEventTypes) == 1 {
		return string(responseEventTypes[0])
	}
	var tokenList []map[string]int
	for _, et := range responseEventTypes {
		splits := strings.Split(string(et), ".")
		for len(splits) > len(tokenList) {
			tokenList = append(tokenList, make(map[string]int))
		}
		for i := range splits {
			if _, ok := tokenList[i][splits[i]]; !ok {
				tokenList[i][splits[i]] = 0
			}
			tokenList[i][splits[i]]++
		}
	}
	var result strings.Builder
	for i := range tokenList {
		if len(tokenList[i]) > 1 {
			break
		}
		var firstKey string
		for key := range tokenList[i] {
			firstKey = key
			break
		}
		if tokenList[i][firstKey] == 1 {
			break
		}
		result.WriteString(firstKey)
		result.WriteRune('.')
	}
	if result.Len() == 0 {
		return ">"
	}
	return result.String()
}

// RequestWithSyncSub does the same thing as Request but using a sync subscription.
func (conn *NatsJetConnection) RequestWithSyncSub(ctx context.Context, requestCe cloudevents.Event, responseEventTypes []common.EventType) (cloudevents.Event, error) {

	transactionID := GetTransactionID(&requestCe)
	if transactionID == "" {
		return cloudevents.Event{}, fmt.Errorf("missing transaction ID in STAN request")
	}
	subjectFilter := make(map[string]struct{})
	for _, et := range responseEventTypes {
		subjectFilter[string(et)] = struct{}{}
	}
	sub, err := conn.jet.SubscribeSync(common.EventTypePrefix+">", nats.AckAll())
	if err != nil {
		return cloudevents.Event{}, err
	}
	defer sub.Unsubscribe()

	err = conn.Publish(requestCe)
	if err != nil {
		return cloudevents.Event{}, err
	}

	for {
		msg, err := sub.NextMsgWithContext(ctx)
		if err != nil {
			return cloudevents.Event{}, err
		}
		_ = msg.Ack()
		if _, ok := subjectFilter[msg.Subject]; !ok {
			continue
		}
		ce, err := ConvertNats(msg)
		if err != nil {
			continue
		}
		tid := GetTransactionID(&ce)
		if tid == transactionID {
			return ce, nil
		}
	}
}

// Request sends a request event, and returns a promise that wait for response.
//
// TODO compute subscription subject from responseEventTypes, e.g. use
// computeSubscribeSubjectFromEventTypes(), so that we receive fewer events that
// are not relevant.
func (conn *NatsJetConnection) Request(requestCe cloudevents.Event, responseEventTypes []common.EventType) (EventResponsePromise, error) {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.Request"})

	transactionID := GetTransactionID(&requestCe)
	if transactionID == "" {
		return nil, fmt.Errorf("missing transaction ID in NatsJS request")
	}
	var responseChan = make(chan cloudevents.Event)
	subjectFilter := make(map[string]struct{})
	for _, et := range responseEventTypes {
		subjectFilter[string(et)] = struct{}{}
	}
	// TODO right now we subscribe for all events, we should try to narrow this down.
	sub, err := conn.jet.Subscribe(common.EventTypePrefix+">", func(msg *nats.Msg) {
		_ = msg.Ack()
		if _, ok := subjectFilter[msg.Subject]; !ok {
			logger.WithField("subject", msg.Subject).Trace("skipped")
			return
		}
		ce, err := ConvertNats(msg)
		if err != nil {
			logger.WithError(err).Trace("fail to convert to cloudevent")
			return
		}
		tid := GetTransactionID(&ce)
		if tid == transactionID {
			logger.Trace("transaction matched")
			responseChan <- ce
		} else {
			logger.Trace("transaction does not match")
		}
	}, nats.AckAll(), nats.MaxAckPending(100))
	if err != nil {
		return nil, err
	}

	err = conn.Publish(requestCe)
	if err != nil {
		err2 := sub.Unsubscribe()
		if err2 != nil {
			return nil, fmt.Errorf("%w, %s", err, err2.Error())
		}
		return nil, err
	}

	return conn.newPromise(sub, responseChan), nil
}

// WaitForResponse ...
func (conn *NatsJetConnection) WaitForResponse(transactionID common.TransactionID, responseEventTypes []common.EventType) (EventResponsePromise, error) {
	logger := log.WithFields(log.Fields{"function": "NatsJetConnection.WaitForResponse"})

	if transactionID == "" {
		return nil, fmt.Errorf("missing transaction ID in NatsJS request")
	}
	var responseChan = make(chan cloudevents.Event)
	subjectFilter := make(map[string]struct{})
	for _, et := range responseEventTypes {
		subjectFilter[string(et)] = struct{}{}
	}
	sub, err := conn.jet.Subscribe(common.EventTypePrefix+">", func(msg *nats.Msg) {
		if _, ok := subjectFilter[msg.Subject]; !ok {
			logger.WithField("subject", msg.Subject).Trace("skipped")
			return
		}
		ce, err := ConvertNats(msg)
		if err != nil {
			logger.WithError(err).Trace("fail to convert to cloudevent")
			return
		}
		tid := GetTransactionID(&ce)
		if tid == transactionID {
			logger.Trace("transaction matched")
			responseChan <- ce
		} else {
			logger.Trace("transaction does not match")
		}
	})
	if err != nil {
		return nil, err
	}

	return conn.newPromise(sub, responseChan), nil
}

func (conn *NatsJetConnection) newPromise(sub *nats.Subscription, responseChan <-chan cloudevents.Event) *NatsJetEventResponsePromise {
	return &NatsJetEventResponsePromise{
		responseChan: responseChan,
		sub:          sub,
	}
}

// NatsJetEventResponsePromise ...
type NatsJetEventResponsePromise struct {
	responseChan <-chan cloudevents.Event
	sub          *nats.Subscription
	returnChan   chan cloudevents.Event
}

var _ EventResponsePromise = &NatsJetEventResponsePromise{}

// Response ...
func (rp *NatsJetEventResponsePromise) Response(ctx context.Context) (cloudevents.Event, error) {
	defer rp.sub.Unsubscribe()
	select {
	case <-ctx.Done():
		return cloudevents.Event{}, ctx.Err()
	case ce := <-rp.responseChan:
		return ce, nil
	}
}

// ResponseChan ...
// TODO consider have channel carry error as well (make the channel type a struct with cloudevent and error)
func (rp *NatsJetEventResponsePromise) ResponseChan(ctx context.Context) <-chan cloudevents.Event {
	// create a new channel so that we can unsubscribe upon receiving the response
	if rp.returnChan == nil {
		rp.returnChan = make(chan cloudevents.Event)
	}
	go func() {
		defer rp.sub.Unsubscribe()
		select {
		case ce := <-rp.responseChan:
			rp.returnChan <- ce
		case <-ctx.Done():
		}
	}()
	return rp.returnChan
}

// Close ...
func (rp *NatsJetEventResponsePromise) Close() error {
	return rp.sub.Unsubscribe()
}

// NatsJetResponseWriter implements EventResponseWriter, this is used for EventHandlerFunc to publish events.
type NatsJetResponseWriter struct {
	conn *NatsJetConnection
	tid  common.TransactionID
}

var _ EventResponseWriter = NatsJetResponseWriter{}

// Write publish cloudevent, only set transaction ID when cloudevent does not have one
func (rw NatsJetResponseWriter) Write(ce cloudevents.Event) error {
	if tid := GetTransactionID(&ce); tid == "" {
		SetTransactionID(&ce, rw.tid)
	}
	return rw.conn.Publish(ce)
}

// NoResponse ...
func (rw NatsJetResponseWriter) NoResponse() {}
