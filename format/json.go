// Package format -- this file contains utility functions for dealing with formats
package format

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/santhosh-tekuri/jsonschema"
)

// DecodeJSONStrictly will take data and a target ojbect and return an error if extra json fields are found in the target
// This function can be used for simple json format validation and decoding
func DecodeJSONStrictly(data []byte, target interface{}) error {
	if len(data) == 0 {
		return fmt.Errorf("data is empty")
	}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	return decoder.Decode(target)
}

// ValidateJSONSchema validates json data using a schema and returns an error if the data is not valid
func ValidateJSONSchema(data []byte, schema []byte) error {
	return ValidateJSONSchemaWithResourceURL(data, "in_mem_schema.json", schema)
}

// ValidateJSONSchemaWithResourceURL validates json data using a schema and returns an error if the data is not valid
func ValidateJSONSchemaWithResourceURL(data []byte, schemaURL string, schema []byte) error {
	jc := jsonschema.NewCompiler()
	err := jc.AddResource(schemaURL, strings.NewReader(string(schema)))
	if err != nil {
		return err
	}

	sch, err := jc.Compile(schemaURL)
	if err != nil {
		return err
	}

	err = sch.Validate(strings.NewReader(string(data)))
	if err != nil {
		return err
	}

	return nil
}
