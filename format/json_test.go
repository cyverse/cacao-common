package format

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type testobject struct {
	ID    string `json:"id"`
	Owner string `json:"owner,omitempty"`
	Field string `json:"field,omitempty"`
}

const (
	testobjectSchema string = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
		"type": "object",
		"properties": {
			"id": {
				"type": "string",
				"minLength": 1,
				"maxLength": 50,
				"pattern": "^[A-Za-z0-9].*"
			},
			"owner": {
				"type": "string"
			},
			"field": {
				"type": "string"
			}
		},
		"required": ["id"],
		"additionalProperties": false
	}`

	testobjectcomplexSchema = `
	{
		"$schema": "http://json-schema.org/draft-07/schema#",
	 	"definitions": {
			"testobject": ` + testobjectSchema + `
	 	},
	 	"type": "object",
	 	"properties": {
			"id": {
				"type": "string",
				"minLength": 1,
				"maxLength": 50,
				"pattern": "^[A-Za-z0-9].*"
			},
			"owner": {
				"type": "string"
			},
			"testfloat": {
				"type": "number"
			},
			"testobj": {
				"$ref": "#/definitions/testobject"
			},
			"testmap": {
				"type": "object",
				"additionalProperties": true		
			},
			"testarr": {
				"type": "array",
				"items": {
					"type": "string"
				}
			},
			"testarr2": {
				"type": "array",
				"items": {
					"$ref": "#/definitions/testobject"
				}
			}
		},
		"required": ["id"],
		"additionalProperties": true
	}`
)

func TestDecodeJSONStrictlySuccess(t *testing.T) {
	testJSONString := `{
		"id": "test134"
	}`
	var obj testobject
	err := DecodeJSONStrictly([]byte(testJSONString), &obj)
	assert.NoError(t, err)
}

func TestDecodeJSONStrictlyUnknownFieldError(t *testing.T) {
	testJSONString := `{
		"id": "test134",
		"unknown_field": 1234
	}`
	var obj testobject
	err := DecodeJSONStrictly([]byte(testJSONString), &obj)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "unknown field")
}

func TestValidateJSONSchemaMandateFieldError(t *testing.T) {
	testJSONString := `{}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectSchema))
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "missing properties")
}

func TestValidateJSONSchemaSuccess(t *testing.T) {
	testJSONString := `{
		"id": "test134"
	}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectSchema))
	assert.NoError(t, err)
}

func TestValidateJSONSchemaUnknownFieldError(t *testing.T) {
	testJSONString := `{
		"id": "test134",
		"unknown_field": 1234
	}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectSchema))
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "not allowed")
}

func TestValidateJSONSchemaComplexMandateFieldError(t *testing.T) {
	testJSONString := `{}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectcomplexSchema))
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "missing properties")
}

func TestValidateJSONSchemaComplexSuccess(t *testing.T) {
	testJSONString := `{
		"id": "test134",
		"testfloat": 322.223,
		"testobj": {
			"id": "test134"
		},
		"testmap": {
			"abc": "def",
			"ccc": 1233
		},
		"testarr": ["abc", "def"],
		"testarr2": [
			{
				"id": "test134"
			}
		]
	}`
	err := ValidateJSONSchema([]byte(testJSONString), []byte(testobjectcomplexSchema))
	assert.NoError(t, err)
}
