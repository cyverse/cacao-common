package http

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// This checks that the json struct tag is aligned between Credential and CredentialUpdate struct.
func TestCredentialHTTPStructTag(t *testing.T) {
	var httpCred = Credential{
		Username:          "username123",
		Value:             "value123",
		Type:              "type123",
		ID:                "id123",
		Name:              "name123",
		Description:       "description123",
		IsSystem:          true,
		IsHidden:          true,
		Visibility:        "Default",
		Tags:              []string{"tag1", "tag2"},
		CreatedAt:         time.Now(),
		UpdatedAt:         time.Now(),
		UpdatedBy:         "testuser123",
		UpdatedEmulatorBy: "testuser456",
	}
	marshal, err := json.Marshal(httpCred)
	if err != nil {
		panic(err)
	}
	var update CredentialUpdate
	err = json.Unmarshal(marshal, &update)
	if err != nil {
		panic(err)
	}
	assert.Equal(t, CredentialUpdate{
		Name:            func() *string { return &httpCred.Name }(),
		Value:           func() *string { return &httpCred.Value }(),
		Description:     func() *string { return &httpCred.Description }(),
		Visibility:      func() *string { return &httpCred.Visibility }(),
		UpdateOrAddTags: nil,
		DeleteTags:      nil,
	}, update)

}
