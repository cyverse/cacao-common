package http

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func Test_DeploymentUpdateStruct(t *testing.T) {
	deploymentStruct := reflect.TypeOf(Deployment{})
	deploymentUpdateStruct := reflect.TypeOf(DeploymentUpdate{})
	assert.Equal(t, reflect.Struct, deploymentStruct.Kind())
	assert.Equal(t, reflect.Struct, deploymentUpdateStruct.Kind())
	// update struct should not contain more fields than the deployment struct
	assert.LessOrEqual(t, deploymentUpdateStruct.NumField(), deploymentStruct.NumField())
	deploymentStructFields := map[string]reflect.StructField{}
	for i := 0; i < deploymentStruct.NumField(); i++ {
		field := deploymentStruct.Field(i)
		deploymentStructFields[field.Name] = field
	}
	for i := 0; i < deploymentUpdateStruct.NumField(); i++ {
		updateField := deploymentUpdateStruct.Field(i)
		// check if field of update struct is null-able, either ptr type or slice
		if !assert.True(t, reflect.Ptr == updateField.Type.Kind() || reflect.Slice == updateField.Type.Kind()) {
			t.Errorf("field %s(%s) is not nullable", updateField.Name, updateField.Type.Kind().String())
		}
		field, ok := deploymentStructFields[updateField.Name]
		if assert.True(t, ok) {
			if field.Type.Kind() == reflect.Slice {
				assert.Equal(t, reflect.Slice, updateField.Type.Kind())
			} else {
				// for non-slice field, the type in update struct should be the pointer type of the field in deployment struct
				assert.Equal(t, "*"+field.Type.String(), updateField.Type.String())
			}
			assert.Equal(t, field.Tag, updateField.Tag)
		}
	}
}
