package http

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// UserAction represents a user action returned by the HTTP API.
type UserAction struct {
	ID          common.ID              `json:"id"`
	Owner       string                 `json:"owner"`
	Name        string                 `json:"name"`
	Description string                 `json:"description,omitempty"`
	Public      bool                   `json:"public"`
	Type        service.UserActionType `json:"type"`
	Action      service.UserActionItem `json:"action"`
	CreatedAt   time.Time              `json:"created_at"`
	UpdatedAt   time.Time              `json:"updated_at"`
}
