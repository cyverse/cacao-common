package http

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// TemplateType represents a template type returned by the HTTP API.
type TemplateType struct {
	Name          service.TemplateTypeName       `json:"name"`
	Formats       []service.TemplateFormat       `json:"formats,omitempty"`        // e.g., {'yaml', 'json'}
	Engine        service.TemplateEngine         `json:"engine"`                   // e.g., 'terraform'
	ProviderTypes []service.TemplateProviderType `json:"provider_types,omitempty"` // e.g., {'openstack', 'aws', `kubernetes`}
}

// TemplateCustomFieldTypeQueryResult represents a template custom field type query result returned by the HTTP API.
type TemplateCustomFieldTypeQueryResult struct {
	Name     string      `json:"name"`
	DataType string      `json:"data_type"`
	Value    interface{} `json:"value"`
}

// TemplateCustomFieldType represents a template custom field type returned by the HTTP API.
type TemplateCustomFieldType struct {
	Name                      string                                     `json:"name"`
	Description               string                                     `json:"description,omitempty"`
	QueryMethod               service.TemplateCustomFieldTypeQueryMethod `json:"query_method"`
	QueryTarget               string                                     `json:"query_target"`
	QueryData                 string                                     `json:"query_data,omitempty"`
	QueryResultJSONPathFilter string                                     `json:"query_result_jsonpath_filter,omitempty"`

	QueryParams map[string]string `json:"query_params,omitempty"`
}

// Template represents a template returned by the HTTP API.
type Template struct {
	ID              common.ID                  `json:"id"`
	Owner           string                     `json:"owner"`
	Name            string                     `json:"name"`
	Description     string                     `json:"description,omitempty"`
	Public          bool                       `json:"public"`
	Deleted         bool                       `json:"deleted"`
	Source          service.TemplateSource     `json:"source"`
	LatestVersionID common.ID                  `json:"latest_version_id"`
	Metadata        service.TemplateMetadata   `json:"metadata"`
	UIMetadata      service.TemplateUIMetadata `json:"ui_metadata"`
	CreatedAt       time.Time                  `json:"created_at"`
	UpdatedAt       time.Time                  `json:"updated_at"`

	CredentialID                 string `json:"credential_id,omitempty"`                   // for import and sync
	IncludeCacaoReservedPurposes bool   `json:"include_cacao_reserved_purposes,omitempty"` // for list
	Sync                         bool   `json:"sync,omitempty"`                            // indicate sync
}

// TemplateVersion represents a template version returned by the HTTP API.
type TemplateVersion struct {
	ID         common.ID                  `json:"id"`
	TemplateID common.ID                  `json:"template_id"`
	Source     service.TemplateSource     `json:"source"`
	Disabled   bool                       `json:"disabled"`
	DisabledAt time.Time                  `json:"disabled_at"`
	Metadata   service.TemplateMetadata   `json:"metadata"`
	UIMetadata service.TemplateUIMetadata `json:"ui_metadata"`
	CreatedAt  time.Time                  `json:"created_at"`
}
