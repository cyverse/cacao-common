package http

import "time"

// Credential is a struct that defines the schema of credential used for http API (REST).
// Be aware that changing this struct will result in changes in REST api.
type Credential struct {
	Username    string `json:"username,omitempty"`
	Value       string `json:"value,omitempty"`
	Type        string `json:"type,omitempty"`
	ID          string `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	IsSystem    bool   `json:"is_system,omitempty"`
	IsHidden    bool   `json:"is_hidden,omitempty"`
	Disabled    bool   `json:"disabled,omitempty"`
	Visibility  string `json:"visibility,omitempty"`
	// Deprecated we are moving to key-value tags, array tags are deprecated
	Tags              []string          `json:"tags,omitempty"`    // TODO remove []string tags
	TagsKV            map[string]string `json:"tags_kv,omitempty"` // TODO consider rename json struct tag name after remove []string tags
	CreatedAt         time.Time         `json:"created_at"`
	UpdatedAt         time.Time         `json:"updated_at"`
	UpdatedBy         string            `json:"updated_by"`
	UpdatedEmulatorBy string            `json:"updated_emulator_by"`
}

// CredentialUpdate is a struct that defines the schema of the update/patch endpoint of REST api.
// Only non-nil field will be updated.
// Be aware that changing this struct will result in changes in REST api.
type CredentialUpdate struct {
	Name        *string `json:"name,omitempty"`
	Value       *string `json:"value,omitempty"`
	Description *string `json:"description,omitempty"`
	// TODO expose disable to REST later on, keep it to NATS api for now
	//Disabled    *bool   `json:"disabled,omitempty"`
	Visibility *string `json:"visibility,omitempty"`
	// update or add (if tag name does not exist) tags
	UpdateOrAddTags map[string]string   `json:"update_tags,omitempty"`
	DeleteTags      map[string]struct{} `json:"delete_tags,omitempty"`
}

// NeedToUpdate checks if there is a need to update credential.
func (update CredentialUpdate) NeedToUpdate() bool {
	if update.Name != nil {
		return true
	}
	if update.Value != nil {
		return true
	}
	if update.Description != nil {
		return true
	}
	//if update.Disabled != nil {
	//	return true
	//}
	if update.Visibility != nil {
		return true
	}
	if len(update.UpdateOrAddTags) > 0 {
		return true
	}
	if len(update.DeleteTags) > 0 {
		return true
	}
	return false
}
