// Package messaging contains utility functions for dealing with a massaging service
package messaging

import (
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
)

// MockNatsMessage mocks Nats Msg
type MockNatsMessage struct {
	Data         []byte
	ResponseData []byte
}

// Respond responds
func (msg *MockNatsMessage) Respond(data []byte) error {
	msg.ResponseData = data
	return nil
}

// MockNatsConnection mocks Nats connection
// implements QueryEventService
type MockNatsConnection struct {
	Config                   *NatsConfig
	CommonSubjectRegex       *regexp.Regexp
	HandlerLock              sync.Mutex
	EventHandlers            map[common.QueryOp]QueryEventHandler
	DefaultEventHandler      QueryEventHandler
	CloudEventHandlers       map[common.QueryOp]QueryCloudEventHandler
	DefaultCloudEventHandler QueryCloudEventHandler
}

// CreateMockNatsConnection creates MockNatsConnection
func CreateMockNatsConnection(config *NatsConfig, eventHandlerMappings []QueryEventHandlerMapping) (*MockNatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "CreateMockNatsConnection",
	})

	if len(config.WildcardSubject) == 0 {
		err := fmt.Errorf("failed to subscribe an empty subject")
		logger.Error(err)
		return nil, err
	}

	escapedSubject := strings.ReplaceAll(config.WildcardSubject, ".", "\\.")
	wildcard1Regex := strings.ReplaceAll(escapedSubject, "*", "(\\w+)")
	wildcard2Regex := strings.ReplaceAll(wildcard1Regex, ">", "(.+)")

	// test compile
	reg, err := regexp.Compile(wildcard2Regex)
	if err != nil {
		logger.WithError(err).Errorf("failed to compile a subject %s", config.WildcardSubject)
		return nil, err
	}

	mockNatsConn := &MockNatsConnection{
		Config:                   config,
		CommonSubjectRegex:       reg,
		HandlerLock:              sync.Mutex{},
		EventHandlers:            map[common.QueryOp]QueryEventHandler{},
		DefaultEventHandler:      nil,
		CloudEventHandlers:       map[common.QueryOp]QueryCloudEventHandler{},
		DefaultCloudEventHandler: nil,
	}

	// lock handlers
	mockNatsConn.HandlerLock.Lock()

	// Add handlers before subscribe
	for _, eventHandlerMapping := range eventHandlerMappings {
		if len(eventHandlerMapping.Subject) > 0 {
			if eventHandlerMapping.EventHandler != nil {
				mockNatsConn.EventHandlers[common.QueryOp(eventHandlerMapping.Subject)] = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				mockNatsConn.CloudEventHandlers[common.QueryOp(eventHandlerMapping.Subject)] = eventHandlerMapping.CloudEventHandler
			}
		} else {
			// default
			if eventHandlerMapping.EventHandler != nil {
				mockNatsConn.DefaultEventHandler = eventHandlerMapping.EventHandler
			} else if eventHandlerMapping.CloudEventHandler != nil {
				mockNatsConn.DefaultCloudEventHandler = eventHandlerMapping.CloudEventHandler
			}
		}
	}

	mockNatsConn.HandlerLock.Unlock()

	return mockNatsConn, nil
}

// Disconnect disconnects Nats connection
func (conn *MockNatsConnection) Disconnect() error {
	conn.CommonSubjectRegex = nil

	conn.HandlerLock.Lock()
	conn.EventHandlers = map[common.QueryOp]QueryEventHandler{}
	conn.DefaultEventHandler = nil
	conn.CloudEventHandlers = map[common.QueryOp]QueryCloudEventHandler{}
	conn.DefaultCloudEventHandler = nil
	conn.HandlerLock.Unlock()
	return nil
}

// AddEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives subject and JSON data of an event, and returns data object (not required to JSONfy)
func (conn *MockNatsConnection) AddEventHandler(subject common.QueryOp, eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.AddEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.EventHandlers[common.QueryOp(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddDefaultEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives subject and JSON data of an event, and returns data object (not required to JSONfy)
func (conn *MockNatsConnection) AddDefaultEventHandler(eventHandler QueryEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.AddDefaultEventHandler",
	})

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// AddCloudEventHandler adds a new event handler function to a specified subject. The subject must match the wildcard subject specified in NatsConfig.WildcardSubject
// eventHandler receives a cloudevent of an event, and returns a cloudevent
func (conn *MockNatsConnection) AddCloudEventHandler(subject common.QueryOp, eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.AddCloudEventHandler",
	})

	if len(subject) == 0 {
		err := fmt.Errorf("failed to add an event handler for an empty subject")
		return err
	}

	logger.Tracef("adding an event handler for a subject %s", subject)

	conn.HandlerLock.Lock()
	conn.CloudEventHandlers[common.QueryOp(subject)] = eventHandler
	conn.HandlerLock.Unlock()

	logger.Tracef("added an event handler for a subject %s", subject)
	return nil
}

// AddDefaultCloudEventHandler adds a default event handler function. The handler is called when there is no matching handlers for the subject
// eventHandler receives a cloudevent of an event, and returns a cloudevent
func (conn *MockNatsConnection) AddDefaultCloudEventHandler(eventHandler QueryCloudEventHandler) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.AddDefaultCloudEventHandler",
	})

	logger.Trace("adding a default event handler")

	conn.HandlerLock.Lock()
	conn.DefaultCloudEventHandler = eventHandler
	conn.HandlerLock.Unlock()

	logger.Trace("added a default event handler")
	return nil
}

// Request publishes Nats event
// automatically wraps the data with cloud objects, and pills when it returns
func (conn *MockNatsConnection) Request(ctx context.Context, subject common.QueryOp, data interface{}) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.Request",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEvent(data, string(subject), conn.Config.ClientID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	responseMsg, err := conn.request(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	responseCE := cloudevents.NewEvent()
	err = json.Unmarshal(responseMsg, &responseCE)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event from response message")
		return nil, err
	}

	return responseCE.Data(), nil
}

// RequestWithTransactionID publishes Nats event
// transactionID will be passed along to the CreateCloudEvent if given
// automatically wraps the data with cloud objects, and pills when it returns
func (conn *MockNatsConnection) RequestWithTransactionID(ctx context.Context, subject common.QueryOp, data interface{}, transactionID common.TransactionID) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.RequestWithTransactionID",
	})

	logger.Tracef("requesting a subject %s", subject)

	ce, err := CreateCloudEventWithTransactionID(data, string(subject), conn.Config.ClientID, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event for subject %s", subject)
		return nil, err
	}

	responseMsg, err := conn.request(&ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	responseCE := cloudevents.NewEvent()
	err = json.Unmarshal(responseMsg, &responseCE)
	if err != nil {
		logger.WithError(err).Errorf("failed to create a cloud event from response message")
		return nil, err
	}

	return responseCE.Data(), nil
}

// RequestCloudEvent publishes Nats event, request and response via cloud event objects
func (conn *MockNatsConnection) RequestCloudEvent(ctx context.Context, ce *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.RequestCloudEvent",
	})

	// we put a subject into Event.Type instead of Event.Subject
	// please refer 'CreateCloudEvent' function in cloudevent.go
	logger.Tracef("publishing a subject %s", ce.Type())

	responseMsg, err := conn.request(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to request a subject %s", ce.Type())
		return nil, err
	}

	logger.Tracef("requested a subject %s", ce.Type())

	return responseMsg, nil
}

func (conn *MockNatsConnection) request(event *cloudevents.Event) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.messaging",
		"function": "MockNatsConnection.request",
	})

	if conn.CommonSubjectRegex.Match([]byte(event.Type())) {
		// we put a subject into Event.Type instead of Event.Subject
		// please refer 'CreateCloudEvent' function in cloudevent.go
		logger.Tracef("handling an event %s", event.Type())
		conn.HandlerLock.Lock()
		defer conn.HandlerLock.Unlock()

		// call handler
		if handler, ok := conn.EventHandlers[common.QueryOp(event.Type())]; ok {
			// has the handler for the event
			transactionID := GetTransactionID(event)
			response, err := handler(common.QueryOp(event.Type()), transactionID, event.Data())
			if err != nil {
				logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
				return nil, err
			}

			// convert to CE
			ce, err := CreateCloudEventWithTransactionID(response, event.Type(), conn.Config.ClientID, transactionID)
			if err != nil {
				logger.WithError(err).Errorf("failed to convert response to cloud event")
				return nil, err
			}

			eventJSON, err := ce.MarshalJSON()
			if err != nil {
				logger.WithError(err).Errorf("failed to marshal cloud event to JSON")
				return nil, err
			}

			return eventJSON, nil
		} else if handler, ok := conn.CloudEventHandlers[common.QueryOp(event.Type())]; ok {
			// has the handler for the event
			response, err := handler(event)
			if err != nil {
				logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
				return nil, err
			}
			return response, nil
		} else if conn.DefaultEventHandler != nil {
			// has the handler for the event
			transactionID := GetTransactionID(event)
			response, err := conn.DefaultEventHandler(common.QueryOp(event.Type()), transactionID, event.Data())
			if err != nil {
				logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
				return nil, err
			}

			// convert to CE
			ce, err := CreateCloudEventWithTransactionID(response, event.Type(), conn.Config.ClientID, transactionID)
			if err != nil {
				logger.WithError(err).Errorf("failed to convert response to cloud event")
				return nil, err
			}

			eventJSON, err := ce.MarshalJSON()
			if err != nil {
				logger.WithError(err).Errorf("failed to marshal cloud event to JSON")
				return nil, err
			}

			return eventJSON, nil
		} else if conn.DefaultCloudEventHandler != nil {
			// has the handler for the event
			response, err := conn.DefaultCloudEventHandler(event)
			if err != nil {
				logger.WithError(err).Errorf("failed to handle an event %s", event.Type())
				return nil, err
			}
			return response, nil
		} else {
			err := fmt.Errorf("failed to find an event handler for a type %s", event.Type())
			logger.Error(err)
			return nil, err
		}
	}
	return nil, nil
}
