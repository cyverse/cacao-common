package git

import (
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	// Fill ssl private key here to test ssh auth
	sslPrivateKey = []byte(``)
)

func TestCloneGitHubRepositoryToDisk(t *testing.T) {
	repoConfig := NewGitRepositoryConfigWithNoAuth("https://github.com/CyVerse-Ansible/ansible-k3s", BranchReferenceType, "master")

	options := &DiskStorageOption{
		ClonePath: "/tmp/test_git",
	}

	clone, err := NewGitCloneToDisk(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitHubRepositoryToRAM(t *testing.T) {
	repoConfig := NewGitRepositoryConfigWithNoAuth("https://github.com/CyVerse-Ansible/ansible-k3s", BranchReferenceType, "master")

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitLabRepositoryDisk(t *testing.T) {
	repoConfig := NewGitRepositoryConfigWithNoAuth("https://gitlab.com/cyverse/cacao-tf-os-ops", BranchReferenceType, "main")

	options := &DiskStorageOption{
		ClonePath: "/tmp/test_git",
	}

	clone, err := NewGitCloneToDisk(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitLabRepositoryRAM(t *testing.T) {
	repoConfig := NewGitRepositoryConfigWithNoAuth("https://gitlab.com/cyverse/cacao-tf-os-ops", BranchReferenceType, "main")

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitLabRepositoryRAM2(t *testing.T) {
	// omit some url parts
	repoConfig := NewGitRepositoryConfigWithNoAuth("gitlab.com/cyverse/cacao-tf-os-ops", BranchReferenceType, "main")

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

/*
func TestCloneGitLabRepositoryRAMWithSSHAuth(t *testing.T) {
	privateKey := sslPrivateKey

	repoConfig := NewGitRepositoryConfigWithSSHAuth("gitlab.com:cyverse/cacao-tf-os-ops.git", "git", privateKey, "", BranchReferenceType, "main")

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("README.md")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}

func TestCloneGitLabRepositoryRAMWithSSHAuthFail(t *testing.T) {
	privateKey := sslPrivateKey

	// wrong url
	repoConfig := NewGitRepositoryConfigWithSSHAuth("gitlab.com/cyverse/cacao-tf-os-ops.git", "git", privateKey, "", BranchReferenceType, "main")

	options := &RAMStorageOption{
		SizeLimit: 0,
	}

	_, err := NewGitCloneToRAM(repoConfig, options)
	assert.Error(t, err)
}
*/

func TestCloneGitLabRepositoryToRAMWithSizeLimitFail(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://gitlab.com/cyverse/cacao-tf-os-ops",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "main",
	}

	// set 100 bytes size limit, this must fail
	options := &RAMStorageOption{
		SizeLimit: 100, // 100Bytes
	}

	_, err := NewGitCloneToRAM(repoConfig, options)
	// this must fail because it exceeds size limit
	assert.Error(t, err)
}

func TestCloneGitLabRepositoryToRAMWithSizeLimitPass(t *testing.T) {
	repoConfig := &RepositoryConfig{
		URL:           "https://gitlab.com/cyverse/cacao-tf-os-ops",
		Username:      "",
		Password:      "",
		ReferenceType: BranchReferenceType,
		ReferenceName: "main",
	}

	// set 1MB size limit, this must succeed
	options := &RAMStorageOption{
		SizeLimit: 1024 * 1024, // 1MB
	}

	clone, err := NewGitCloneToRAM(repoConfig, options)
	// this must pass
	assert.NoError(t, err)

	defer clone.Release()

	// test reading a file
	fs := clone.GetFileSystem()
	f, err := fs.Open("single-image/.cacao/metadata.json")
	assert.NoError(t, err)

	data, err := io.ReadAll(f)
	assert.NoError(t, err)
	assert.NotEmpty(t, data)
}
