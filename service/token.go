package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"strings"
	"time"
)

// TokenType is the type of token
type TokenType string

const (
	// PersonalTokenType is a token for personal use
	PersonalTokenType TokenType = "personal"
	// DelegatedTokenType is a token for delegated account / api access
	DelegatedTokenType TokenType = "delegated"
	// InternalDeployType is a token for internal use by cacao
	InternalDeployType TokenType = "internal-deploy"
)

// String returns the string representation of a TokenType
func (t TokenType) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TokenType
func (t TokenType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts JSON/string into the appropriate TokenType enum
func (t *TokenType) UnmarshalJSON(b []byte) error {
	// Token types are represented as strings
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// validate and convert to TokenType
	switch strings.ToLower(s) {
	case string(PersonalTokenType):
		*t = PersonalTokenType
	case string(DelegatedTokenType):
		*t = DelegatedTokenType
	case string(InternalDeployType):
		*t = InternalDeployType
	case "":
	default:
		return fmt.Errorf("token type is invalid: %s", s)
	}
	return nil
}

// TokenClient is the client for Token
type TokenClient interface {
	// AuthCheck checks for authN and authZ of a token on an operation, and resolve to username of owner if successful.
	// TODO pass operation identifier to this to check authZ.
	AuthCheck(ctx context.Context, actor Actor, token common.TokenID) (*TokenModel, error)
	Get(ctx context.Context, actor Actor, tokenID common.ID) (*TokenModel, error)
	List(ctx context.Context, actor Actor) ([]TokenModel, error)
	ListTypes(ctx context.Context, actor Actor) ([]TokenType, error)
	Create(ctx context.Context, actor Actor, token TokenCreateModel) (common.TokenID, error)
	Update(ctx context.Context, actor Actor, token TokenModel) error
	UpdateFields(ctx context.Context, actor Actor, token TokenModel, updateFieldNames []string) error
	Delete(ctx context.Context, actor Actor, tokenID common.ID) error
	Revoke(ctx context.Context, actor Actor, tokenID common.ID) error
}

// TokenModel is the token model
type TokenModel struct {
	Session

	ID               common.ID         `json:"id"`
	Owner            string            `json:"owner"`
	Name             string            `json:"name"`
	Description      string            `json:"description,omitempty"`
	Public           bool              `json:"public"`
	Expiration       time.Time         `json:"expiration"`
	Start            *time.Time        `json:"start,omitempty"`
	Scopes           string            `json:"scopes"`
	DelegatedContext string            `json:"delegated_context,omitempty"`
	Type             TokenType         `json:"type"`
	Tags             map[string]string `json:"tags"`
	LastAccessed     *time.Time        `json:"last_accessed"`
	CreatedAt        time.Time         `json:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at"`

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
}

// TokenAuthCheckRequest is the request to check authN and authZ of a token on an operation.
type TokenAuthCheckRequest struct {
	Session
	Token common.TokenID `json:"token"`
	// TODO add operation as part of the request to check for authorization once we support scope and delegation.
	//OperationIdentifier string // this is a stub
}

// TokenCreateModel is the Token model for exposing extra Token secret only on create
type TokenCreateModel struct {
	Session

	ID               common.ID         `json:"id"`
	Owner            string            `json:"owner"`
	Name             string            `json:"name"`
	Description      string            `json:"description,omitempty"`
	Public           bool              `json:"public"`
	Expiration       time.Time         `json:"expiration"`
	Start            *time.Time        `json:"start,omitempty"`
	Scopes           string            `json:"scopes"`
	DelegatedContext string            `json:"delegated_context,omitempty"`
	Type             TokenType         `json:"type"`
	Tags             map[string]string `json:"tags"`
	CreatedAt        time.Time         `json:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at"`
	Token            string            `json:"token"`
}

// TokenCreateBulkModel is the Token model for creating bulk tokens from an email/DelegatedContexts list. TODO implement
type TokenCreateBulkModel struct {
	Owner             string            `json:"owner"`
	Name              string            `json:"name"`
	Description       string            `json:"description,omitempty"`
	DelegatedContexts string            `json:"DelegatedContexts"`
	Public            bool              `json:"public"`
	Expiration        time.Time         `json:"expiration"`
	Start             *time.Time        `json:"start,omitempty"`
	Scopes            string            `json:"scopes"`
	DelegatedContext  string            `json:"delegated_context,omitempty"`
	Tags              map[string]string `json:"tags"`
	Tokens            []TokenModel      `json:"tokens"`
	//Type            TokenType         `json:"type"` // always delegated.
}

// Verify verifies that the token is correctly formed and valid
func (t *TokenModel) Verify() (bool, error) {
	if len(t.ID) <= 0 {
		return false, errors.New("ID not set, could not verify")
	}
	if len(t.Owner) <= 0 {
		return false, errors.New("owner not set, could not verify")
	}
	if len(t.Name) <= 0 {
		return false, errors.New("name not set, could not verify")
	}
	if len(t.Type) <= 0 {
		return false, errors.New("type not set, could not verify")
	}
	if t.Expiration.IsZero() {
		return false, errors.New("expiration not set, could not verify")
	}
	if !t.Start.IsZero() && t.Start != nil && t.Start.After(t.Expiration) {
		// Start needs to come before the Expiration time.
		return false, errors.New("start is after expiration, could not verify")
	}
	return true, nil
}

// TokenListModel is the representation of a list of Tokens
type TokenListModel struct {
	Session
	Tokens []TokenModel `json:"tokens"`
}

// TokenTypeListModel is the representation of the list of token types
type TokenTypeListModel struct {
	Session
	TokenTypes []TokenType `json:"token_types"`
}
