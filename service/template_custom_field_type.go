package service

import (
	"encoding/json"
	"fmt"
	"strings"
)

// TemplateCustomFieldTypeQueryMethod is the type of a template custom field type query method
type TemplateCustomFieldTypeQueryMethod string

const (
	// TemplateCustomFieldTypeQueryMethodUnknown is for an unknown query method
	TemplateCustomFieldTypeQueryMethodUnknown TemplateCustomFieldTypeQueryMethod = ""
	// TemplateCustomFieldTypeQueryMethodREST is for an api endpoint query method
	TemplateCustomFieldTypeQueryMethodREST TemplateCustomFieldTypeQueryMethod = "rest"
	// TemplateCustomFieldTypeQueryMethodNATS is for a NATS message channel query method
	TemplateCustomFieldTypeQueryMethodNATS TemplateCustomFieldTypeQueryMethod = "nats"
)

// String returns the string representation of a TemplateCustomFieldTypeQueryTargetType.
func (t TemplateCustomFieldTypeQueryMethod) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateCustomFieldTypeQueryTargetType.
func (t TemplateCustomFieldTypeQueryMethod) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateCustomFieldTypeQueryTargetType to the appropriate enumeration constant.
func (t *TemplateCustomFieldTypeQueryMethod) UnmarshalJSON(b []byte) error {
	// TemplateCustomTypeSourceTypes are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateCustomFieldTypeQueryMethodUnknown
	case string(TemplateCustomFieldTypeQueryMethodREST):
		*t = TemplateCustomFieldTypeQueryMethodREST
	case string(TemplateCustomFieldTypeQueryMethodNATS):
		*t = TemplateCustomFieldTypeQueryMethodNATS
	default:
		return fmt.Errorf("invalid template custom field type query method: %s", s)
	}

	return nil
}

// TemplateCustomFieldTypeQueryResult is the standard interface for all Template Custom Field Type Query Result implementations
type TemplateCustomFieldTypeQueryResult interface {
	SessionContext

	GetName() string
	GetDataType() string
	GetValue() interface{}
}

// TemplateCustomFieldTypeQueryResultModel is the Template Custom Field Type query result model
type TemplateCustomFieldTypeQueryResultModel struct {
	Session

	Name     string      `json:"name"`
	DataType string      `json:"data_type"`
	Value    interface{} `json:"value"`
}

// GetName returns the name of the template custom field type
func (t *TemplateCustomFieldTypeQueryResultModel) GetName() string {
	return t.Name
}

// GetDataType returns the data type of the template custom field type
func (t *TemplateCustomFieldTypeQueryResultModel) GetDataType() string {
	return t.DataType
}

// GetValue returns the values of the template custom field type
func (t *TemplateCustomFieldTypeQueryResultModel) GetValue() interface{} {
	return t.Value
}

// TemplateCustomFieldType is the standard interface for all Template Custom Field Type implementations
type TemplateCustomFieldType interface {
	SessionContext

	GetPrimaryID() string // points to Name
	GetName() string
	GetDescription() string
	GetQueryMethod() TemplateCustomFieldTypeQueryMethod
	GetQueryTarget() string
	GetQueryData() string
	GetQueryResultJSONPathFilter() string
	GetUpdateFieldNames() []string

	SetPrimaryID(name string)
	SetName(name string)
	SetDescription(description string)
	SetQueryMethod(method TemplateCustomFieldTypeQueryMethod)
	SetQueryTarget(target string)
	SetQueryData(data string)
	SetQueryResultJSONPathFilter(jsonpath string)
	SetUpdateFieldNames(fieldNames []string)
}

// TemplateCustomFieldTypeModel is the Template Custom Field Type model
type TemplateCustomFieldTypeModel struct {
	Session

	Name                      string                             `json:"name"`
	Description               string                             `json:"description,omitempty"`
	QueryMethod               TemplateCustomFieldTypeQueryMethod `json:"query_method"`
	QueryTarget               string                             `json:"query_target"`
	QueryData                 string                             `json:"query_data,omitempty"`
	QueryResultJSONPathFilter string                             `json:"query_result_jsonpath_filter,omitempty"`

	UpdateFieldNames []string          `json:"update_field_names,omitempty"`
	QueryParams      map[string]string `json:"query_params,omitempty"`
}

// GetPrimaryID returns the primary ID of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) GetPrimaryID() string {
	return t.Name
}

// GetName returns the name of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) GetName() string {
	return t.Name
}

// GetDescription returns the description of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) GetDescription() string {
	return t.Description
}

// GetQueryMethod returns the query method of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) GetQueryMethod() TemplateCustomFieldTypeQueryMethod {
	return t.QueryMethod
}

// GetQueryTarget returns the query target of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) GetQueryTarget() string {
	return t.QueryTarget
}

// GetQueryData returns the query data of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) GetQueryData() string {
	return t.QueryData
}

// GetQueryResultJSONPathFilter returns the JSONPath filter that filters query result data
func (t *TemplateCustomFieldTypeModel) GetQueryResultJSONPathFilter() string {
	return t.QueryResultJSONPathFilter
}

// GetUpdateFieldNames returns the field names to be updated
func (t *TemplateCustomFieldTypeModel) GetUpdateFieldNames() []string {
	return t.UpdateFieldNames
}

// GetQueryParams returns the query params for query API
func (t *TemplateCustomFieldTypeModel) GetQueryParams() map[string]string {
	return t.QueryParams
}

// SetPrimaryID sets the primary ID of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) SetPrimaryID(name string) {
	t.Name = name
}

// SetName sets the name of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) SetName(name string) {
	t.Name = name
}

// SetDescription sets the description of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) SetDescription(description string) {
	t.Description = description
}

// SetQueryMethod sets the query method of the Template Custom Field Type data
func (t *TemplateCustomFieldTypeModel) SetQueryMethod(queryMethod TemplateCustomFieldTypeQueryMethod) {
	t.QueryMethod = queryMethod
}

// SetQueryTarget sets the query target of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) SetQueryTarget(target string) {
	t.QueryTarget = target
}

// SetQueryData sets the data of the Template Custom Field Type
func (t *TemplateCustomFieldTypeModel) SetQueryData(data string) {
	t.QueryData = data
}

// SetQueryResultJSONPathFilter sets the JSONPath filter that filters query result data
func (t *TemplateCustomFieldTypeModel) SetQueryResultJSONPathFilter(jsonpath string) {
	t.QueryResultJSONPathFilter = jsonpath
}

// SetUpdateFieldNames sets the field names to be updated
func (t *TemplateCustomFieldTypeModel) SetUpdateFieldNames(fieldNames []string) {
	t.UpdateFieldNames = fieldNames
}

// SetQueryParams sets the query params
func (t *TemplateCustomFieldTypeModel) SetQueryParams(queryParams map[string]string) {
	t.QueryParams = queryParams
}

// TemplateCustomFieldTypeListItemModel is the element type in TemplateCustomFieldTypeModel
type TemplateCustomFieldTypeListItemModel struct {
	Name                      string                             `json:"name"`
	Description               string                             `json:"description,omitempty"`
	QueryMethod               TemplateCustomFieldTypeQueryMethod `json:"query_method"`
	QueryTarget               string                             `json:"query_target"`
	QueryData                 string                             `json:"query_data,omitempty"`
	QueryResultJSONPathFilter string                             `json:"query_result_jsonpath_filter,omitempty"`
}

// TemplateCustomFieldTypeListModel is the Template Custom Field Type list model
type TemplateCustomFieldTypeListModel struct {
	Session

	TemplateCustomFieldTypes []TemplateCustomFieldTypeListItemModel `json:"template_custom_field_types"`
}

// GetTemplateCustomFieldTypes returns template custom field types
func (t *TemplateCustomFieldTypeListModel) GetTemplateCustomFieldTypes() []TemplateCustomFieldType {
	entries := make([]TemplateCustomFieldType, 0, len(t.TemplateCustomFieldTypes))

	for _, entry := range t.TemplateCustomFieldTypes {
		model := TemplateCustomFieldTypeModel{
			Session:                   t.Session,
			Name:                      entry.Name,
			Description:               entry.Description,
			QueryMethod:               entry.QueryMethod,
			QueryTarget:               entry.QueryTarget,
			QueryData:                 entry.QueryData,
			QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
		}

		entries = append(entries, &model)
	}

	return entries
}
