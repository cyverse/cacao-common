package service

import (
	"context"
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats subjects
const (
	// NatsSubjectWorkspace is the prefix of the queue name for Workspace queries
	NatsSubjectWorkspace common.QueryOp = common.NatsQueryOpPrefix + "workspace"

	// WorkspaceListQueryOp is the queue name for WorkspaceList query
	WorkspaceListQueryOp = NatsSubjectWorkspace + ".list"
	// WorkspaceGetQueryOp is the queue name for WorkspaceGet query
	WorkspaceGetQueryOp = NatsSubjectWorkspace + ".get"

	// WorkspaceCreateRequestedEvent is the cloudevent subject for WorkspaceCreate
	WorkspaceCreateRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateRequested"
	// WorkspaceDeleteRequestedEvent is the cloudevent subject for WorkspaceDelete
	WorkspaceDeleteRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteRequested"
	// WorkspaceUpdateRequestedEvent is the cloudevent subject for WorkspaceUpdate
	WorkspaceUpdateRequestedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateRequested"

	// WorkspaceCreatedEvent is the cloudevent subject for WorkspaceCreated
	WorkspaceCreatedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreated"
	// WorkspaceDeletedEvent is the cloudevent subject for WorkspaceDeleted
	WorkspaceDeletedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleted"
	// WorkspaceUpdatedEvent is the cloudevent subject for WorkspaceUpdated
	WorkspaceUpdatedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdated"

	// WorkspaceCreateFailedEvent is the cloudevent subject for WorkspaceCreateFailed
	WorkspaceCreateFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceCreateFailed"
	// WorkspaceDeleteFailedEvent is the cloudevent subject for WorkspaceDeleteFailed
	WorkspaceDeleteFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceDeleteFailed"
	// WorkspaceUpdateFailedEvent is the cloudevent subject for WorkspaceUpdateFailed
	WorkspaceUpdateFailedEvent common.EventType = common.EventTypePrefix + "WorkspaceUpdateFailed"
)

// Nats Client
type natsWorkspaceClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsWorkspaceClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsWorkspaceClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (WorkspaceClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct workspace client")
	}
	return &natsWorkspaceClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// Get returns the workspace with the given workspace ID
func (client *natsWorkspaceClient) Get(ctx context.Context, actor Actor, workspaceID common.ID) (*WorkspaceModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Get",
	})

	if len(workspaceID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	request := WorkspaceModel{
		Session: actor.Session(),
		ID:      workspaceID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, WorkspaceGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var workspaceModel WorkspaceModel
	err = json.Unmarshal(replyCe.Data(), &workspaceModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to workspace object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := workspaceModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &workspaceModel, nil
}

// List returns all workspaces for the user
func (client *natsWorkspaceClient) List(ctx context.Context, actor Actor) ([]WorkspaceModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.List",
	})
	var request Session = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, WorkspaceListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var list WorkspaceListModel
	err = json.Unmarshal(replyCe.Data(), &list)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to workspace list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := list.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return list.Workspaces, nil
}

// Create creates a new workspace
func (client *natsWorkspaceClient) Create(ctx context.Context, actor Actor, workspace WorkspaceModel) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Create",
	})

	promise, err := client.createAsync(ctx, logger, actor, workspace)
	if err != nil {
		return "", err
	}
	response, err := client.createAsyncResponse(ctx, logger, promise)
	if err != nil {
		return "", err
	}
	return response.ID, nil
}

func (client *natsWorkspaceClient) createAsync(ctx context.Context, logger *log.Entry, actor Actor, workspace WorkspaceModel) (messaging2.EventResponsePromise, error) {
	if len(workspace.Name) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Workspace Name")
	}

	workspaceID := workspace.ID
	if !workspaceID.Validate() {
		workspaceID = NewWorkspaceID()
	}
	request := WorkspaceModel{
		Session:           actor.Session(),
		ID:                workspaceID,
		Owner:             actor.Actor,
		Name:              workspace.Name,
		Description:       workspace.Description,
		DefaultProviderID: workspace.DefaultProviderID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, WorkspaceCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	eventsSubscribe := []common.EventType{
		WorkspaceCreatedEvent,
		WorkspaceCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}
	return promise, nil
}

func (client *natsWorkspaceClient) createAsyncResponse(ctx context.Context, logger *log.Entry, promise messaging2.EventResponsePromise) (WorkspaceModel, error) {
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return WorkspaceModel{}, err
	}

	var response WorkspaceModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a workspace create response"
		logger.WithError(err).Error(errorMessage)
		return response, NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return response, nil
	}

	logger.Error(serviceError)
	return response, serviceError
}

// CreateAsync creates a new workspace, but instead of waiting for the response like Create() does, it simply returns a promise.
func (client *natsWorkspaceClient) CreateAsync(ctx context.Context, actor Actor, workspace WorkspaceModel) (messaging2.EventResponsePromise, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.CreateAsync",
	})
	return client.createAsync(ctx, logger, actor, workspace)
}

// CreateAsyncResponse waits for the workspace creation response from the promise and returns the WorkspaceModel and error, if any.
func (client *natsWorkspaceClient) CreateAsyncResponse(ctx context.Context, promise messaging2.EventResponsePromise) (WorkspaceModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.CreateAsyncResponse",
	})
	return client.createAsyncResponse(ctx, logger, promise)
}

// Update updates the workspace with the workspace ID
func (client *natsWorkspaceClient) Update(ctx context.Context, actor Actor, workspace WorkspaceModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Update",
	})

	if !workspace.ID.Validate() {
		return NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	if len(workspace.Name) == 0 {
		return NewCacaoInvalidParameterError("invalid Workspace Name")
	}

	request := WorkspaceModel{
		Session:           actor.Session(),
		ID:                workspace.ID,
		Name:              workspace.Name,
		Description:       workspace.Description,
		DefaultProviderID: workspace.DefaultProviderID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, WorkspaceUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		WorkspaceUpdatedEvent,
		WorkspaceUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response WorkspaceModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a workspace update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// UpdateFields updates the selected fields of the workspace with the workspace ID
func (client *natsWorkspaceClient) UpdateFields(ctx context.Context, actor Actor, workspace WorkspaceModel, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.UpdateFields",
	})

	if !workspace.ID.Validate() {
		return NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(workspace.Name) == 0 {
				return NewCacaoInvalidParameterError("invalid Workspace Name")
			}
		default:
			//pass
		}
	}

	request := WorkspaceModel{
		Session:           actor.Session(),
		ID:                workspace.ID,
		Name:              workspace.Name,
		Description:       workspace.Description,
		DefaultProviderID: workspace.DefaultProviderID,
		UpdateFieldNames:  updateFieldNames,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, WorkspaceUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		WorkspaceUpdatedEvent,
		WorkspaceUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response WorkspaceModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a workspace update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// Delete deletes the workspace with the workspace ID
func (client *natsWorkspaceClient) Delete(ctx context.Context, actor Actor, workspaceID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.Delete",
	})

	if !workspaceID.Validate() {
		return NewCacaoInvalidParameterError("invalid Workspace ID")
	}

	request := WorkspaceModel{
		Session: actor.Session(),
		ID:      workspaceID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, WorkspaceDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		WorkspaceDeletedEvent,
		WorkspaceDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response WorkspaceModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a workspace delete response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}
