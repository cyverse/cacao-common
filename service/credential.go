package service

import (
	"context"
	"errors"
	"time"
)

// CredentialType is the type of credential.
type CredentialType string

// CredentialType Constants
const (
	GitCredentialType           CredentialType = "git"
	PublicSSHKeyCredentialType  CredentialType = "ssh"
	PrivateSSHKeyCredentialType CredentialType = "ssh_private"
	OpenStackCredentialType     CredentialType = "openstack"
	AWSCredentialType           CredentialType = "aws"
	KubeconfigCredentialType    CredentialType = "kubeconfig"
)

// VisibilityType is the visibility of credential, this is not currently used
type VisibilityType string

// CredentialClient ...
type CredentialClient interface {
	Get(ctx context.Context, actor Actor, ID string) (*CredentialModel, error)
	List(ctx context.Context, actor Actor, filter CredentialListFilter) ([]CredentialModel, error)
	Add(ctx context.Context, actor Actor, secret CredentialModel) (credID string, err error)
	Update(ctx context.Context, actor Actor, credID string, update CredentialUpdate) error
	Delete(ctx context.Context, actor Actor, ID string) error
	Generate(ctx context.Context, actor Actor, credential CredentialModel) (credID string, err error)
}

// CredentialModel ...
type CredentialModel struct {
	Session
	ID                string            `json:"id"`
	Name              string            `json:"name"`
	Username          string            `json:"username"`
	Type              CredentialType    `json:"type"`
	Value             string            `json:"value,omitempty"`
	Description       string            `json:"description,omitempty"`
	IsSystem          bool              `json:"is_system,omitempty"`
	IsHidden          bool              `json:"is_hidden,omitempty"`
	Disabled          bool              `json:"disabled"`
	Visibility        VisibilityType    `json:"visibility,omitempty"` // not currently used, neither service nor client respect this value. There is a default value "Default"
	Tags              map[string]string `json:"tags,omitempty"`
	CreatedAt         time.Time         `json:"created_at"`
	UpdatedAt         time.Time         `json:"updated_at"`
	UpdatedBy         string            `json:"updated_by"`
	UpdatedEmulatorBy string            `json:"updated_emulator_by"`
}

// GetRedacted will return a new Secret with the Value set to "REDACTED"
func (c *CredentialModel) GetRedacted() CredentialModel {
	redactedSecret := *c
	redactedSecret.Value = "REDACTED"
	return redactedSecret
}

// Verify verify a credential model
func (c *CredentialModel) Verify() (bool, error) {
	if len(c.Username) <= 0 {
		return false, errors.New("username not set, could not verify")
	}
	if len(c.Value) <= 0 {
		return false, errors.New("value not set, could not verify")
	}
	if len(c.ID) <= 0 {
		return false, errors.New("ID not set, could not verify")
	}
	if len(c.Type) <= 0 {
		return false, errors.New("type not set, could not verify")
	}

	return true, nil
}

// AddTag add a tag to the credential. Tag is added as a tag key (unique within a credential)
func (c *CredentialModel) AddTag(s string) {
	if c.Tags == nil {
		c.Tags = make(map[string]string)
	}
	c.Tags[s] = ""
}

// RemoveTag removes a tag from credential.
func (c *CredentialModel) RemoveTag(s string) {
	delete(c.Tags, s)
}

// GetPrimaryID returns the globally unique ID of the credential, which is the combination of owner's username and credential ID.
func (c *CredentialModel) GetPrimaryID() string {
	return c.Username + "/" + c.ID
}

// GetTags return tags as a list of string (tag keys)
func (c *CredentialModel) GetTags() []string {
	keys := make([]string, 0)
	for key := range c.Tags {
		keys = append(keys, key)
	}
	return keys
}

// RemoveAllTags ...
func (c *CredentialModel) RemoveAllTags() {
	c.Tags = map[string]string{}
}
