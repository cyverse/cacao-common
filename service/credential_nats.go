package service

import "gitlab.com/cyverse/cacao-common/common"

// This file contains structs that define the schema of query & event operations for credential.

// QueryOp & EventType for credential operations
const (
	NatsSubjectCredentials     common.QueryOp = common.NatsQueryOpPrefix + "credentials"
	NatsSubjectCredentialsGet                 = NatsSubjectCredentials + ".get"
	NatsSubjectCredentialsList                = NatsSubjectCredentials + ".list"

	EventCredentialAddRequested        common.EventType = "org.cyverse.events.CredentialAddRequested"
	EventCredentialGenerationRequested common.EventType = "org.cyverse.events.Credential.GenerationRequested"
	EventCredentialUpdateRequested     common.EventType = "org.cyverse.events.CredentialUpdateRequested"
	EventCredentialDeleteRequested     common.EventType = "org.cyverse.events.CredentialDeleteRequested"

	EventCredentialAdded   common.EventType = "org.cyverse.events.CredentialAdded"
	EventCredentialUpdated common.EventType = "org.cyverse.events.CredentialUpdated"
	EventCredentialDeleted common.EventType = "org.cyverse.events.CredentialDeleted"

	EventCredentialAddError    common.EventType = "org.cyverse.events.CredentialAddError"
	EventCredentialUpdateError common.EventType = "org.cyverse.events.CredentialUpdateError"
	EventCredentialDeleteError common.EventType = "org.cyverse.events.CredentialDeleteError"
	EventCredentialGetError    common.EventType = "org.cyverse.events.CredentialGetError"
	EventCredentialListError   common.EventType = "org.cyverse.events.CredentialListError"
)

// CredentialListRequest ...
type CredentialListRequest struct {
	Session `json:",inline"`
	Filter  CredentialListFilter `json:",inline"`
}

// CredentialListFilter contains the filter options supported by the list operation
type CredentialListFilter struct {
	Username string            `json:"username"`            // owner, default to actor
	Type     CredentialType    `json:"type,omitempty"`      // leave empty if all types
	IsSystem *bool             `json:"is_system,omitempty"` // leave nil if both system & non-system
	Disabled *bool             `json:"disabled,omitempty"`  // leave nil if both disabled & enabled
	Tags     map[string]string `json:"tag"`                 // leave tag value as empty if only want to check tag name present
}

// CredentialListReply ...
type CredentialListReply struct {
	Session  `json:",inline"`
	Username string            `json:"username"` // owner
	List     []CredentialModel `json:"list"`
}

// CredentialGetRequest ...
type CredentialGetRequest struct {
	Session  `json:",inline"`
	Username string `json:"username"`
	ID       string `json:"id"`
}

// CredentialGetReply ...
type CredentialGetReply CredentialModel

// CredentialCreateRequest ...
type CredentialCreateRequest CredentialModel

// CredentialCreateResponse ...
type CredentialCreateResponse struct {
	Session  `json:",inline"`
	Username string         `json:"username,omitempty"`
	ID       string         `json:"id,omitempty"`
	Type     CredentialType `json:"type,omitempty"`
}

// CredentialUpdateRequest ...
type CredentialUpdateRequest struct {
	Session  `json:",inline"`
	Username string           `json:"username"`
	ID       string           `json:"id"`
	Update   CredentialUpdate `json:"update"`
}

// CredentialUpdateResponse ...
type CredentialUpdateResponse struct {
	Session      `json:",inline"`
	Username     string         `json:"username"`
	ID           string         `json:"id"`
	Type         CredentialType `json:"type,omitempty"`
	ValueUpdated bool           `json:"value_updated"`
}

// CredentialDeleteRequest ...
type CredentialDeleteRequest struct {
	Session  `json:",inline"`
	Username string `json:"username"`
	ID       string `json:"id"`
}

// CredentialDeleteResponse ...
type CredentialDeleteResponse struct {
	Session  `json:",inline"`
	Username string         `json:"username,omitempty"`
	ID       string         `json:"id,omitempty"`
	Type     CredentialType `json:"type,omitempty"`
}

// CredentialUpdate is a struct that contains fields for credential that can be updated.
// Fields in this struct should correspond to fields in CredentialModel, but the type should be null-able equivalent (e.g. string => *string).
// If the value of a field in this struct is nil, that means the field should not be updated.
type CredentialUpdate struct {
	Name *string `json:"name,omitempty"`
	// modify type could affect other service, there is not a clear use case to modify credential type, credential created with wrong type should be deleted and re-created.
	//Type        *CredentialType `json:"type,omitempty"`
	Value       *string         `json:"value,omitempty"`
	Description *string         `json:"description,omitempty"`
	Disabled    *bool           `json:"disabled,omitempty"`
	Visibility  *VisibilityType `json:"visibility,omitempty"`
	// update or add (if tag name does not exist) tags
	UpdateOrAddTags map[string]string   `json:"update_tags,omitempty"`
	DeleteTags      map[string]struct{} `json:"delete_tags,omitempty"`
}

// NeedToUpdate checks if there is a need to update credential.
func (update CredentialUpdate) NeedToUpdate() bool {
	if update.Name != nil {
		return true
	}
	if update.Value != nil {
		return true
	}
	if update.Description != nil {
		return true
	}
	if update.Disabled != nil {
		return true
	}
	if update.Visibility != nil {
		return true
	}
	if len(update.UpdateOrAddTags) > 0 {
		return true
	}
	if len(update.DeleteTags) > 0 {
		return true
	}
	return false
}
