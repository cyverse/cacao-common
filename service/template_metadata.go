package service

import (
	"encoding/json"
	"fmt"
	"math"
	"reflect"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"go.mongodb.org/mongo-driver/x/bsonx/bsoncore"
)

// TemplatePurpose indicates the reason that a template exists. For example, if the template is intended to be used
// to spin up compute instances then the template purpose would be `compute`. Similarly, if the template is intended
// to be used to provision storage then the template purpose would be `storage`. Templates used for all other purposes
// can be classified as `general`.
type TemplatePurpose string

// Enumeration constants for TemplatePurpose.
const (
	// TemplatePurposeUnknown indicates that the purpose of the template is unspecified.
	TemplatePurposeUnknown TemplatePurpose = ""

	// for V1
	// TemplatePurposeCompute indicates that the template is used to spin up a compute instance.
	TemplatePurposeCompute TemplatePurpose = "compute"
	// TemplatePurposeStorage indicates that the template is used to provision storage.
	TemplatePurposeStorage TemplatePurpose = "storage"

	// for V2
	// TemplatePurposeOpenStackCompute indicates that the template is used to provision an openstack compute instance.
	TemplatePurposeOpenStackCompute TemplatePurpose = "openstack_compute"
	// TemplatePurposeOpenstackStorage indicates that the template is used to provision an openstack (unspecified) storage.
	TemplatePurposeOpenStackStorage TemplatePurpose = "openstack_storage"
	// TemplatePurposeOpenstackStorage indicates that the template is used to provision an openstack share storage.
	TemplatePurposeOpenStackShareStorage TemplatePurpose = "openstack_share_storage"
	// TemplatePurposeOpenstackStorage indicates that the template is used to provision an openstack volume storage.
	TemplatePurposeOpenStackVolumeStorage TemplatePurpose = "openstack_volume_storage"
	// TemplatePurposeGeneral indicates that the template is used to provision a general compute instance.
	TemplatePurposeGeneral TemplatePurpose = "general"
	// TemplatePurposeAWSCompute indicates that the template is used to provision an aws compute instance.
	TemplatePurposeAWSCompute TemplatePurpose = "aws_compute"
	// TemplatePurposeAWSStorage indicates that the template is used to provision an aws (unspecified) storage.
	TemplatePurposeAWSStorage TemplatePurpose = "aws_storage"
	// TemplatePurposeAWSStorage indicates that the template is used to provision an aws EBS storage.
	TemplatePurposeAWSEbsStorage TemplatePurpose = "aws_ebs_storage"
	// TemplatePurposeAWSStorage indicates that the template is used to provision an aws EBS storage.
	TemplatePurposeAWSS3Storage TemplatePurpose = "aws_s3_storage"
	// TemplatePurposeBareSystem indicates that the template is used to provision a bare system.
	TemplatePurposeBareSystem TemplatePurpose = "bare_system"
	// TemplatePurposeNextflow indicates that the template is used to provision a nextflow workflow.
	TemplatePurposeNextflow TemplatePurpose = "nextflow"
	// TemplatePurposeKubernetes indicates that the template is used to provision a kubernetes cluster.
	TemplatePurposeKubernetes TemplatePurpose = "kubernetes"
	// TemplatePurposeMultiCloud indicates that the template is used to provision a multi-cloud cluster.
	TemplatePurposeMultiCloud TemplatePurpose = "multi_cloud"
	// TemplatePurposeCacaoPrerequisite indicates that the template is cacao reserved for prerequisite.
	TemplatePurposeCacaoPrerequisite TemplatePurpose = "cacao_prerequisite"
)

var (
	// TemplateCacaoReservedPurposes defines cacao reserved purposes
	TemplateCacaoReservedPurposes []TemplatePurpose = []TemplatePurpose{TemplatePurposeCacaoPrerequisite}
)

// String returns the string representation of a TemplatePurpose.
func (t TemplatePurpose) String() string {
	if t == TemplatePurposeUnknown {
		return string(TemplatePurposeGeneral)
	}

	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplatePurpose.
func (t TemplatePurpose) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplatePurpose to the appropriate enumeration constant.
func (t *TemplatePurpose) UnmarshalJSON(b []byte) error {
	// Template purposes are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplatePurposeGeneral
	case string(TemplatePurposeGeneral):
		*t = TemplatePurposeGeneral
	// for V1
	case string(TemplatePurposeCompute):
		*t = TemplatePurposeCompute
	case string(TemplatePurposeStorage):
		*t = TemplatePurposeStorage
	// for V2
	case string(TemplatePurposeOpenStackCompute):
		*t = TemplatePurposeOpenStackCompute
	case string(TemplatePurposeOpenStackStorage):
		*t = TemplatePurposeOpenStackStorage
	case string(TemplatePurposeOpenStackShareStorage):
		*t = TemplatePurposeOpenStackShareStorage
	case string(TemplatePurposeOpenStackVolumeStorage):
		*t = TemplatePurposeOpenStackVolumeStorage
	case string(TemplatePurposeAWSCompute):
		*t = TemplatePurposeAWSCompute
	case string(TemplatePurposeAWSStorage):
		*t = TemplatePurposeAWSStorage
	case string(TemplatePurposeAWSEbsStorage):
		*t = TemplatePurposeAWSEbsStorage
	case string(TemplatePurposeAWSS3Storage):
		*t = TemplatePurposeAWSS3Storage
	case string(TemplatePurposeBareSystem):
		*t = TemplatePurposeBareSystem
	case string(TemplatePurposeNextflow):
		*t = TemplatePurposeNextflow
	case string(TemplatePurposeKubernetes):
		*t = TemplatePurposeKubernetes
	case string(TemplatePurposeMultiCloud):
		*t = TemplatePurposeMultiCloud
	case string(TemplatePurposeCacaoPrerequisite):
		*t = TemplatePurposeCacaoPrerequisite
	default:
		return fmt.Errorf("invalid template purpose: %s", s)
	}

	return nil
}

// TemplateCacaoTaskType indicates the type of a task to perform. For example, if the task is to run an ansible script
// then the type would be `ansible`.
type TemplateCacaoTaskType string

// Enumeration constants for TemplateCacaoTaskType.
const (
	// TemplateCacaoTaskTypeUnknown indicates that the type of the task is unspecified.
	TemplateCacaoTaskTypeUnknown TemplateCacaoTaskType = ""

	// TemplateCacaoTaskTypeCacaoAtmosphereLegacy indicates that the task is used to run a command via atmosphere
	TemplateCacaoTaskTypeCacaoAtmosphereLegacy TemplateCacaoTaskType = "cacao_atmosphere_legacy"
	// TemplateCacaoTaskTypeAnsible indicates that the task is used to run a command via ansible
	TemplateCacaoTaskTypeAnsible TemplateCacaoTaskType = "ansible"
	// TemplateCacaoTaskTypeScriptRemote indicates that the task is used to run a command via a remote script
	TemplateCacaoTaskTypeScriptRemote TemplateCacaoTaskType = "script_remote"
	// TemplateCacaoTaskTypeScriptLocal indicates that the task is used to run a command via a local script
	TemplateCacaoTaskTypeScriptLocal TemplateCacaoTaskType = "script_local"
	// TemplateCacaoTaskTypeDocker indicates that the task is used to run a docker instance
	TemplateCacaoTaskTypeDocker TemplateCacaoTaskType = "docker"
	// TemplateCacaoTaskTypeDeploymentRunAction indicates that the task is used to run a deployment run action
	TemplateCacaoTaskTypeDeploymentRunAction TemplateCacaoTaskType = "deployment_run_action"
)

// String returns the string representation of a TemplateCacaoTaskType.
func (t TemplateCacaoTaskType) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateCacaoTaskType.
func (t TemplateCacaoTaskType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateCacaoTaskType to the appropriate enumeration constant.
func (t *TemplateCacaoTaskType) UnmarshalJSON(b []byte) error {
	// Template cacao task types are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateCacaoTaskTypeUnknown
	case string(TemplateCacaoTaskTypeCacaoAtmosphereLegacy):
		*t = TemplateCacaoTaskTypeCacaoAtmosphereLegacy
	case string(TemplateCacaoTaskTypeAnsible):
		*t = TemplateCacaoTaskTypeAnsible
	case string(TemplateCacaoTaskTypeScriptRemote):
		*t = TemplateCacaoTaskTypeScriptRemote
	case string(TemplateCacaoTaskTypeScriptLocal):
		*t = TemplateCacaoTaskTypeScriptLocal
	case string(TemplateCacaoTaskTypeDocker):
		*t = TemplateCacaoTaskTypeDocker
	case string(TemplateCacaoTaskTypeDeploymentRunAction):
		*t = TemplateCacaoTaskTypeDeploymentRunAction
	default:
		return fmt.Errorf("invalid template cacao task type: %s", s)
	}

	return nil
}

// TemplateCacaoTaskExecuteOnType indicates where a task to perform.
type TemplateCacaoTaskExecuteOnType string

// Enumeration constants for TemplateCacaoTaskExecuteOnType.
const (
	// TemplateCacaoTaskExecuteOnTypeUnknown indicates that the type of the task execution is unspecified.
	TemplateCacaoTaskExecuteOnTypeUnknown TemplateCacaoTaskExecuteOnType = ""

	// TemplateCacaoTaskExecuteOnTypeAll indicates that the task will be performed on all instances
	TemplateCacaoTaskExecuteOnTypeAll TemplateCacaoTaskExecuteOnType = "all"
	// TemplateCacaoTaskExecuteOnTypeAllButFirst indicates that the task will be performed on all but the first instance
	TemplateCacaoTaskExecuteOnTypeAllButFirst TemplateCacaoTaskExecuteOnType = "all_but_first"
	// TemplateCacaoTaskExecuteOnTypeParticular indicates that the task will be performed on a particular instance
	TemplateCacaoTaskExecuteOnTypeParticular TemplateCacaoTaskExecuteOnType = "particular"
)

// TemplateCacaoTaskExecuteOn indicates where a task to perform.
type TemplateCacaoTaskExecuteOn struct {
	Type   TemplateCacaoTaskExecuteOnType
	NodeID int // only used when Type is 'TemplateCacaoTaskExecuteOnTypeParticular'
}

// String returns the string representation of a TemplateCacaoTaskExecuteOn.
func (t TemplateCacaoTaskExecuteOn) String() string {
	if t.Type == TemplateCacaoTaskExecuteOnTypeUnknown {
		return string(TemplateCacaoTaskExecuteOnTypeAll)
	}

	if t.Type == TemplateCacaoTaskExecuteOnTypeParticular {
		return fmt.Sprintf("%d", t.NodeID)
	}

	return string(t.Type)
}

// MarshalJSON returns the JSON representation of a TemplateCacaoTaskExecuteOn.
func (t TemplateCacaoTaskExecuteOn) MarshalJSON() ([]byte, error) {
	if t.Type == TemplateCacaoTaskExecuteOnTypeParticular {
		return json.Marshal(t.NodeID)
	}

	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateCacaoTaskExecuteOn to the appropriate enumeration constant.
func (t *TemplateCacaoTaskExecuteOn) UnmarshalJSON(b []byte) error {
	// Template cacao task execute on types are represented as strings or ints in JSON.

	var i int
	err := json.Unmarshal(b, &i)
	if err != nil {
		// not int
		var s string
		err := json.Unmarshal(b, &s)
		if err != nil {
			return err
		}

		// Validate and convert the value.
		switch strings.ToLower(s) {
		case "":
			t.Type = TemplateCacaoTaskExecuteOnTypeAll
		case string(TemplateCacaoTaskExecuteOnTypeAll):
			t.Type = TemplateCacaoTaskExecuteOnTypeAll
		case string(TemplateCacaoTaskExecuteOnTypeAllButFirst):
			t.Type = TemplateCacaoTaskExecuteOnTypeAllButFirst
		default:
			// try cast to int value
			intVal, err := strconv.Atoi(s)
			if err == nil {
				t.Type = TemplateCacaoTaskExecuteOnTypeParticular
				t.NodeID = intVal
				return nil
			}
			return fmt.Errorf("invalid template cacao task execute on: %s", s)
		}

		t.NodeID = 0
		return nil
	}

	t.Type = TemplateCacaoTaskExecuteOnTypeParticular
	t.NodeID = i
	return nil
}

// MarshalBSONValue returns the BSON representation of a TemplateCacaoTaskExecuteOn.
func (t TemplateCacaoTaskExecuteOn) MarshalBSONValue() (bsontype.Type, []byte, error) {
	if t.Type == TemplateCacaoTaskExecuteOnTypeParticular {
		bsonType, bsonBytes, err := bson.MarshalValue(t.NodeID)
		return bsonType, bsonBytes, err
	}

	bsonType, bsonBytes, err := bson.MarshalValue(t.String())
	return bsonType, bsonBytes, err
}

// UnmarshalBSONValue converts the BSON representation of a TemplateCacaoTaskExecuteOn to the appropriate enumeration constant.
func (t *TemplateCacaoTaskExecuteOn) UnmarshalBSONValue(bt bsontype.Type, b []byte) error {
	// Template cacao task execute on types are represented as strings or ints in JSON.
	if bt == bsontype.Int32 {
		i, _, ok := bsoncore.ReadInt32(b)
		if !ok {
			return fmt.Errorf("failed to unmarshal template cacao task execute on (int32) from bson")
		}

		t.Type = TemplateCacaoTaskExecuteOnTypeParticular
		t.NodeID = int(i)
		return nil
	} else if bt == bsontype.String {
		s, _, ok := bsoncore.ReadString(b)
		if ok {
			switch strings.ToLower(s) {
			case "":
				t.Type = TemplateCacaoTaskExecuteOnTypeAll
			case string(TemplateCacaoTaskExecuteOnTypeAll):
				t.Type = TemplateCacaoTaskExecuteOnTypeAll
			case string(TemplateCacaoTaskExecuteOnTypeAllButFirst):
				t.Type = TemplateCacaoTaskExecuteOnTypeAllButFirst
			default:
				// try cast to int value
				intVal, err := strconv.Atoi(s)
				if err == nil {
					t.Type = TemplateCacaoTaskExecuteOnTypeParticular
					t.NodeID = intVal
					return nil
				}
				return fmt.Errorf("invalid template cacao task execute on: %s", s)
			}

			t.NodeID = 0
			return nil
		}
	}

	return fmt.Errorf("failed to unmarshal template cacao task execute on to bson")
}

// TemplateCacaoTaskRunAction indicates what to do when the task type is `run_deployment_action`.
type TemplateCacaoTaskRunAction string

// Enumeration constants for TemplateCacaoTaskRunAction.
const (
	// TemplateCacaoTaskRunActionUnknown indicates that the run action of the task is unspecified.
	TemplateCacaoTaskRunActionUnknown TemplateCacaoTaskRunAction = ""

	// TemplateCacaoTaskRunActionRunDelete indicates that it deletes run at the step.
	TemplateCacaoTaskRunActionRunDelete TemplateCacaoTaskRunAction = "run_delete"
	// TemplateCacaoTaskRunActionRunParamUpdate indicates that it updates parameters of a run at the step.
	TemplateCacaoTaskRunActionRunParamUpdate TemplateCacaoTaskRunAction = "run_param_update"
)

// String returns the string representation of a TemplateCacaoTaskRunAction.
func (t TemplateCacaoTaskRunAction) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateCacaoTaskRunAction.
func (t TemplateCacaoTaskRunAction) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateCacaoTaskRunAction to the appropriate enumeration constant.
func (t *TemplateCacaoTaskRunAction) UnmarshalJSON(b []byte) error {
	// Template purposes are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateCacaoTaskRunActionUnknown
	case string(TemplateCacaoTaskRunActionRunDelete):
		*t = TemplateCacaoTaskRunActionRunDelete
	case string(TemplateCacaoTaskRunActionRunParamUpdate):
		*t = TemplateCacaoTaskRunActionRunParamUpdate
	default:
		return fmt.Errorf("invalid template cacao task run action: %s", s)
	}

	return nil
}

// TemplateCacaoTaskKeyVal is a struct for representing key-value formatted values.
type TemplateCacaoTaskKeyVal struct {
	Key   string `json:"key,omitempty"`
	Value string `json:"value,omitempty"`
}

// UnmarshalJSON converts the JSON representation of a TemplateCacaoTaskKeyVal to the object form.
func (t *TemplateCacaoTaskKeyVal) UnmarshalJSON(b []byte) error {
	// Template cacao task key val are represented as strings or objects in JSON.

	var kv TemplateCacaoTaskKeyVal
	err := json.Unmarshal(b, &kv)
	if err != nil {
		// not object
		var s string
		err := json.Unmarshal(b, &s)
		if err != nil {
			return err
		}

		idx := strings.Index(s, "=")
		if idx <= 0 {
			return fmt.Errorf("the value given is not in key=value format")
		}

		t.Key = s[:idx]
		t.Value = s[idx+1:]
		return nil
	}

	*t = kv
	return nil
}

// TemplateCacaoTask is a struct for a pre/post task in metadata
type TemplateCacaoTask struct {
	Type                 TemplateCacaoTaskType      `json:"type,omitempty"`
	Location             string                     `json:"location,omitempty"`
	ExecuteOn            TemplateCacaoTaskExecuteOn `json:"execute_on,omitempty"`
	Arguments            []string                   `json:"args,omitempty"`
	EnvironmentVariables []TemplateCacaoTaskKeyVal  `json:"env,omitempty"`
	RunAction            TemplateCacaoTaskRunAction `json:"run_action,omitempty"`
	RunParams            []TemplateCacaoTaskKeyVal  `json:"run_params,omitempty"`
}

// TemplateParameter is a struct for a parameter in metadata
type TemplateParameter struct {
	Name        string        `json:"name,omitempty"`
	Type        string        `json:"type,omitempty"`
	Description string        `json:"description,omitempty"`
	Default     interface{}   `json:"default,omitempty"`
	Enum        []interface{} `json:"enum,omitempty"`
	// DataValidationType can have something like "ip_address" or "username" to validate data value
	// application can determine how to use this
	DataValidationType string `json:"data_validation_type,omitempty"`
	Required           bool   `json:"required,omitempty"`
	Editable           bool   `json:"editable,omitempty"`
	Base64             bool   `json:"base64,omitempty"`
}

type templateParameterInternal TemplateParameter

// UnmarshalJSON converts the JSON representation of a TemplateParameter to the object form.
func (param *TemplateParameter) UnmarshalJSON(b []byte) error {
	var param2 templateParameterInternal
	err := json.Unmarshal(b, &param2)
	if err != nil {
		return err
	}

	// validate
	*param = TemplateParameter(param2)

	err = param.CorrectDataType()
	if err != nil {
		return err
	}

	return nil
}

func (param *TemplateParameter) convToInt64(n interface{}) (int64, error) {
	switch n := n.(type) {
	case int:
		return int64(n), nil
	case int8:
		return int64(n), nil
	case int16:
		return int64(n), nil
	case int32:
		return int64(n), nil
	case int64:
		return int64(n), nil
	case uint:
		return int64(uint64(n)), nil
	case uintptr:
		return int64(uint64(n)), nil
	case uint8:
		return int64(uint64(n)), nil
	case uint16:
		return int64(uint64(n)), nil
	case uint32:
		return int64(uint64(n)), nil
	case uint64:
		return int64(uint64(n)), nil
	case float32:
		return int64(n), nil
	case float64:
		return int64(n), nil
	}
	return 0, fmt.Errorf("failed to convert %v to int64", n)
}

func (param *TemplateParameter) convToFloat64(n interface{}) (float64, error) {
	switch n := n.(type) {
	case int, int8, int16, int32, int64, uint, uintptr, uint8, uint16, uint32, uint64:
		i64, err := param.convToInt64(n)
		if err != nil {
			return 0, err
		}
		return float64(i64), nil
	case float32:
		return float64(n), nil
	case float64:
		return float64(n), nil
	}
	return 0, fmt.Errorf("failed to convert %v to float64", n)
}

// CorrectDataType corrects data type
// Updates Default and Enum fields to have correct data types
func (param *TemplateParameter) CorrectDataType() error {
	hasDefaultValue := false
	if param.Default != nil {
		hasDefaultValue = true
	}

	hasEnum := false
	if len(param.Enum) > 0 {
		hasEnum = true
	}

	switch strings.ToLower(param.Type) {
	case "integer", "int":
		param.Type = "integer"
	case "boolean", "bool":
		param.Type = "bool"
	case "float", "double", "number":
		param.Type = "float"
	}

	switch strings.ToLower(param.Type) {
	case "integer":
		if hasDefaultValue {
			convVal, err := param.convToInt64(param.Default)
			if err != nil {
				return err
			}

			// type cast and overwrite
			param.Default = convVal
		}

		if hasEnum {
			enum := []interface{}{}
			for _, enumValue := range param.Enum {
				convEnum, err := param.convToInt64(enumValue)
				if err != nil {
					return err
				}

				enum = append(enum, convEnum)
			}

			// type cast and overwrite
			param.Enum = enum
		}
	case "bool":
		if hasDefaultValue {
			defaultValueBool, isDefaultValueBool := param.Default.(bool)
			if !isDefaultValueBool {
				return fmt.Errorf("field '%s' default value is not boolean", param.Name)
			}

			// type cast and overwrite
			param.Default = defaultValueBool
		}
	case "float":
		if hasDefaultValue {
			convVal, err := param.convToFloat64(param.Default)
			if err != nil {
				return err
			}
			// type cast and overwrite
			param.Default = convVal
		}

		if hasEnum {
			enum := []interface{}{}
			for _, enumValue := range param.Enum {
				convEnum, err := param.convToFloat64(enumValue)
				if err != nil {
					return err
				}

				enum = append(enum, convEnum)
			}

			// type cast and overwrite
			param.Enum = enum
		}
	default: // for string, or other cacao template custom field types
		if hasDefaultValue {
			defaultValueString, isDefaultValueString := param.Default.(string)
			if !isDefaultValueString {
				return fmt.Errorf("field '%s' default value is not string", param.Name)
			}

			// type cast and overwrite
			param.Default = defaultValueString
		}

		if hasEnum {
			enum := []interface{}{}
			for _, enumValue := range param.Enum {
				enumValueString, isEnumValueString := enumValue.(string)
				if !isEnumValueString {
					return fmt.Errorf("field '%s' enum value %v is not string", param.Name, enumValue)
				}

				enum = append(enum, enumValueString)
			}

			// type cast and overwrite
			param.Enum = enum
		}
	}

	return nil
}

// CheckValue checks value against parameter type and correct the format.
// nil is allowed and used to indicate that no value is provided for the parameter, and should use default value instead.
func (param *TemplateParameter) CheckValue(value interface{}) (typeName string, resultValue interface{}, err error) {

	// nil means the value is not provided
	if value == nil {
		if param.Default == nil {
			return "", nil, NewCacaoInvalidParameterError(fmt.Sprintf("missing required parameter %s", param.Name))
		}
		resultValue = param.Default
	} else {
		resultValue = value
	}

	switch param.Type {
	case "integer":
		resultValue, err = param.convToInt64(resultValue)
		if err != nil {
			err = NewCacaoInvalidParameterError(fmt.Sprintf("parameter value for %s is bad type, expected %s, got %s", param.Name, param.Type, reflect.TypeOf(resultValue).String()))
			return "", nil, err
		}
	case "float":
		resultValue, err = param.convToFloat64(resultValue)
		if err != nil {
			err = NewCacaoInvalidParameterError(fmt.Sprintf("parameter value for %s is bad type, expected %s, got %s", param.Name, param.Type, reflect.TypeOf(resultValue).String()))
			return "", nil, err
		}
	case "bool":
		if boolVal, ok := resultValue.(bool); ok {
			resultValue = boolVal
		} else {
			err = NewCacaoInvalidParameterError(fmt.Sprintf("parameter value for %s is bad type, expected %s, got %s", param.Name, param.Type, reflect.TypeOf(resultValue).String()))
			return "", nil, err
		}
	case "string":
		if stringVal, ok := resultValue.(string); ok {
			resultValue = stringVal
		} else {
			err = NewCacaoInvalidParameterError(fmt.Sprintf("parameter value for %s is bad type, expected %s, got %s", param.Name, param.Type, reflect.TypeOf(resultValue).String()))
			return "", nil, err
		}
	default:
		if strings.HasPrefix(param.Type, "cacao_") {
			// assume special types are string type for now
			if stringVal, ok := resultValue.(string); ok {
				resultValue = stringVal
			} else {
				err = NewCacaoInvalidParameterError(fmt.Sprintf("parameter value for %s is bad type, expected %s, got %s", param.Name, "string", reflect.TypeOf(resultValue).String()))
				return "", nil, err
			}
		} else {
			err = NewCacaoInvalidParameterError(fmt.Sprintf("parameter %s has unknown type %s", param.Name, param.Type))
			return "", nil, err
		}
	}

	if len(param.Enum) > 0 {
		// has enum val
		correctEnumValue := false
		for _, enumValue := range param.Enum {
			if enumValue == resultValue {
				// correct value
				correctEnumValue = true
				break
			}
		}

		if !correctEnumValue {
			return param.Type, nil, fmt.Errorf("parameter %s value %s must be one of %v", param.Name, value, param.Enum)
		}
	}

	return param.Type, resultValue, nil
}

// CheckValueFromString checks value in string format against parameter type and correct the format.
// When default is defined for the parameter, empty string ("") is treated as lack of value.
func (param *TemplateParameter) CheckValueFromString(value string) (string, interface{}, error) {
	var paramValue interface{} = value
	if param.Default != nil {
		paramValue = param.Default
	}

	if len(value) > 0 {
		switch param.Type {
		case "integer":
			parseFloat, err := strconv.ParseFloat(value, 64)
			if err != nil {
				return param.Type, nil, fmt.Errorf("unable to parse parameter %s value %s to int", param.Name, value)
			}

			paramValue = int64(math.Trunc(parseFloat))
		case "bool":
			parseBool, err := strconv.ParseBool(value)
			if err != nil {
				return param.Type, nil, fmt.Errorf("unable to parse parameter %s value %s to bool", param.Name, value)
			}

			paramValue = parseBool
		case "float":
			parseFloat, err := strconv.ParseFloat(value, 64)
			if err != nil {
				return param.Type, nil, fmt.Errorf("unable to parse parameter %s value %s to int", param.Name, value)
			}

			paramValue = parseFloat
		default:
			paramValue = value
		}
	}

	if len(param.Enum) > 0 {
		// has enum val
		correctEnumValue := false
		for _, enumValue := range param.Enum {
			if enumValue == paramValue {
				// correct value
				correctEnumValue = true
				break
			}
		}

		if !correctEnumValue {
			return param.Type, nil, fmt.Errorf("parameter %s value %s must be one of %v", param.Name, value, param.Enum)
		}
	}

	return param.Type, paramValue, nil
}

// TemplateFilter is a struct for a filter in metadata
type TemplateFilter struct {
	Key   TemplateFilterKey `json:"key,omitempty"`
	Value interface{}       `json:"value,omitempty"`
}

// TemplateFilterKey indicates the type of a filter key.
type TemplateFilterKey string

// Enumeration constants for TemplateFilterType.
const (
	// TemplateFilterKeyUnknown indicates that the value is unknown
	TemplateFilterKeyUnknown TemplateFilterKey = ""
	// TemplateFilterKeyImageAllowed indicates that the value is a list of images allowed
	TemplateFilterKeyImageAllowed TemplateFilterKey = "images_allowed"
	// TemplateFilterKeyImageBlocked indicates that the value is a list of images blocked
	TemplateFilterKeyImageBlocked TemplateFilterKey = "images_blocked"
)

// String returns the string representation of a TemplateFilterKey.
func (t TemplateFilterKey) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateFilterKey.
func (t TemplateFilterKey) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateFilterKey to the appropriate enumeration constant.
func (t *TemplateFilterKey) UnmarshalJSON(b []byte) error {
	// Template purposes are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateFilterKeyUnknown
	case string(TemplateFilterKeyImageAllowed):
		*t = TemplateFilterKeyImageAllowed
	case string(TemplateFilterKeyImageBlocked):
		*t = TemplateFilterKeyImageBlocked
	default:
		return fmt.Errorf("invalid template filter key: %s", s)
	}

	return nil
}

// TemplateMetadata is a struct for template metadata
// The struct is generated from a metadata file (e.g., https://github.com/cyverse/tf-openstack-single-image/blob/master/metadata.json)
type TemplateMetadata struct {
	SchemaURL         string              `json:"schema_url,omitempty"`
	SchemaVersion     string              `json:"schema_version,omitempty"`
	Name              string              `json:"name,omitempty"`
	Author            string              `json:"author,omitempty"`
	AuthorEmail       string              `json:"author_email,omitempty"`
	Description       string              `json:"description,omitempty"`
	DocURL            string              `json:"doc_url,omitempty"`
	TemplateType      TemplateTypeName    `json:"template_type,omitempty"`
	Purpose           TemplatePurpose     `json:"purpose,omitempty"`
	PublishedVersions []string            `json:"published_versions,omitempty"`
	CacaoPreTasks     []TemplateCacaoTask `json:"cacao_pre_tasks,omitempty"`
	CacaoPostTasks    []TemplateCacaoTask `json:"cacao_post_tasks,omitempty"`
	// Parameters stores template parameters
	// e.g.,
	// [
	//     {"name": "instance_count", "default": 1, "type": "integer"},
	//     {"name": "instance_name", "type": "string"}
	// ]
	Parameters []TemplateParameter `json:"parameters,omitempty"`
	// Filters add filters to screen out field values
	// e.g.,
	// [
	//     {"Key": "images_allowed", "value": "xxxxx"}
	//]
	Filters []TemplateFilter `json:"filters,omitempty"`
}

// GetParameter returns TemplateParameter with the given name
// If the parameter with the given name does not exist, returns nil
func (meta *TemplateMetadata) GetParameter(name string) *TemplateParameter {
	for _, param := range meta.Parameters {
		if param.Name == name {
			return &param
		}
	}
	return nil
}

// GetFilter returns TemplateFilter with the given key
// If the filter with the given key does not exist, returns nil
func (meta *TemplateMetadata) GetFilter(key TemplateFilterKey) *TemplateFilter {
	for _, filter := range meta.Filters {
		if filter.Key == key {
			return &filter
		}
	}
	return nil
}

// GetFilterValueStrArr returns TemplateFilter value with the given key
func (meta *TemplateMetadata) GetFilterValueStrArr(key TemplateFilterKey) []string {
	filter := meta.GetFilter(key)
	if filter == nil {
		return nil
	}

	if filter.Value == nil {
		return nil
	}

	if filterStrArr, ok := filter.Value.([]string); ok {
		return filterStrArr
	}

	if filterStr, ok := filter.Value.(string); ok {
		return []string{filterStr}
	}

	return nil
}

// GetImagesAllowed returns images allowed
func (meta *TemplateMetadata) GetImagesAllowed() []string {
	return meta.GetFilterValueStrArr(TemplateFilterKeyImageAllowed)
}

// GetImagesBlocked returns images blocked
func (meta *TemplateMetadata) GetImagesBlocked() []string {
	return meta.GetFilterValueStrArr(TemplateFilterKeyImageBlocked)
}
