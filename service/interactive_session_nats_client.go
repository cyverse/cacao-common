package service

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"strings"
)

// Nats subjects
const (
	// NatsSubjectInteractiveSession is the prefix of the queue name for Interactivesession queries
	NatsSubjectInteractiveSession common.QueryOp = common.NatsQueryOpPrefix + "interactivesession"

	// InteractiveSessionListQueryOp is the queue name for InteractiveSessionList query
	InteractiveSessionListQueryOp = NatsSubjectInteractiveSession + ".list"
	// InteractiveSessionGetQueryOp is the queue name for InteractiveSessionGet query
	InteractiveSessionGetQueryOp = NatsSubjectInteractiveSession + ".get"
	// InteractiveSessionGetByInstanceIDQueryOp is the queue name for InteractiveSessionGetByInstanceID query
	InteractiveSessionGetByInstanceIDQueryOp = NatsSubjectInteractiveSession + ".getByInstanceID"
	// InteractiveSessionGetByInstanceAddressQueryOp is the queue name for InteractiveSessionGetByInstanceAddress query
	InteractiveSessionGetByInstanceAddressQueryOp = NatsSubjectInteractiveSession + ".getByInstanceAddress"
	// InteractiveSessionCheckPrerequisitesQueryOp is the queue name for InteractiveSessionCheckPrerequisites query
	InteractiveSessionCheckPrerequisitesQueryOp = NatsSubjectInteractiveSession + ".checkPrerequisites"

	// InteractiveSessionCreateRequestedEvent is the cloudevent subject for InteractiveSessionCreate
	InteractiveSessionCreateRequestedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionCreateRequested"
	// InteractiveSessionDeactivateRequestedEvent is the cloudevent subject for InteractiveSessionDeactivate
	InteractiveSessionDeactivateRequestedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionDeactivateRequested"

	// InteractiveSessionCreatedEvent is the cloudevent subject for InteractiveSessionCreated
	InteractiveSessionCreatedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionCreated"
	// InteractiveSessionDeactivatedEvent is the cloudevent subject for InteractiveSessionDeactivated
	InteractiveSessionDeactivatedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionDeactivated"

	// InteractiveSessionCreateFailedEvent is the cloudevent subject for InteractiveSessionCreateFailed
	InteractiveSessionCreateFailedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionCreateFailed"
	// InteractiveSessionDeactivateFailedEvent is the cloudevent subject for InteractiveSessionDeactivateFailed
	InteractiveSessionDeactivateFailedEvent common.EventType = common.EventTypePrefix + "InteractiveSessionDeactivateFailed"
)

// Nats Client
type natsInteractiveSessionClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsInteractiveSessionClientFromConn creates a new client from existing
// Connections.
//
// Note if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsInteractiveSessionClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (InteractiveSessionClient, error) {
	log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsInteractiveSessionClient",
	}).Trace("called")

	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct interactive session client")
	}
	return &natsInteractiveSessionClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// Get returns the interactive session with the given interactive session ID
// TODO make use of context.Context for queries
func (client *natsInteractiveSessionClient) Get(ctx context.Context, actor Actor, interactiveSessionID common.ID) (*InteractiveSessionModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	if !interactiveSessionID.Validate() {
		return nil, NewCacaoInvalidParameterError("invalid InteractiveSession ID")
	}

	request := InteractiveSessionModel{
		Session: actor.Session(),
		ID:      interactiveSessionID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, InteractiveSessionGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var interactiveSessionModel InteractiveSessionModel
	err = json.Unmarshal(replyCe.Data(), &interactiveSessionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &interactiveSessionModel, nil
}

// GetByInstanceID returns the interactive session with the given instance ID
func (client *natsInteractiveSessionClient) GetByInstanceID(ctx context.Context, actor Actor, instanceID string) (*InteractiveSessionModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.GetByInstanceID",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	if len(instanceID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Instance ID")
	}

	request := InteractiveSessionModel{
		Session:    actor.Session(),
		InstanceID: instanceID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, InteractiveSessionGetByInstanceIDQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var interactiveSessionModel InteractiveSessionModel
	err = json.Unmarshal(replyCe.Data(), &interactiveSessionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &interactiveSessionModel, nil
}

// GetByInstanceAddress returns the interactive session with the given instance address
func (client *natsInteractiveSessionClient) GetByInstanceAddress(ctx context.Context, actor Actor, instanceAddress string) (*InteractiveSessionModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.GetByInstanceAddress",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	if len(instanceAddress) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Instance Address")
	}
	request := InteractiveSessionModel{
		Session:         actor.Session(),
		InstanceAddress: instanceAddress,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, InteractiveSessionGetByInstanceAddressQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var interactiveSessionModel InteractiveSessionModel
	err = json.Unmarshal(replyCe.Data(), &interactiveSessionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := interactiveSessionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &interactiveSessionModel, nil
}

// List returns all active/creating interactive sessions for the user
func (client *natsInteractiveSessionClient) List(ctx context.Context, actor Actor) ([]InteractiveSessionModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	var request Session = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, InteractiveSessionListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}
	var list InteractiveSessionListModel
	err = json.Unmarshal(replyCe.Data(), &list)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to interactive session list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := list.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return list.InteractiveSessions, nil
}

// CheckPrerequisites checks if the given interactive session can be created
func (client *natsInteractiveSessionClient) CheckPrerequisites(ctx context.Context, actor Actor, protocol InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.CheckPrerequisites",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	if len(instanceAddress) == 0 {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAddress")
	}

	if len(instanceAdminUsername) == 0 {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAdminUsername")
	}

	if len(protocol) == 0 {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession Protocol")
	}

	if protocol == InteractiveSessionProtocolUnknown {
		return false, NewCacaoInvalidParameterError("invalid InteractiveSession Protocol")
	}

	request := InteractiveSessionModel{
		Session:               actor.Session(),
		Owner:                 actor.Actor,
		InstanceAddress:       instanceAddress,
		InstanceAdminUsername: instanceAdminUsername,
		Protocol:              protocol,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, InteractiveSessionCheckPrerequisitesQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return false, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return false, err
	}

	var session Session
	err = json.Unmarshal(replyCe.Data(), &session)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to session object"
		logger.WithError(err).Error(errorMessage)
		return false, NewCacaoMarshalError(errorMessage)
	}

	serviceError := session.GetServiceError()
	if serviceError != nil {
		if strings.Contains(serviceError.ContextualError(), "Prerequisites are not met") {
			// soft err
			return false, nil
		}

		logger.Error(serviceError)
		return false, serviceError
	}
	return true, nil
}

// Create creates a new interactive session
func (client *natsInteractiveSessionClient) Create(ctx context.Context, actor Actor, interactiveSession InteractiveSessionModel) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Create",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	if len(interactiveSession.InstanceID) == 0 {
		return "", NewCacaoInvalidParameterError("invalid InteractiveSession InstanceID")
	}

	if len(interactiveSession.InstanceAddress) == 0 {
		return "", NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAddress")
	}

	if len(interactiveSession.InstanceAdminUsername) == 0 {
		return "", NewCacaoInvalidParameterError("invalid InteractiveSession InstanceAdminUsername")
	}

	if interactiveSession.Protocol == InteractiveSessionProtocolUnknown {
		return "", NewCacaoInvalidParameterError("invalid InteractiveSession Protocol")
	}

	if len(interactiveSession.CloudID) == 0 {
		return "", NewCacaoInvalidParameterError("invalid InteractiveSession CloudID")
	}

	interactiveSessionID := interactiveSession.ID
	if !interactiveSessionID.Validate() {
		interactiveSessionID = NewInteractiveSessionID() // TODO maybe have service itself generate the ID?
	}

	request := InteractiveSessionModel{
		Session:               actor.Session(),
		ID:                    interactiveSessionID,
		Owner:                 actor.Actor,
		InstanceID:            interactiveSession.InstanceID,
		InstanceAddress:       interactiveSession.InstanceAddress,
		InstanceAdminUsername: interactiveSession.InstanceAdminUsername,
		CloudID:               interactiveSession.CloudID,
		Protocol:              interactiveSession.Protocol,
	}

	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, InteractiveSessionCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", err
	}
	eventsSubscribe := []common.EventType{
		InteractiveSessionCreatedEvent,
		InteractiveSessionCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("error while wait for response")
		return "", err
	}
	var response InteractiveSessionModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal an interactive session create response"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return interactiveSessionID, nil
	}

	logger.Error(serviceError)
	return "", serviceError
}

// Deactivate deactivates the interactive session
func (client *natsInteractiveSessionClient) Deactivate(ctx context.Context, actor Actor, interactiveSessionID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsInteractiveSessionClient.Deactivate",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	if !interactiveSessionID.Validate() {
		return NewCacaoInvalidParameterError("invalid InteractiveSessionID")
	}
	request := InteractiveSessionModel{
		Session: actor.Session(),
		ID:      interactiveSessionID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, InteractiveSessionDeactivateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	promise, err := client.eventConn.Request(requestCe, []common.EventType{
		InteractiveSessionDeactivatedEvent,
		InteractiveSessionDeactivateFailedEvent,
	})
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("error while wait for response")
		return err
	}
	var response InteractiveSessionModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal an interactive session deactivate response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}

	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}
	logger.Error(serviceError)
	return serviceError
}
