package service

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats subjects
const (
	// NatsSubjectProvider is the prefix of the queue name for Provider queries
	NatsSubjectProvider common.QueryOp = common.NatsQueryOpPrefix + "provider"

	// ProviderGetQueryOp is the queue name for ProviderGet query
	ProviderGetQueryOp = NatsSubjectProvider + ".get"
	// ProviderListQueryOp is the queue name for ProviderList query
	ProviderListQueryOp = NatsSubjectProvider + ".list"

	// ProviderCreateRequestedEvent is the cloudevent subject for ProviderCreate
	ProviderCreateRequestedEvent common.EventType = common.EventTypePrefix + "ProviderCreateRequested"
	// ProviderDeleteAdminRequestedEvent is the cloudevent subject for ProviderDeleteAdmin
	ProviderDeleteRequestedEvent common.EventType = common.EventTypePrefix + "ProviderDeleteRequested"
	// ProviderUpdateRequestedEvent is the cloudevent subject for ProviderUpdate
	ProviderUpdateRequestedEvent common.EventType = common.EventTypePrefix + "ProviderUpdateRequested"

	// ProviderCreatedEvent is the cloudevent subject for ProviderCreated
	ProviderCreatedEvent common.EventType = common.EventTypePrefix + "ProviderCreated"
	// ProviderDeletedEvent is the cloudevent subject for ProviderDeleted
	ProviderDeletedEvent common.EventType = common.EventTypePrefix + "ProviderDeleted"
	// ProviderUpdatedEvent is the cloudevent subject for ProviderUpdated
	ProviderUpdatedEvent common.EventType = common.EventTypePrefix + "ProviderUpdated"

	// ProviderCreateFailedEvent is the cloudevent subject for ProviderCreateFailed
	ProviderCreateFailedEvent common.EventType = common.EventTypePrefix + "ProviderCreateFailed"
	// ProviderDeleteFailedEvent is the cloudevent subject for ProviderDeleteFailed
	ProviderDeleteFailedEvent common.EventType = common.EventTypePrefix + "ProviderDeleteFailed"
	// ProviderUpdateFailedEvent is the cloudevent subject for ProviderUpdateFailed
	ProviderUpdateFailedEvent common.EventType = common.EventTypePrefix + "ProviderUpdateFailed"
)

// Nats Client
type natsProviderClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsProviderClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsProviderClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (ProviderClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct provider client")
	}
	return &natsProviderClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// Get returns the provider with the given provider ID
func (client *natsProviderClient) Get(ctx context.Context, actor Actor, providerID common.ID) (*ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Get",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"provider": providerID,
	})

	if len(providerID) == 0 {
		return nil, fmt.Errorf("invalid Provider ID")
	}

	request := ProviderRequest{
		Session: actor.Session(),
		ProviderModel: ProviderModel{
			ID: providerID,
		},
	}
	requestCe, err := messaging2.CreateCloudEvent(request, ProviderGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var response ProviderResponse
	err = json.Unmarshal(replyCe.Data(), &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal response JSON to object")
		return nil, err
	}

	if err := response.GetServiceError(); err != nil {
		return nil, err
	}
	return &response.ProviderModel, nil
}

// List returns all providers for the user
func (client *natsProviderClient) List(ctx context.Context, actor Actor) ([]ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	var request Session = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, ProviderListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var response ProviderListModel
	err = json.Unmarshal(replyCe.Data(), &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal response JSON to object")
		return nil, err
	}

	if err := response.GetServiceError(); err != nil {
		return nil, err
	}
	return response.List, nil
}

// Create creates a new provider
func (client *natsProviderClient) Create(ctx context.Context, actor Actor, provider ProviderModel) (common.ID, common.TransactionID, error) {
	logger := log.WithFields(log.Fields{
		"package":      "service",
		"function":     "natsProviderClient.Create",
		"actor":        actor.Actor,
		"emulator":     actor.Emulator,
		"providerName": provider.Name,
		"providerType": provider.Type,
	})

	if len(provider.Name) == 0 {
		return "", "", fmt.Errorf("invalid Provider Name")
	}

	request := ProviderRequest{
		Session: actor.Session(),
		ProviderModel: ProviderModel{
			Name:     provider.Name,
			URL:      provider.URL,
			Type:     provider.Type,
			Metadata: provider.Metadata,
		},
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, ProviderCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", "", err
	}
	eventsSubscribe := []common.EventType{
		ProviderCreatedEvent,
		ProviderCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return "", "", err
	}

	var response ProviderResponse
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal a provider create response")
		return "", messaging2.GetTransactionID(&responseCe), NewCacaoMarshalError("unable to unmarshal a provider create response")
	}
	if err := response.GetServiceError(); err != nil {
		return "", messaging2.GetTransactionID(&responseCe), err
	}
	return response.ID, messaging2.GetTransactionID(&responseCe), nil
}

// Delete deletes the provider with the provider ID
func (client *natsProviderClient) Delete(ctx context.Context, actor Actor, providerID common.ID) (common.TransactionID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Delete",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"provider": providerID,
	})

	if len(providerID) == 0 {
		return "", fmt.Errorf("invalid Provider ID")
	}

	request := ProviderRequest{
		Session: actor.Session(),
		ProviderModel: ProviderModel{
			ID: providerID,
		},
	}

	transactionID := messaging2.NewTransactionID()

	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, ProviderDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", err
	}
	eventsSubscribe := []common.EventType{
		ProviderDeletedEvent,
		ProviderDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return "", err
	}
	var response ProviderResponse
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal a provider delete response")
		return messaging2.GetTransactionID(&responseCe), NewCacaoMarshalError("unable to unmarshal a provider delete response")
	}
	if err := response.GetServiceError(); err != nil {
		return messaging2.GetTransactionID(&responseCe), err
	}
	return messaging2.GetTransactionID(&responseCe), nil
}

// Update updates the provider with the provider ID
func (client *natsProviderClient) Update(ctx context.Context, actor Actor, provider ProviderModel) (common.TransactionID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsProviderClient.Update",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
		"provider": provider.ID,
	})

	if len(provider.ID) == 0 {
		return "", fmt.Errorf("invalid Provider ID")
	}

	request := ProviderRequest{
		Session: actor.Session(),
		ProviderModel: ProviderModel{
			ID:       provider.ID,
			Name:     provider.Name,
			URL:      provider.URL,
			Type:     provider.Type,
			Metadata: provider.Metadata,
		},
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, ProviderUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", err
	}
	eventsSubscribe := []common.EventType{
		ProviderUpdatedEvent,
		ProviderUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return "", err
	}
	var response ProviderResponse
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal a provider update response")
		return "", NewCacaoMarshalError("unable to unmarshal a provider update response")
	}
	if err := response.GetServiceError(); err != nil {
		return messaging2.GetTransactionID(&responseCe), err
	}
	return messaging2.GetTransactionID(&responseCe), nil
}
