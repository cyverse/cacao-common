package service

import (
	"context"
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats subjects
const (
	// NatsSubjectTemplate is the prefix of the queue name for Template queries
	NatsSubjectTemplate common.QueryOp = common.NatsQueryOpPrefix + "template"

	/////////////////////////////////////////////////////////////////////////
	// TemplateSourceType
	/////////////////////////////////////////////////////////////////////////
	// TemplateSourceTypeListQueryOp is the queue name for TemplateSourceTypeList query
	TemplateSourceTypeListQueryOp = NatsSubjectTemplate + ".listSourceTypes"

	/////////////////////////////////////////////////////////////////////////
	// TemplateType
	/////////////////////////////////////////////////////////////////////////
	// TemplateTypeListQueryOp is the queue name for TemplateTypeList query
	TemplateTypeListQueryOp = NatsSubjectTemplate + ".listTypes"
	// TemplateTypeGetQueryOp is the queue name for TemplateTypeGet query
	TemplateTypeGetQueryOp = NatsSubjectTemplate + ".getType"

	// TemplateTypeCreateRequestedEvent is the cloudevent subject for TemplateTypeCreate
	TemplateTypeCreateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateTypeCreateRequested"
	// TemplateTypeDeleteRequestedEvent is the cloudevent subject for TemplateTypeDelete
	TemplateTypeDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TemplateTypeDeleteRequested"
	// TemplateTypeUpdateRequestedEvent is the cloudevent subject for TemplateTypeUpdate
	TemplateTypeUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateTypeUpdateRequested"

	// TemplateTypeCreatedEvent is the cloudevent subject for TemplateTypeCreated
	TemplateTypeCreatedEvent common.EventType = common.EventTypePrefix + "TemplateTypeCreated"
	// TemplateTypeDeletedEvent is the cloudevent subject for TemplateTypeDeleted
	TemplateTypeDeletedEvent common.EventType = common.EventTypePrefix + "TemplateTypeDeleted"
	// TemplateTypeUpdatedEvent is the loudevent subject for TemplateTypeUpdated
	TemplateTypeUpdatedEvent common.EventType = common.EventTypePrefix + "TemplateTypeUpdated"

	// TemplateTypeCreateFailedEvent is the cloudevent subject for TemplateTypeCreateFailed
	TemplateTypeCreateFailedEvent common.EventType = common.EventTypePrefix + "TemplateTypeCreateFailed"
	// TemplateTypeDeleteFailedEvent is the eloudevent subject for TemplateTypeDeleteFailed
	TemplateTypeDeleteFailedEvent common.EventType = common.EventTypePrefix + "TemplateTypeDeleteFailed"
	// TemplateTypeUpdateFailedEvent is the loudevent subject for TemplateTypeUpdateFailed
	TemplateTypeUpdateFailedEvent common.EventType = common.EventTypePrefix + "TemplateTypeUpdateFailed"

	/////////////////////////////////////////////////////////////////////////
	// TemplateCustomFieldType
	/////////////////////////////////////////////////////////////////////////
	// TemplateCustomFieldTypeListQueryOp is the queue name for TemplateCustomFieldTypeList query
	TemplateCustomFieldTypeListQueryOp = NatsSubjectTemplate + ".listCustomFieldTypes"
	// TemplateCustomFieldTypeGetQueryOp is the queue name for TemplateCustomFieldTypeGet query
	TemplateCustomFieldTypeGetQueryOp = NatsSubjectTemplate + ".getCustomFieldType"
	// TemplateCustomFieldTypeQueryQueryOp is the queue name for TemplateCustomFieldTypeQuery query
	TemplateCustomFieldTypeQueryQueryOp = NatsSubjectTemplate + ".queryCustomFieldType"

	// TemplateCustomFieldTypeCreateRequestedEvent is the cloudevent subject for TemplateCustomFieldTypeCreate
	TemplateCustomFieldTypeCreateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeCreateRequested"

	// TemplateCustomFieldTypeDeleteRequestedEvent is the cloudevent subject for TemplateCustomFieldTypeDelete
	TemplateCustomFieldTypeDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeDeleteRequested"
	// TemplateCustomFieldTypeUpdateRequestedEvent is the cloudevent subject for TemplateCustomFieldTypeUpdate
	TemplateCustomFieldTypeUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeUpdateRequested"

	// TemplateCustomFieldTypeCreatedEvent is the cloudevent subject for TemplateCustomFieldTypeCreated
	TemplateCustomFieldTypeCreatedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeCreated"
	// TemplateCustomFieldTypeDeletedEvent is the cloudevent subject for TemplateCustomFieldTypeDeleted
	TemplateCustomFieldTypeDeletedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeDeleted"
	// TemplateCustomFieldTypeUpdatedEvent is the cloudevent subject for TemplateCustomFieldTypeUpdated
	TemplateCustomFieldTypeUpdatedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeUpdated"

	// TemplateCustomFieldTypeCreateFailedEvent is the cloudevent subject for TemplateCustomFieldTypeCreateFailed
	TemplateCustomFieldTypeCreateFailedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeCreateFailed"
	// TemplateCustomFieldTypeDeleteFailedEvent is the cloudevent subject for TemplateCustomFieldTypeDeleteFailed
	TemplateCustomFieldTypeDeleteFailedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeDeleteFailed"
	// TemplateCustomFieldTypeUpdateFailedEvent is the cloudevent subject for TemplateCustomFieldTypeUpdateFailed
	TemplateCustomFieldTypeUpdateFailedEvent common.EventType = common.EventTypePrefix + "TemplateCustomFieldTypeUpdateFailed"

	/////////////////////////////////////////////////////////////////////////
	// Template
	/////////////////////////////////////////////////////////////////////////
	// TemplateListQueryOp is the queue name for TemplateList query
	TemplateListQueryOp = NatsSubjectTemplate + ".list"
	// TemplateGetQueryOp is the queue name for TemplateGet query
	TemplateGetQueryOp = NatsSubjectTemplate + ".get"
	// TemplateVersionListQueryOp is the queue name for TemplateVersionList query
	TemplateVersionListQueryOp = NatsSubjectTemplate + ".version.list"
	// TemplateVersionGetQueryOp is the queue name for TemplateVersionGet query
	TemplateVersionGetQueryOp = NatsSubjectTemplate + ".version.get"

	// TemplateImportRequestedEvent is the cloudevent subject for TemplateImport
	TemplateImportRequestedEvent common.EventType = common.EventTypePrefix + "TemplateImportRequested"
	// TemplateDeleteRequestedEvent is the cloudevent subject for TemplateDelete
	TemplateDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TemplateDeleteRequested"
	// TemplateUpdateRequestedEvent is the cloudevent subject for TemplateUpdate
	TemplateUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TemplateUpdateRequested"
	// TemplateSyncRequestedEvent is the cloudevent subject for TemplateSync
	TemplateSyncRequestedEvent common.EventType = common.EventTypePrefix + "TemplateSyncRequested"
	// TemplateVersionDisableRequestedEvent is the cloudevent subject for disable request for a template version
	TemplateVersionDisableRequestedEvent common.EventType = common.EventTypePrefix + "Template.Version.DisableRequested"

	// TemplateImportedEvent is the cloudevent subject for TemplateImported
	TemplateImportedEvent common.EventType = common.EventTypePrefix + "TemplateImported"
	// TemplateDeletedEvent is the cloudevent subject for TemplateDeleted
	TemplateDeletedEvent common.EventType = common.EventTypePrefix + "TemplateDeleted"
	// TemplateUpdatedEvent is the cloudevent subject for TemplateUpdated
	TemplateUpdatedEvent common.EventType = common.EventTypePrefix + "TemplateUpdated"
	// TemplateSyncedEvent is the cloudevent subject for TemplateSynced
	TemplateSyncedEvent common.EventType = common.EventTypePrefix + "TemplateSynced"

	// TemplateImportFailedEvent is the cloudevent subject for TemplateImportFailed
	TemplateImportFailedEvent common.EventType = common.EventTypePrefix + "TemplateImportFailed"
	// TemplateDeleteFailedEvent is the cloudevent subject for TemplateDeleteFailed
	TemplateDeleteFailedEvent common.EventType = common.EventTypePrefix + "TemplateDeleteFailed"
	// TemplateUpdateFailedEvent is the cloudevent subject for TemplateUpdateFailed
	TemplateUpdateFailedEvent common.EventType = common.EventTypePrefix + "TemplateUpdateFailed"
	// TemplateSyncFailedEvent is the cloudevent subject for TemplateSyncFailed
	TemplateSyncFailedEvent common.EventType = common.EventTypePrefix + "TemplateSyncFailed"

	// TemplateVersionDisabledEvent is the cloudevent subject for VersionDisabled
	TemplateVersionDisabledEvent common.EventType = common.EventTypePrefix + "Template.Version.Disabled"
	// TemplateVersionDisableFailedEvent is the cloudevent subject for VersionDisableFailed
	TemplateVersionDisableFailedEvent common.EventType = common.EventTypePrefix + "Template.Version.DisableFailed"
)

// Nats Client
type natsTemplateClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsTemplateClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsTemplateClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (TemplateClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct deployment client")
	}
	return &natsTemplateClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// ListSourceTypes returns all template source types
func (client *natsTemplateClient) ListSourceTypes(ctx context.Context, actor Actor) ([]TemplateSourceType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListSourceTypes",
	})

	var request Session = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateSourceTypeListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var templateSourceTypeListModel TemplateSourceTypeListModel
	err = json.Unmarshal(replyCe.Data(), &templateSourceTypeListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template source type list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateSourceTypeListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateSourceTypeListModel.GetTemplateSourceTypes(), nil
}

// GetType returns the template type with the given template type name
func (client *natsTemplateClient) GetType(ctx context.Context, actor Actor, typeName TemplateTypeName) (TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.GetType",
	})

	if len(typeName) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Template type name")
	}

	request := TemplateTypeModel{
		Session: actor.Session(),
		Name:    typeName,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateTypeGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var templateTypeModel TemplateTypeModel
	err = json.Unmarshal(replyCe.Data(), &templateTypeModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template type object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateTypeModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &templateTypeModel, nil
}

// ListTypes returns all template types for the user
func (client *natsTemplateClient) ListTypes(ctx context.Context, actor Actor) ([]TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListTypes",
	})
	request := TemplateTypeModel{
		Session:       actor.Session(),
		ProviderTypes: []TemplateProviderType{},
	}
	return client.listTypes(ctx, logger, request)
}

// ListTypesForProviderType returns all template types that support given provider type
func (client *natsTemplateClient) ListTypesForProviderType(ctx context.Context, actor Actor, providerType TemplateProviderType) ([]TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListTypesForProviderType",
	})
	if len(providerType) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Provider type")
	}
	request := TemplateTypeModel{
		Session: actor.Session(),
		ProviderTypes: []TemplateProviderType{
			providerType,
		},
	}
	return client.listTypes(ctx, logger, request)
}

func (client *natsTemplateClient) listTypes(ctx context.Context, logger *log.Entry, request TemplateTypeModel) ([]TemplateType, error) {
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateTypeListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var templateTypeListModel TemplateTypeListModel
	err = json.Unmarshal(replyCe.Data(), &templateTypeListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template type list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateTypeListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateTypeListModel.GetTemplateTypes(), nil
}

// Create creates a new template type
func (client *natsTemplateClient) CreateType(ctx context.Context, actor Actor, templateType TemplateType) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.CreateType",
	})

	if len(templateType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Name")
	}

	if len(templateType.GetFormats()) == 0 {
		return NewCacaoInvalidParameterError("formats are empty")
	}

	if len(templateType.GetEngine()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Engine")
	}

	if len(templateType.GetProviderTypes()) == 0 {
		return NewCacaoInvalidParameterError("provider types are empty")
	}

	request := TemplateTypeModel{
		Session:       actor.Session(),
		Name:          templateType.GetName(),
		Formats:       templateType.GetFormats(),
		Engine:        templateType.GetEngine(),
		ProviderTypes: templateType.GetProviderTypes(),
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateTypeCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateTypeCreatedEvent,
		TemplateTypeCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateTypeModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template type create response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// UpdateType updates the template type with the template type name
func (client *natsTemplateClient) UpdateType(ctx context.Context, actor Actor, templateType TemplateType) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateType",
	})

	if len(templateType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateType Name")
	}

	if len(templateType.GetFormats()) == 0 {
		return NewCacaoInvalidParameterError("formats are empty")
	}

	if len(templateType.GetEngine()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Engine")
	}

	if len(templateType.GetProviderTypes()) == 0 {
		return NewCacaoInvalidParameterError("provider types are empty")
	}

	request := TemplateTypeModel{
		Session:       actor.Session(),
		Name:          templateType.GetName(),
		Formats:       templateType.GetFormats(),
		Engine:        templateType.GetEngine(),
		ProviderTypes: templateType.GetProviderTypes(),
	}
	return client.updateType(ctx, logger, actor, request)
}

// UpdateTypeFields updates the selected fields of the template type with the template type name
func (client *natsTemplateClient) UpdateTypeFields(ctx context.Context, actor Actor, templateType TemplateType, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateTypeFields",
	})

	if len(templateType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateType Name")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "formats":
			if len(templateType.GetFormats()) == 0 {
				return NewCacaoInvalidParameterError("formats are empty")
			}
		case "engine":
			if len(templateType.GetEngine()) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Engine")
			}
		case "provider_types":
			if len(templateType.GetProviderTypes()) == 0 {
				return NewCacaoInvalidParameterError("provider types are empty")
			}
		default:
			//pass
		}
	}

	request := TemplateTypeModel{
		Session:          actor.Session(),
		Name:             templateType.GetName(),
		Formats:          templateType.GetFormats(),
		Engine:           templateType.GetEngine(),
		ProviderTypes:    templateType.GetProviderTypes(),
		UpdateFieldNames: updateFieldNames,
	}
	return client.updateType(ctx, logger, actor, request)
}

func (client *natsTemplateClient) updateType(ctx context.Context, logger *log.Entry, actor Actor, request TemplateTypeModel) error {
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateTypeUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateTypeUpdatedEvent,
		TemplateTypeUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateTypeModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template type update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// DeleteType deletes the template type with the template type name
func (client *natsTemplateClient) DeleteType(ctx context.Context, actor Actor, typeName TemplateTypeName) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.DeleteType",
	})

	if len(typeName) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Type Name")
	}

	request := TemplateTypeModel{
		Session: actor.Session(),
		Name:    typeName,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateTypeDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateTypeDeletedEvent,
		TemplateTypeDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateTypeModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template type delete response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// GetCustomFieldType returns the template custom field type with the given template custom field type name
func (client *natsTemplateClient) GetCustomFieldType(ctx context.Context, actor Actor, customFieldTypeName string) (TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.GetCustomFieldType",
	})

	if len(customFieldTypeName) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Template custom field type name")
	}

	request := TemplateCustomFieldTypeModel{
		Session: actor.Session(),
		Name:    customFieldTypeName,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateCustomFieldTypeGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var templateCustomFieldTypeModel TemplateCustomFieldTypeModel
	err = json.Unmarshal(replyCe.Data(), &templateCustomFieldTypeModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template custom field type object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateCustomFieldTypeModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &templateCustomFieldTypeModel, nil
}

// QueryCustomFieldType queries to query target defined in the template custom field type with the given name and returns results
func (client *natsTemplateClient) QueryCustomFieldType(ctx context.Context, actor Actor, customFieldTypeName string, queryParams map[string]string) (TemplateCustomFieldTypeQueryResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.QueryCustomFieldType",
	})

	if len(customFieldTypeName) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Template custom field type name")
	}

	request := TemplateCustomFieldTypeModel{
		Session:     actor.Session(),
		Name:        customFieldTypeName,
		QueryParams: queryParams,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateCustomFieldTypeQueryQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var templateCustomFieldTypeQueryResultModel TemplateCustomFieldTypeQueryResultModel
	err = json.Unmarshal(replyCe.Data(), &templateCustomFieldTypeQueryResultModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template custom field type query result object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateCustomFieldTypeQueryResultModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &templateCustomFieldTypeQueryResultModel, nil
}

// ListCustomFieldTypes returns all template custom field types
func (client *natsTemplateClient) ListCustomFieldTypes(ctx context.Context, actor Actor) ([]TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListCustomFieldTypes",
	})
	var request Session = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateCustomFieldTypeListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var templateCustomFieldTypeListModel TemplateCustomFieldTypeListModel
	err = json.Unmarshal(replyCe.Data(), &templateCustomFieldTypeListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template custom field type list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := templateCustomFieldTypeListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return templateCustomFieldTypeListModel.GetTemplateCustomFieldTypes(), nil
}

// CreateCustomFieldType creates a new template custom field type
func (client *natsTemplateClient) CreateCustomFieldType(ctx context.Context, actor Actor, customFieldType TemplateCustomFieldType) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.CreateCustomFieldType",
	})

	if len(customFieldType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType Name")
	}

	if len(customFieldType.GetQueryMethod()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType QueryMethod")
	}

	if len(customFieldType.GetQueryTarget()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType QueryTarget")
	}

	request := TemplateCustomFieldTypeModel{
		Session:                   actor.Session(),
		Name:                      customFieldType.GetName(),
		Description:               customFieldType.GetDescription(),
		QueryMethod:               customFieldType.GetQueryMethod(),
		QueryTarget:               customFieldType.GetQueryTarget(),
		QueryData:                 customFieldType.GetQueryData(),
		QueryResultJSONPathFilter: customFieldType.GetQueryResultJSONPathFilter(),
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateCustomFieldTypeCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateCustomFieldTypeCreatedEvent,
		TemplateCustomFieldTypeCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateCustomFieldTypeModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template custom field type create response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// UpdateCustomFieldType updates the template custom field type with the name
func (client *natsTemplateClient) UpdateCustomFieldType(ctx context.Context, actor Actor, customFieldType TemplateCustomFieldType) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateCustomFieldType",
	})

	if len(customFieldType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType Name")
	}

	if len(customFieldType.GetQueryMethod()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType QueryMethod")
	}

	if len(customFieldType.GetQueryTarget()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType QueryTarget")
	}

	request := TemplateCustomFieldTypeModel{
		Session:                   actor.Session(),
		Name:                      customFieldType.GetName(),
		Description:               customFieldType.GetDescription(),
		QueryMethod:               customFieldType.GetQueryMethod(),
		QueryTarget:               customFieldType.GetQueryTarget(),
		QueryData:                 customFieldType.GetQueryData(),
		QueryResultJSONPathFilter: customFieldType.GetQueryResultJSONPathFilter(),
	}
	return client.updateCustomFieldType(ctx, logger, request)
}

// UpdateCustomFieldTypeFields updates the selected fields of the template custom field type with the name
func (client *natsTemplateClient) UpdateCustomFieldTypeFields(ctx context.Context, actor Actor, customFieldType TemplateCustomFieldType, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateCustomFieldTypeFields",
	})

	if len(customFieldType.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType Name")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(customFieldType.GetName()) == 0 {
				return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType Name")
			}
		case "query_method":
			if len(customFieldType.GetQueryMethod()) == 0 {
				return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType Type")
			}
		case "query_target":
			if len(customFieldType.GetQueryTarget()) == 0 {
				return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType QueryTarget")
			}
		default:
			//pass
		}
	}

	request := TemplateCustomFieldTypeModel{
		Session:                   actor.Session(),
		Name:                      customFieldType.GetName(),
		Description:               customFieldType.GetDescription(),
		QueryMethod:               customFieldType.GetQueryMethod(),
		QueryTarget:               customFieldType.GetQueryTarget(),
		QueryData:                 customFieldType.GetQueryData(),
		QueryResultJSONPathFilter: customFieldType.GetQueryResultJSONPathFilter(),
		UpdateFieldNames:          updateFieldNames,
	}
	return client.updateCustomFieldType(ctx, logger, request)
}

func (client *natsTemplateClient) updateCustomFieldType(ctx context.Context, logger *log.Entry, request TemplateCustomFieldTypeModel) error {
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateCustomFieldTypeUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateCustomFieldTypeUpdatedEvent,
		TemplateCustomFieldTypeUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateCustomFieldTypeModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template custom field type update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// DeleteCustomFieldType deletes the template custom field type with the name
func (client *natsTemplateClient) DeleteCustomFieldType(ctx context.Context, actor Actor, customFieldTypeName string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTCFRegistryClient.DeleteCustomFieldType",
	})

	if len(customFieldTypeName) == 0 {
		return NewCacaoInvalidParameterError("invalid TemplateCustomFieldType Name")
	}

	request := TemplateCustomFieldTypeModel{
		Session: actor.Session(),
		Name:    customFieldTypeName,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateCustomFieldTypeDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateCustomFieldTypeDeletedEvent,
		TemplateCustomFieldTypeDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateCustomFieldTypeModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template custom field type delete response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// Get returns the template with the given template ID
func (client *natsTemplateClient) Get(ctx context.Context, actor Actor, templateID common.ID) (Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Get",
	})

	if len(templateID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid Template ID")
	}

	request := TemplateModel{
		Session: actor.Session(),
		ID:      templateID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var reply TemplateModel
	err = json.Unmarshal(replyCe.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &reply, nil
}

// List returns all templates for the user
func (client *natsTemplateClient) List(ctx context.Context, actor Actor, includeCacaoReservedPurposes bool) ([]Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.List",
	})

	request := TemplateModel{
		Session:                      actor.Session(),
		IncludeCacaoReservedPurposes: includeCacaoReservedPurposes,
	}

	requestCe, err := messaging2.CreateCloudEvent(request, TemplateListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var reply TemplateListModel
	err = json.Unmarshal(replyCe.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return reply.GetTemplates(), nil
}

// Import imports a new template
func (client *natsTemplateClient) Import(ctx context.Context, actor Actor, template Template, credentialID string) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Import",
	})

	templateSource := template.GetSource()

	if len(templateSource.Type) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid Template Source Type")
	}

	if len(templateSource.URI) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid Template Source URI")
	}

	if templateSource.Visibility == TemplateSourceVisibilityPrivate {
		// private repo needs access - check credentialID
		if len(credentialID) == 0 {
			return common.ID(""), NewCacaoInvalidParameterError("invalid Credential ID for private template source")
		}
	}

	templateID := template.GetID()
	if !templateID.Validate() {
		templateID = NewTemplateID()
	}

	request := TemplateModel{
		Session:      actor.Session(),
		ID:           templateID,
		Owner:        actor.Actor,
		Name:         template.GetName(),
		Description:  template.GetDescription(),
		Public:       template.IsPublic(),
		Source:       template.GetSource(),
		Metadata:     template.GetMetadata(),
		UIMetadata:   template.GetUIMetadata(),
		CredentialID: credentialID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateImportRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", err
	}
	eventsSubscribe := []common.EventType{
		TemplateImportedEvent,
		TemplateImportFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return "", err
	}

	var response TemplateModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template import response"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return response.GetPrimaryID(), nil
	}

	logger.Error(serviceError)
	return "", serviceError
}

// Update updates the template with the template ID
func (client *natsTemplateClient) Update(ctx context.Context, actor Actor, template Template) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Update",
	})

	if !template.GetID().Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	if len(template.GetName()) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Name")
	}

	templateSource := template.GetSource()

	if len(templateSource.Type) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Source Type")
	}

	if len(templateSource.URI) == 0 {
		return NewCacaoInvalidParameterError("invalid Template Source URI")
	}

	request := TemplateModel{
		Session:     actor.Session(),
		ID:          template.GetID(),
		Name:        template.GetName(),
		Description: template.GetDescription(),
		Public:      template.IsPublic(),
		Source:      template.GetSource(),
	}
	return client.updateTemplate(ctx, logger, request)
}

// UpdateFields updates the selected fields of the template with the template ID
func (client *natsTemplateClient) UpdateFields(ctx context.Context, actor Actor, template Template, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.UpdateFields",
	})

	if !template.GetID().Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(template.GetName()) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Name")
			}
		case "source":
			templateSource := template.GetSource()

			if len(templateSource.Type) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Source Type")
			}

			if len(templateSource.URI) == 0 {
				return NewCacaoInvalidParameterError("invalid Template Source URI")
			}
		default:
			//pass
		}
	}

	request := TemplateModel{
		Session:          actor.Session(),
		ID:               template.GetID(),
		Name:             template.GetName(),
		Description:      template.GetDescription(),
		Public:           template.IsPublic(),
		Source:           template.GetSource(),
		UpdateFieldNames: updateFieldNames,
	}
	return client.updateTemplate(ctx, logger, request)
}
func (client *natsTemplateClient) updateTemplate(ctx context.Context, logger *log.Entry, request TemplateModel) error {
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateUpdatedEvent,
		TemplateUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// Sync syncs the template with the template ID
func (client *natsTemplateClient) Sync(ctx context.Context, actor Actor, templateID common.ID, credentialID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Sync",
	})

	if !templateID.Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	// credentialID may be empty if the template source is public

	request := TemplateModel{
		Session:      actor.Session(),
		ID:           templateID,
		CredentialID: credentialID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateSyncRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateSyncedEvent,
		TemplateSyncFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template sync response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// Delete deletes the template with the template ID
func (client *natsTemplateClient) Delete(ctx context.Context, actor Actor, templateID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.Delete",
	})

	if !templateID.Validate() {
		return NewCacaoInvalidParameterError("invalid Template ID")
	}

	request := TemplateModel{
		Session: actor.Session(),
		ID:      templateID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateDeletedEvent,
		TemplateDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template delete response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// GetTemplateVersion returns the template version with the given ID
func (client *natsTemplateClient) GetTemplateVersion(ctx context.Context, actor Actor, versionID common.ID) (*TemplateVersion, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.GetTemplateVersion",
	})

	if len(versionID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid TemplateVersion ID")
	}

	request := TemplateVersionModel{
		Session: actor.Session(),
		TemplateVersion: TemplateVersion{
			ID: versionID,
		},
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TemplateVersionGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var reply TemplateVersionModel
	err = json.Unmarshal(replyCe.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template version object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &reply.TemplateVersion, nil
}

// ListTemplateVersions returns all template versions for a template that is accessible by the user
func (client *natsTemplateClient) ListTemplateVersions(ctx context.Context, actor Actor, templateID common.ID) ([]TemplateVersion, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.ListTemplateVersions",
	})

	request := TemplateVersionModel{
		Session: actor.Session(),
		TemplateVersion: TemplateVersion{
			TemplateID: templateID,
		},
	}

	requestCe, err := messaging2.CreateCloudEvent(request, TemplateVersionListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var reply TemplateVersionListModel
	err = json.Unmarshal(replyCe.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to template version list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return reply.Versions, nil
}

// DisableTemplateVersion disable a template version prevent it from being used by new deployment and new runs.
func (client *natsTemplateClient) DisableTemplateVersion(ctx context.Context, actor Actor, versionID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTemplateClient.DisableTemplateVersion",
	})

	if !versionID.Validate() {
		return NewCacaoInvalidParameterError("invalid Template Version ID")
	}

	var request = TemplateVersionDisableRequest{
		Session:   actor.Session(),
		VersionID: versionID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TemplateVersionDisableRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TemplateVersionDisabledEvent,
		TemplateVersionDisableFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TemplateVersionDisableResponse
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a template version disable response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}
