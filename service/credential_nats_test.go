package service

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCredentialUpdate_NeedToUpdate(t *testing.T) {
	type fields struct {
		Name            *string
		Value           *string
		Description     *string
		Disabled        *bool
		Visibility      *VisibilityType
		UpdateOrAddTags map[string]string
		DeleteTags      map[string]struct{}
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name:   "empty struct",
			fields: fields{},
			want:   false,
		},
		{
			name: "name",
			fields: fields{
				Name: func() *string { var s = "string123"; return &s }(),
			},
			want: true,
		},
		{
			name: "value",
			fields: fields{
				Value: func() *string { var s = "string123"; return &s }(),
			},
			want: true,
		},
		{
			name: "description",
			fields: fields{
				Description: func() *string { var s = "string123"; return &s }(),
			},
			want: true,
		},
		{
			name: "UpdateOrAddTags empty non-nil",
			fields: fields{
				UpdateOrAddTags: map[string]string{},
			},
			want: false,
		},
		{
			name: "UpdateOrAddTags non-empty",
			fields: fields{
				UpdateOrAddTags: map[string]string{
					"foo": "",
				},
			},
			want: true,
		},
		{
			name: "DeleteTags empty non-nil",
			fields: fields{
				DeleteTags: map[string]struct{}{},
			},
			want: false,
		},
		{
			name: "DeleteTags non-empty",
			fields: fields{
				DeleteTags: map[string]struct{}{
					"foo": {},
				},
			},
			want: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			update := CredentialUpdate{
				Name:            tt.fields.Name,
				Value:           tt.fields.Value,
				Description:     tt.fields.Description,
				Disabled:        tt.fields.Disabled,
				Visibility:      tt.fields.Visibility,
				UpdateOrAddTags: tt.fields.UpdateOrAddTags,
				DeleteTags:      tt.fields.DeleteTags,
			}
			assert.Equalf(t, tt.want, update.NeedToUpdate(), "NeedToUpdate()")
		})
	}
}
