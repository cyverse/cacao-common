package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats Subjects root, get, list and other channels
const (
	NatsSubjectUsers                common.QueryOp = common.NatsQueryOpPrefix + "users"
	NatsSubjectUsersGet             common.QueryOp = NatsSubjectUsers + ".get"
	NatsSubjectUsersGetWithSettings common.QueryOp = NatsSubjectUsers + ".getwithsettings"
	NatsSubjectUsersList            common.QueryOp = NatsSubjectUsers + ".list"

	EventUserLogin common.EventType = common.EventTypePrefix + "UserLogin"

	EventUserAddRequested    common.EventType = common.EventTypePrefix + "UserAddRequested"
	EventUserUpdateRequested common.EventType = common.EventTypePrefix + "UserUpdateRequested"
	EventUserDeleteRequested common.EventType = common.EventTypePrefix + "UserDeleteRequested"

	EventUserAdded   common.EventType = common.EventTypePrefix + "UserAdded"
	EventUserUpdated common.EventType = common.EventTypePrefix + "UserUpdated"
	EventUserDeleted common.EventType = common.EventTypePrefix + "UserDeleted"

	EventUserAddError    common.EventType = common.EventTypePrefix + "UserAddError"
	EventUserUpdateError common.EventType = common.EventTypePrefix + "UserUpdateError"
	EventUserDeleteError common.EventType = common.EventTypePrefix + "UserDeleteError"
)

// This object should never be serialized/marshalled
type natsUserClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsUserClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsUserClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (UserClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct deployment client")
	}
	return &natsUserClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// Add creates a new user
func (client *natsUserClient) Add(ctx context.Context, actor Actor, user UserModel) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Add",
	}).Trace("called")
	return client.publishRequestEvent(ctx, actor, user, EventUserAddRequested)
}

// Update updates a user's attributes.
func (client *natsUserClient) Update(ctx context.Context, actor Actor, user UserModel) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Update",
	}).Trace("called")
	return client.publishRequestEvent(ctx, actor, user, EventUserUpdateRequested)
}

// Update updates a user's attributes.
func (client *natsUserClient) Update1(ctx context.Context, actor Actor, username string, update UserUpdate) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Update",
	}).Trace("called")
	//client.publishRequestEvent(ctx, actor, UserUpdateRequest{
	//	Session: Session{},
	//	Update:  UserUpdate{},
	//}, EventUserUpdateRequested)
	eventOpType := EventUserUpdateRequested
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.publishRequestEvent",
	})
	logger.WithFields(log.Fields{
		"username":    username,
		"eventOpType": string(eventOpType),
	}).Trace("publishRequestEvent() called")

	if username == "" {
		return NewCacaoInvalidParameterErrorWithOptions(UserUsernameNotSetError)
	}
	request := UserUpdateRequest{
		Session:  actor.Session(),
		Username: username,
		Update:   update,
	}
	tid := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, eventOpType, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return err
	}
	// listens to different event type depends on event operation
	var etList = []common.EventType{EventUserUpdated, EventUserUpdateError}
	promise, err := client.eventConn.Request(requestCe, etList)
	if err != nil {
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return err
	}
	var response UserModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// save will do the logic for saving a user whether it's a create or update, but also allow for async operation
func (client *natsUserClient) publishRequestEvent(ctx context.Context, actor Actor, user UserModel, eventOpType common.EventType) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.publishRequestEvent",
	})
	logger.WithFields(log.Fields{
		"username":    user.Username,
		"eventOpType": string(eventOpType),
	}).Trace("publishRequestEvent() called")

	if user.Username == "" {
		return NewCacaoInvalidParameterError(UserUsernameNotSetError)
	}
	user.Session = actor.Session()
	tid := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(user, eventOpType, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return err
	}
	// listens to different event type depends on event operation
	var etList []common.EventType
	switch eventOpType {
	case EventUserAddRequested:
		etList = []common.EventType{EventUserAdded, EventUserAddError}
	case EventUserUpdateRequested:
		etList = []common.EventType{EventUserUpdated, EventUserUpdateError}
	case EventUserDeleteRequested:
		etList = []common.EventType{EventUserDeleted, EventUserDeleteError}
	default:
		panic("unknown op") // unknown op must not be passed
	}
	promise, err := client.eventConn.Request(requestCe, etList)
	if err != nil {
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return err
	}
	var response UserModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// this does NOT wait for any response event, just publish the event.
func (client *natsUserClient) publishEvent(ctx context.Context, actor Actor, user UserModel, eventOpType common.EventType) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.publishEvent",
	})
	logger.WithFields(log.Fields{
		"username":    user.Username,
		"eventOpType": string(eventOpType),
	}).Trace("publishRequestEvent() called")

	if user.Username == "" {
		return NewCacaoInvalidParameterError(UserUsernameNotSetError)
	}
	user.Session = actor.Session()
	tid := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(user, eventOpType, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return err
	}
	logger.Trace("async publishing the event " + eventOpType)
	err = client.eventConn.Publish(requestCe)
	if err != nil {
		errorMessage := "unable to publish a update user event"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoCommunicationError(errorMessage)
	}
	return nil
}

// Get fetches a user by username. Only admin can fetch other user.
func (client *natsUserClient) Get(ctx context.Context, actor Actor, username string, withSettings bool) (*UserModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Get",
	})
	logger.Trace("called")

	if username == "" {
		return nil, NewCacaoInvalidParameterError(UserUsernameNotSetError)
	}

	user := UserModel{
		Session:  actor.Session(),
		Username: username,
	}
	subject := NatsSubjectUsersGet
	if withSettings {
		subject = NatsSubjectUsersGetWithSettings
	}
	requestCe, err := messaging2.CreateCloudEvent(user, subject, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}
	err = json.Unmarshal(replyCe.Data(), &user)
	if err != nil {
		errorMsg := "fail to unmarshal to user object"
		logger.WithError(err).Error(errorMsg)
		return nil, NewCacaoMarshalError(errorMsg)
	}
	// try to use service error first, fall back to ErrorType
	serviceError := user.GetServiceError()
	if serviceError != nil {
		return nil, serviceError
	}
	// FIXME migrate from ErrorType to service error
	if user.ErrorType != "" {
		return nil, errors.New(user.ErrorType)
	}
	return &user, err
}

// Delete deletes a user by username.
func (client *natsUserClient) Delete(ctx context.Context, actor Actor, user UserModel) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Delete",
	}).Trace("called")
	return client.publishRequestEvent(ctx, actor, user, EventUserDeleteRequested)
}

// UserHasLoggedIn publish an event that indicate the user has logged in.
func (client *natsUserClient) UserHasLoggedIn(ctx context.Context, actor Actor, user UserModel) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Login",
	}).Trace("called")
	return client.publishRequestEvent(ctx, actor, user, EventUserLogin)
}

// Search searches for a list of users by filter.
func (client *natsUserClient) Search(ctx context.Context, actor Actor, filter UserListFilter) (UserList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.Search",
	})
	logger.Trace("started")
	ul := &UserListModel{
		Session: actor.Session(),
		Filter:  filter,
	}

	err := client.loadList(ctx, ul)
	if err != nil {
		ul = nil
	}
	return ul, err
}

// SearchNext continue an existing search.
func (client *natsUserClient) SearchNext(ctx context.Context, actor Actor, ul UserList) error {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.SearchNext",
	})
	logger.Trace("natsUserClient.SearchNext() started")

	var ulm = ul.(*UserListModel)

	if ulm.NextStart < 0 {
		return NewCacaoInvalidParameterError(UserListLoadBoundaryError)
	}

	// just set the filter index to the new starting point
	ulm.Filter.Start = ulm.NextStart

	return client.loadList(ctx, ulm)
}

func (client *natsUserClient) loadList(ctx context.Context, ul *UserListModel) error {
	logger := log.WithFields(log.Fields{
		"adapters": "cacao-common.service",
		"function": "natsUserClient.loadList",
	})
	logger.Trace("called")

	if ul.Filter == (UserListFilter{}) { // compare with an uninitialized UserListFilter
		// we do not currently support dumping the entire user list this way.
		return NewCacaoInvalidParameterError(UserListFilterNotSetError)
	}
	if ul.Filter.Value == "" {
		logger.WithField("field", ul.Filter.Field).Warning(UserListFilterValueEmptyWarning)
	}
	if ul.Filter.MaxItems < 1 {
		return NewCacaoInvalidParameterError(UserListFilterInvalidMaxItemsError)
	}
	if ul.Filter.Start < 0 {
		return NewCacaoInvalidParameterError(UserListFilterInvalidStartIndexError)
	}
	if ul.Filter.SortBy == 0 { // default sort
		ul.Filter.SortBy = AscendingSort
	}

	requestCe, err := messaging2.CreateCloudEvent(ul, NatsSubjectUsersList, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	// TODO consider streaming replies (accept multiple replies instead of one)
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	err = json.Unmarshal(replyCe.Data(), ul)
	if err != nil {
		errorMsg := "fail to unmarshal to user list"
		logger.WithError(err).Error(errorMsg)
		return NewCacaoMarshalError(errorMsg)
	}
	return ul.Session.GetServiceError()
}
