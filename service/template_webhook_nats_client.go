package service

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

const (
	// NatsSubjectTemplateWebhook is the prefix of the queue name for Template Webhook queries
	NatsSubjectTemplateWebhook common.QueryOp = common.NatsQueryOpPrefix + "template.webhook"

	// TemplateWebhookListQueryOp is the queue name for TemplateWebhookList query
	TemplateWebhookListQueryOp = NatsSubjectTemplateWebhook + ".list"

	// TemplateWebhookCreateRequestedEvent  is the cloudevent subject for TemplateWebhookCreationRequest
	TemplateWebhookCreateRequestedEvent common.EventType = common.EventTypePrefix + "Template.Webhook.CreateRequested"
	// TemplateWebhookDeleteRequestedEvent is the cloudevent subject for TemplateWebhookDeletionRequest
	TemplateWebhookDeleteRequestedEvent common.EventType = common.EventTypePrefix + "Template.Webhook.DeleteRequested"

	// TemplateWebhookCreatedEvent is the cloudevent subject for TemplateWebhookCreationResponse
	TemplateWebhookCreatedEvent common.EventType = common.EventTypePrefix + "Template.Webhook.Created"
	// TemplateWebhookDeletedEvent is the cloudevent subject for TemplateWebhookDeletionResponse
	TemplateWebhookDeletedEvent common.EventType = common.EventTypePrefix + "Template.Webhook.Deleted"

	// TemplateWebhookCreateFailedEvent is the cloudevent subject for TemplateWebhookCreationResponse
	TemplateWebhookCreateFailedEvent common.EventType = common.EventTypePrefix + "Template.Webhook.CreateFailed"
	// TemplateWebhookDeleteFailedEvent is the cloudevent subject for TemplateWebhookDeletionResponse
	TemplateWebhookDeleteFailedEvent common.EventType = common.EventTypePrefix + "Template.Webhook.DeleteFailed"
)

// TemplateWebhookClient is interface for client that interact with TemplateWebhookService
type TemplateWebhookClient interface {
	List(ctx context.Context, actor Actor, user string, platform TemplateWebhookPlatformType) ([]TemplateWebhook, error)
	Create(ctx context.Context, actor Actor, webhook TemplateWebhook) (TemplateWebhook, error)
	Delete(ctx context.Context, actor Actor, templateID common.ID) error
	// TODO add an new operation to rotate secret
}

type natsWebhookClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsTemplateWebhookClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsTemplateWebhookClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (TemplateWebhookClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct template webhook client")
	}
	return &natsWebhookClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// List template webhooks owned by a user
func (n *natsWebhookClient) List(ctx context.Context, actor Actor, user string, platform TemplateWebhookPlatformType) ([]TemplateWebhook, error) {
	ce, err := messaging2.CreateCloudEvent(TemplateWebhookListRequest{
		Session:  actor.Session(),
		Owner:    user,
		Template: "",
		Platform: platform,
	}, TemplateWebhookListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		return nil, err
	}
	relyCe, err := n.queryConn.Request(ctx, ce)
	if err != nil {
		return nil, err
	}
	var reply TemplateWebhookListReply
	err = json.Unmarshal(relyCe.Data(), &reply)
	if err != nil {
		return nil, err
	}
	return reply.Webhooks, reply.GetServiceError()
}

// Create ...
func (n *natsWebhookClient) Create(ctx context.Context, actor Actor, webhook TemplateWebhook) (TemplateWebhook, error) {
	tid := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(TemplateWebhookCreationRequest{
		Session:         actor.Session(),
		TemplateWebhook: webhook,
	}, TemplateWebhookCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return TemplateWebhook{}, err
	}
	promise, err := n.eventConn.Request(requestCe, []common.EventType{TemplateWebhookCreatedEvent, TemplateWebhookCreateFailedEvent})
	if err != nil {
		return TemplateWebhook{}, err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return TemplateWebhook{}, err
	}
	var response TemplateWebhookCreationResponse
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		return TemplateWebhook{}, err
	}
	return response.TemplateWebhook, response.GetServiceError()
}

// Delete ...
func (n *natsWebhookClient) Delete(ctx context.Context, actor Actor, templateID common.ID) error {
	tid := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(TemplateWebhookDeletionRequest{
		Session: actor.Session(),
		TemplateWebhook: TemplateWebhook{
			TemplateID: templateID,
		},
	}, TemplateWebhookDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return err
	}
	promise, err := n.eventConn.Request(requestCe, []common.EventType{TemplateWebhookDeletedEvent, TemplateWebhookDeleteFailedEvent})
	if err != nil {
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return err
	}
	var response TemplateWebhookDeletionResponse
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		return err
	}
	return response.GetServiceError()
}
