// Package service - this package file will contain base service interfaces and structs for embedding into other service packages
package service

// SessionContext is the base interface for objects for microservices, it enables actor, emulator and error information
// to be passed around between service and its client as part of the object.
type SessionContext interface {
	GetSessionActor() string    // username of the person making requests
	GetSessionEmulator() string // set if the Actor's is being emulated by another user, presumably an admin
	SetSessionActor(string)
	SetSessionEmulator(string)
	GetError() (string, string) // gets the error type and message
	GetServiceError() CacaoError
}

// Session implements SessionContext.
// ErrorType & ErrorMessage fields are the old way of passing error, there is only a few service that still uses it to signal error.
// We will migrate everything to using ServiceError.
type Session struct {
	SessionActor    string `json:"actor"`
	SessionEmulator string `json:"emulator,omitempty"`
	// Deprecated: use ServiceError instead
	ErrorType string `json:"error_type,omitempty"` // the type of error
	// Deprecated: use ServiceError instead
	ErrorMessage string         `json:"error_message,omitempty"` // custom or contextual error message
	ServiceError CacaoErrorBase `json:"service_error,omitempty"` // Ultimately, this will replace ErrorType and ErrorMessage fields above.
}

// SetSessionActor ...
func (b *Session) SetSessionActor(s string) {
	b.SessionActor = s
}

// SetSessionEmulator ...
func (b *Session) SetSessionEmulator(s string) {
	b.SessionEmulator = s
}

// GetSessionActor ...
func (b Session) GetSessionActor() string {
	return b.SessionActor
}

// GetSessionEmulator ...
func (b Session) GetSessionEmulator() string {
	return b.SessionEmulator
}

// GetError returns error type & msg.
// Note this is the old way of passing error from service to client.
// Deprecated: use GetServiceError instead.
func (b Session) GetError() (string, string) {
	return b.ErrorType, b.ErrorMessage
}

// GetServiceError returns the service error (which implements built-in error interface).
// This is the newer way to passing error from service to client.
func (b Session) GetServiceError() CacaoError {
	if len(b.ServiceError.StandardMessage) == 0 {
		// no error
		return nil
	}

	cerr, err := convertBaseToCacaoError(b.ServiceError)
	if err != nil {
		// CacaoErrorBase type
		return &b.ServiceError
	}

	// specific Cacao*Error type
	return cerr
}

// Actor is the user that performs certain action.
// This is for scenario where you only need to express the user that initiate actions without including error like Session does.
type Actor struct {
	Actor    string
	Emulator string
}

// IsSet returns true if actor is set
func (a Actor) IsSet() bool {
	return a.Actor != ""
}

// Session returns a Session object that copy SessionActor and SessionEmulator fields from Actor.
// This is useful for service client to convert Actor to Session in request.
func (a Actor) Session() Session {
	return Session{
		SessionActor:    a.Actor,
		SessionEmulator: a.Emulator,
	}
}

// ActorFromSession construct Actor from service.Session
func ActorFromSession(session Session) Actor {
	return Actor{
		Actor:    session.GetSessionActor(),
		Emulator: session.GetSessionEmulator(),
	}
}

// CopySessionActors copies over the actor and emulator in Session.
// This is useful when copying actors from request to response.
func CopySessionActors(session Session) Session {
	return Session{
		SessionActor:    session.SessionActor,
		SessionEmulator: session.SessionEmulator,
	}
}
