package service

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/cyverse/cacao-common/db"
	"go.mongodb.org/mongo-driver/bson"
)

// TemplateUIKeyVal is a struct for representing key-value formatted values.
type TemplateUIKeyVal struct {
	Key   string      `json:"key,omitempty"`
	Value interface{} `json:"value,omitempty"`
}

// TemplateUIField is a struct for a ui wizard field in UI metadata
type TemplateUIField struct {
	Name       string             `json:"name,omitempty"`
	FieldType  string             `json:"field_type,omitempty"`
	UILabel    string             `json:"ui_label,omitempty"`
	HelpText   string             `json:"help_text,omitempty"`
	Parameters []TemplateUIKeyVal `json:"parameters,omitempty"`
}

// GetParametersMap returns a map for Parameters array
func (field TemplateUIField) GetParametersMap() map[string]interface{} {
	parametersMap := map[string]interface{}{}
	for _, parameter := range field.Parameters {
		parametersMap[parameter.Key] = parameter.Value
	}

	return parametersMap
}

// TemplateUIGroupType defines group type.
type TemplateUIGroupType string

// Enumeration constants for TemplateUIGroupType.
const (
	// TemplateUIGroupTypeUnknown indicates that the group type is unspecified.
	TemplateUIGroupTypeUnknown TemplateUIGroupType = ""

	// TemplateUIGroupTypeGroup indicates that the group is a simple group.
	TemplateUIGroupTypeGroup TemplateUIGroupType = "group"
	// TemplateUIGroupTypeAdvancedSettings indicates that the group is an advanced settings.
	TemplateUIGroupTypeAdvancedSettings TemplateUIGroupType = "advanced_settings"
	// TemplateUIGroupTypeRow indicates that the group is a row.
	TemplateUIGroupTypeRow TemplateUIGroupType = "row"
)

// String returns the string representation of a TemplateUIGroupType.
func (t TemplateUIGroupType) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateUIGroupType.
func (t TemplateUIGroupType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateUIGroupType to the appropriate enumeration constant.
func (t *TemplateUIGroupType) UnmarshalJSON(b []byte) error {
	// Template purposes are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateUIGroupTypeUnknown
	case string(TemplateUIGroupTypeGroup):
		*t = TemplateUIGroupTypeGroup
	case string(TemplateUIGroupTypeAdvancedSettings):
		*t = TemplateUIGroupTypeAdvancedSettings
	case string(TemplateUIGroupTypeRow):
		*t = TemplateUIGroupTypeRow
	default:
		return fmt.Errorf("invalid template UI group type: %s", s)
	}

	return nil
}

// TemplateUIGroup is a struct for a ui wizard group in UI metadata
type TemplateUIGroup struct {
	Type        TemplateUIGroupType   `json:"type,omitempty"`
	Title       string                `json:"title,omitempty"`
	HelpText    string                `json:"help_text,omitempty"`
	Collapsible bool                  `json:"collapsible,omitempty"`
	Toggleable  bool                  `json:"toggleable,omitempty"`
	Items       []TemplateUIGroupItem `json:"items,omitempty"`
}

// TemplateUIRowType defines row type.
type TemplateUIRowType string

// Enumeration constants for TemplateUIRowType.
const (
	// TemplateUIRowTypeUnknown indicates that the row type is unspecified.
	TemplateUIRowTypeUnknown TemplateUIRowType = ""

	// TemplateUIRowTypeRow indicates that the row is a simple row.
	TemplateUIRowTypeRow TemplateUIRowType = "row"
)

// String returns the string representation of a TemplateUIRowType.
func (t TemplateUIRowType) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateUIRowType.
func (t TemplateUIRowType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateUIRowType to the appropriate enumeration constant.
func (t *TemplateUIRowType) UnmarshalJSON(b []byte) error {
	// Template purposes are represented as strings in JSON.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateUIRowTypeUnknown
	case string(TemplateUIRowTypeRow):
		*t = TemplateUIRowTypeRow
	default:
		return fmt.Errorf("invalid template UI row type: %s", s)
	}

	return nil
}

// TemplateUIRow is a struct for a ui wizard row in UI metadata
type TemplateUIRow struct {
	Type   TemplateUIRowType `json:"type,omitempty"`
	Fields []TemplateUIField `json:"fields,omitempty"`
}

// TemplateUIGroupItem is one of TemplateUIField, TemplateUIGroup, or TemplateUIRow
type TemplateUIGroupItem struct {
	item interface{}
}

// IsGroup returns true if the group item is group type
func (item *TemplateUIGroupItem) IsGroup() bool {
	switch item.item.(type) {
	case TemplateUIGroup, *TemplateUIGroup:
		return true
	default:
		return false
	}
}

// GetGroup returns group item as *TemplateUIGroup type
func (item *TemplateUIGroupItem) GetGroup() *TemplateUIGroup {
	switch item.item.(type) {
	case TemplateUIGroup:
		group := item.item.(TemplateUIGroup)
		return &group
	case *TemplateUIGroup:
		group := item.item.(*TemplateUIGroup)
		return group
	default:
		return nil
	}
}

// IsField returns true if the group item is field type
func (item *TemplateUIGroupItem) IsField() bool {
	switch item.item.(type) {
	case TemplateUIField, *TemplateUIField:
		return true
	default:
		return false
	}
}

// GetField returns group item as *TemplateUIField type
func (item *TemplateUIGroupItem) GetField() *TemplateUIField {
	switch item.item.(type) {
	case TemplateUIField:
		field := item.item.(TemplateUIField)
		return &field
	case *TemplateUIField:
		field := item.item.(*TemplateUIField)
		return field
	default:
		return nil
	}
}

// IsRow returns true if the group item is row type
func (item *TemplateUIGroupItem) IsRow() bool {
	switch item.item.(type) {
	case TemplateUIRow, *TemplateUIRow:
		return true
	default:
		return false
	}
}

// GetRow returns group item as *TemplateUIRow type
func (item *TemplateUIGroupItem) GetRow() *TemplateUIRow {
	switch item.item.(type) {
	case TemplateUIRow:
		row := item.item.(TemplateUIRow)
		return &row
	case *TemplateUIRow:
		row := item.item.(*TemplateUIRow)
		return row
	default:
		return nil
	}
}

// MarshalJSON returns the JSON representation of a TemplateUIGroupItem.
func (item *TemplateUIGroupItem) MarshalJSON() ([]byte, error) {
	switch item.item.(type) {
	case TemplateUIGroup, *TemplateUIGroup, TemplateUIField, *TemplateUIField, TemplateUIRow, *TemplateUIRow:
		return json.Marshal(item.item)
	default:
		return nil, fmt.Errorf("field value (%v) is not supported types", item.item)
	}
}

// UnmarshalJSON converts the JSON representation of a TemplateUIGroupItem to the appropriate object.
func (item *TemplateUIGroupItem) UnmarshalJSON(b []byte) error {
	var bmap map[string]interface{}
	err := json.Unmarshal(b, &bmap)
	if err != nil {
		return fmt.Errorf("json value (%v) cannot be unmarshalled to map", string(b))
	}

	if _, ok := bmap["name"]; ok {
		// field
		var field TemplateUIField
		err := json.Unmarshal(b, &field)
		if err != nil {
			return fmt.Errorf("json value (%v) cannot be unmarshalled to ui field", string(b))
		}
		item.item = field
		return nil
	}

	if _, ok := bmap["type"]; ok {
		if _, ok2 := bmap["fields"]; ok2 {
			// row
			var row TemplateUIRow
			err = json.Unmarshal(b, &row)
			if err != nil {
				return fmt.Errorf("json value (%v) cannot be unmarshalled to ui row", string(b))
			}
			item.item = row
			return nil
		}
	}

	if _, ok := bmap["type"]; ok {
		if _, ok2 := bmap["items"]; ok2 {
			// group
			var group TemplateUIGroup
			err = json.Unmarshal(b, &group)
			if err != nil {
				return fmt.Errorf("json value (%v) cannot be unmarshalled to ui group", string(b))
			}
			item.item = group
			return nil
		}
	}

	return fmt.Errorf("json value (%v) cannot be unmarshalled, type not supported", string(b))
}

// MarshalBSON returns the BSON representation of a TemplateUIGroupItem.
func (item TemplateUIGroupItem) MarshalBSON() ([]byte, error) {
	switch item.item.(type) {
	case TemplateUIGroup, *TemplateUIGroup, TemplateUIField, *TemplateUIField, TemplateUIRow, *TemplateUIRow:
		registry, err := db.GetBSONRegistry()
		if err != nil {
			return nil, err
		}
		return bson.MarshalWithRegistry(registry, item.item)
	default:
		return nil, fmt.Errorf("field value (%v) is not supported types", item.item)
	}
}

// UnmarshalBSON converts the BSON representation of a TemplateUIGroupItem to the appropriate object.
func (item *TemplateUIGroupItem) UnmarshalBSON(b []byte) error {
	registry, err := db.GetBSONRegistry()
	if err != nil {
		return err
	}

	var bmap map[string]interface{}
	err = bson.UnmarshalWithRegistry(registry, b, &bmap)
	if err != nil {
		return fmt.Errorf("bson value (%v) cannot be unmarshalled to map", string(b))
	}

	if _, ok := bmap["name"]; ok {
		// field
		var field TemplateUIField
		err := bson.UnmarshalWithRegistry(registry, b, &field)
		if err != nil {
			return fmt.Errorf("bson value (%v) cannot be unmarshalled to ui field", string(b))
		}
		item.item = field
		return nil
	}

	if _, ok := bmap["type"]; ok {
		if _, ok2 := bmap["fields"]; ok2 {
			// row
			var row TemplateUIRow
			err = bson.UnmarshalWithRegistry(registry, b, &row)
			if err != nil {
				return fmt.Errorf("bson value (%v) cannot be unmarshalled to ui row", string(b))
			}
			item.item = row
			return nil
		}
	}

	if _, ok := bmap["type"]; ok {
		if _, ok2 := bmap["items"]; ok2 {
			// group
			var group TemplateUIGroup
			err = bson.UnmarshalWithRegistry(registry, b, &group)
			if err != nil {
				return fmt.Errorf("bson value (%v) cannot be unmarshalled to ui group", string(b))
			}
			item.item = group
			return nil
		}
	}

	return fmt.Errorf("bson value (%v) cannot be unmarshalled, type not supported", string(b))
}

// TemplateUIStep is a struct for a ui wizard step in UI metadata
type TemplateUIStep struct {
	Title    string                `json:"title,omitempty"`
	HelpText string                `json:"help_text,omitempty"`
	Items    []TemplateUIGroupItem `json:"items,omitempty"`
}

// TemplateUIMetadata is a struct for template UI metadata
type TemplateUIMetadata struct {
	SchemaURL     string           `json:"schema_url,omitempty"`
	SchemaVersion string           `json:"schema_version,omitempty"`
	Author        string           `json:"author,omitempty"`
	AuthorEmail   string           `json:"author_email,omitempty"`
	Description   string           `json:"description,omitempty"`
	DocURL        string           `json:"doc_url,omitempty"`
	Steps         []TemplateUIStep `json:"steps,omitempty"`
}
