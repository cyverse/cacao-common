package service

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/db"
	"go.mongodb.org/mongo-driver/bson"
)

func TestUserActionItem_Conversion(t *testing.T) {
	jsonBytes := `{
		"method": "script",
		"source_type": "url",
		"data_type": "text/x-shellscript",
		"data":"https://www.shellscript.sh/eg/first.sh.txt"
	}`

	var actionItem UserActionItem

	err := json.Unmarshal([]byte(jsonBytes), &actionItem)
	assert.NoError(t, err)

	assert.True(t, actionItem.IsScriptAction())
}

func TestUserActionItem_BSON(t *testing.T) {
	jsonBytes := `{
		"method": "script",
		"source_type": "url",
		"data_type": "text/x-shellscript",
		"data": "https://www.shellscript.sh/eg/first.sh.txt"
	}`

	var actionItem UserActionItem

	err := json.Unmarshal([]byte(jsonBytes), &actionItem)
	assert.NoError(t, err)

	assert.True(t, actionItem.IsScriptAction())
	scriptAction := actionItem.GetScriptAction()

	assert.Equal(t, "https://www.shellscript.sh/eg/first.sh.txt", scriptAction.Data)

	registry, err := db.GetBSONRegistry()
	assert.NoError(t, err)

	bsonBytes, err := bson.MarshalWithRegistry(registry, actionItem)
	assert.NoError(t, err)

	var actionItem2 UserActionItem
	err = bson.UnmarshalWithRegistry(registry, bsonBytes, &actionItem2)
	assert.NoError(t, err)

	assert.True(t, actionItem2.IsScriptAction())
	scriptAction2 := actionItem2.GetScriptAction()

	assert.Equal(t, UserActionMethodScript, scriptAction2.Method)
	assert.Equal(t, UserActionScriptSourceTypeURL, scriptAction2.SourceType)
	assert.Equal(t, UserActionScriptDataTypeShellScript, scriptAction2.DataType)
	assert.Equal(t, "https://www.shellscript.sh/eg/first.sh.txt", scriptAction2.Data)
}

func TestUserAction_Conversion(t *testing.T) {
	jsonBytes := `{
		"id": "useraction_akl23jikfjiev",
		"owner": "test_user",
		"name": "script",
		"public": true,
		"type": "instance_execution",
		"action": {
			"method": "script",
			"source_type": "url",
			"data_type": "text/x-shellscript",
			"data": "https://www.shellscript.sh/eg/first.sh.txt"
		},
		"created_at": "0001-01-01T00:00:00Z",
		"updated_at": "0001-01-01T00:00:00Z"
	}`

	var model UserActionModel

	err := json.Unmarshal([]byte(jsonBytes), &model)
	assert.NoError(t, err)

	assert.Equal(t, "script", model.Name)
	assert.True(t, model.Action.IsScriptAction())
	scriptAction := model.Action.GetScriptAction()

	assert.Equal(t, "https://www.shellscript.sh/eg/first.sh.txt", scriptAction.Data)
}

func TestUserAction_BSON(t *testing.T) {
	jsonBytes := `{
		"id": "useraction_akl23jikfjiev",
		"owner": "test_user",
		"name": "script",
		"public": true,
		"type": "instance_execution",
		"action": {
			"method": "script",
			"source_type": "url",
			"data_type": "text/x-shellscript",
			"data": "https://www.shellscript.sh/eg/first.sh.txt"
		},
		"created_at": "0001-01-01T00:00:00Z",
		"updated_at": "0001-01-01T00:00:00Z"
	}`

	var model UserActionModel

	err := json.Unmarshal([]byte(jsonBytes), &model)
	assert.NoError(t, err)

	registry, err := db.GetBSONRegistry()
	assert.NoError(t, err)

	assert.True(t, model.Action.IsScriptAction())
	scriptAction := model.Action.GetScriptAction()

	assert.Equal(t, "https://www.shellscript.sh/eg/first.sh.txt", scriptAction.Data)

	bsonBytes, err := bson.MarshalWithRegistry(registry, model)
	assert.NoError(t, err)

	var model2 UserActionModel
	err = bson.UnmarshalWithRegistry(registry, bsonBytes, &model2)
	assert.NoError(t, err)

	assert.Equal(t, "test_user", model2.Owner)
	assert.Equal(t, "script", model2.Name)
	assert.True(t, model2.Action.IsScriptAction())
	scriptAction2 := model2.Action.GetScriptAction()

	assert.Equal(t, "https://www.shellscript.sh/eg/first.sh.txt", scriptAction2.Data)
}
