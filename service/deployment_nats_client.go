package service

import (
	"context"
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"
)

// natsDeploymentClient implements DeploymentClient
type natsDeploymentClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewDeploymentClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewDeploymentClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (DeploymentClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct deployment client")
	}
	return natsDeploymentClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// Get fetch a single deployment by its ID
func (client natsDeploymentClient) Get(ctx context.Context, actor Actor, deploymentID common.ID) (*Deployment, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Get",
	})
	query := DeploymentGetQuery{
		Session: actor.Session(),
		ID:      deploymentID}

	ce, err := client.natsRequest(ctx, query, DeploymentGetQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentGetReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &reply.Deployment, nil
}

// Search returns a list of deployment, with optional filters and sort.
func (client natsDeploymentClient) Search(ctx context.Context, actor Actor, option DeploymentListOption) (DeploymentList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Search",
	})
	query := DeploymentListQuery{
		Session:       actor.Session(),
		Filters:       option.Filter,
		SortBy:        option.SortBy,
		SortDirection: option.SortDirection,
		Offset:        option.Offset,
		FullRunObject: option.FullRunObject,
		Limit:         option.PageSize,
	}
	replyCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx, query, DeploymentListQueryType, replyCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	deploymentList := make([]Deployment, 0)
	for _, ce := range eventList {
		var reply DeploymentListReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		deploymentList = append(deploymentList, reply.Deployments...)
	}

	return deploymentListModel{
		Option:      option,
		Deployments: deploymentList,
	}, nil
}

// SearchNext returns a list of deployment, with optional filters and sort.
func (client natsDeploymentClient) SearchNext(ctx context.Context, actor Actor, list DeploymentList) (DeploymentList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.SearchNext",
	})
	query := DeploymentListQuery{
		Session:       actor.Session(),
		Filters:       list.GetOptions().Filter,
		SortBy:        list.GetOptions().SortBy,
		SortDirection: list.GetOptions().SortDirection,
		Offset:        list.GetNextStart(),
		FullRunObject: list.GetOptions().FullRunObject,
		Limit:         list.GetOptions().PageSize,
	}
	replyCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx2, cancel := context.WithTimeout(ctx, time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx2, query, DeploymentListQueryType, replyCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	deploymentList := make([]Deployment, 0)
	for _, ce := range eventList {
		var reply DeploymentListReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		deploymentList = append(deploymentList, reply.Deployments...)
	}

	return deploymentListModel{
		Option:      list.GetOptions(),
		Deployments: deploymentList,
	}, nil
}

// GetRun returns a specific deployment run.
func (client natsDeploymentClient) GetRun(ctx context.Context, actor Actor, deployment, run common.ID) (*DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.GetRun",
	})
	query := DeploymentGetRunQuery{
		Session:    actor.Session(),
		Deployment: deployment,
		Run:        run,
	}

	ce, err := client.natsRequest(ctx, query, DeploymentGetRunQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentGetRunReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetRunReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &reply.Run, nil
}

// SearchRun returns a list of deployment run.
func (client natsDeploymentClient) SearchRun(ctx context.Context, actor Actor, option DeploymentRunListOption) (DeploymentRunList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.SearchRun",
	})
	query := DeploymentListRunQuery{
		Session:    actor.Session(),
		Deployment: option.Deployment,
		RequestPagination: RequestPagination{
			Offset:        option.Offset,
			PageSizeLimit: option.PageSize,
		},
	}
	repliesCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListRunReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx2, cancel := context.WithTimeout(ctx, time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx2, query, DeploymentListRunQueryType, repliesCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	runList := make([]DeploymentRun, 0)
	for _, ce := range eventList {
		var reply DeploymentListRunReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListRunReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		runList = append(runList, reply.Runs...)
	}

	return deploymentRunListModel{
		Runs:   runList,
		Option: option,
	}, nil
}

// SearchRunNext continues the Search with pagination.
func (client natsDeploymentClient) SearchRunNext(ctx context.Context, actor Actor, list DeploymentRunList) (DeploymentRunList, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.SearchRunNext",
	})
	query := DeploymentListRunQuery{
		Session:    actor.Session(),
		Deployment: list.GetOption().Deployment,
		RequestPagination: RequestPagination{
			Offset:        list.GetNextStart(),
			PageSizeLimit: list.GetSize(),
		},
	}
	repliesCountExtractor := func(ce *cloudevents.Event) int {
		var reply DeploymentListRunReply
		err := json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			return 0
		}
		return reply.TotalReplies
	}

	ctx2, cancel := context.WithTimeout(ctx, time.Second*5)
	eventList, err := client.natsRequestMultiRelies(ctx2, query, DeploymentListRunQueryType, repliesCountExtractor)
	cancel()
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	runList := make([]DeploymentRun, 0)
	for _, ce := range eventList {
		var reply DeploymentListRunReply
		err = json.Unmarshal(ce.Data(), &reply)
		if err != nil {
			errorMessage := "unable to unmarshal response JSON to DeploymentListRunReply"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoMarshalError(errorMessage)
		}

		serviceError := reply.GetServiceError()
		if serviceError != nil {
			logger.Error(serviceError)
			return nil, serviceError
		}

		runList = append(runList, reply.Runs...)
	}

	return deploymentRunListModel{
		Runs:   runList,
		Option: list.GetOption(),
	}, nil
}

// GetRawState fetch the raw state of a deployment run.
func (client natsDeploymentClient) GetRawState(ctx context.Context, actor Actor, deployment, run common.ID) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.GetRawState",
	})
	query := DeploymentGetRawStateQuery{
		Session:    actor.Session(),
		Deployment: deployment,
		Run:        run,
	}

	ce, err := client.natsRequest(ctx, query, DeploymentGetRawStateQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentGetRawStateReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetRawStateReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return reply.RawState, nil
}

// GetLogs fetch the logs of a deployment run.
// Deprecated: logs itself should be fetched over HTTP instead of over NATS, use NATS for fetching HTTP URL to the logs
func (client natsDeploymentClient) GetLogs(ctx context.Context, actor Actor, deployment, run common.ID) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.GetLogs",
	})
	query := DeploymentGetLogsQuery{
		Session:    actor.Session(),
		Deployment: deployment,
		Run:        run,
	}

	ce, err := client.natsRequest(ctx, query, DeploymentGetLogsQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return "", err
	}

	var reply DeploymentGetLogsReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentGetLogsReply"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return "", serviceError
	}

	return reply.Logs, nil
}

// ListDeploymentLogsLocations fetches list of locations (URL) that one can download logs for a deployment or a deployment run.
func (client natsDeploymentClient) ListDeploymentLogsLocations(ctx context.Context, actor Actor, deployment common.ID) ([]LogsLocation, error) {
	return client.listLogsLocations(ctx, actor, deployment, "")
}

// ListRunLogsLocations fetches list of locations (URL) that one can download logs for a deployment or a deployment run.
func (client natsDeploymentClient) ListRunLogsLocations(ctx context.Context, actor Actor, deployment, run common.ID) ([]LogsLocation, error) {
	if run == "" {
		return nil, NewCacaoGeneralError("run ID is empty, cannot fetch logs location for the deployment run")
	}
	return client.listLogsLocations(ctx, actor, deployment, run)
}

func (client natsDeploymentClient) listLogsLocations(ctx context.Context, actor Actor, deployment, run common.ID) ([]LogsLocation, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.listLogsLocations",
	})
	query := DeploymentLogsLocationQuery{
		Session:      actor.Session(),
		Deployment:   deployment,
		TemplateType: "",
		Run:          run,
	}

	ce, err := client.natsRequest(ctx, query, DeploymentGetLogsLocationQueryType)
	if err != nil {
		logger.WithError(err).Errorf("unable to request")
		return nil, err
	}

	var reply DeploymentLogsLocationReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to DeploymentLogsLocationReply"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := reply.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	if len(reply.LogsLocations) == 0 {
		if run == "" {
			return nil, NewCacaoNotFoundError("no logs found for deployment")
		}
		return nil, NewCacaoNotFoundError("no logs found for deployment run")
	}

	return reply.LogsLocations, nil
}

// CreateAsync sends request to create a deployment
func (client natsDeploymentClient) CreateAsync(actor Actor, tid common.TransactionID, option DeploymentCreateParam) error {
	event := DeploymentCreationRequest{
		Session:     actor.Session(),
		CreateParam: option,
	}
	return client.stanPublish(event, DeploymentCreationRequested, tid)
}

// Create creates a deployment. Deployment ID is returned on success.
func (client natsDeploymentClient) Create(ctx context.Context, actor Actor, tid common.TransactionID, option DeploymentCreateParam) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Create",
	})
	if tid == "" {
		tid = messaging2.NewTransactionID()
	}

	listenerEventTypes := []common.EventType{DeploymentCreated, DeploymentCreateFailed}
	promise, err := client.eventConn.WaitForResponse(tid, listenerEventTypes)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return "", err
	}

	err = client.CreateAsync(actor, tid, option)
	if err != nil {
		logger.WithError(err).Error("unable to create a deployment")
		promise.Close()
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return "", err
	}
	var resultEvent DeploymentCreationResult
	err = json.Unmarshal(responseCe.Data(), &resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return "", serviceError
	}

	receivedEventType := common.EventType(responseCe.Type())
	if receivedEventType == DeploymentCreated {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return resultEvent.ID, nil
	}

	return common.ID(""), NewCacaoGeneralError("failed to create a deployment")
}

// RefreshStateAsync sends request to refresh state
func (client natsDeploymentClient) RefreshStateAsync(actor Actor, tid common.TransactionID, deployment common.ID) error {
	event := DeploymentStateRefresh{
		Session: actor.Session(),
		ID:      deployment,
	}
	return client.stanPublish(event, DeploymentStateRefreshRequested, tid)
}

// RefreshState actively refresh the state of a deployment's latest run. The refreshed state is returned on success.
//
// Return (nil, nil) if the state refresh has started, but not completed.
func (client natsDeploymentClient) RefreshState(ctx context.Context, actor Actor, tid common.TransactionID, deployment common.ID) (*DeploymentStateView, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.RefreshState",
	})
	if tid == "" {
		tid = messaging2.NewTransactionID()
	}

	listenerEventTypes := []common.EventType{DeploymentStateRefreshed, EventDeploymentStateRefreshStarted, DeploymentStateRefreshFailed}
	promise, err := client.eventConn.WaitForResponse(tid, listenerEventTypes)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return nil, err
	}

	err = client.RefreshStateAsync(actor, tid, deployment)
	if err != nil {
		logger.WithError(err).Error("unable to refresh deployment state")
		promise.Close()
		return nil, err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return nil, err
	}

	receivedEventType := common.EventType(responseCe.Type())
	if receivedEventType == EventDeploymentStateRefreshStarted {
		// if the state refresh is only started and not finished (state refresh could take a few minutes), then return nil as state view to indicate that.
		return nil, nil
	}

	var resultEvent DeploymentStateRefreshResult
	err = json.Unmarshal(responseCe.Data(), &resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	if receivedEventType == DeploymentStateRefreshed {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return &resultEvent.State, nil
	}

	return nil, NewCacaoGeneralError("failed to refresh deployment state")
}

// UpdateAsync ...
func (client natsDeploymentClient) UpdateAsync(actor Actor, tid common.TransactionID, deployment Deployment, updateFields []string) error {
	event := DeploymentUpdateRequest{
		Session:      actor.Session(),
		Deployment:   deployment,
		UpdateFields: updateFields,
	}
	return client.stanPublish(event, DeploymentUpdateRequested, tid)
}

// Update will update certain fields in the deployment. Note that not all fields are updatable.
func (client natsDeploymentClient) Update(ctx context.Context, actor Actor, tid common.TransactionID, deployment Deployment, updateFields []string) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Update",
		"id":       deployment.ID,
	})
	if deployment.ID == "" || !deployment.ID.Validate() {
		return "", NewCacaoInvalidParameterError("invalid deployment ID")
	}
	if tid == "" {
		tid = messaging2.NewTransactionID()
	}

	listenerEventTypes := []common.EventType{DeploymentUpdated, DeploymentUpdateFailed}
	promise, err := client.eventConn.WaitForResponse(tid, listenerEventTypes)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return "", err
	}

	err = client.UpdateAsync(actor, tid, deployment, updateFields)
	if err != nil {
		logger.WithError(err).Error("unable to update deployment")
		promise.Close()
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return "", err
	}
	var resultEvent DeploymentUpdateResult
	err = json.Unmarshal(responseCe.Data(), &resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return "", serviceError
	}

	receivedEventType := common.EventType(responseCe.Type())
	if receivedEventType == DeploymentUpdated {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return resultEvent.ID, nil
	}

	return "", NewCacaoGeneralError("failed to update deployment")
}

// UpdateParametersAsync sends request to update parameters of a deployment.
// Only the parameters that needs to be updated are required.
func (client natsDeploymentClient) UpdateParametersAsync(actor Actor, tid common.TransactionID, deployment common.ID, param DeploymentParameterValues) error {
	event := DeploymentParameterUpdateRequest{
		Session:     actor.Session(),
		Deployment:  deployment,
		ParamValues: param,
	}
	return client.stanPublish(event, DeploymentParameterUpdateRequested, tid)
}

// UpdateParameters updates the parameter and create a deployment run. The newly created deployment run ID
// is returned on success.
func (client natsDeploymentClient) UpdateParameters(ctx context.Context, actor Actor, tid common.TransactionID, deployment common.ID, param DeploymentParameterValues) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.UpdateParameters",
	})
	if tid == "" {
		tid = messaging2.NewTransactionID()
	}

	listenerEventTypes := []common.EventType{DeploymentParameterUpdated, DeploymentParameterUpdateFailed}
	promise, err := client.eventConn.WaitForResponse(tid, listenerEventTypes)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return "", err
	}

	err = client.UpdateParametersAsync(actor, tid, deployment, param)
	if err != nil {
		logger.WithError(err).Error("unable to update deployment parameters")
		promise.Close()
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return "", err
	}
	var resultEvent DeploymentParameterUpdateResult
	err = json.Unmarshal(responseCe.Data(), &resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return "", serviceError
	}

	receivedEventType := common.EventType(responseCe.Type())
	if receivedEventType == DeploymentParameterUpdated {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result")
		return resultEvent.ID, nil
	}

	return "", NewCacaoGeneralError("failed to update deployment parameters")
}

// DeleteAsync sends request to delete a deployment
func (client natsDeploymentClient) DeleteAsync(actor Actor, tid common.TransactionID, deployment common.ID) error {
	event := DeploymentDeletionRequest{
		Session: actor.Session(),
		ID:      deployment}
	return client.stanPublish(event, DeploymentDeletionRequested, tid)
}

// Delete deletes a deployment.
func (client natsDeploymentClient) Delete(ctx context.Context, actor Actor, tid common.TransactionID, deployment common.ID) (deleted bool, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.Delete",
	})
	if tid == "" {
		tid = messaging2.NewTransactionID()
	}
	listenerEventTypes := []common.EventType{DeploymentDeleted, DeploymentDeletionStarted, DeploymentDeleteFailed}
	promise, err := client.eventConn.WaitForResponse(tid, listenerEventTypes)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return false, err
	}

	err = client.DeleteAsync(actor, tid, deployment)
	if err != nil {
		logger.WithError(err).Error("unable to delete a deployment")
		promise.Close()
		return false, err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return false, err
	}
	var resultEvent DeploymentDeletionResult
	err = json.Unmarshal(responseCe.Data(), &resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return false, NewCacaoMarshalError(errorMessage)
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return false, serviceError
	}

	receivedEventType := common.EventType(responseCe.Type())
	if receivedEventType == DeploymentDeleted {
		logger.WithFields(log.Fields{"ID": resultEvent.ID}).Trace("received result, deleted")
		return true, nil
	} else if receivedEventType == DeploymentDeletionStarted {
		// This event means that the deletion request is received and the deletion process has started.
		// But the deployment may not have been deleted yet.
		// There will be another DeploymentDeleted/DeploymentDeletionFailed event after this to indicate the final
		// result of the deletion, but the final event that not be tracked by this function.
		logger.WithFields(log.Fields{
			"ID": resultEvent.ID,
		}).Trace("received result, deletion started")
		return false, nil
	}
	return false, NewCacaoGeneralError("failed to delete a deployment")
}

// CreateRunAsync sends request to create a deployment run
func (client natsDeploymentClient) CreateRunAsync(actor Actor, tid common.TransactionID, param DeploymentRunCreateParam) error {
	event := DeploymentCreateRunRequest{
		Session:     actor.Session(),
		CreateParam: param,
	}
	return client.stanPublish(event, DeploymentCreateRunRequested, tid)
}

// CreateRun creates a new deployment run. Deployment Run is returned on success.
func (client natsDeploymentClient) CreateRun(ctx context.Context, actor Actor, tid common.TransactionID, param DeploymentRunCreateParam) (*DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.CreateRun",
	})
	if tid == "" {
		tid = messaging2.NewTransactionID()
	}

	listenerEventTypes := []common.EventType{DeploymentRunCreated, DeploymentRunCreateFailed}
	promise, err := client.eventConn.WaitForResponse(tid, listenerEventTypes)
	if err != nil {
		logger.WithError(err).Error("unable to listen for a response")
		return nil, err
	}

	err = client.CreateRunAsync(actor, tid, param)
	if err != nil {
		logger.WithError(err).Error("unable to create a deployment run")
		promise.Close()
		return nil, err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return nil, err
	}
	var resultEvent DeploymentCreateRunResult
	err = json.Unmarshal(responseCe.Data(), &resultEvent)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := resultEvent.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	if common.EventType(responseCe.Type()) == DeploymentRunCreated {
		logger.WithFields(log.Fields{"runID": resultEvent.Run.ID}).Trace("received result")
		return &resultEvent.Run, nil
	}

	return nil, NewCacaoGeneralError("failed to create a deployment run")
}

func (client natsDeploymentClient) stanPublish(event interface{}, eventType common.EventType, tid common.TransactionID) error {
	ce, err := messaging2.CreateCloudEventWithTransactionID(event, eventType, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return err
	}
	return client.eventConn.Publish(ce)
}

// TODO make use of context.Context
func (client natsDeploymentClient) natsRequest(ctx context.Context, query interface{}, queryType common.QueryOp) (*cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.natsRequest",
	})
	requestCe, err := messaging2.CreateCloudEvent(query, queryType, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		errorMessage := "unable to create a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		return nil, err
	}
	return &replyCe, nil
}

func (client natsDeploymentClient) natsRequestMultiRelies(ctx context.Context, query interface{}, queryType common.QueryOp, replyCountExtractor func(*cloudevents.Event) int) ([]cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsDeploymentClient.natsRequestMultiRelies",
	})
	requestCe, err := messaging2.CreateCloudEvent(query, queryType, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		errorMessage := "unable to create a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	replyStream, err := client.queryConn.RequestStream(ctx, requestCe)
	if err != nil {
		return nil, err
	}
	var list = make([]cloudevents.Event, 0)
	for replyStream.ExpectNext() {
		ce, err := replyStream.Next()
		if err != nil {
			return nil, err
		}
		list = append(list, ce)
	}
	return list, nil
}

// Request a list of NATS replies that is streamed on the same reply subject.
// repliesCountExtractor is used to extract the total number of replies from the 1st reply.
//
// Deprecated: FIXME new reply streaming method is different used in natsDeploymentClient.natsRequestMultiRelies is different, this requires both client and server to be migrated.
func natsRequestMultiRelies(ctx context.Context, nc *nats.Conn, query interface{}, queryType common.QueryOp,
	source string, replyCountExtractor func(*cloudevents.Event) int) ([]cloudevents.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsWorkspaceClient.natsRequestMultiRelies",
	})
	ce, err := messaging2.CreateCloudEvent(query, queryType, source)
	if err != nil {
		errorMessage := "unable to create a cloud event"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		errorMessage := "unable to marshal a cloud event to JSON"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	replySubject := nats.NewInbox()
	sub, err := nc.SubscribeSync(replySubject)
	if err != nil {
		errorMessage := "unable to subscribe"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	defer sub.Unsubscribe()

	err = nc.PublishRequest(string(queryType), replySubject, payload)
	if err != nil {
		errorMessage := "unable to request"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}

	getNextReply := func(sub *nats.Subscription) (*cloudevents.Event, error) {
		msg, err := sub.NextMsgWithContext(ctx)
		if err != nil {
			errorMessage := "unable to get next message"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoCommunicationError(errorMessage)
		}

		// convert the message to a cloud event
		ce, err = messaging2.ConvertNats(msg)
		if err != nil {
			errorMessage := "unable to convert a NATS message to a cloud event"
			logger.WithError(err).Error(errorMessage)
			return nil, NewCacaoCommunicationError(errorMessage)
		}
		return &ce, err
	}

	// 1st reply
	replyEvent, err := getNextReply(sub)
	if err != nil {
		return nil, err
	}

	totalReplyCount := replyCountExtractor(replyEvent) // get total number of replies from 1st reply
	if totalReplyCount < 1 {
		errorMessage := "bad totalReplyCount"
		logger.WithField("totalReplyCount", totalReplyCount).Error(errorMessage)
		return nil, NewCacaoCommunicationError(errorMessage)
	}
	allReply := []cloudevents.Event{*replyEvent}

	// rest of replies
	for i := 1; i < totalReplyCount; i++ {
		replyEvent, err = getNextReply(sub)
		if err != nil {
			return nil, err
		}
		allReply = append(allReply, *replyEvent)
	}
	return allReply, nil
}
