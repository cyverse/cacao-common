package service

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/cloudevents/sdk-go/v2/event"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// EventType constants for operations that manipulates user configs/settings
const (
	NatsSubjectUserConfigsGet   common.QueryOp = NatsSubjectUsers + ".getconfigs"
	NatsSubjectUserFavoritesGet common.QueryOp = NatsSubjectUsers + ".getfavorites"
	NatsSubjectUserRecentsGet   common.QueryOp = NatsSubjectUsers + ".getrecents"
	NatsSubjectUserSettingsGet  common.QueryOp = NatsSubjectUsers + ".getsettings"

	EventUserConfigSetRequested      common.EventType = common.EventTypePrefix + "UserConfigSetRequested"
	EventUserFavoriteAddRequested    common.EventType = common.EventTypePrefix + "UserFavoriteAddRequested"
	EventUserFavoriteDeleteRequested common.EventType = common.EventTypePrefix + "UserFavoriteDeleteRequested"
	EventUserRecentSetRequested      common.EventType = common.EventTypePrefix + "UserRecentSetRequested"

	EventUserConfigSet       common.EventType = common.EventTypePrefix + "UserConfigSet"
	EventUserFavoriteAdded   common.EventType = common.EventTypePrefix + "UserFavoriteAdded"
	EventUserFavoriteDeleted common.EventType = common.EventTypePrefix + "UserFavoriteDeleted"
	EventUserRecentSet       common.EventType = common.EventTypePrefix + "UserRecentSet"

	EventUserConfigSetError      common.EventType = common.EventTypePrefix + "UserConfigSetError"
	EventUserFavoriteAddError    common.EventType = common.EventTypePrefix + "UserFavoriteAddError"
	EventUserFavoriteDeleteError common.EventType = common.EventTypePrefix + "UserFavoriteDeleteError"
	EventUserRecentSetError      common.EventType = common.EventTypePrefix + "UserRecentSetError"
)

// UserSettingsRequest is used to pass requests (query & event) over nats
type UserSettingsRequest struct {
	Session
	Username string `json:"username"`
	Key      string `json:"key"`
	Value    string `json:"value"`
}

// UserSettingsReply is used to receiving query reply over nats
type UserSettingsReply struct {
	Session
	common.UserSettings `json:",inline"`
}

func (client *natsUserClient) userSettingsRequest(ctx context.Context, actor Actor, subject common.QueryOp, username string, key string, value string) (*event.Event, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.userSettingsRequest",
	})
	logger.Trace("started")

	if username == "" {
		return nil, errors.New(UserUsernameNotSetError)
	}

	var data = UserSettingsRequest{
		Session:  actor.Session(),
		Username: username,
		Key:      key,
		Value:    value,
	}
	requestCe, err := messaging2.CreateCloudEvent(data, subject, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	return &replyCe, err
}

// GetConfigs ...
func (client *natsUserClient) GetConfigs(ctx context.Context, actor Actor, username string) (*map[string]string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.GetConfigs",
	})
	ce, err := client.userSettingsRequest(ctx, actor, NatsSubjectUserConfigsGet, username, "", "")
	if err != nil {
		return nil, err
	}
	logger.Debugf("%+v", ce.Data())
	var reply UserSettingsReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		return nil, err
	}
	if err := reply.GetServiceError(); err != nil {
		return nil, err
	}
	return &reply.Configs, nil
}

// GetFavorites ...
func (client *natsUserClient) GetFavorites(ctx context.Context, actor Actor, username string) (*map[string][]string, error) {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.GetFavorites",
	}).Trace("called")
	ce, err := client.userSettingsRequest(ctx, actor, NatsSubjectUserFavoritesGet, username, "", "")
	if err != nil {
		return nil, err
	}
	var reply UserSettingsReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		return nil, err
	}
	if err := reply.GetServiceError(); err != nil {
		return nil, err
	}
	return &reply.Favorites, nil
}

// GetRecents ...
func (client *natsUserClient) GetRecents(ctx context.Context, actor Actor, username string) (*map[string]string, error) {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.GetRecents",
	}).Trace("called")
	ce, err := client.userSettingsRequest(ctx, actor, NatsSubjectUserRecentsGet, username, "", "")
	if err != nil {
		return nil, err
	}
	var reply UserSettingsReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		return nil, err
	}
	if err := reply.GetServiceError(); err != nil {
		return nil, err
	}
	return &reply.Recents, nil
}

// GetSettings ...
func (client *natsUserClient) GetSettings(ctx context.Context, actor Actor, username string) (*common.UserSettings, error) {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.GetSettings",
	}).Trace("called")
	ce, err := client.userSettingsRequest(ctx, actor, NatsSubjectUserSettingsGet, username, "", "")
	if err != nil {
		return nil, err
	}
	var reply UserSettingsReply
	err = json.Unmarshal(ce.Data(), &reply)
	if err != nil {
		return nil, err
	}
	if err := reply.GetServiceError(); err != nil {
		return nil, err
	}
	return &reply.UserSettings, nil
}

func (client *natsUserClient) publishSettingsRequestEvent(ctx context.Context, actor Actor, username string, key string, value string, eventOpType common.EventType) error {
	logger := log.WithFields(log.Fields{
		"package":     "cacao-common.service",
		"function":    "natsUserClient.publishSettingsRequestEvent",
		"username":    username,
		"eventOpType": string(eventOpType),
	})
	logger.Trace("called")

	var data = UserSettingsRequest{
		Session:  actor.Session(),
		Username: username,
		Key:      key,
		Value:    value,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(data, eventOpType, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	// listens to different event type depends on event operation
	var etList []common.EventType
	switch eventOpType {
	case EventUserConfigSetRequested:
		etList = []common.EventType{EventUserConfigSet, EventUserConfigSetError}
	case EventUserFavoriteAddRequested:
		etList = []common.EventType{EventUserFavoriteAdded, EventUserFavoriteAddError}
	case EventUserFavoriteDeleteRequested:
		etList = []common.EventType{EventUserFavoriteDeleted, EventUserFavoriteDeleteError}
	case EventUserRecentSetRequested:
		etList = []common.EventType{EventUserRecentSet, EventUserRecentSetError}
	default:
		panic("unknown op") // unknown op must not be passed
	}
	promise, err := client.eventConn.Request(requestCe, etList)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("error while wait for response")
		return err
	}
	var response UserModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// SetConfig ...
func (client *natsUserClient) SetConfig(ctx context.Context, actor Actor, username string, key string, value string) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.SetConfig",
	}).Trace("called")
	return client.publishSettingsRequestEvent(ctx, actor, username, key, value, EventUserConfigSetRequested)
}

// AddFavorite ...
func (client *natsUserClient) AddFavorite(ctx context.Context, actor Actor, username string, key string, value string) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.AddFavorite",
	}).Trace("called")
	return client.publishSettingsRequestEvent(ctx, actor, username, key, value, EventUserFavoriteAddRequested)
}

// DeleteFavorite ...
func (client *natsUserClient) DeleteFavorite(ctx context.Context, actor Actor, username string, key string, value string) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.DeleteFavorite",
	}).Trace("called")
	return client.publishSettingsRequestEvent(ctx, actor, username, key, value, EventUserFavoriteDeleteRequested)
}

// SetRecent ...
func (client *natsUserClient) SetRecent(ctx context.Context, actor Actor, username string, key string, value string) error {
	log.WithFields(log.Fields{
		"package":  "cacao-common.service",
		"function": "natsUserClient.SetRecent",
	}).Trace("called")
	return client.publishSettingsRequestEvent(ctx, actor, username, key, value, EventUserRecentSetRequested)
}
