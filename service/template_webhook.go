package service

import (
	"gitlab.com/cyverse/cacao-common/common"
	"time"
)

// TemplateWebhookPlatformType represents the platform type of template webhook (e.g. gitlab, github)
type TemplateWebhookPlatformType string

// Constants for TemplateWebhookPlatformType
const (
	TemplateWebhookGithubPlatform TemplateWebhookPlatformType = "github"
	TemplateWebhookGitlabPlatform TemplateWebhookPlatformType = "gitlab"
)

// TemplateWebhook is the service representation of a template webhook.
type TemplateWebhook struct {
	TemplateID     common.ID                   `json:"template_id"`
	Platform       TemplateWebhookPlatformType `json:"platform"`
	Gitlab         TemplateGitlabWebhook       `json:"gitlab,omitempty"`
	Github         TemplateGithubWebhook       `json:"github,omitempty"`
	CreatedAt      time.Time                   `json:"created_at"`
	LastExecutedAt time.Time                   `json:"last_executed_at,omitempty"`
}

// TemplateGitlabWebhook ...
type TemplateGitlabWebhook struct {
	// Token is only exposed in creation response
	Token string `json:"token"`
}

// TemplateGithubWebhook ...
type TemplateGithubWebhook struct {
	// Key is only exposed in creation response
	Key string `json:"key"`
}

// TemplateWebhookListRequest is the cloudevent payload for list query
type TemplateWebhookListRequest struct {
	Session  `json:",inline"`
	Owner    string                      `json:"owner"`
	Template common.ID                   `json:"template_id"`
	Platform TemplateWebhookPlatformType `json:"platform"`
}

// TemplateWebhookListReply is the cloudevent payload for list reply
type TemplateWebhookListReply struct {
	Session  `json:",inline"`
	Webhooks []TemplateWebhook `json:"webhooks"`
}

// TemplateWebhookCreationRequest is the cloudevent payload for webhook creation request
type TemplateWebhookCreationRequest struct {
	Session         `json:",inline"`
	TemplateWebhook `json:"template_webhook"`
}

// TemplateWebhookCreationResponse is the cloudevent payload for webhook creation response
type TemplateWebhookCreationResponse = TemplateWebhookCreationRequest

// TemplateWebhookDeletionRequest  is the cloudevent payload for webhook deletion request
type TemplateWebhookDeletionRequest = TemplateWebhookCreationRequest

// TemplateWebhookDeletionResponse is the cloudevent payload for webhook deletion response
type TemplateWebhookDeletionResponse = TemplateWebhookCreationRequest
