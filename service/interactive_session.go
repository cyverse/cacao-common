package service

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// InteractiveSessionProtocol is the protocol of an interactive session
type InteractiveSessionProtocol string

const (
	// InteractiveSessionProtocolUnknown is for an unknown interactive session protocol
	InteractiveSessionProtocolUnknown InteractiveSessionProtocol = ""
	// InteractiveSessionProtocolSSH is for a SSH interactive session protocol
	InteractiveSessionProtocolSSH InteractiveSessionProtocol = "ssh"
	// InteractiveSessionProtocolVNC is for a VNC interactive session protocol
	InteractiveSessionProtocolVNC InteractiveSessionProtocol = "vnc"
)

// String returns the string representation of a InteractiveSessionProtocol.
func (p InteractiveSessionProtocol) String() string {
	return string(p)
}

// MarshalJSON returns the JSON representation of a InteractiveSessionProtocol.
func (p InteractiveSessionProtocol) MarshalJSON() ([]byte, error) {
	return json.Marshal(p.String())
}

// UnmarshalJSON converts the JSON representation of a InteractiveSessionProtocol to the appropriate enumeration constant.
func (p *InteractiveSessionProtocol) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*p = InteractiveSessionProtocolUnknown
	case "ssh":
		*p = InteractiveSessionProtocolSSH
	case "vnc":
		*p = InteractiveSessionProtocolVNC
	default:
		return fmt.Errorf("invalid interactive session format: %s", s)
	}

	return nil
}

// InteractiveSessionState is the state of an interactive session
type InteractiveSessionState string

const (
	// InteractiveSessionStateUnknown is for the unknown interactive session
	InteractiveSessionStateUnknown InteractiveSessionState = ""
	// InteractiveSessionStateCreating is for the interactive session in creation
	InteractiveSessionStateCreating InteractiveSessionState = "creating"
	// InteractiveSessionStateFailed is for the interactive session failed in creation
	InteractiveSessionStateFailed InteractiveSessionState = "failed"
	// InteractiveSessionStateActive is for the active interactive session
	InteractiveSessionStateActive InteractiveSessionState = "active"
	// InteractiveSessionStateInactive is for the inactive interactive session
	InteractiveSessionStateInactive InteractiveSessionState = "inactive"
)

// String returns the string representation of a InteractiveSessionState.
func (p InteractiveSessionState) String() string {
	return string(p)
}

// MarshalJSON returns the JSON representation of a InteractiveSessionState.
func (p InteractiveSessionState) MarshalJSON() ([]byte, error) {
	return json.Marshal(p.String())
}

// UnmarshalJSON converts the JSON representation of a InteractiveSessionState to the appropriate enumeration constant.
func (p *InteractiveSessionState) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*p = InteractiveSessionStateUnknown
	case "creating":
		*p = InteractiveSessionStateCreating
	case "failed":
		*p = InteractiveSessionStateFailed
	case "active":
		*p = InteractiveSessionStateActive
	case "inactive":
		*p = InteractiveSessionStateInactive
	default:
		return fmt.Errorf("invalid interactive session state: %s", s)
	}

	return nil
}

// InteractiveSessionClient is the client for InteractiveSession
// The ISMS Client contains three methods: Create, List and Check.
//
//	Get: Is used to get an interactive session stored in the database.
//	GetByInstanceID: Is used to get an interactive session stored in the database by instance ID.
//	GetByInstanceAddress: Is used to get an interactive session stored in the database by instance address.
//	ListAll: Is used to list all interactive sessions stored in the database.
//	List: Is used to list all active/creating interactive sessions.
//	CheckPrerequisites: Is used to check if the given interactive session can be created. Please provide the following fields when checking:
//		InstanceAddress // IP Address of the instance (String)
//		InstanceAdminUsername // Admin username of the instance (string)
//		Protocol // Either SSH or VNC (String)
//	Create: Is used to create and interactive session. Please provide the following fields when creating a session:
//		InstanceID // (String)
//		InstanceAddress // IP Address of the instance (String)
//		InstanceAdminUsername // Admin username of the instance (string)
//		Protocol // Either SSH or VNC (String)
//		CloudID // Identifier of the cloud where the instance is hosted. (String)
//	Deactivate: Is used to change the state of an interactive session to inactive.
type InteractiveSessionClient interface {
	Get(ctx context.Context, actor Actor, interactiveSessionID common.ID) (*InteractiveSessionModel, error)
	GetByInstanceID(ctx context.Context, actor Actor, instanceID string) (*InteractiveSessionModel, error)
	GetByInstanceAddress(ctx context.Context, actor Actor, instanceAddress string) (*InteractiveSessionModel, error)

	List(ctx context.Context, actor Actor) ([]InteractiveSessionModel, error)
	CheckPrerequisites(ctx context.Context, actor Actor, protocol InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)

	Create(ctx context.Context, actor Actor, interactiveSession InteractiveSessionModel) (common.ID, error)
	Deactivate(ctx context.Context, actor Actor, interactiveSessionID common.ID) error
}

// InteractiveSessionModel is the InteractiveSession model
// The proper model for the ISMS. Containing all needed fields both incoming and outgoing.
//
//	InstanceID: ID of the instance to connect to.
//	InstanceAddress: IP-Address of the instance to connect to.
//	InstanceAdminUsername: Admin username in the instance to login
//	CloudID: ID of the cloud to connect to.
//	Protocol: Protocol of the session
//	RedirectURL: Contains the URL containing both IP+Encrypted Key returned by the ISMS. Used to redirect the user
//		to a Guacamole Lite server.
//	State: state of the interactive session
type InteractiveSessionModel struct {
	Session

	ID                    common.ID                  `json:"id"`
	Owner                 string                     `json:"owner"`
	InstanceID            string                     `json:"instance_id"`
	InstanceAddress       string                     `json:"instance_address"`
	InstanceAdminUsername string                     `json:"instance_admin_username"`
	CloudID               string                     `json:"cloud_id"`
	Protocol              InteractiveSessionProtocol `json:"protocol"`
	RedirectURL           string                     `json:"redirect_url"`
	State                 InteractiveSessionState    `json:"state"`
	CreatedAt             time.Time                  `json:"created_at"`
	UpdatedAt             time.Time                  `json:"updated_at"`
}

// NewInteractiveSessionID generates a new InteractiveSessionID
func NewInteractiveSessionID() common.ID {
	return common.NewID("interactivesession")
}

// GetPrimaryID returns the primary ID of the InteractiveSession
func (i *InteractiveSessionModel) GetPrimaryID() common.ID {
	return i.ID
}

// InteractiveSessionListModel is the InteractiveSession list model
type InteractiveSessionListModel struct {
	Session
	InteractiveSessions []InteractiveSessionModel `json:"interactive_sessions"`
}
