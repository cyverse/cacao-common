package service

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"go.mongodb.org/mongo-driver/bson"
)

// UserActionType is the type of user action
type UserActionType string

const (
	// UserActionTypeUnknown is for an unknown action
	UserActionTypeUnknown UserActionType = ""
	// UserActionTypeInstanceExecution is for an action that executes on instances
	UserActionTypeInstanceExecution UserActionType = "instance_execution"
)

// String returns the string representation of a UserActionType.
func (u UserActionType) String() string {
	return string(u)
}

// MarshalJSON returns the JSON representation of a UserActionType.
func (u UserActionType) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.String())
}

// UnmarshalJSON converts the JSON representation of a UserActionType to the appropriate enumeration constant.
func (u *UserActionType) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*u = UserActionTypeUnknown
	case string(UserActionTypeInstanceExecution):
		*u = UserActionTypeInstanceExecution
	default:
		return fmt.Errorf("invalid user action type: %s", s)
	}

	return nil
}

// UserActionMethod is the method of user action
type UserActionMethod string

const (
	// UserActionMethodUnknown is for an unknown action
	UserActionMethodUnknown UserActionMethod = ""
	// UserActionMethodScript is for a script action
	UserActionMethodScript UserActionMethod = "script"
)

// String returns the string representation of a UserActionMethod.
func (u UserActionMethod) String() string {
	return string(u)
}

// MarshalJSON returns the JSON representation of a UserActionMethod.
func (u UserActionMethod) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.String())
}

// UnmarshalJSON converts the JSON representation of a UserActionMethod to the appropriate enumeration constant.
func (u *UserActionMethod) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*u = UserActionMethodUnknown
	case string(UserActionMethodScript):
		*u = UserActionMethodScript
	default:
		return fmt.Errorf("invalid user action method: %s", s)
	}

	return nil
}

// UserActionScriptSourceType is the type of user action script source
type UserActionScriptSourceType string

const (
	// UserActionScriptSourceTypeUnknown is for an unknown user action script source
	UserActionScriptSourceTypeUnknown UserActionScriptSourceType = ""
	// UserActionScriptSourceTypeURL is for an URL user action script source
	UserActionScriptSourceTypeURL UserActionScriptSourceType = "url"
	// UserActionScriptSourceTypeRaw is for a raw user action script source
	UserActionScriptSourceTypeRaw UserActionScriptSourceType = "raw"
)

// String returns the string representation of a UserActionScriptSourceType.
func (u UserActionScriptSourceType) String() string {
	return string(u)
}

// MarshalJSON returns the JSON representation of a UserActionScriptSourceType.
func (u UserActionScriptSourceType) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.String())
}

// UnmarshalJSON converts the JSON representation of a UserActionScriptSourceType to the appropriate enumeration constant.
func (u *UserActionScriptSourceType) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*u = UserActionScriptSourceTypeUnknown
	case string(UserActionScriptSourceTypeURL):
		*u = UserActionScriptSourceTypeURL
	case string(UserActionScriptSourceTypeRaw):
		*u = UserActionScriptSourceTypeRaw
	default:
		return fmt.Errorf("invalid user action script source type: %s", s)
	}

	return nil
}

// UserActionScriptDataType is the type of user action script data
type UserActionScriptDataType string

const (
	// UserActionScriptDataTypeUnknown is for an unknown user action script data
	UserActionScriptDataTypeUnknown UserActionScriptDataType = ""
	// UserActionScriptDataTypePython is for a python user action script data
	UserActionScriptDataTypePython UserActionScriptDataType = "text/x-python"
	// UserActionScriptDataTypeShellScript is for a shell script user action script data
	UserActionScriptDataTypeShellScript UserActionScriptDataType = "text/x-shellscript"
	// UserActionScriptDataTypePerl is for a perl user action script data
	UserActionScriptDataTypePerl UserActionScriptDataType = "application/x-perl"
)

// String returns the string representation of a UserActionScriptDataType.
func (u UserActionScriptDataType) String() string {
	return string(u)
}

// MarshalJSON returns the JSON representation of a UserActionScriptDataType.
func (u UserActionScriptDataType) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.String())
}

// UnmarshalJSON converts the JSON representation of a UserActionScriptDataType to the appropriate enumeration constant.
func (u *UserActionScriptDataType) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*u = UserActionScriptDataTypeUnknown
	case string(UserActionScriptDataTypePython), "python", "py", "python2", "python3":
		*u = UserActionScriptDataTypePython
	case string(UserActionScriptDataTypeShellScript), "shellscript", "shell", "shell_script", "shell-script", "bash", "sh":
		*u = UserActionScriptDataTypeShellScript
	case string(UserActionScriptDataTypePerl), "perl", "pl":
		*u = UserActionScriptDataTypePerl
	default:
		return fmt.Errorf("invalid user action script data type: %s", s)
	}

	return nil
}

// UserActionClient is the client for UserAction
type UserActionClient interface {
	// UserAction
	Get(ctx context.Context, actor Actor, userActionID common.ID) (*UserActionModel, error)
	List(ctx context.Context, actor Actor) ([]UserActionModel, error)

	Create(ctx context.Context, actor Actor, userAction UserActionModel) (common.ID, error)
	Update(ctx context.Context, actor Actor, userAction UserActionModel) error
	UpdateFields(ctx context.Context, actor Actor, userAction UserActionModel, updateFieldNames []string) error
	Delete(ctx context.Context, actor Actor, userActionID common.ID) error
}

// UserScriptActionItem is a struct for user script action item
type UserScriptActionItem struct {
	Method     UserActionMethod           `json:"method"` // this should be script
	SourceType UserActionScriptSourceType `json:"source_type"`
	DataType   UserActionScriptDataType   `json:"data_type"`
	Data       string                     `json:"data"`
}

// UserActionItem is a struct for user action item
type UserActionItem struct {
	item interface{} // should be UserScriptActionItem for now
}

// IsScriptAction returns true if the user action item is script action
func (item *UserActionItem) IsScriptAction() bool {
	switch item.item.(type) {
	case UserScriptActionItem, *UserScriptActionItem:
		return true
	default:
		return false
	}
}

// SetScriptAction sets user script action item
func (item *UserActionItem) SetScriptAction(scriptAction *UserScriptActionItem) {
	item.item = scriptAction
}

// GetScriptAction returns user action item as *UserScriptActionItem type
func (item *UserActionItem) GetScriptAction() *UserScriptActionItem {
	switch item.item.(type) {
	case UserScriptActionItem:
		scriptActionItem := item.item.(UserScriptActionItem)
		return &scriptActionItem
	case *UserScriptActionItem:
		scriptActionItem := item.item.(*UserScriptActionItem)
		return scriptActionItem
	default:
		return nil
	}
}

// MarshalJSON returns the JSON representation of a UserActionItem.
func (item UserActionItem) MarshalJSON() ([]byte, error) {
	if item.item == nil {
		item.item = UserScriptActionItem{}
	}

	switch item.item.(type) {
	case UserScriptActionItem, *UserScriptActionItem:
		return json.Marshal(item.item)
	default:
		return nil, fmt.Errorf("field value (%v) is not supported types", item.item)
	}
}

// UnmarshalJSON converts the JSON representation of a UserActionItem to the appropriate item.
func (item *UserActionItem) UnmarshalJSON(b []byte) error {
	// field
	var scriptActionItem UserScriptActionItem
	err := json.Unmarshal(b, &scriptActionItem)
	if err != nil {
		return fmt.Errorf("json value (%v) cannot be unmarshalled to script action item", string(b))
	}
	item.item = scriptActionItem
	return nil
}

// MarshalBSON returns the BSON representation of a UserActionItem.
func (item UserActionItem) MarshalBSON() ([]byte, error) {
	if item.item == nil {
		item.item = UserScriptActionItem{}
	}

	switch item.item.(type) {
	case UserScriptActionItem, *UserScriptActionItem:
		registry, err := db.GetBSONRegistry()
		if err != nil {
			return nil, err
		}
		return bson.MarshalWithRegistry(registry, item.item)
	default:
		return nil, fmt.Errorf("field value (%v) is not supported types", item.item)
	}
}

// UnmarshalBSON converts the BSON representation of a UserActionItem to the appropriate object.
func (item *UserActionItem) UnmarshalBSON(b []byte) error {
	registry, err := db.GetBSONRegistry()
	if err != nil {
		return err
	}

	// field
	var scriptActionItem UserScriptActionItem
	err = bson.UnmarshalWithRegistry(registry, b, &scriptActionItem)
	if err != nil {
		return fmt.Errorf("bson value (%v) cannot be unmarshalled to script action item", string(b))
	}
	item.item = scriptActionItem
	return nil
}

// NewUserActionItemForScriptAction creates UserActionItem for script action
func NewUserActionItemForScriptAction(sourceType UserActionScriptSourceType, dataType UserActionScriptDataType, data string) UserActionItem {
	userActionItem := UserActionItem{}
	userScriptActionItem := UserScriptActionItem{
		Method:     UserActionMethodScript,
		SourceType: sourceType,
		DataType:   dataType,
		Data:       data,
	}

	userActionItem.item = userScriptActionItem
	return userActionItem
}

// UserActionModel is the user action model
type UserActionModel struct {
	Session

	ID          common.ID      `json:"id"`
	Owner       string         `json:"owner"`
	Name        string         `json:"name"`
	Description string         `json:"description,omitempty"`
	Public      bool           `json:"public"`
	Type        UserActionType `json:"type"` // e.g., instance_execution
	Action      UserActionItem `json:"action"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
}

// NewUserActionID generates a new UserActionID
func NewUserActionID() common.ID {
	return common.NewID("useraction")
}

// GetPrimaryID returns the primary ID of the UserAction
func (u *UserActionModel) GetPrimaryID() common.ID {
	return u.ID
}

// UserActionListModel is the user action list model
type UserActionListModel struct {
	Session
	UserActions []UserActionModel `json:"user_actions"`
}
