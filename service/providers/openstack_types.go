package providers

import (
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// Client is the interface to the provider service.

// OpenStackQueryPrefix is prefix for query operation for openstack provider
const OpenStackQueryPrefix common.QueryOp = common.NatsQueryOpPrefix + "providers.openstack."

// ApplicationCredentialsListOp is the operation for listing credentials.
const ApplicationCredentialsListOp common.QueryOp = "appcredentials.list"

// ApplicationCredentialsGetOp is the operation for getting a single credential.
const ApplicationCredentialsGetOp common.QueryOp = "appcredentials.get"

// ProjectsListOp is the operation for listing projects.
const ProjectsListOp common.QueryOp = "projects.list"

// ProjectsGetOp is the operation for getting a single project.
const ProjectsGetOp common.QueryOp = "projects.get"

// CatalogListOp is the operation for listing catalog.
const CatalogListOp common.QueryOp = "catalog.list"

// DNSZoneListOp is the operation for listing DNS zone.
const DNSZoneListOp common.QueryOp = "zone.list"

// DNSRecordsetListOp is the operation for listing DNS recordset.
const DNSRecordsetListOp common.QueryOp = "recordset.list"

// TokenGetOp is the operation for getting a single token.
const TokenGetOp common.QueryOp = "token.get"

// EventApplicationCredentialsCreationRequested request the creation of an application credential
const EventApplicationCredentialsCreationRequested common.EventType = common.EventTypePrefix + "ApplicationCredentialsCreationRequested"

// EventApplicationCredentialsCreated is success response to EventApplicationCredentialsCreationRequested
const EventApplicationCredentialsCreated common.EventType = common.EventTypePrefix + "ApplicationCredentialsCreated"

// EventApplicationCredentialsCreateFailed is failure response to EventApplicationCredentialsCreationRequested
const EventApplicationCredentialsCreateFailed common.EventType = common.EventTypePrefix + "ApplicationCredentialsCreateFailed"

// EventApplicationCredentialsDeletionRequested request the deletion of an application credential
const EventApplicationCredentialsDeletionRequested common.EventType = common.EventTypePrefix + "ApplicationCredentialsDeletionRequested"

// EventApplicationCredentialsDeleted is success response to EventApplicationCredentialsDeletionRequested
const EventApplicationCredentialsDeleted common.EventType = common.EventTypePrefix + "ApplicationCredentialsDeleted"

// EventApplicationCredentialsDeleteFailed is failure response to EventApplicationCredentialsDeletionRequested
const EventApplicationCredentialsDeleteFailed common.EventType = common.EventTypePrefix + "ApplicationCredentialsDeleteFailed"

// GetApplicationCredentialReply ...
type GetApplicationCredentialReply struct {
	BaseProviderReply     `json:",inline"`
	ApplicationCredential *ApplicationCredential `json:"application_credential"`
}

// ToCloudEvent ...
func (r GetApplicationCredentialReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+ApplicationCredentialsGetOp, source)
}

// ApplicationCredentialListReply ...
type ApplicationCredentialListReply struct {
	BaseProviderReply      `json:",inline"`
	ApplicationCredentials []ApplicationCredential `json:"application_credentials"`
}

// ToCloudEvent ...
func (r ApplicationCredentialListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+ApplicationCredentialsListOp, source)
}

// GetProjectReply ...
type GetProjectReply struct {
	BaseProviderReply `json:",inline"`
	Project           *Project `json:"project"`
}

// ToCloudEvent ...
func (r GetProjectReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+ProjectsGetOp, source)
}

// ProjectListReply ...
type ProjectListReply struct {
	BaseProviderReply `json:",inline"`
	Projects          []Project `json:"projects"`
}

// ToCloudEvent ...
func (r ProjectListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+ProjectsListOp, source)
}

// CatalogListReply ...
type CatalogListReply struct {
	BaseProviderReply `json:",inline"`
	Catalog           []CatalogEntry `json:"catalog"`
}

// ToCloudEvent ...
func (r CatalogListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+CatalogListOp, source)
}

// DNSZoneListReply ...
type DNSZoneListReply struct {
	BaseProviderReply `json:",inline"`
	Zone              []DNSZone `json:"catalog"`
}

// ToCloudEvent ...
func (r DNSZoneListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+DNSZoneListOp, source)
}

// DNSRecordsetListReply ...
type DNSRecordsetListReply struct {
	BaseProviderReply `json:",inline"`
	Recordset         []DNSRecordset `json:"catalog"`
}

// ToCloudEvent ...
func (r DNSRecordsetListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+DNSRecordsetListOp, source)
}

// GetTokenReply ...
type GetTokenReply struct {
	BaseProviderReply `json:",inline"`
	Token             Token `json:"token"`
}

// ToCloudEvent ...
func (r GetTokenReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+TokenGetOp, source)
}

// CreateApplicationCredentialResponse is struct for response to application credential creation request. Event types: EventApplicationCredentialsCreated or EventApplicationCredentialsCreateFailed.
type CreateApplicationCredentialResponse struct {
	BaseProviderReply `json:",inline"`
	CredentialID      string `json:"credential_id"`
}

// DeleteApplicationCredentialResponse is struct for response to application credential deletion request,
type DeleteApplicationCredentialResponse struct {
	BaseProviderReply `json:",inline"`
	CredentialID      string `json:"credential_id"`
}

// ApplicationCredentialCreationArgs is the argument for ProviderRequest for creating OpenStack application credential. It contains the possible settings for creating application credential.
type ApplicationCredentialCreationArgs struct {
	Scope       ProjectScope `json:"scope" mapstructure:"scope"`
	NamePostfix string       `json:"name" mapstructure:"name"` // this will be appended to the end of the generated names (the name of application credential, and ID of CACAO credential)
}

// ApplicationCredential is application credential for OpenStack
type ApplicationCredential struct {
	ID           string        `json:"id"`
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	ExpiresAt    interface{}   `json:"expires_at"`
	ProjectID    string        `json:"project_id"`
	Roles        []interface{} `json:"roles"`
	Unrestricted bool          `json:"unrestricted"`
}

// Project is OpenStack Project
type Project struct {
	ID          string                 `json:"id"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	DomainID    string                 `json:"domain_id"`
	Enabled     bool                   `json:"enabled"`
	IsDomain    bool                   `json:"is_domain"`
	ParentID    string                 `json:"parent_id"`
	Properties  map[string]interface{} `json:"properties"`
}

// Token issued by OpenStack.
// Note this is used to represent tokens created by the service, rather than token used to authenticate with OpenStack.
type Token struct {
	Expires   string `json:"expires"`
	ID        string `json:"id"`
	ProjectID string `json:"project_id"`
	UserID    string `json:"user_id"`
}

// CatalogEntry is an entry in the OpenStack catalog, it contains endpoint info for a service.
type CatalogEntry struct {
	Name      string            `json:"name"`
	Type      string            `json:"type"`
	Endpoints []CatalogEndpoint `json:"endpoints"`
}

// CatalogEndpoint is an API endpoint in the OpenStack catalog.
type CatalogEndpoint struct {
	ID        string `json:"id"`
	Interface string `json:"interface"`
	RegionID  string `json:"region_id"`
	URL       string `json:"url"`
	Region    string `json:"region"`
}

// DNSZone is DNS zone from OpenStack Designate.
type DNSZone struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Type   string `json:"type"`
	Serial int    `json:"serial"`
	Status string `json:"status"`
	Action string `json:"action"`
}

// DNSRecordset is DNS recordset from OpenStack Designate.
type DNSRecordset struct {
	ID string `json:"id"`
	// domain name
	Name string `json:"name"`
	// type of DNS record, e.g. type A, type NS
	Type string `json:"type"`
	// content of the record, for type A, this is IP address
	Records []string `json:"records"`
	// status of recordset, e.g. "ACTIVE"
	Status string `json:"status"`
	Action string `json:"action"`
}

// DNSRecordsetListArgs is the argument to list DNSRecordset.
type DNSRecordsetListArgs struct {
	Zone string `json:"zone"` // use all for list recordset for all zones in the project
}

// OpenStackCredentialListArgs is the argument to credential list for openstack provider.
type OpenStackCredentialListArgs struct {
	// Filter the credential list by OpenStack Project ID, takes priority over ProjectName
	ProjectID string `json:"project_id"`
	// Filter the credential list by OpenStack Project Name, ignored if ProjectID is specified
	ProjectName string `json:"project_name"`
}

// OpenStackProviderError is what gets returned when an error occurs during the
// processing of a request.
type OpenStackProviderError struct {
	Error         string `json:"error"`
	TransactionID string `json:"transaction_id"`
}

// ProjectScope is scope for a specific OpenStack project in a specific domain.
type ProjectScope struct {
	Project struct {
		Name string `json:"name,omitempty"`
		ID   string `json:"id,omitempty"`
	} `json:"project"`
	Domain struct {
		Name string `json:"name,omitempty"`
		ID   string `json:"id,omitempty"`
	} `json:"domain"`
}

// Scoped returns true if the scope is project scope.
func (s ProjectScope) Scoped() bool {
	if s.Project.Name == "" && s.Project.ID == "" {
		return false
	}
	if s.Domain.Name == "" && s.Domain.ID == "" {
		return false
	}
	return true
}

// Unscoped return true if the scope is unscoped.
func (s ProjectScope) Unscoped() bool {
	if s.Project.Name == "" && s.Project.ID == "" && s.Domain.Name == "" && s.Domain.ID == "" {
		return true
	}
	return false
}

// CheckScoped checks if the scope has necessary field populated.
func (s ProjectScope) CheckScoped() error {
	if s.Project.Name == "" && s.Project.ID == "" {
		return service.NewCacaoInvalidParameterError("missing project name or id")
	}
	if s.Domain.Name == "" && s.Domain.ID == "" {
		return service.NewCacaoInvalidParameterError("missing domain name or id")
	}
	return nil
}

// OpenStackCredentialError ...
type OpenStackCredentialError struct {
	service.CacaoErrorBase
}

const (
	// NotOpenStackCredentialErrorMessage indicates that credential is not an OpenStack credential due to bad schema.
	NotOpenStackCredentialErrorMessage service.CacaoStandardErrorMessage = "not openstack credential"
	// NotApplicationCredentialErrorMessage indicates that credential is not an application credential due to bad schema,
	// but it has the correct schema as some other type of openstack credential
	NotApplicationCredentialErrorMessage service.CacaoStandardErrorMessage = "not application credential"
)

// NewNotOpenStackCredentialError creates a new error for indicating that credential is not an OpenStack credential
// due to bad schema.
func NewNotOpenStackCredentialError(credID string, contextualMsg string) service.CacaoError {
	err := OpenStackCredentialError{}
	err.SetContextualError(fmt.Sprintf("%s, %s", credID, contextualMsg))
	err.SetStandardError(NotOpenStackCredentialErrorMessage)
	return &err
}

// NewNotApplicationCredentialError creates a new error for indicating that credential is not an application credential
// due to bad schema, but it has the correct schema as some other type of openstack credential.
func NewNotApplicationCredentialError(credID string, contextualMsg string) service.CacaoError {
	err := OpenStackCredentialError{}
	err.SetContextualError(fmt.Sprintf("%s, %s", credID, contextualMsg))
	err.SetStandardError(NotApplicationCredentialErrorMessage)
	return &err
}
