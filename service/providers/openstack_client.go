package providers

import (
	"context"
	"encoding/json"
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// OpenStackProvider is the interface for all provider services.
// All operations will require a credential, if credential ID is empty (not provided),
// then provider-openstack-service will attempt to select a credential by searching through user's credentials.
// The credential ID needs to be under the name of the session actor.
type OpenStackProvider interface {
	AuthenticationTest(ctx context.Context, actor service.Actor, variables map[string]string) error
	CredentialList(ctx context.Context, actor service.Actor, projectID, projectName string) ([]service.CredentialModel, error)
	RegionList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Region, error)
	ApplicationCredentialList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]ApplicationCredential, error)
	GetApplicationCredential(ctx context.Context, actor service.Actor, applicationCredentialID string, credOpt CredentialOption) (*ApplicationCredential, error)
	CreateApplicationCredential(ctx context.Context, actor service.Actor, scope ProjectScope, name string, credOpt CredentialOption) (string, error)
	DeleteApplicationCredential(ctx context.Context, actor service.Actor, credentialID string) error
	FlavorList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]Flavor, error)
	GetFlavor(ctx context.Context, actor service.Actor, region string, flavorID string, credOpt CredentialOption) (*Flavor, error)
	ImageList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]OpenStackImage, error)
	GetImage(ctx context.Context, actor service.Actor, region string, imageID string, credOpt CredentialOption) (*OpenStackImage, error)
	ProjectList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Project, error)
	GetProject(ctx context.Context, actor service.Actor, projectID string, credOpt CredentialOption) (*Project, error)
	ListCatalog(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]CatalogEntry, error)
	ListZone(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]DNSZone, error)
	ListRecordset(ctx context.Context, actor service.Actor, credOpt CredentialOption, zoneID string) ([]DNSRecordset, error)
	ListAllRecordset(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]DNSRecordset, error)
	GetToken(ctx context.Context, actor service.Actor, credOpt CredentialOption) (*Token, error)
	// PopulateCache(ctx context.Context, actor service.Actor, credOpt CredentialOption) error
}

// natsOpenStack is an implementation of Provider that sends requests
// over NATS and waits for responses.
type natsOpenStack struct {
	provider  common.ID
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewOpenStackProviderFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewOpenStackProviderFromConn(provider common.ID, queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (OpenStackProvider, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct openstack provider client")
	}
	return &natsOpenStack{
		provider:  provider,
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// CredentialList lists all OpenStack credentials that is suitable to be used on
// a specific instance of OpenStack Provider (specified by provider ID)
func (p *natsOpenStack) CredentialList(ctx context.Context, actor service.Actor, projectID, projectName string) ([]service.CredentialModel, error) {
	var reply CredentialListReply
	if err := p.doOperation(
		ctx,
		actor,
		CredentialListOp,
		WithCredentialID("")(),
		OpenStackCredentialListArgs{
			ProjectID:   projectID,
			ProjectName: projectName,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Credentials, nil
}

// ApplicationCredentialList gets the listing of available credentials and returns it.
func (p *natsOpenStack) ApplicationCredentialList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]ApplicationCredential, error) {
	var reply ApplicationCredentialListReply
	if err := p.doOperation(
		ctx,
		actor,
		ApplicationCredentialsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.ApplicationCredentials, nil
}

// GetApplicationCredential returns a specific application credential by its UUID.
func (p *natsOpenStack) GetApplicationCredential(ctx context.Context, actor service.Actor, id string, credOpt CredentialOption) (*ApplicationCredential, error) {
	var reply GetApplicationCredentialReply
	if err := p.doOperation(
		ctx,
		actor,
		ApplicationCredentialsGetOp,
		credOpt(),
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.ApplicationCredential, nil
}

// CreateApplicationCredential creates an application credential, and save it to credential microservice. This returns the credential ID on success.
func (p *natsOpenStack) CreateApplicationCredential(ctx context.Context, actor service.Actor, scope ProjectScope, name string, credOpt CredentialOption) (credID string, err error) {

	if err = scope.CheckScoped(); err != nil {
		return "", err
	}
	var responseCe cloudevents.Event
	err = p.doStanOperation(
		ctx,
		actor,
		EventApplicationCredentialsCreationRequested,
		credOpt(),
		ApplicationCredentialCreationArgs{
			Scope:       scope,
			NamePostfix: name,
		},
		[]common.EventType{EventApplicationCredentialsCreateFailed, EventApplicationCredentialsCreated},
		&responseCe)
	if err != nil {
		return "", err
	}
	return p.handleAppCredCreationResponse(responseCe)
}

func (p *natsOpenStack) handleAppCredCreationResponse(responseCe cloudevents.Event) (credID string, err error) {

	switch common.EventType(responseCe.Type()) {
	case EventApplicationCredentialsCreateFailed:
		var response CreateApplicationCredentialResponse
		err = json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return "", service.NewCacaoMarshalError(err.Error())
		}
		return "", fmt.Errorf("fail to create application credential, %w", response.BaseProviderReply.Session.GetServiceError())
	case EventApplicationCredentialsCreated:
		var response CreateApplicationCredentialResponse
		err = json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return "", err
		}
		return response.CredentialID, nil
	default:
		return "", fmt.Errorf("unknown response event type, %s", responseCe.Type())
	}
}

// DeleteApplicationCredential deletes an application credential. This uses an application credential (stored in credential microservice) to delete itself.
// The application credential needs to be unrestricted.
// If the credential is not an application credential (in terms of schema) then the deletion will fail with either NotOpenStackCredentialErrorMessage
// or NotApplicationCredentialErrorMessage.
// If the credential has the correct schema of an application credential, but the application credential does not exist (deleted or never existed at all),
// this operation will return no error.
func (p *natsOpenStack) DeleteApplicationCredential(ctx context.Context, actor service.Actor, credentialID string) error {
	if credentialID == "" {
		return service.NewCacaoInvalidParameterError("empty credential ID")
	}

	var responseCe cloudevents.Event
	err := p.doStanOperation(
		ctx,
		actor,
		EventApplicationCredentialsDeletionRequested,
		WithCredentialID(credentialID)(),
		nil, // no args for this operation
		[]common.EventType{EventApplicationCredentialsDeleteFailed, EventApplicationCredentialsDeleted},
		&responseCe)
	if err != nil {
		return err
	}
	return p.handleAppCredDeletionResponse(responseCe)
}

func (p *natsOpenStack) handleAppCredDeletionResponse(responseCe cloudevents.Event) error {
	switch common.EventType(responseCe.Type()) {
	case EventApplicationCredentialsDeleteFailed:
		var response DeleteApplicationCredentialResponse
		err := json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return service.NewCacaoMarshalError(err.Error())
		}
		return response.Session.GetServiceError() //fmt.Errorf("fail to delete application credential, %w", response.Session.GetServiceError())
	case EventApplicationCredentialsDeleted:
		var response DeleteApplicationCredentialResponse
		err := json.Unmarshal(responseCe.Data(), &response)
		if err != nil {
			return service.NewCacaoMarshalError(err.Error())
		}
		return nil
	default:
		return fmt.Errorf("unknown response event type, %s", responseCe.Type())
	}
}

// AuthenticationTest ...
func (p *natsOpenStack) AuthenticationTest(ctx context.Context, actor service.Actor, variables map[string]string) error {
	var reply AuthenticationTestReply
	if err := p.doOperation(
		ctx,
		actor,
		AuthenticationTestOp,
		ProviderCredential{
			Type: ProviderCredentialID,
			Data: "",
		},
		variables,
		&reply,
	); err != nil {
		return err
	}
	if reply.Session.GetServiceError() != nil {
		return reply.Session.GetServiceError()
	}
	return nil
}

// ImageList gets the listing of available images and returns it.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) ImageList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]OpenStackImage, error) {
	var reply ImageListReply[OpenStackImage]
	if err := p.doOperation(
		ctx,
		actor,
		ImagesListOp,
		credOpt(),
		ImageListingArgs{
			Region: region,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Images, nil
}

// GetImage returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) GetImage(ctx context.Context, actor service.Actor, region string, imageID string, credOpt CredentialOption) (*OpenStackImage, error) {
	var reply GetImageReply[OpenStackImage]
	if err := p.doOperation(
		ctx,
		actor,
		ImagesGetOp,
		credOpt(),
		ImageGetInArgs{
			ID:     imageID,
			Region: region,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Image, nil
}

// FlavorList returns a list of the available flavors.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) FlavorList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]Flavor, error) {
	var reply FlavorListReply

	if err := p.doOperation(
		ctx,
		actor,
		FlavorsListOp,
		credOpt(),
		FlavorListingArgs{
			Region: region,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavors, nil
}

// GetFlavor returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsOpenStack) GetFlavor(ctx context.Context, actor service.Actor, region string, flavorID string, credOpt CredentialOption) (*Flavor, error) {
	var reply GetFlavorReply
	if err := p.doOperation(
		ctx,
		actor,
		FlavorsGetOp,
		credOpt(),
		FlavorGetInArgs{
			ID:     flavorID,
			Region: region,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavor, nil
}

// ProjectList returns a list of the available projects.
func (p *natsOpenStack) ProjectList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Project, error) {
	var reply ProjectListReply

	if err := p.doOperation(
		ctx,
		actor,
		ProjectsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Projects, nil
}

// GetProject returns a specific project by its UUID.
func (p *natsOpenStack) GetProject(ctx context.Context, actor service.Actor, id string, credOpt CredentialOption) (*Project, error) {
	var reply GetProjectReply
	if err := p.doOperation(
		ctx,
		actor,
		ProjectsGetOp,
		credOpt(),
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Project, nil
}

// RegionList returns a list of the available regions.
func (p *natsOpenStack) RegionList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Region, error) {
	var reply RegionListReply

	if err := p.doOperation(
		ctx,
		actor,
		RegionsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Regions, nil
}

// ListCatalog returns a list of catalog entries.
func (p *natsOpenStack) ListCatalog(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]CatalogEntry, error) {
	var reply CatalogListReply

	if err := p.doOperation(
		ctx,
		actor,
		CatalogListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Catalog, nil
}

// ListZone lists all zones in the projects.
func (p *natsOpenStack) ListZone(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]DNSZone, error) {
	var reply DNSZoneListReply

	if err := p.doOperation(
		ctx,
		actor,
		DNSZoneListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Zone, nil
}

// ListRecordset list all recordset in a single zone in the project.
func (p *natsOpenStack) ListRecordset(ctx context.Context, actor service.Actor, credOpt CredentialOption, zoneID string) ([]DNSRecordset, error) {
	var reply DNSRecordsetListReply

	if err := p.doOperation(
		ctx,
		actor,
		DNSRecordsetListOp,
		credOpt(),
		DNSRecordsetListArgs{
			Zone: zoneID,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Recordset, nil
}

// ListAllRecordset list all recordset in all zones in the project.
func (p *natsOpenStack) ListAllRecordset(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]DNSRecordset, error) {
	var reply DNSRecordsetListReply

	if err := p.doOperation(
		ctx,
		actor,
		DNSRecordsetListOp,
		credOpt(),
		DNSRecordsetListArgs{
			Zone: "all",
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Recordset, nil
}

// GetToken issues a token with scope the same as the scope of the credential used.
func (p *natsOpenStack) GetToken(ctx context.Context, actor service.Actor, credOpt CredentialOption) (*Token, error) {
	var reply GetTokenReply

	if err := p.doOperation(
		ctx,
		actor,
		TokenGetOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return &reply.Token, nil
}

// PopulateCache tells the OpenStack provider to fetch the images, flavors, projects, and application credentials for a user
//func (p *natsOpenStack) PopulateCache(ctx context.Context, actor service.Actor, credOpt CredentialOption) error {
//	var reply CachePopulateReply
//	if err := p.doOperation(
//		ctx,
//		actor,
//		CachePopulateOp,
//		credOpt(),
//		nil,
//		&reply,
//	); err != nil {
//		return err
//	}
//	if reply.Session.GetServiceError() != nil {
//		return reply.Session.GetServiceError()
//	}
//	return nil
//}

func (p *natsOpenStack) doOperation(
	ctx context.Context,
	actor service.Actor,
	op common.QueryOp,
	credential ProviderCredential,
	args interface{},
	output interface{},
) error {
	var subject = p.subject(op)
	request := ProviderRequest{
		Session:    actor.Session(),
		Operation:  op,
		Provider:   p.provider,
		Credential: credential,
		Args:       args,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, subject, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		return err
	}
	replyCe, err := p.queryConn.Request(ctx, requestCe)
	if err != nil {
		return err
	}
	err = json.Unmarshal(replyCe.Data(), output)
	if err != nil {
		return service.NewCacaoMarshalError(err.Error())
	}
	return nil
}

func (p *natsOpenStack) subject(op common.QueryOp) common.QueryOp {
	return OpenStackQueryPrefix + op
}

// NATS Streaming (Stan) operation
func (p *natsOpenStack) doStanOperation(
	ctx context.Context,
	actor service.Actor,
	eventType common.EventType,
	credential ProviderCredential,
	args interface{},
	responseEventTypes []common.EventType,
	output *cloudevents.Event,
) error {
	request := ProviderRequest{
		Session:    actor.Session(),
		Operation:  "", // operation is specified by EventType
		Provider:   p.provider,
		Credential: credential,
		Args:       args,
	}
	tid := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, eventType, messaging2.AutoPopulateCloudEventSource, tid)
	if err != nil {
		return err
	}
	promise, err := p.eventConn.Request(requestCe, responseEventTypes)
	if err != nil {
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return err
	}
	*output = responseCe
	return nil
}
