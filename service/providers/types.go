package providers

import (
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// AuthenticationTestOp is the operation for testing authentication.
const AuthenticationTestOp common.QueryOp = "authentication.test"

// CredentialListOp is the operation for listing catalog.
const CredentialListOp common.QueryOp = "credential.list"

// ImagesListOp is the operation for listing images.
const ImagesListOp common.QueryOp = "images.list"

// ImagesGetOp is the operation for getting a single image.
const ImagesGetOp common.QueryOp = "images.get"

// FlavorsListOp is the operation for listing flavors.
const FlavorsListOp common.QueryOp = "flavors.list"

// FlavorsGetOp is the operation for getting a single flavor.
const FlavorsGetOp common.QueryOp = "flavors.get"

// RegionsListOp is the operation for listing regions.
const RegionsListOp common.QueryOp = "regions.list"

// CachePopulateOp is the operation for forcing a cache population for a user.
// const CachePopulateOp common.QueryOp = "cache.populate"

// Credentials represents auth information about the user.
type Credentials struct {
	ID      string      `json:"id" mapstructure:"id"`
	ProvEnv Environment `json:"openstack_env,omitempty" mapstructure:"openstack_env"`
}

// ProviderRequest ...
type ProviderRequest struct {
	service.Session `json:",inline"`
	Operation       common.QueryOp     `json:"op"`
	Provider        common.ID          `json:"provider"`
	Credential      ProviderCredential `json:"credential"`
	// Args depends on the type of query operation
	Args interface{} `json:"args"`
}

// BaseProviderReply ...
type BaseProviderReply struct {
	Session   service.Session `json:",inline"`
	Operation common.QueryOp  `json:"op"`
	Provider  common.ID       `json:"provider"`
}

// AuthenticationTestReply ...
type AuthenticationTestReply struct {
	BaseProviderReply `json:",inline"`
	Result            string `json:"result"`
}

// ToCloudEvent ...
func (r AuthenticationTestReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+AuthenticationTestOp, source)
}

// GetImageReply ...
type GetImageReply[T OpenStackImage | AWSImage] struct {
	BaseProviderReply `json:",inline"`
	Image             *T `json:"image"`
}

// ToCloudEvent ...
func (r GetImageReply[T]) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+ImagesGetOp, source)
}

// ImageListReply ...
type ImageListReply[T OpenStackImage | AWSImage] struct {
	BaseProviderReply `json:",inline"`
	Images            []T `json:"images"`
}

// ToCloudEvent ...
func (r ImageListReply[T]) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+ImagesListOp, source)
}

// GetFlavorReply ...
type GetFlavorReply struct {
	BaseProviderReply `json:",inline"`
	Flavor            *Flavor `json:"flavor"`
}

// ToCloudEvent ...
func (r GetFlavorReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+FlavorsGetOp, source)
}

// FlavorListReply ...
type FlavorListReply struct {
	BaseProviderReply `json:",inline"`
	Flavors           []Flavor `json:"flavors"`
}

// ToCloudEvent ...
func (r FlavorListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+FlavorsListOp, source)
}

// RegionListReply ...
type RegionListReply struct {
	BaseProviderReply `json:",inline"`
	Regions           []Region `json:"regions"`
}

// ToCloudEvent ...
func (r RegionListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+RegionsListOp, source)
}

// CachePopulateReply ...
// type CachePopulateReply BaseProviderReply

// ToCloudEvent ...
// func (r CachePopulateReply) ToCloudEvent(source string) (cloudevents.Event, error) {
// 	return messaging2.CreateCloudEvent(r, OpenStackQueryPrefix+CachePopulateOp, source)
// }

// Flavor (or size) represents the compute, memory and storage capacity of a computing
// instance.
// For reference, here is more information about flavor for OpenStack (https://docs.openstack.org/nova/latest/user/flavors.html)
type Flavor struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	RAM       int64  `json:"ram"`
	Ephemeral int64  `json:"ephemeral"`
	VCPUs     int64  `json:"vcpus"`
	IsPublic  bool   `json:"is_public"`
	Disk      int64  `json:"disk"`
}

// FlavorListingArgs are the arguments for listing flavors operation.
// For OpenStack, this represents the available options that can be passed to
// `openstack flavor list`.
type FlavorListingArgs struct {
	Public  bool
	Private bool
	All     bool
	Limit   int64
	Region  string
}

// String implements the Stringer interface for FlavorListingArgs. Note: this
// is not the same as the command-line representation.
func (f *FlavorListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v", f.Public, f.Private, f.All, f.Limit)
}

// FlavorGetInArgs are the arguments for getting a single flavor.
type FlavorGetInArgs struct {
	ID     string `json:"id"`
	Region string `json:"region"`
}

// Environment contains a map of string keys and string values representing the environment
// in which OpenStack CLI commands should execute.  This is important for setting the
// appropriate environment variables for accessing OpenStack
type Environment map[string]string

// OpenStackImage represents an image in OpenStack.
type OpenStackImage struct {
	Image
	DiskFormat      string   `json:"disk_format"`
	ContainerFormat string   `json:"container_format"`
	Checksum        string   `json:"checksum"`
	MinDisk         int64    `json:"min_disk"`
	MinRAM          int64    `json:"min_ram"`
	Visibility      string   `json:"visibility"`
	Protected       bool     `json:"protected"`
	Project         string   `json:"project"`
	Size            int64    `json:"size"`
	Tags            []string `json:"tags"`
}

// AWSImage is a struct that represents an AWS image
type AWSImage struct {
	Image
	// TODO: add AWS specific fields
}

// Image describes an VM image, and it's the shared part atrtibutes between OpenStack and AWS images
type Image struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}

// ImageListingSortOption contains the key and direction for a sorting
// option passed to the openstack command-line.
type ImageListingSortOption struct {
	Key       string
	Direction string
}

// ImageListingArgs contains the possibly settings for listing images.
type ImageListingArgs struct {
	Public        bool
	Private       bool
	Community     bool
	Shared        bool
	Name          string
	Status        string
	MemberStatus  string
	Project       string
	ProjectDomain string
	Tag           string
	Limit         int64
	SortOpts      []ImageListingSortOption
	Properties    []string // should be in the key=value format
	Region        string
}

// String implements the String interface for ImageListingArgss. Note:
// this does not return the command-line arguments for the corresponding
// openstack command.
func (i *ImageListingArgs) String() string {
	return fmt.Sprintf("%v %v %v %v %v %v %v %v %v %v %v %v %v",
		i.Public,
		i.Private,
		i.Community,
		i.Shared,
		i.Name,
		i.Status,
		i.MemberStatus,
		i.Project,
		i.ProjectDomain,
		i.Tag,
		i.Limit,
		i.SortOpts,
		i.Properties,
	)
}

// ImageGetInArgs are the available options for getting a single image.
type ImageGetInArgs struct {
	ID     string `json:"id"`
	Region string `json:"region"`
}

// Region is a cloud region.
// For OpenStack Region, ID and Name generally have the same value.
// For AWS, an example would be us-east-1.
type Region struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// CredentialListReply ...
type CredentialListReply struct {
	BaseProviderReply `json:",inline"`
	Credentials       []service.CredentialModel
}

// ToCloudEvent ...
func (r CredentialListReply) ToCloudEvent(source string, providerQueryPrefix common.QueryOp) (cloudevents.Event, error) {
	return messaging2.CreateCloudEvent(r, providerQueryPrefix+CredentialListOp, source)
}
