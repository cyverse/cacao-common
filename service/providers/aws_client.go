package providers

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// AWSProvider is the interface for all provider services.
// All operations will require a credential, if credential ID is empty (not provided),
// then provider-openstack-service will attempt to select a credential by searching through user's credentials.
// The credential ID needs to be under the name of the session actor.
type AWSProvider interface {
	AuthenticationTest(ctx context.Context, actor service.Actor, variables map[string]string) error
	CredentialList(ctx context.Context, actor service.Actor) ([]service.CredentialModel, error)
	RegionList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Region, error)
	FlavorList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]Flavor, error)
	GetFlavor(ctx context.Context, actor service.Actor, region string, flavorID string, credOpt CredentialOption) (*Flavor, error)
	ImageList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]AWSImage, error)
	GetImage(ctx context.Context, actor service.Actor, region string, imageID string, credOpt CredentialOption) (*AWSImage, error)
}

// natsAWS is an implementation of Provider that sends requests
// over NATS and waits for responses.
type natsAWS struct {
	provider  common.ID
	queryConn messaging2.QueryConnection
}

// NewAWSProviderFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewAWSProviderFromConn(provider common.ID, queryConn messaging2.QueryConnection) (AWSProvider, error) {
	// currently aws provider service has no event operations
	if queryConn == nil {
		return nil, fmt.Errorf("query connection is nil, cannot construct aws provider client")
	}
	return &natsAWS{
		provider:  provider,
		queryConn: queryConn,
	}, nil
}

// AuthenticationTest ...
func (p *natsAWS) AuthenticationTest(ctx context.Context, actor service.Actor, variables map[string]string) error {
	var reply AuthenticationTestReply
	if err := p.doOperation(
		ctx,
		actor,
		AuthenticationTestOp,
		ProviderCredential{
			Type: ProviderCredentialID,
			Data: "",
		},
		variables,
		&reply,
	); err != nil {
		return err
	}
	if reply.Session.GetServiceError() != nil {
		return reply.Session.GetServiceError()
	}
	return nil
}

// CredentialList lists all AWS credentials that are suitable to be used on a
// specific instance of AWS Provider (specified by provider ID) Currently, this
// is implemented for the sake of being compatible with openstack provider, but
// functionally it is just list all AWS credential, there is no additional
// filtering.
func (p *natsAWS) CredentialList(ctx context.Context, actor service.Actor) ([]service.CredentialModel, error) {
	var reply CredentialListReply
	if err := p.doOperation(
		ctx,
		actor,
		CredentialListOp,
		WithCredentialID("")(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Credentials, nil
}

// ImageList gets the listing of available images and returns it.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsAWS) ImageList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]AWSImage, error) {
	var reply ImageListReply[AWSImage]
	if err := p.doOperation(
		ctx,
		actor,
		ImagesListOp,
		credOpt(),
		ImageListingArgs{
			Region: region,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Images, nil
}

// GetImage returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsAWS) GetImage(ctx context.Context, actor service.Actor, region string, imageID string, credOpt CredentialOption) (*AWSImage, error) {
	var reply GetImageReply[AWSImage]
	if err := p.doOperation(
		ctx,
		actor,
		ImagesGetOp,
		credOpt(),
		ImageGetInArgs{
			Region: region,
			ID:     imageID,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Image, nil
}

// FlavorList returns a list of the available flavors.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsAWS) FlavorList(ctx context.Context, actor service.Actor, region string, credOpt CredentialOption) ([]Flavor, error) {
	var reply FlavorListReply

	if err := p.doOperation(
		ctx,
		actor,
		FlavorsListOp,
		credOpt(),
		FlavorListingArgs{
			Region: region,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavors, nil
}

// GetFlavor returns a specific image by its UUID.
// This operation will use the specified credential ID if provided (non-empty).
func (p *natsAWS) GetFlavor(ctx context.Context, actor service.Actor, region string, flavorID string, credOpt CredentialOption) (*Flavor, error) {
	var reply GetFlavorReply
	if err := p.doOperation(
		ctx,
		actor,
		FlavorsGetOp,
		credOpt(),
		FlavorGetInArgs{
			Region: region,
			ID:     flavorID,
		},
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Flavor, nil
}

// ProjectList returns a list of the available projects.
func (p *natsAWS) ProjectList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Project, error) {
	var reply ProjectListReply

	if err := p.doOperation(
		ctx,
		actor,
		ProjectsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Projects, nil
}

// GetProject returns a specific project by its UUID.
func (p *natsAWS) GetProject(ctx context.Context, actor service.Actor, id string, credOpt CredentialOption) (*Project, error) {
	var reply GetProjectReply
	if err := p.doOperation(
		ctx,
		actor,
		ProjectsGetOp,
		credOpt(),
		id,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Project, nil
}

// RegionList returns a list of the available regions.
func (p *natsAWS) RegionList(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]Region, error) {
	var reply RegionListReply

	if err := p.doOperation(
		ctx,
		actor,
		RegionsListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Regions, nil
}

// ListCatalog returns a list of catalog entries.
func (p *natsAWS) ListCatalog(ctx context.Context, actor service.Actor, credOpt CredentialOption) ([]CatalogEntry, error) {
	var reply CatalogListReply

	if err := p.doOperation(
		ctx,
		actor,
		CatalogListOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return reply.Catalog, nil
}

// GetToken issues a token with scope the same as the scope of the credential used.
func (p *natsAWS) GetToken(ctx context.Context, actor service.Actor, credOpt CredentialOption) (*Token, error) {
	var reply GetTokenReply

	if err := p.doOperation(
		ctx,
		actor,
		TokenGetOp,
		credOpt(),
		nil,
		&reply,
	); err != nil {
		return nil, err
	}
	if reply.Session.GetServiceError() != nil {
		return nil, reply.Session.GetServiceError()
	}
	return &reply.Token, nil
}

func (p *natsAWS) doOperation(
	ctx context.Context,
	actor service.Actor,
	op common.QueryOp,
	credential ProviderCredential,
	args interface{},
	output interface{},
) error {
	// note: check credential type before calling this function
	var subject = p.subject(op)
	request := ProviderRequest{
		Session:    actor.Session(),
		Operation:  op,
		Provider:   p.provider,
		Credential: credential,
		Args:       args,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, subject, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		return err
	}
	replyCe, err := p.queryConn.Request(ctx, requestCe)
	if err != nil {
		return err
	}
	err = json.Unmarshal(replyCe.Data(), output)
	if err != nil {
		return service.NewCacaoMarshalError(err.Error())
	}
	return nil
}

func (p *natsAWS) subject(op common.QueryOp) common.QueryOp {
	return AWSQueryPrefix + op
}
