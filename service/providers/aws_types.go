package providers

import (
	"gitlab.com/cyverse/cacao-common/common"
)

// AWSQueryPrefix is prefix for query operation for AWS provider
const AWSQueryPrefix common.QueryOp = common.NatsQueryOpPrefix + "providers.aws."
