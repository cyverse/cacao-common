package providers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestProviderCredential_UnmarshalJSON(t *testing.T) {
	type fields struct {
		Type string
		Data interface{}
	}
	type args struct {
		b []byte
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantErr  bool
		expected ProviderCredential
	}{
		{
			name:   "null",
			fields: fields{},
			args: args{
				b: []byte("null"),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "empty object",
			fields: fields{},
			args: args{
				b: []byte("{}"),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "unknown type",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"foobar", "data": "aaa"}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "empty type",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"", "data": "aaa"}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "type not string",
			fields: fields{},
			args: args{
				b: []byte(`{"type": 123, "data": "aaa"}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "credentialID",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"credentialID", "data": "myCredID"}`),
			},
			wantErr: false,
			expected: ProviderCredential{
				Type: ProviderCredentialID,
				Data: "myCredID",
			},
		},
		{
			// credentialID CAN be empty, this is used to indicate that service should search for credentials to use.
			name:   "credentialID EmptyString",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"credentialID", "data": ""}`),
			},
			wantErr: false,
			expected: ProviderCredential{
				Type: ProviderCredentialID,
				Data: "",
			},
		},
		{
			name:   "credentialID NotString",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"credentialID", "data": 123}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "credentialID Null",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"credentialID", "data": null}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "openstackToken Scoped",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": {"token": "aaaaa", "scope":{"project":{"id":"project_123"}, "domain":{"id":"domain_123"}}}}`),
			},
			wantErr: false,
			expected: ProviderCredential{
				Type: ProviderOpenStackTokenCredential,
				Data: OpenStackTokenCredential{
					Token: "aaaaa",
					Scope: ProjectScope{
						Project: struct {
							Name string `json:"name,omitempty"`
							ID   string `json:"id,omitempty"`
						}{
							Name: "",
							ID:   "project_123",
						},
						Domain: struct {
							Name string `json:"name,omitempty"`
							ID   string `json:"id,omitempty"`
						}{
							Name: "",
							ID:   "domain_123",
						},
					},
				},
			},
		},
		{
			name:   "openstackToken Unscoped",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": {"token": "aaaaa", "scope":{}}}`),
			},
			wantErr: false,
			expected: ProviderCredential{
				Type: ProviderOpenStackTokenCredential,
				Data: OpenStackTokenCredential{
					Token: "aaaaa",
					Scope: ProjectScope{},
				},
			},
		},
		{
			name:   "openstackToken Scope null",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": {"token": "aaaaa", "scope":null}}`),
			},
			wantErr: false,
			expected: ProviderCredential{
				Type: ProviderOpenStackTokenCredential,
				Data: OpenStackTokenCredential{
					Token: "aaaaa",
					Scope: ProjectScope{},
				},
			},
		},
		{
			name:   "openstackToken Scope not object",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": {"token": "aaaaa", "scope": 123}}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "openstackToken EmptyString",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": {"token": ""}}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "openstackToken NotObject",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": 123}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
		{
			name:   "openstackToken Null",
			fields: fields{},
			args: args{
				b: []byte(`{"type":"openstackToken", "data": null}`),
			},
			wantErr:  true,
			expected: ProviderCredential{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &ProviderCredential{
				Type: tt.fields.Type,
				Data: tt.fields.Data,
			}
			if err := c.UnmarshalJSON(tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr {
				assert.Equal(t, tt.expected, *c)
			}
		})
	}
}

func TestProviderCredential_MarshalJSON(t *testing.T) {
	type fields struct {
		Type string
		Data interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			name: "unknown type",
			fields: fields{
				Type: "foobar",
				Data: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty type",
			fields: fields{
				Type: "",
				Data: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "credentialID",
			fields: fields{
				Type: "credentialID",
				Data: "myCredID",
			},
			want:    []byte(`{"type":"credentialID","data":"myCredID"}`),
			wantErr: false,
		},
		{
			name: "credentialID EmptyString",
			fields: fields{
				Type: "credentialID",
				Data: "",
			},
			want:    []byte(`{"type":"credentialID","data":""}`),
			wantErr: false,
		},
		{
			name: "credentialID Nil",
			fields: fields{
				Type: "credentialID",
				Data: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "credentialID NotString",
			fields: fields{
				Type: "credentialID",
				Data: 123,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "openstackToken Unscoped",
			fields: fields{
				Type: "openstackToken",
				Data: OpenStackTokenCredential{
					Token: "aaaaa",
					Scope: ProjectScope{},
				},
			},
			want:    []byte(`{"type":"openstackToken","data":{"token":"aaaaa","scope":{"project":{},"domain":{}}}}`),
			wantErr: false,
		},
		{
			name: "openstackToken Scoped",
			fields: fields{
				Type: "openstackToken",
				Data: OpenStackTokenCredential{
					Token: "aaaaa",
					Scope: ProjectScope{
						Project: struct {
							Name string `json:"name,omitempty"`
							ID   string `json:"id,omitempty"`
						}{
							Name: "name123",
							ID:   "",
						},
						Domain: struct {
							Name string `json:"name,omitempty"`
							ID   string `json:"id,omitempty"`
						}{
							Name: "",
							ID:   "abcdefg",
						},
					},
				},
			},
			want:    []byte(`{"type":"openstackToken","data":{"token":"aaaaa","scope":{"project":{"name":"name123"},"domain":{"id":"abcdefg"}}}}`),
			wantErr: false,
		},
		{
			name: "openstackToken EmptyToken",
			fields: fields{
				Type: "openstackToken",
				Data: OpenStackTokenCredential{
					Token: "",
					Scope: ProjectScope{},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "openstackToken NotString",
			fields: fields{
				Type: "openstackToken",
				Data: 123,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "openstackToken Nil",
			fields: fields{
				Type: "openstackToken",
				Data: nil,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &ProviderCredential{
				Type: tt.fields.Type,
				Data: tt.fields.Data,
			}
			got, err := c.MarshalJSON()
			if tt.wantErr {
				assert.Error(t, err, "MarshalJSON()")
			} else {
				assert.NoError(t, err, "MarshalJSON()")
			}
			if tt.want != nil && got != nil {
				assert.Equalf(t, string(tt.want), string(got), "MarshalJSON()")
			} else {
				assert.Equalf(t, tt.want, got, "MarshalJSON()")
			}
		})
	}
}
