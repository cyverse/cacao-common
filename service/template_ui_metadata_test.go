package service

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/db"
	"go.mongodb.org/mongo-driver/bson"
)

func TestTemplateUIGroup_Conversion(t *testing.T) {
	jsonBytes := `{
		"type": "advanced_settings",
		"collapsible": true,
		"title": "Advanced",
		"items": [
			{
				"name": "root_storage",
				"field_type": "plain_text",
				"ui_label": "Root storage",
				"parameters": [
					{ "key": "windows_disclaimer", "value": true }
				]
			}
		]
	}`

	var groupItem TemplateUIGroupItem

	err := json.Unmarshal([]byte(jsonBytes), &groupItem)
	assert.NoError(t, err)

	assert.True(t, groupItem.IsGroup())
}

func TestTemplateUIStep_Conversion(t *testing.T) {
	jsonBytes := `{
		"title": "Parameters",
		"items": [
			{
				"name": "region",
				"field_type": "plain_text",
				"ui_label": "Choose Region"
			},
			{
				"name": "instance_name",
				"field_type": "plain_text",
				"ui_label": "Instance name"
			},
			{
				"name": "power_state",
				"field_type": "plain_text",
				"ui_label": "Power state"
			},
			{
				"name": "image_name",
				"field_type": "plain_text",
				"ui_label": "Boot image name",
				"parameters": [
					{ "key": "windows_disclaimer", "value": true }
				]
			},
			{
				"name": "instance_count",
				"field_type": "plain_text",
				"ui_label": "# of Instances"
			},
			{
				"name": "flavor",
				"field_type": "plain_text",
				"ui_label": "Size"
			},
			{
				"type": "advanced_settings",
				"collapsible": true,
				"title": "Advanced",
				"items": [
					{
						"name": "root_storage",
						"field_type": "plain_text",
						"ui_label": "Root storage",
						"parameters": [
							{ "key": "windows_disclaimer", "value": true }
						]
					}
				]
			}
		]
	}`

	var step TemplateUIStep

	err := json.Unmarshal([]byte(jsonBytes), &step)
	assert.NoError(t, err)

	assert.Equal(t, "Parameters", step.Title)
	assert.Len(t, step.Items, 7)

	// ui field
	assert.True(t, step.Items[0].IsField())
	assert.True(t, step.Items[1].IsField())
	assert.True(t, step.Items[2].IsField())
	assert.True(t, step.Items[3].IsField())
	assert.True(t, step.Items[4].IsField())
	assert.True(t, step.Items[5].IsField())

	// ui group
	assert.True(t, step.Items[6].IsGroup())
}

func TestTemplateUIMetadata_Conversion(t *testing.T) {
	jsonBytes := `{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/ui-schemas/v1/schema.json",
		"schema_version": "1",
		"author": "Sean Davey",
		"author_email": "sdavey@arizona.edu",
		"steps": [
			{
				"title": "Parameters",
				"items": [
					{
						"name": "region",
						"field_type": "plain_text",
						"ui_label": "Choose Region"
					},
					{
						"name": "instance_name",
						"field_type": "plain_text",
						"ui_label": "Instance name"
					},
					{
						"name": "power_state",
						"field_type": "plain_text",
						"ui_label": "Power state"
					},
					{
						"name": "image_name",
						"field_type": "plain_text",
						"ui_label": "Boot image name",
						"parameters": [
							{ "key": "windows_disclaimer", "value": true }
						]
					},
					{
						"name": "instance_count",
						"field_type": "plain_text",
						"ui_label": "# of Instances"
					},
					{
						"name": "flavor",
						"field_type": "plain_text",
						"ui_label": "Size"
					},
					{
						"type": "advanced_settings",
						"collapsible": true,
						"title": "Advanced",
						"items": [
							{
								"name": "root_storage",
								"field_type": "plain_text",
								"ui_label": "Root storage",
								"parameters": [
									{ "key": "windows_disclaimer", "value": true }
								]
							}
						]
					}
				]
			}
		]
	}`

	var uiMetadata TemplateUIMetadata

	err := json.Unmarshal([]byte(jsonBytes), &uiMetadata)
	assert.NoError(t, err)

	assert.Equal(t, "Sean Davey", uiMetadata.Author)
	assert.Len(t, uiMetadata.Steps, 1)

	// steps field
	step := uiMetadata.Steps[0]
	assert.Equal(t, "Parameters", step.Title)
	assert.Len(t, step.Items, 7)

	// ui field
	assert.True(t, step.Items[0].IsField())
	assert.True(t, step.Items[1].IsField())
	assert.True(t, step.Items[2].IsField())
	assert.True(t, step.Items[3].IsField())
	assert.True(t, step.Items[4].IsField())
	assert.True(t, step.Items[5].IsField())
	assert.True(t, step.Items[6].IsGroup())

	assert.Equal(t, TemplateUIGroupTypeAdvancedSettings, step.Items[6].GetGroup().Type)
}

func TestTemplateUIMetadata_BSON(t *testing.T) {
	jsonBytes := `{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/ui-schemas/v1/schema.json",
		"schema_version": "1",
		"author": "Sean Davey",
		"author_email": "sdavey@arizona.edu",
		"steps": [
			{
				"title": "Parameters",
				"items": [
					{
						"name": "region",
						"field_type": "plain_text",
						"ui_label": "Choose Region"
					},
					{
						"name": "instance_name",
						"field_type": "plain_text",
						"ui_label": "Instance name"
					},
					{
						"name": "power_state",
						"field_type": "plain_text",
						"ui_label": "Power state"
					},
					{
						"name": "image_name",
						"field_type": "plain_text",
						"ui_label": "Boot image name",
						"parameters": [
							{ "key": "windows_disclaimer", "value": true }
						]
					},
					{
						"name": "instance_count",
						"field_type": "plain_text",
						"ui_label": "# of Instances"
					},
					{
						"name": "flavor",
						"field_type": "plain_text",
						"ui_label": "Size"
					},
					{
						"type": "advanced_settings",
						"collapsible": true,
						"title": "Advanced",
						"items": [
							{
								"name": "root_storage",
								"field_type": "plain_text",
								"ui_label": "Root storage",
								"parameters": [
									{ "key": "windows_disclaimer", "value": true }
								]
							}
						]
					}
				]
			}
		]
	}`

	var uiMetadata TemplateUIMetadata

	err := json.Unmarshal([]byte(jsonBytes), &uiMetadata)
	assert.NoError(t, err)

	registry, err := db.GetBSONRegistry()
	assert.NoError(t, err)

	bsonBytes, err := bson.MarshalWithRegistry(registry, uiMetadata)
	assert.NoError(t, err)

	var uiMetadata2 TemplateUIMetadata
	err = bson.UnmarshalWithRegistry(registry, bsonBytes, &uiMetadata2)
	assert.NoError(t, err)

	assert.Equal(t, "Sean Davey", uiMetadata2.Author)
	assert.Len(t, uiMetadata2.Steps, 1)

	// steps field
	step := uiMetadata2.Steps[0]
	assert.Equal(t, "Parameters", step.Title)
	assert.Len(t, step.Items, 7)

	// ui field
	assert.True(t, step.Items[0].IsField())
	assert.True(t, step.Items[1].IsField())
	assert.True(t, step.Items[2].IsField())
	assert.True(t, step.Items[3].IsField())
	assert.True(t, step.Items[4].IsField())
	assert.True(t, step.Items[5].IsField())
	assert.True(t, step.Items[6].IsGroup())
	assert.Equal(t, TemplateUIGroupTypeAdvancedSettings, step.Items[6].GetGroup().Type)
}
