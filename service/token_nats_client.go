package service

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats subjects
const (
	// NatsSubjectToken is the prefix of the queue name for Token queries
	NatsSubjectToken common.QueryOp = common.NatsQueryOpPrefix + "token"

	// TokenListQueryOp is the queue name for TokenList query
	TokenListQueryOp = NatsSubjectToken + ".list"
	// TokenGetQueryOp is the queue name for TokenGet query
	TokenGetQueryOp = NatsSubjectToken + ".get"
	// TokenAuthCheckQueryOp is the queue name for checking authN and authZ of a token
	TokenAuthCheckQueryOp = NatsSubjectToken + ".authCheck"
	// TokenTypesListQueryOp is the cloudevent subject for ListTokenTypes
	TokenTypesListQueryOp = NatsSubjectToken + ".listTypes"
	// TokenCreateRequestedEvent is the cloudevent subject for TokenCreate
	TokenCreateRequestedEvent common.EventType = common.EventTypePrefix + "TokenCreateRequested"
	// TokenDeleteRequestedEvent is the cloudevent subject for TokenDelete
	TokenDeleteRequestedEvent common.EventType = common.EventTypePrefix + "TokenDeleteRequested"
	// TokenUpdateRequestedEvent is the cloudevent subject for TokenUpdate
	TokenUpdateRequestedEvent common.EventType = common.EventTypePrefix + "TokenUpdateRequested"
	// TokenRevokeRequestedEvent is the cloudevent subject for TokenRevoke
	TokenRevokeRequestedEvent common.EventType = common.EventTypePrefix + "TokenRevokeRequested"
	// TokenCreatedEvent is the cloudevent subject for TokenCreated
	TokenCreatedEvent common.EventType = common.EventTypePrefix + "TokenCreated"
	// TokenDeletedEvent is the cloudevent subject for TokenDeleted
	TokenDeletedEvent common.EventType = common.EventTypePrefix + "TokenDeleted"
	// TokenUpdatedEvent is the cloudevent subject for TokenUpdated
	TokenUpdatedEvent common.EventType = common.EventTypePrefix + "TokenUpdated"
	// TokenRevokedEvent is the cloudevent subject for TokenRevoked
	TokenRevokedEvent common.EventType = common.EventTypePrefix + "TokenRevoked"
	// TokenCreateFailedEvent is the cloudevent subject for TokenCreateFailed
	TokenCreateFailedEvent common.EventType = common.EventTypePrefix + "TokenCreateFailed"
	// TokenDeleteFailedEvent is the cloudevent subject for TokenDeleteFailed
	TokenDeleteFailedEvent common.EventType = common.EventTypePrefix + "TokenDeleteFailed"
	// TokenUpdateFailedEvent is the cloudevent subject for TokenUpdateFailed
	TokenUpdateFailedEvent common.EventType = common.EventTypePrefix + "TokenUpdateFailed"
	// TokenRevokeFailedEvent is the cloudevent subject for TokenRevokeFailed
	TokenRevokeFailedEvent common.EventType = common.EventTypePrefix + "TokenRevokeFailed"
)

// Nats Client
type natsTokenClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

func (client *natsTokenClient) Revoke(ctx context.Context, actor Actor, tokenID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.Revoke",
	})

	if !tokenID.Validate() {
		return NewCacaoInvalidParameterError("invalid Token ID")
	}

	request := TokenModel{
		Session: actor.Session(),
		ID:      tokenID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TokenRevokeRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TokenRevokedEvent,
		TokenRevokeFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TokenModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a token revoke response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

func (client *natsTokenClient) ListTypes(ctx context.Context, actor Actor) ([]TokenType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.ListTypes",
	})

	request := TokenTypeListModel{
		Session: actor.Session(),
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TokenTypesListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}
	var tokenTypes TokenTypeListModel
	err = json.Unmarshal(replyCe.Data(), &tokenTypes)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to token types list"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	return tokenTypes.TokenTypes, nil
}

// AuthCheck checks for authN and authZ of a token on an operation, and resolve to username of owner if successful
func (client *natsTokenClient) AuthCheck(ctx context.Context, actor Actor, token common.TokenID) (*TokenModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.Validate",
	})

	if len(token) <= 0 {
		return nil, NewCacaoInvalidParameterError("invalid Token")
	}

	request := TokenAuthCheckRequest{
		Session: actor.Session(),
		Token:   token,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TokenAuthCheckQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var tokenModel TokenModel
	err = json.Unmarshal(replyCe.Data(), &tokenModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to token object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := tokenModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &tokenModel, nil
}

func (client *natsTokenClient) Get(ctx context.Context, actor Actor, tokenID common.ID) (*TokenModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.Get",
	})

	if len(tokenID) <= 0 {
		return nil, NewCacaoInvalidParameterError("invalid Token ID")
	}

	request := TokenModel{
		Session: actor.Session(),
		ID:      tokenID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, TokenGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var tokenModel TokenModel
	err = json.Unmarshal(replyCe.Data(), &tokenModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to token object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := tokenModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &tokenModel, nil
}

func (client *natsTokenClient) List(ctx context.Context, actor Actor) ([]TokenModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.List",
	})
	var request = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, TokenListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var list TokenListModel
	err = json.Unmarshal(replyCe.Data(), &list)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to token list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := list.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return list.Tokens, nil
}

func (client *natsTokenClient) Create(ctx context.Context, actor Actor, token TokenCreateModel) (common.TokenID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.Create",
	})
	if len(token.Name) == 0 {
		return "", NewCacaoInvalidParameterError("invalid token name")
	}
	request := TokenModel{
		Session:          actor.Session(),
		ID:               token.ID,
		Owner:            actor.Actor,
		Name:             token.Name,
		Description:      token.Description,
		Public:           token.Public,
		Expiration:       token.Expiration,
		Start:            token.Start,
		Scopes:           token.Scopes,
		DelegatedContext: token.DelegatedContext,
		Type:             token.Type,
		Tags:             token.Tags,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TokenCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", err
	}
	eventsSubscribe := []common.EventType{
		TokenCreatedEvent,
		TokenCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return "", err
	}

	var response TokenCreateModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a token create response"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return common.TokenID(response.Token), nil
	}

	logger.Error(serviceError)
	return "", serviceError
}

func (client *natsTokenClient) Update(ctx context.Context, actor Actor, token TokenModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.Update",
	})

	if !token.ID.Validate() {
		return NewCacaoInvalidParameterError("invalid token id")
	}
	if len(token.Name) == 0 {
		return NewCacaoInvalidParameterError("invalid token name")
	}
	if len(token.Scopes) <= 0 {
		return NewCacaoInvalidParameterError("invalid token scopes")
	}
	request := TokenModel{
		Session:     actor.Session(),
		ID:          token.ID,
		Name:        token.Name,
		Description: token.Description,
		Public:      token.Public,
		Scopes:      token.Scopes,
	}

	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TokenUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TokenUpdatedEvent,
		TokenUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TokenModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a token update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}
	logger.Error(serviceError)
	return serviceError
}

func (client *natsTokenClient) UpdateFields(ctx context.Context, actor Actor, token TokenModel, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.UpdateFields",
	})

	if !token.ID.Validate() {
		return NewCacaoInvalidParameterError("invalid Token ID")
	}

	request := TokenModel{
		Session:          actor.Session(),
		ID:               token.ID,
		UpdateFieldNames: updateFieldNames,
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(token.Name) <= 0 {
				return NewCacaoInvalidParameterError("invalid token name")
			}
			request.Name = token.Name
		case "description":
			if len(token.Description) == 0 {
				return NewCacaoInvalidParameterError("invalid token description")
			}
			request.Description = token.Description
		case "last_accessed":
			if actor.Actor != ReservedCacaoSystemActor {
				return NewCacaoInvalidParameterError("invalid field last_accessed")
			}
			request.LastAccessed = token.LastAccessed
		}
	}

	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TokenUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TokenUpdatedEvent,
		TokenUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TokenModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a token update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

func (client *natsTokenClient) Delete(ctx context.Context, actor Actor, tokenID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsTokenClient.Delete",
	})

	if !tokenID.Validate() {
		return NewCacaoInvalidParameterError("invalid Token ID")
	}

	request := TokenModel{
		Session: actor.Session(),
		ID:      tokenID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, TokenDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		TokenDeletedEvent,
		TokenDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response TokenModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a token delete response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// NewNatsTokenClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsTokenClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (TokenClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct token client")
	}
	return &natsTokenClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}
