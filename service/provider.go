package service

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"time"
)

// ProviderClient is the client for provider metadata service
type ProviderClient interface {
	Get(ctx context.Context, actor Actor, id common.ID) (*ProviderModel, error)
	List(ctx context.Context, actor Actor) ([]ProviderModel, error)
	Create(ctx context.Context, actor Actor, provider ProviderModel) (common.ID, common.TransactionID, error)
	Delete(ctx context.Context, actor Actor, id common.ID) (common.TransactionID, error)
	Update(ctx context.Context, actor Actor, provider ProviderModel) (common.TransactionID, error)
}

// ProviderType is the type of provider
type ProviderType string

// ProviderType constants
const (
	OpenStackProviderType ProviderType = "openstack"
	AWSProviderType       ProviderType = "aws"
)

// ProviderModel represents a provider
type ProviderModel struct {
	ID        common.ID              `json:"id" bson:"_id"`
	Name      string                 `json:"name" bson:"name"`
	Type      ProviderType           `json:"type" bson:"type"`
	URL       string                 `json:"url" bson:"url"`
	Owner     string                 `json:"owner"`
	Public    bool                   `json:"public"`
	CreatedAt time.Time              `json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt time.Time              `json:"updated_at,omitempty" bson:"updated_at"`
	Metadata  map[string]interface{} `json:"metadata,omitempty" bson:"metadata"`
}

// ProviderRequest is the request object used for operations (get/list/create/update/delete),
// this is the struct that is being embedded into data field of cloudevent.
type ProviderRequest struct {
	Session
	ProviderModel
}

// ProviderResponse is the response/reply struct for operations like get/create/update/delete.
type ProviderResponse ProviderRequest

// ProviderListModel ...
type ProviderListModel struct {
	Session
	List []ProviderModel `json:"list"`
}
