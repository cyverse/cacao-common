package service

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// TemplateFormat is the format of a template
type TemplateFormat string

const (
	// TemplateFormatUnknown is for a template in unkonwn format
	TemplateFormatUnknown TemplateFormat = ""
	// TemplateFormatYAML is for a template in yaml format
	TemplateFormatYAML TemplateFormat = "yaml"
	// TemplateFormatJSON is for a template in json format
	TemplateFormatJSON TemplateFormat = "json"
	// TemplateFormatHCL is for a template in hcl format, the format used by Terraform for *.tf files
	TemplateFormatHCL TemplateFormat = "hcl"
)

// String returns the string representation of a TemplateFormat.
func (t TemplateFormat) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateFormat.
func (t TemplateFormat) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateFormat to the appropriate enumeration constant.
func (t *TemplateFormat) UnmarshalJSON(b []byte) error {
	// Template formats are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateFormatUnknown
	case string(TemplateFormatYAML):
		*t = TemplateFormatYAML
	case string(TemplateFormatJSON):
		*t = TemplateFormatJSON
	case string(TemplateFormatHCL):
		*t = TemplateFormatHCL
	default:
		return fmt.Errorf("invalid template format: %s", s)
	}

	return nil
}

// TemplateEngine is the type of a template engine
type TemplateEngine string

const (
	// TemplateEngineUnknown is for an unknown template engine
	TemplateEngineUnknown TemplateEngine = ""
	// TemplateEngineTerraform is for terraform template engine
	TemplateEngineTerraform TemplateEngine = "terraform"
	// TemplateEngineKubernetes is for kuberntes template engine, this refers raw manifest (no parameterization)
	TemplateEngineKubernetes TemplateEngine = "kubernetes"
)

// String returns the string representation of a TemplateEngine.
func (t TemplateEngine) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateEngine.
func (t TemplateEngine) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateEngine to the appropriate enumeration constant.
func (t *TemplateEngine) UnmarshalJSON(b []byte) error {
	// Template engines are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateEngineUnknown
	case string(TemplateEngineTerraform):
		*t = TemplateEngineTerraform
	case string(TemplateEngineKubernetes):
		*t = TemplateEngineKubernetes
	default:
		return fmt.Errorf("invalid template engine: %s", s)
	}

	return nil
}

// TemplateProviderType is the type of a template provider
type TemplateProviderType string

const (
	// TemplateProviderTypeUnknown is for a template for unknown provider
	TemplateProviderTypeUnknown TemplateProviderType = ""
	// TemplateProviderTypeOpenStack is for a template for Openstack provider
	TemplateProviderTypeOpenStack TemplateProviderType = "openstack"
	// TemplateProviderTypeAWS is for a template for AWS provider
	TemplateProviderTypeAWS TemplateProviderType = "aws"
	// TemplateProviderTypeGCP is for a template for GCP provider
	TemplateProviderTypeGCP TemplateProviderType = "gcp"
	// TemplateProviderTypeAzure is for a template for Azure provider
	TemplateProviderTypeAzure TemplateProviderType = "azure"
	// TemplateProviderTypeKubernetes is for a template for Kubernetes provider
	TemplateProviderTypeKubernetes TemplateProviderType = "kubernetes"
)

// String returns the string representation of a TemplateProviderType.
func (t TemplateProviderType) String() string {
	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateProviderType.
func (t TemplateProviderType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateProviderType to the appropriate enumeration constant.
func (t *TemplateProviderType) UnmarshalJSON(b []byte) error {
	// Template providers are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateProviderTypeUnknown
	case string(TemplateProviderTypeOpenStack):
		*t = TemplateProviderTypeOpenStack
	case string(TemplateProviderTypeAWS):
		*t = TemplateProviderTypeAWS
	case string(TemplateProviderTypeGCP):
		*t = TemplateProviderTypeGCP
	case string(TemplateProviderTypeAzure):
		*t = TemplateProviderTypeAzure
	case string(TemplateProviderTypeKubernetes):
		*t = TemplateProviderTypeKubernetes
	default:
		return fmt.Errorf("invalid template provider type: %s", s)
	}

	return nil
}

// TemplateTypeName is name (primary identifier) of template type
type TemplateTypeName string

// TemplateSourceType is the type of a template source
type TemplateSourceType string

const (
	// TemplateSourceTypeUnknown is for a template source from unknown
	TemplateSourceTypeUnknown TemplateSourceType = ""

	// TemplateSourceTypeGit is for a template source from git
	TemplateSourceTypeGit TemplateSourceType = "git"
)

// String returns the string representation of a TemplateSourceType.
func (t TemplateSourceType) String() string {
	if t == TemplateSourceTypeUnknown {
		return string(TemplateSourceTypeGit)
	}

	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateSourceType.
func (t TemplateSourceType) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateSourceType to the appropriate enumeration constant.
func (t *TemplateSourceType) UnmarshalJSON(b []byte) error {
	// Template source type are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateSourceTypeGit
	case "git":
		*t = TemplateSourceTypeGit
	default:
		return fmt.Errorf("invalid template source type: %s", s)
	}

	return nil
}

// TemplateSourceCredential is a struct for storing credentials to access template source
type TemplateSourceCredential struct {
	Username string `json:"GIT_USERNAME,omitempty"`
	Password string `json:"GIT_PASSWORD,omitempty"`
	SSHKey   string `json:"GIT_SSH_KEY,omitempty"`
}

// TemplateSourceVisibility is the visibility of a template source
type TemplateSourceVisibility string

const (
	// TemplateSourceVisibilityUnknown is for an unknown template source
	TemplateSourceVisibilityUnknown TemplateSourceVisibility = ""

	// TemplateSourceVisibilityPublic is for a public template source, no authentication is required to access the source
	TemplateSourceVisibilityPublic TemplateSourceVisibility = "public"
	// TemplateSourceVisibilityPrivate is for a private template source, the user’s credentials must be used to access the source
	TemplateSourceVisibilityPrivate TemplateSourceVisibility = "private"
)

// String returns the string representation of a TemplateSourceVisibility.
func (t TemplateSourceVisibility) String() string {
	if t == TemplateSourceVisibilityUnknown {
		return string(TemplateSourceVisibilityPrivate)
	}

	return string(t)
}

// MarshalJSON returns the JSON representation of a TemplateSourceVisibility.
func (t TemplateSourceVisibility) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.String())
}

// UnmarshalJSON converts the JSON representation of a TemplateSourceVisibility to the appropriate enumeration constant.
func (t *TemplateSourceVisibility) UnmarshalJSON(b []byte) error {
	// Template source visibilities are represented as strings.
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	// Validate and convert the value.
	switch strings.ToLower(s) {
	case "":
		*t = TemplateSourceVisibilityPrivate
	case "public":
		*t = TemplateSourceVisibilityPublic
	case "private":
		*t = TemplateSourceVisibilityPrivate
	default:
		return fmt.Errorf("invalid template source visibility: %s", s)
	}

	return nil
}

// TemplateSource is a struct for template source information
type TemplateSource struct {
	Type TemplateSourceType `json:"type"`
	URI  string             `json:"uri"` // e.g., 'github.com/cyverse/sometemplate'
	// AccessParameters is used to pass additional parameters for accessing source (e.g., git branch name ...)
	// e.g., for git,
	// "branch": "master"
	// "tag": "tag_test"
	// "path": "/sub_dir_to_template_in_repo"
	AccessParameters map[string]interface{}   `json:"access_parameters"`
	Visibility       TemplateSourceVisibility `json:"source_visibility"` // e.g., 'public' or 'private'
}

// GitTemplateSource is a convenience type for access the AccessParameters of TemplateSource.
// This offers better type safety than accessing the map[string]interface{} directly.
type GitTemplateSource struct {
	URI              string                            `json:"uri"`
	AccessParameters GitTemplateSourceAccessParameters `json:"access_parameters"`
	Visibility       TemplateSourceVisibility          `json:"source_visibility"`
}

// GitTemplateSourceAccessParameters ...
type GitTemplateSourceAccessParameters struct {
	Branch string `json:"branch" mapstructure:"branch"`
	Tag    string `json:"tag" mapstructure:"tag"`
	Path   string `json:"path" mapstructure:"path"`
	Commit string `json:"commit" mapstructure:"commit"`
}

// Git convert to GitTemplateSource. If TemplateSource is not type git, then return nil.
func (ts TemplateSource) Git() *GitTemplateSource {
	if ts.Type != TemplateSourceTypeGit {
		return nil
	}
	ap := GitTemplateSourceAccessParameters{}
	err := mapstructure.Decode(ts.AccessParameters, &ap)
	if err != nil {
		// should not happen, unless the structs or the map contains types that is not serializable.
		log.WithError(err).Panic("fail to convert TemplateSource to GitTemplateSource")
	}
	return &GitTemplateSource{
		URI:              ts.URI,
		AccessParameters: ap,
		Visibility:       ts.Visibility,
	}
}

// TemplateClient is the client for Template
type TemplateClient interface {
	// Source Types
	ListSourceTypes(ctx context.Context, actor Actor) ([]TemplateSourceType, error)

	// Types
	GetType(ctx context.Context, actor Actor, templateTypeName TemplateTypeName) (TemplateType, error)
	ListTypes(ctx context.Context, actor Actor) ([]TemplateType, error)
	ListTypesForProviderType(ctx context.Context, actor Actor, providerType TemplateProviderType) ([]TemplateType, error)

	CreateType(ctx context.Context, actor Actor, templateType TemplateType) error
	UpdateType(ctx context.Context, actor Actor, templateType TemplateType) error
	UpdateTypeFields(ctx context.Context, actor Actor, templateType TemplateType, updateFieldNames []string) error
	DeleteType(ctx context.Context, actor Actor, templateTypeName TemplateTypeName) error

	// Custom Field Type
	ListCustomFieldTypes(ctx context.Context, actor Actor) ([]TemplateCustomFieldType, error)
	GetCustomFieldType(ctx context.Context, actor Actor, customFieldTypeName string) (TemplateCustomFieldType, error)
	QueryCustomFieldType(ctx context.Context, actor Actor, customFieldTypeName string, queryParams map[string]string) (TemplateCustomFieldTypeQueryResult, error)

	CreateCustomFieldType(ctx context.Context, actor Actor, customFieldType TemplateCustomFieldType) error
	UpdateCustomFieldType(ctx context.Context, actor Actor, customFieldType TemplateCustomFieldType) error
	UpdateCustomFieldTypeFields(ctx context.Context, actor Actor, customFieldType TemplateCustomFieldType, updateFieldNames []string) error
	DeleteCustomFieldType(ctx context.Context, actor Actor, customFieldTypeName string) error

	// Template
	List(ctx context.Context, actor Actor, includeCacaoReservedPurposes bool) ([]Template, error)
	Get(ctx context.Context, actor Actor, templateID common.ID) (Template, error)

	Import(ctx context.Context, actor Actor, template Template, credentialID string) (common.ID, error)
	Update(ctx context.Context, actor Actor, template Template) error
	UpdateFields(ctx context.Context, actor Actor, template Template, updateFieldNames []string) error
	Sync(ctx context.Context, actor Actor, templateID common.ID, credentialID string) error
	Delete(ctx context.Context, actor Actor, templateID common.ID) error

	// TemplateVersion
	ListTemplateVersions(ctx context.Context, actor Actor, templateID common.ID) ([]TemplateVersion, error)
	GetTemplateVersion(ctx context.Context, actor Actor, versionID common.ID) (*TemplateVersion, error)
	DisableTemplateVersion(ctx context.Context, actor Actor, versionID common.ID) error
}

// TemplateType is the standard interface for all TemplateType implementations
type TemplateType interface {
	SessionContext

	GetPrimaryID() TemplateTypeName // points to Name
	GetName() TemplateTypeName
	GetFormats() []TemplateFormat
	GetEngine() TemplateEngine
	GetProviderTypes() []TemplateProviderType
	GetUpdateFieldNames() []string

	SetPrimaryID(id TemplateTypeName)
	SetName(name TemplateTypeName)
	SetFormats(formats []TemplateFormat)
	SetEngine(engine TemplateEngine)
	SetProviderTypes(providerTypes []TemplateProviderType)
	SetUpdateFieldNames(fieldNames []string)
}

// Template is the standard interface for all Template implementations
type Template interface {
	SessionContext

	GetPrimaryID() common.ID // points to ID
	GetID() common.ID
	GetOwner() string
	GetName() string
	GetDescription() string
	IsPublic() bool
	IsDeleted() bool
	GetSource() TemplateSource
	GetMetadata() TemplateMetadata
	GetUIMetadata() TemplateUIMetadata
	GetCreatedAt() time.Time
	GetUpdatedAt() time.Time
	GetUpdateFieldNames() []string
	GetCredentialID() string
	GetIncludeCacaoReservedPurposes() bool
	GetLatestVersionID() common.ID

	SetPrimaryID(id common.ID)
	SetID(id common.ID)
	SetName(name string)
	SetDescription(description string)
	SetPublic(isPublic bool)
	SetSource(source TemplateSource)
	SetUpdateFieldNames(fieldNames []string)
	SetCredentialID(credentialID string)
	SetIncludeCacaoReservedPurposes(include bool)
}

// TemplateSourceTypeListModel is the template type list model
type TemplateSourceTypeListModel struct {
	Session

	TemplateSourceTypes []TemplateSourceType `json:"template_source_types"`
}

// GetTemplateSourceTypes returns template source types
func (t *TemplateSourceTypeListModel) GetTemplateSourceTypes() []TemplateSourceType {
	return t.TemplateSourceTypes
}

// TemplateTypeModel is the type of template and is only compatible to certain cloud providers.
// Implements TemplateType.
type TemplateTypeModel struct {
	Session

	Name          TemplateTypeName       `json:"name"`
	Formats       []TemplateFormat       `json:"formats,omitempty"`        // e.g., {'yaml', 'json'}
	Engine        TemplateEngine         `json:"engine,omitempty"`         // e.g., 'terraform'
	ProviderTypes []TemplateProviderType `json:"provider_types,omitempty"` // e.g., {'openstack', 'aws', `kubernetes`}

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
}

// GetPrimaryID returns the primary ID of the TemplateType
func (t *TemplateTypeModel) GetPrimaryID() TemplateTypeName {
	return t.Name
}

// GetName returns the name of the TemplateType
func (t *TemplateTypeModel) GetName() TemplateTypeName {
	return t.Name
}

// GetFormats returns the formats supported by the TemplateType
func (t *TemplateTypeModel) GetFormats() []TemplateFormat {
	return t.Formats
}

// GetEngine returns the engine for the TemplateType
func (t *TemplateTypeModel) GetEngine() TemplateEngine {
	return t.Engine
}

// GetProviderTypes returns the provider types supported by the TemplateType
func (t *TemplateTypeModel) GetProviderTypes() []TemplateProviderType {
	return t.ProviderTypes
}

// GetUpdateFieldNames returns the field names to be updated
func (t *TemplateTypeModel) GetUpdateFieldNames() []string {
	return t.UpdateFieldNames
}

// SetPrimaryID sets the primary ID of the TemplateType
func (t *TemplateTypeModel) SetPrimaryID(id TemplateTypeName) {
	t.Name = id
}

// SetName sets the name of the TemplateType
func (t *TemplateTypeModel) SetName(name TemplateTypeName) {
	t.Name = name
}

// SetFormats sets the formats supported by the TemplateType
func (t *TemplateTypeModel) SetFormats(formats []TemplateFormat) {
	t.Formats = formats
}

// SetEngine sets the engine for the TemplateType
func (t *TemplateTypeModel) SetEngine(engine TemplateEngine) {
	t.Engine = engine
}

// SetProviderTypes sets the provider types supported by the TemplateType
func (t *TemplateTypeModel) SetProviderTypes(providerTypes []TemplateProviderType) {
	t.ProviderTypes = providerTypes
}

// SetUpdateFieldNames sets the field names to be updated
func (t *TemplateTypeModel) SetUpdateFieldNames(fieldNames []string) {
	t.UpdateFieldNames = fieldNames
}

// TemplateTypeListItemModel is the element type in TemplateTypeListModel
type TemplateTypeListItemModel struct {
	Name          TemplateTypeName       `json:"name"`
	Formats       []TemplateFormat       `json:"formats,omitempty"`        // e.g., {'yaml', 'json'}
	Engine        TemplateEngine         `json:"engine"`                   // e.g., 'terraform'
	ProviderTypes []TemplateProviderType `json:"provider_types,omitempty"` // e.g., {'openstack', 'aws', `kubernetes`}
}

// TemplateTypeListModel is the template type list model
type TemplateTypeListModel struct {
	Session

	TemplateTypes []TemplateTypeListItemModel `json:"template_types"`
}

// GetTemplateTypes returns template types
func (t *TemplateTypeListModel) GetTemplateTypes() []TemplateType {
	templateTypes := make([]TemplateType, 0, len(t.TemplateTypes))

	for _, templateType := range t.TemplateTypes {
		templateTypeModel := TemplateTypeModel{
			Session:       t.Session,
			Name:          templateType.Name,
			Formats:       templateType.Formats,
			Engine:        templateType.Engine,
			ProviderTypes: templateType.ProviderTypes,
		}

		templateTypes = append(templateTypes, &templateTypeModel)
	}

	return templateTypes
}

// TemplateModel is the template model. Implements Template.
type TemplateModel struct {
	Session

	ID              common.ID          `json:"id"`
	Owner           string             `json:"owner"`
	Name            string             `json:"name"`
	Description     string             `json:"description"`
	Public          bool               `json:"public"`
	Deleted         bool               `json:"deleted"`
	Source          TemplateSource     `json:"source"`
	LatestVersionID common.ID          `json:"latest_version_id"`
	Metadata        TemplateMetadata   `json:"metadata"`
	UIMetadata      TemplateUIMetadata `json:"ui_metadata"`
	CreatedAt       time.Time          `json:"created_at"`
	UpdatedAt       time.Time          `json:"updated_at"`

	UpdateFieldNames             []string `json:"update_field_names,omitempty"`
	CredentialID                 string   `json:"credential_id,omitempty"`                   // for import and sync
	IncludeCacaoReservedPurposes bool     `json:"include_cacao_reserved_purposes,omitempty"` // for list
}

// NewTemplateID generates a new TemplateID
func NewTemplateID() common.ID {
	return common.NewID("template")
}

// GetPrimaryID returns the primary ID of the Template
func (t *TemplateModel) GetPrimaryID() common.ID {
	return t.ID
}

// GetID returns the ID of the Template
func (t *TemplateModel) GetID() common.ID {
	return t.ID
}

// GetOwner returns the owner of the Template
func (t *TemplateModel) GetOwner() string {
	return t.Owner
}

// GetName returns the name of the Template
func (t *TemplateModel) GetName() string {
	return t.Name
}

// GetDescription returns the description of the Template
func (t *TemplateModel) GetDescription() string {
	return t.Description
}

// IsPublic returns the accessibility of the Template, returns True if it is publically accessible
func (t *TemplateModel) IsPublic() bool {
	return t.Public
}

// IsDeleted returns the true if the template is deleted.
func (t *TemplateModel) IsDeleted() bool {
	return t.Deleted
}

// GetSource returns the source of the Template
func (t *TemplateModel) GetSource() TemplateSource {
	return t.Source
}

// GetMetadata returns the metadata of the Template
func (t *TemplateModel) GetMetadata() TemplateMetadata {
	return t.Metadata
}

// GetUIMetadata returns the ui metadata of the Template
func (t *TemplateModel) GetUIMetadata() TemplateUIMetadata {
	return t.UIMetadata
}

// GetCreatedAt returns the creation time of the Template
func (t TemplateModel) GetCreatedAt() time.Time {
	return t.CreatedAt
}

// GetUpdatedAt returns the modified time of the Template
func (t TemplateModel) GetUpdatedAt() time.Time {
	return t.UpdatedAt
}

// GetUpdateFieldNames returns the field names to be updated
func (t *TemplateModel) GetUpdateFieldNames() []string {
	return t.UpdateFieldNames
}

// GetCredentialID returns the credential id of the Template
func (t TemplateModel) GetCredentialID() string {
	return t.CredentialID
}

// GetIncludeCacaoReservedPurposes returns if it include Templates having cacao reserved purposes
func (t TemplateModel) GetIncludeCacaoReservedPurposes() bool {
	return t.IncludeCacaoReservedPurposes
}

// GetLatestVersionID returns the ID of latest version of the template
func (t TemplateModel) GetLatestVersionID() common.ID {
	return t.LatestVersionID
}

// SetPrimaryID sets the primary ID of the Template
func (t *TemplateModel) SetPrimaryID(id common.ID) {
	t.ID = id
}

// SetID sets the ID of the Template
func (t *TemplateModel) SetID(id common.ID) {
	t.ID = id
}

// SetName sets the name of the Template
func (t *TemplateModel) SetName(name string) {
	t.Name = name
}

// SetDescription sets the description of the Template
func (t *TemplateModel) SetDescription(description string) {
	t.Description = description
}

// SetPublic sets the accessibility of the Template
func (t *TemplateModel) SetPublic(isPublic bool) {
	t.Public = isPublic
}

// SetSource sets the source of the Template
func (t *TemplateModel) SetSource(source TemplateSource) {
	t.Source = source
}

// SetUpdateFieldNames sets the field names to be updated
func (t *TemplateModel) SetUpdateFieldNames(fieldNames []string) {
	t.UpdateFieldNames = fieldNames
}

// SetCredentialID sets the credential id of the Template
func (t *TemplateModel) SetCredentialID(credentialID string) {
	t.CredentialID = credentialID
}

// SetIncludeCacaoReservedPurposes sets to include Templates having cacao reserved purposes
func (t *TemplateModel) SetIncludeCacaoReservedPurposes(include bool) {
	t.IncludeCacaoReservedPurposes = include
}

// TemplateListItemModel is the element type in TemplateListModel
type TemplateListItemModel struct {
	ID              common.ID          `json:"id"`
	Owner           string             `json:"owner"`
	Name            string             `json:"name"`
	Description     string             `json:"description"`
	Public          bool               `json:"public"`
	Deleted         bool               `json:"deleted"`
	Source          TemplateSource     `json:"source"`
	LatestVersionID common.ID          `json:"latest_version_id"`
	Metadata        TemplateMetadata   `json:"metadata"`
	UIMetadata      TemplateUIMetadata `json:"ui_metadata"`
	CreatedAt       time.Time          `json:"created_at"`
	UpdatedAt       time.Time          `json:"updated_at"`
}

// TemplateListModel is the template list model
type TemplateListModel struct {
	Session

	Templates []TemplateListItemModel `json:"templates"`
}

// GetTemplates returns templates
func (t *TemplateListModel) GetTemplates() []Template {
	templates := make([]Template, 0, len(t.Templates))

	for _, template := range t.Templates {
		templateModel := TemplateModel{
			Session:     t.Session,
			ID:          template.ID,
			Owner:       template.Owner,
			Name:        template.Name,
			Description: template.Description,
			Public:      template.Public,
			Source:      template.Source,
			Metadata:    template.Metadata,
			UIMetadata:  template.UIMetadata,
			CreatedAt:   template.CreatedAt,
			UpdatedAt:   template.UpdatedAt,
		}

		templates = append(templates, &templateModel)
	}

	return templates
}

// TemplateVersion is a version of a template, it captures the template metadata at a specific checkpoint (e.g. git commit).
type TemplateVersion struct {
	ID         common.ID      `json:"id"`
	TemplateID common.ID      `json:"template_id"`
	Source     TemplateSource `json:"source"`
	Disabled   bool           `json:"disabled"`
	DisabledAt time.Time      `json:"disabled_at"`
	// Credential that is needed to get the template from source, empty if not needed
	CredentialID string             `json:"credential_id"`
	Metadata     TemplateMetadata   `json:"metadata"`
	UIMetadata   TemplateUIMetadata `json:"ui_metadata"`
	CreatedAt    time.Time          `json:"created_at"`
}

// TemplateVersionDisableRequest is the event body for events related to template version
type TemplateVersionDisableRequest struct {
	Session
	VersionID common.ID `json:"version_id"`
}

// TemplateVersionDisableResponse is the response event of TemplateVersionDisableRequest
type TemplateVersionDisableResponse = TemplateVersionModel

// TemplateVersionModel is the body of the query/reply for getting a single version, and query for listing versions
type TemplateVersionModel struct {
	Session
	TemplateVersion
}

// TemplateVersionListModel is the body of reply for listing versions
type TemplateVersionListModel struct {
	Session
	Versions []TemplateVersion `json:"versions"`
}
