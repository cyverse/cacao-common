// Package service this file contains general error strings, types, or constants that are used within the service package
package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateCacaoError(t *testing.T) {
	cerr := NewCacaoUnauthorizedError("test unauthorized access")
	assert.Error(t, cerr)
	assert.NotEmpty(t, cerr)
}

func TestMarshalCacaoError(t *testing.T) {
	cerr := NewCacaoUnauthorizedError("test unauthorized access")
	assert.Error(t, cerr)
	assert.NotEmpty(t, cerr)
	jsonBytes, err := cerr.MarshalJSON()
	assert.NoError(t, err)
	assert.NotEmpty(t, jsonBytes)

	cerr2, err := ConvertJSONToCacaoError(jsonBytes)
	assert.NoError(t, err)
	assert.NotEmpty(t, cerr2)

	assert.IsType(t, cerr, cerr2)
}
