package service

import (
	"context"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// JS2AllocationClient is the client for JS2Allocation
type JS2AllocationClient interface {
	// Deprecated: new js2allocation service no longer support this
	GetUser(ctx context.Context, actor Actor, nocache bool) (*JS2UserModel, error)
	List(ctx context.Context, actor Actor, nocache bool) ([]JS2ProjectModel, error)
}

// JS2UserModel is the JS2 User model
type JS2UserModel struct {
	Session

	Owner        string    `json:"owner"`
	TACCUsername string    `json:"tacc_username"`
	RetrievedAt  time.Time `json:"retrieved_at"`

	NoCache bool `json:"nocache"`
}

// GetPrimaryID returns the primary ID of the JS2 User
func (u *JS2UserModel) GetPrimaryID() string {
	return u.Owner
}

// JS2PI is a struct for JetStream PI information
type JS2PI struct {
	Username  string `json:"username"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

// JS2Allocation is a struct for JetStream Allocation information
type JS2Allocation struct {
	ID               string    `json:"id"`
	Start            time.Time `json:"start"`
	End              time.Time `json:"end"`
	ProjectCode      string    `json:"project_code"`
	Resource         string    `json:"resource"`
	ComputeAllocated float64   `json:"compute_allocated"`
	ComputeUsed      float64   `json:"compute_used"`
	StorageAllocated float64   `json:"storage_allocated"`
}

// JS2ProjectModel is the JS2 Project model
type JS2ProjectModel struct {
	Session

	ID          common.ID       `json:"id"`
	Owner       string          `json:"owner"`
	Title       string          `json:"title"`
	Description string          `json:"description,omitempty"`
	PI          JS2PI           `json:"pi"`
	Allocations []JS2Allocation `json:"allocations"`
	RetrievedAt time.Time       `json:"retrieved_at"`
}

// NewJS2ProjectID generates a new JS2ProjectID
func NewJS2ProjectID() common.ID {
	return common.NewID("js2project")
}

// GetPrimaryID returns the primary ID of the JS2 Project
func (p *JS2ProjectModel) GetPrimaryID() common.ID {
	return p.ID
}

// JS2ProjectListModel is the JS2 Project list model
type JS2ProjectListModel struct {
	Session
	Projects []JS2ProjectModel `json:"projects"`
	NoCache  bool              `json:"nocache"`
}
