package service

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats subjects
const (
	// NatsSubjectJS2Allocation is the prefix of the queue name for JS2Allocation queries
	NatsSubjectJS2Allocation common.QueryOp = common.NatsQueryOpPrefix + "js2allocation"

	// JS2UserGetQueryOp is the queue name for JS2UserGet query
	JS2UserGetQueryOp = NatsSubjectJS2Allocation + ".getUser"
	// JS2ProjectListQueryOp is the queue name for JS2ProjectList query
	JS2ProjectListQueryOp = NatsSubjectJS2Allocation + ".list"
)

// Nats Client
type natsJS2AllocationClient struct {
	queryConn messaging2.QueryConnection
}

// NewNatsJS2AllocationClientFromConn creates a new client from existing
// Connections.
func NewNatsJS2AllocationClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (JS2AllocationClient, error) {
	if queryConn == nil {
		return nil, fmt.Errorf("query connection is nil, cannot construct js2allocation client")
	}
	return &natsJS2AllocationClient{queryConn: queryConn}, nil
}

// GetUser returns JS2 User for the user
// Deprecated: new js2allocation service no longer support this
func (client *natsJS2AllocationClient) GetUser(ctx context.Context, actor Actor, nocache bool) (*JS2UserModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsJS2AllocationClient.GetUser",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})
	logger.Warn("deprecated")
	request := JS2UserModel{
		Session: actor.Session(),
		NoCache: nocache,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, JS2UserGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var userModel JS2UserModel
	err = json.Unmarshal(replyCe.Data(), &userModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to JS2 user object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := userModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return &userModel, nil
}

// List returns all JS2 Projects for the user
func (client *natsJS2AllocationClient) List(ctx context.Context, actor Actor, nocache bool) ([]JS2ProjectModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsJS2AllocationClient.List",
		"actor":    actor.Actor,
		"emulator": actor.Emulator,
	})

	request := JS2ProjectListModel{
		Session: actor.Session(),
		NoCache: nocache,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, JS2ProjectListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var projectListModel JS2ProjectListModel
	err = json.Unmarshal(replyCe.Data(), &projectListModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to JS2 project list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := projectListModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return projectListModel.Projects, nil
}
