package service

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

// WorkspaceClient is the client for Workspace
type WorkspaceClient interface {
	Get(ctx context.Context, actor Actor, workspaceID common.ID) (*WorkspaceModel, error)
	List(ctx context.Context, actor Actor) ([]WorkspaceModel, error)

	Create(ctx context.Context, actor Actor, workspace WorkspaceModel) (common.ID, error)
	CreateAsync(ctx context.Context, actor Actor, workspace WorkspaceModel) (messaging2.EventResponsePromise, error)
	CreateAsyncResponse(ctx context.Context, promise messaging2.EventResponsePromise) (WorkspaceModel, error)
	Update(ctx context.Context, actor Actor, workspace WorkspaceModel) error
	UpdateFields(ctx context.Context, actor Actor, workspace WorkspaceModel, updateFieldNames []string) error
	Delete(ctx context.Context, actor Actor, workspaceID common.ID) error
}

// WorkspaceModel is the workspace model
type WorkspaceModel struct {
	Session

	ID                common.ID `json:"id"`
	Owner             string    `json:"owner"`
	Name              string    `json:"name"`
	Description       string    `json:"description,omitempty"`
	DefaultProviderID common.ID `json:"default_provider_id,omitempty"`
	CreatedAt         time.Time `json:"created_at"`
	UpdatedAt         time.Time `json:"updated_at"`

	UpdateFieldNames []string `json:"update_field_names,omitempty"`
}

// NewWorkspaceID generates a new WorkspaceID
func NewWorkspaceID() common.ID {
	return common.NewID("workspace")
}

// GetPrimaryID returns the primary ID of the Workspace
func (w *WorkspaceModel) GetPrimaryID() common.ID {
	return w.ID
}

// WorkspaceListModel is the workspace list model
type WorkspaceListModel struct {
	Session
	Workspaces []WorkspaceModel `json:"workspaces"`
}
