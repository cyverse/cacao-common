// Package service is a temporary package and will eventually be replace
// with cacao-types/service/user.go
// TODO: EJS - replace with cacao-types
package service

import (
	"context"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
)

const (
	// DefaultUserListItemsMax is the default maximum number of UserList items to return
	DefaultUserListItemsMax = 500

	// ReservedCacaoSystemActor is a special actor that is used by the API gateway and perhaps other internal services,
	// which may be used to perform adminstrative operations on the user microservice, such as autocreating users
	// This is not a real user and logic should be added to prohibit creation of such a username
	ReservedCacaoSystemActor = "RESERVED_CACAO_SYSTEM_ACTOR"

	// ReservedAnonymousActor is a special actor that is used by the API gateway to denote public access. This is used
	// for endpoints that do not require authentication, but needs to publish events/query onto the message bus.
	ReservedAnonymousActor = "RESERVED_ANONYMOUS_ACTOR"
)

// UserClient is client for user microservice.
type UserClient interface {
	Add(ctx context.Context, actor Actor, user UserModel) error
	Update(ctx context.Context, actor Actor, user UserModel) error
	Get(ctx context.Context, actor Actor, username string, withSettings bool) (*UserModel, error) // get a user based on username
	Delete(ctx context.Context, actor Actor, user UserModel) error
	// UserHasLoggedIn will publish a notification event that notify relevant services that a user has logged in.
	UserHasLoggedIn(ctx context.Context, actor Actor, user UserModel) error

	GetSettings(ctx context.Context, actor Actor, username string) (*common.UserSettings, error)
	GetConfigs(ctx context.Context, actor Actor, username string) (*map[string]string, error)
	GetFavorites(ctx context.Context, actor Actor, username string) (*map[string][]string, error)
	GetRecents(ctx context.Context, actor Actor, username string) (*map[string]string, error)
	SetConfig(ctx context.Context, actor Actor, username string, key string, value string) error
	AddFavorite(ctx context.Context, actor Actor, username string, key string, value string) error
	DeleteFavorite(ctx context.Context, actor Actor, username string, key string, value string) error
	SetRecent(ctx context.Context, actor Actor, username string, key string, value string) error

	Search(ctx context.Context, actor Actor, filter UserListFilter) (UserList, error) // Given a filter, return a user list up to DefaultUserListItemsMax
	SearchNext(ctx context.Context, actor Actor, userlist UserList) error             // load the next in a search, given the original UserList
}

// UserModel represents one user
type UserModel struct {
	Session
	Username          string              `json:"username"`
	FirstName         string              `json:"first_name,omitempty"`
	LastName          string              `json:"last_name,omitempty"`
	PrimaryEmail      string              `json:"primary_email,omitempty"`
	IsAdmin           bool                `json:"is_admin,omitempty"`
	DisabledAt        time.Time           `json:"disabled_at,omitempty"`
	WithSettings      bool                `json:"with_settings,omitempty"`
	UserSettings      common.UserSettings `json:"settings,omitempty"`
	CreatedAt         time.Time           `json:"created_at"`
	UpdatedAt         time.Time           `json:"updated_at"`
	UpdatedBy         string              `json:"updated_by"`
	UpdatedEmulatorBy string              `json:"updated_emulator_by"`
}

// GetPrimaryID returns the username of user.
func (u UserModel) GetPrimaryID() string {
	return u.Username
}

// UserList is the interface that defines objects that can retrieve lists of users
// TODO - currently not implemented
type UserList interface {
	SessionContext

	GetFilter() UserListFilter
	GetUsers() []UserModel
	GetStart() int           // index of start of current list, -1 if no results or error, available after successful Load()
	GetSize() int            // size of the current list, 0 if nothing is found, -1 if error, available after successful Load()
	GetTotalFilterSize() int // total size of filter, 0 if nothing is found, -1 if error
	GetNextStart() int       // index of next partition to retrieve, -1 if error is found, set to TotalFilterSize if all results returned
}

// UserListFilter is the struct that defines the filter for a UserList
type UserListFilter struct {
	Field           FilterField   `json:"field"`
	Value           string        `json:"value"`                    // if not set, defaults to all
	SortBy          SortDirection `json:"sort_direction,omitempty"` // default is AscendingSort
	MaxItems        int           `json:"max_items,omitempty"`      // requested max items to return in the list, defaults to DefaultUserListItemsMax
	Start           int           `json:"start_index"`              // start index to retrieve from total list, default 0
	IncludeDisabled bool          `json:"include_disabled"`         // if true, then disabled users will also be included in the filter (not yet implemented)
}

// FilterField is the name of the User field to be filtered on when listing users.
type FilterField string

// FilterField constants
const (
	UsernameFilterField FilterField = "UsernameField"
	IsAdminFilterField  FilterField = "IsAdminField"
)

// SortDirection is the sort direction in the list result, this is used for deployment list and user list(to be implemented)
type SortDirection int

// SortDirection constants
const (
	AscendingSort  SortDirection = 1 // This should be the default if omitted
	DescendingSort SortDirection = -1
)

// UserListModel implements UserList.
type UserListModel struct {
	Session
	Filter     UserListFilter
	Users      []UserModel
	StartIndex int `json:"start_index"`
	TotalSize  int `json:"total_size"`
	NextStart  int `json:"next_start"`
}

// GetFilter ...
func (ul *UserListModel) GetFilter() UserListFilter {
	return ul.Filter
}

// GetUsers ...
func (ul *UserListModel) GetUsers() []UserModel {
	return ul.Users
}

// GetStart ...
func (ul *UserListModel) GetStart() int {
	return ul.StartIndex
}

// GetSize ...
func (ul *UserListModel) GetSize() int {
	return len(ul.Users)
}

// GetTotalFilterSize ...
func (ul *UserListModel) GetTotalFilterSize() int {
	return ul.TotalSize
}

// GetNextStart ...
func (ul *UserListModel) GetNextStart() int {
	return ul.NextStart
}

// HasNext ...
func (ul *UserListModel) HasNext() bool {
	return ul.NextStart >= 0
}

// UserUpdateRequest ...
type UserUpdateRequest struct {
	Session
	Username string     `json:"username"`
	Update   UserUpdate `json:",inline"`
}

// UserUpdate ...
type UserUpdate struct {
	FirstName    *string              `json:"first_name,omitempty"`
	LastName     *string              `json:"last_name,omitempty"`
	PrimaryEmail *string              `json:"primary_email,omitempty"`
	IsAdmin      *bool                `json:"is_admin,omitempty"`
	Disabled     *bool                `json:"disabled,omitempty"`
	UserSettings *common.UserSettings `json:"settings,omitempty"`
}
