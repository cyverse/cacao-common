package service

import (
	"context"
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Nats subjects
const (
	// NatsSubjectUserAction is the prefix of the queue name for UserAction queries
	NatsSubjectUserAction common.QueryOp = common.NatsQueryOpPrefix + "user_action"

	// UserActionListQueryOp is the queue name for UserActionList query
	UserActionListQueryOp = NatsSubjectUserAction + ".list"
	// UserActionGetQueryOp is the queue name for UserActionGet query
	UserActionGetQueryOp = NatsSubjectUserAction + ".get"

	// UserActionCreateRequestedEvent is the cloudevent subject for UserActionCreate
	UserActionCreateRequestedEvent common.EventType = common.EventTypePrefix + "UserActionCreateRequested"
	// UserActionDeleteRequestedEvent is the cloudevent subject for UserActionDelete
	UserActionDeleteRequestedEvent common.EventType = common.EventTypePrefix + "UserActionDeleteRequested"
	// UserActionUpdateRequestedEvent is the cloudevent subject for UserActionUpdate
	UserActionUpdateRequestedEvent common.EventType = common.EventTypePrefix + "UserActionUpdateRequested"

	// UserActionCreatedEvent is the cloudevent subject for UserActionCreated
	UserActionCreatedEvent common.EventType = common.EventTypePrefix + "UserActionCreated"
	// UserActionDeletedEvent is the cloudevent subject for UserActionDeleted
	UserActionDeletedEvent common.EventType = common.EventTypePrefix + "UserActionDeleted"
	// UserActionUpdatedEvent is the cloudevent subject for UserActionUpdated
	UserActionUpdatedEvent common.EventType = common.EventTypePrefix + "UserActionUpdated"

	// UserActionCreateFailedEvent is the cloudevent subject for UserActionCreateFailed
	UserActionCreateFailedEvent common.EventType = common.EventTypePrefix + "UserActionCreateFailed"
	// UserActionDeleteFailedEvent is the cloudevent subject for UserActionDeleteFailed
	UserActionDeleteFailedEvent common.EventType = common.EventTypePrefix + "UserActionDeleteFailed"
	// UserActionUpdateFailedEvent is the cloudevent subject for UserActionUpdateFailed
	UserActionUpdateFailedEvent common.EventType = common.EventTypePrefix + "UserActionUpdateFailed"
)

// Nats Client
type natsUserActionClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsUserActionClientFromConn creates a new client from existing connections.
//
// Note, if you only need query functionality, then only supply queryConn, same
// for event functionality and eventConn.
func NewNatsUserActionClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (UserActionClient, error) {
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct user action client")
	}
	return &natsUserActionClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}, nil
}

// Get returns the user action with the given user action ID
func (client *natsUserActionClient) Get(ctx context.Context, actor Actor, userActionID common.ID) (*UserActionModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserActionClient.Get",
	})

	if len(userActionID) == 0 {
		return nil, NewCacaoInvalidParameterError("invalid UserAction ID")
	}

	request := UserActionModel{
		Session: actor.Session(),
		ID:      userActionID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, UserActionGetQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var userActionModel UserActionModel
	err = json.Unmarshal(replyCe.Data(), &userActionModel)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to user action object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := userActionModel.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}
	return &userActionModel, nil
}

// List returns all user actions for the user
func (client *natsUserActionClient) List(ctx context.Context, actor Actor) ([]UserActionModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserActionClient.List",
	})
	var request Session = actor.Session()
	requestCe, err := messaging2.CreateCloudEvent(request, UserActionListQueryOp, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return nil, err
	}
	replyCe, err := client.queryConn.Request(ctx, requestCe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return nil, err
	}

	var list UserActionListModel
	err = json.Unmarshal(replyCe.Data(), &list)
	if err != nil {
		errorMessage := "unable to unmarshal response JSON to user action list object"
		logger.WithError(err).Error(errorMessage)
		return nil, NewCacaoMarshalError(errorMessage)
	}

	serviceError := list.GetServiceError()
	if serviceError != nil {
		logger.Error(serviceError)
		return nil, serviceError
	}

	return list.UserActions, nil
}

// Create creates a new user action
func (client *natsUserActionClient) Create(ctx context.Context, actor Actor, userAction UserActionModel) (common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserActionClient.Create",
	})

	if len(userAction.Name) == 0 {
		return common.ID(""), NewCacaoInvalidParameterError("invalid UserAction Name")
	}

	userActionID := userAction.ID
	if !userActionID.Validate() {
		userActionID = NewUserActionID()
	}
	request := UserActionModel{
		Session:     actor.Session(),
		ID:          userActionID,
		Owner:       actor.Actor,
		Name:        userAction.Name,
		Description: userAction.Description,
		Public:      userAction.Public,
		Type:        userAction.Type,
		Action:      userAction.Action,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, UserActionCreateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return "", err
	}
	eventsSubscribe := []common.EventType{
		UserActionCreatedEvent,
		UserActionCreateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return "", err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return "", err
	}

	var response UserActionModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user action create response"
		logger.WithError(err).Error(errorMessage)
		return "", NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return response.ID, nil
	}

	logger.Error(serviceError)
	return "", serviceError
}

// Update updates the user action with the user action ID
func (client *natsUserActionClient) Update(ctx context.Context, actor Actor, userAction UserActionModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserActionClient.Update",
	})

	if !userAction.ID.Validate() {
		return NewCacaoInvalidParameterError("invalid UserAction ID")
	}

	if len(userAction.Name) == 0 {
		return NewCacaoInvalidParameterError("invalid UserAction Name")
	}

	request := UserActionModel{
		Session:     actor.Session(),
		ID:          userAction.ID,
		Name:        userAction.Name,
		Description: userAction.Description,
		Public:      userAction.Public,
		Type:        userAction.Type,
		Action:      userAction.Action,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, UserActionUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		UserActionUpdatedEvent,
		UserActionUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response UserActionModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user action update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// UpdateFields updates the selected fields of the user action with the user action ID
func (client *natsUserActionClient) UpdateFields(ctx context.Context, actor Actor, userAction UserActionModel, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserActionClient.UpdateFields",
	})

	if !userAction.ID.Validate() {
		return NewCacaoInvalidParameterError("invalid UserAction ID")
	}

	if len(updateFieldNames) == 0 {
		return nil
	}

	for _, updateFieldName := range updateFieldNames {
		switch updateFieldName {
		case "name":
			if len(userAction.Name) == 0 {
				return NewCacaoInvalidParameterError("invalid UserAction Name")
			}
		default:
			//pass
		}
	}

	request := UserActionModel{
		Session:          actor.Session(),
		ID:               userAction.ID,
		Name:             userAction.Name,
		Description:      userAction.Description,
		Public:           userAction.Public,
		Type:             userAction.Type,
		Action:           userAction.Action,
		UpdateFieldNames: updateFieldNames,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, UserActionUpdateRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		UserActionUpdatedEvent,
		UserActionUpdateFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response UserActionModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user action update response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}

// Delete deletes the user action with the user action ID
func (client *natsUserActionClient) Delete(ctx context.Context, actor Actor, userActionID common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsUserActionClient.Delete",
	})

	if !userActionID.Validate() {
		return NewCacaoInvalidParameterError("invalid UserAction ID")
	}

	request := UserActionModel{
		Session: actor.Session(),
		ID:      userActionID,
	}
	transactionID := messaging2.NewTransactionID()
	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, UserActionDeleteRequestedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Error("fail to create cloudevent")
		return err
	}
	eventsSubscribe := []common.EventType{
		UserActionDeletedEvent,
		UserActionDeleteFailedEvent,
	}
	promise, err := client.eventConn.Request(requestCe, eventsSubscribe)
	if err != nil {
		logger.WithError(err).Error("fail to request")
		return err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		logger.WithError(err).Error("fail to wait for response")
		return err
	}

	var response UserActionModel
	err = json.Unmarshal(responseCe.Data(), &response)
	if err != nil {
		errorMessage := "unable to unmarshal a user action delete response"
		logger.WithError(err).Error(errorMessage)
		return NewCacaoMarshalError(errorMessage)
	}
	serviceError := response.GetServiceError()
	if serviceError == nil {
		return nil
	}

	logger.Error(serviceError)
	return serviceError
}
