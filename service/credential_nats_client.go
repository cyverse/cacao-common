package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

type natsCredentialClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewNatsCredentialClientFromConn creates a new client from existing
// Connections. Note if you only need query functionality, then only supply
// queryConn, same for event functionality and eventConn.
func NewNatsCredentialClientFromConn(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) (CredentialClient, error) {
	log.WithFields(log.Fields{
		"package":  "service",
		"function": "NewNatsCredentialClientFromConn",
	}).Trace("called")
	if queryConn == nil && eventConn == nil {
		return nil, fmt.Errorf("query and event connection are both nil, cannot construct credential client")
	}
	newCredentialSvc := natsCredentialClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
	return &newCredentialSvc, nil
}

// Get fetch a single credential with the given ID owned by a user
func (credsvc *natsCredentialClient) Get(ctx context.Context, actor Actor, ID string) (*CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "services",
		"function": "natsCredentialClient.Get",
		"username": actor.Actor,
		"id":       ID,
	})
	logger.Trace("called")

	if credsvc.queryConn == nil {
		return nil, fmt.Errorf("query connection is not set for credential client")
	}

	if len(ID) <= 0 {
		return nil, NewCacaoInvalidParameterError("invalid credential ID")
	}

	request := CredentialGetRequest{
		Session:  actor.Session(),
		Username: actor.Actor,
		ID:       ID,
	}
	requestCe, err := messaging2.CreateCloudEvent(request, NatsSubjectCredentialsGet, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		return nil, err
	}
	replyCe, err := credsvc.queryConn.Request(ctx, requestCe)
	if err != nil {
		return nil, err
	}

	var reply CredentialModel
	err = json.Unmarshal(replyCe.Data(), &reply)
	if err != nil {
		return nil, NewCacaoMarshalError(err.Error())
	}

	// final check for error
	if reply.GetServiceError() != nil {
		logger.WithField("errType", reply.ErrorType).Trace("error was received")
		return &reply, reply.GetServiceError()
	}
	if reply.ErrorType != "" {
		logger.WithField("errType", reply.ErrorType).Trace("error was received")
		err = errors.New(reply.ErrorType)
	}

	return &reply, err
}

// List lists all credentials owned by a user
func (credsvc *natsCredentialClient) List(ctx context.Context, actor Actor, filter CredentialListFilter) ([]CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "services",
		"function": "natsCredentialClient.List",
	})
	logger.Trace("called")

	if credsvc.queryConn == nil {
		return nil, fmt.Errorf("query connection is not set for credential client")
	}

	request := CredentialListRequest{
		Session: actor.Session(),
		Filter: CredentialListFilter{
			Username: actor.Actor,
			Type:     filter.Type,
			IsSystem: filter.IsSystem,
			Disabled: filter.Disabled,
			Tags:     filter.Tags,
		},
	}
	requestCe, err := messaging2.CreateCloudEvent(request, NatsSubjectCredentialsList, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		return nil, err
	}
	replyCe, err := credsvc.queryConn.Request(ctx, requestCe)
	if err != nil {
		return nil, err
	}

	var list []CredentialModel
	err = json.Unmarshal(replyCe.Data(), &list)
	if err != nil {
		return nil, err
	}

	return list, err
}

// Add creates a credential at the specified ID, will overwrite existing one with the same ID.
func (credsvc *natsCredentialClient) Add(ctx context.Context, actor Actor, credential CredentialModel) (credID string, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "services",
		"function": "natsCredentialClient.Add",
	})
	logger.Trace("called")

	if credsvc.eventConn == nil {
		return "", fmt.Errorf("event connection is not set for credential client")
	}

	valid, verr := credential.Verify()
	if !valid || verr != nil {
		logger.WithFields(log.Fields{"error": verr}).Error("Unable to verify provided Credential")
		return "", verr
	}
	credential.SetSessionActor(actor.Actor)
	credential.SetSessionEmulator(actor.Emulator)
	responseCe, err := credsvc.stanRequestSync(ctx, credential, EventCredentialAddRequested, []common.EventType{EventCredentialAdded, EventCredentialAddError})
	if err != nil {
		return "", err
	}
	var resp CredentialCreateResponse
	err = json.Unmarshal(responseCe.Data(), &resp)
	if err != nil {
		logMsg := "unable to unmarshal a credential delete response"
		logger.WithError(err).Error(logMsg)
		return "", NewCacaoMarshalError(err.Error())
	}
	if resp.GetServiceError() != nil {
		return "", resp.GetServiceError()
	}
	if common.EventType(responseCe.Type()) == EventCredentialAdded {
		if resp.ID == "" {
			return "", fmt.Errorf("credential successfully created, but credential ID is not returned")
		}
		return resp.ID, nil
	}
	return resp.ID, fmt.Errorf("unknown error, operation failed, but error is not included in response event")
}

func (credsvc *natsCredentialClient) Generate(ctx context.Context, actor Actor, credential CredentialModel) (credID string, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "services",
		"function": "natsCredentialClient.Generate",
	})
	logger.Trace("called")

	if credsvc.eventConn == nil {
		return "", fmt.Errorf("event connection is not set for credential client")
	}

	var verr error
	if credential.Username == "" {
		verr = errors.New("username not set, could not verify")
	} else if credential.Type == "" {
		verr = errors.New("type not set, could not verify")
	}
	if verr != nil {
		logger.WithFields(log.Fields{"error": verr}).Error("Unable to verify provided Credential")
		return "", verr
	}

	credential.SetSessionActor(actor.Actor)
	credential.SetSessionEmulator(actor.Emulator)
	responseCe, err := credsvc.stanRequestSync(ctx, credential, EventCredentialGenerationRequested, []common.EventType{EventCredentialAdded, EventCredentialAddError})
	if err != nil {
		return "", err
	}
	var resp CredentialCreateResponse
	err = json.Unmarshal(responseCe.Data(), &resp)
	if err != nil {
		logMsg := "unable to unmarshal a credential delete response"
		logger.WithError(err).Error(logMsg)
		return "", NewCacaoMarshalError(err.Error())
	}
	if resp.GetServiceError() != nil {
		return "", resp.GetServiceError()
	}
	if common.EventType(responseCe.Type()) == EventCredentialAdded {
		if resp.ID == "" {
			return "", fmt.Errorf("credential successfully created, but credential ID is not returned")
		}
		return resp.ID, nil
	}
	return resp.ID, fmt.Errorf("unknown error, operation failed, but error is not included in response event")
}

// Update is the same Add() as of now.
func (credsvc *natsCredentialClient) Update(ctx context.Context, actor Actor, credID string, update CredentialUpdate) error {
	logger := log.WithFields(log.Fields{
		"package":  "services",
		"function": "natsCredentialClient.Update",
	})
	logger.Trace("called")

	if credsvc.eventConn == nil {
		return fmt.Errorf("event connection is not set for credential client")
	}

	if !actor.IsSet() {
		return NewCacaoInvalidParameterError("username(actor) cannot be empty, unable to update credential")
	}
	if credID == "" {
		return NewCacaoInvalidParameterError("credential ID cannot be empty, unable to update credential")
	}
	req := CredentialUpdateRequest{
		Session:  actor.Session(),
		Username: actor.Actor,
		ID:       credID,
		Update:   update,
	}

	responseCe, err := credsvc.stanRequestSync(ctx, req, EventCredentialUpdateRequested, []common.EventType{EventCredentialUpdated, EventCredentialUpdateError})
	if err != nil {
		return err
	}
	switch common.EventType(responseCe.Type()) {
	case EventCredentialUpdated:
		return nil
	case EventCredentialUpdateError:
	}
	var resp CredentialUpdateResponse
	err = json.Unmarshal(responseCe.Data(), &resp)
	if err != nil {
		logMsg := "unable to unmarshal a credential update response"
		logger.WithError(err).Error(logMsg)
		return err
	}
	if resp.GetServiceError() != nil {
		return resp.GetServiceError()
	}
	return fmt.Errorf("unknown error, credential update operation failed, but error is not included in response event")
}

// Delete deletes a credential of the given ID owned by a user.
func (credsvc *natsCredentialClient) Delete(ctx context.Context, actor Actor, ID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "service",
		"function": "natsCredentialClient.Delete",
	})
	logger.Trace("called")

	if credsvc.eventConn == nil {
		return fmt.Errorf("event connection is not set for credential client")
	}

	if len(ID) == 0 {
		return fmt.Errorf("invalid ID format")
	}
	request := CredentialDeleteRequest{
		Session:  actor.Session(),
		Username: actor.Actor,
		ID:       ID,
	}
	responseCe, err := credsvc.stanRequestSync(ctx, request, EventCredentialDeleteRequested, []common.EventType{EventCredentialDeleteError, EventCredentialDeleted})
	if err != nil {
		return err
	}

	switch common.EventType(responseCe.Type()) {
	case EventCredentialDeleted:
		return nil
	case EventCredentialDeleteError:
	}
	var resp CredentialDeleteResponse
	err = json.Unmarshal(responseCe.Data(), &resp)
	if err != nil {
		logMsg := "unable to unmarshal a credential delete response"
		logger.WithError(err).Error(logMsg)
		return fmt.Errorf("%s, %w", logMsg, err)
	}
	if resp.GetServiceError() != nil {
		return resp.GetServiceError()
	}
	return nil
}

// send a request via STAN and wait for response.
// this is a synchronized method.
func (credsvc *natsCredentialClient) stanRequestSync(ctx context.Context, request interface{}, requestEventType common.EventType, responseEventTypes []common.EventType) (cloudevents.Event, error) {
	transactionID := messaging2.NewTransactionID()

	requestCe, err := messaging2.CreateCloudEventWithTransactionID(request, requestEventType, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		return cloudevents.Event{}, err
	}
	promise, err := credsvc.eventConn.Request(requestCe, responseEventTypes)
	if err != nil {
		return cloudevents.Event{}, err
	}
	responseCe, err := promise.Response(ctx)
	if err != nil {
		return cloudevents.Event{}, err
	}
	return responseCe, nil
}
