package service

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/db"
	"go.mongodb.org/mongo-driver/bson"
)

func TestTemplateParameter_CheckValue(t *testing.T) {
	type fields struct {
		Name               string
		Type               string
		Description        string
		Default            interface{}
		Enum               []interface{}
		DataValidationType string
		Required           bool
		Editable           bool
		Base64             bool
	}
	type args struct {
		value interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		want1   interface{}
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "empty string no default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "",
			},
			want:    "string",
			want1:   "", // empty string is a valid string value, so we use the input parameter value as is
			wantErr: assert.NoError,
		},
		{
			name: "empty string w/ non-empty default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            "default-str",
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "",
			},
			want:    "string",
			want1:   "", // empty string is a valid string value, lack of value (which triggers default value) is represented by nil
			wantErr: assert.NoError,
		},
		{
			name: "empty string w/ empty default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            "",
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "",
			},
			want:    "string",
			want1:   "", // use the default value
			wantErr: assert.NoError,
		},
		{
			name: "0 w/ default",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            123,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 0,
			},
			want:    "integer",
			want1:   int64(0),
			wantErr: assert.NoError,
		},
		{
			name: "string w/ default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            "foo",
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: nil,
			},
			want:    "string",
			want1:   "foo",
			wantErr: assert.NoError,
		},
		{
			name: "string w/ enum",
			fields: fields{
				Name:    "param1",
				Type:    "string",
				Default: nil,
				Enum: []interface{}{
					"foo",
					"bar",
				},
				DataValidationType: "",
			},
			args: args{
				value: "foo",
			},
			want:    "string",
			want1:   "foo",
			wantErr: assert.NoError,
		},
		{
			name: "string value not in enum",
			fields: fields{
				Name:    "param1",
				Type:    "string",
				Default: nil,
				Enum: []interface{}{
					"foo",
					"bar",
				},
				DataValidationType: "",
			},
			args: args{
				value: "foobar",
			},
			want:    "string",
			want1:   nil,
			wantErr: assert.Error,
		},
		{
			name: "integer",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 123,
			},
			want:    "integer",
			want1:   int64(123),
			wantErr: assert.NoError,
		},
		{
			name: "integer from float",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 123.5,
			},
			want:    "integer",
			want1:   int64(123),
			wantErr: assert.NoError,
		},
		{
			name: "integer from float (no decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 123.0,
			},
			want:    "integer",
			want1:   int64(123),
			wantErr: assert.NoError,
		},
		{
			name: "integer from json number (zero decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("123.0"), &result)
					return result
				}(),
			},
			want:    "integer",
			want1:   int64(123),
			wantErr: assert.NoError,
		},
		{
			name: "integer from json number (has decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("123.25"), &result)
					return result
				}(),
			},
			want:    "integer",
			want1:   int64(123),
			wantErr: assert.NoError,
		},
		{
			name: "integer from json number (no decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("123"), &result)
					return result
				}(),
			},
			want:    "integer",
			want1:   int64(123),
			wantErr: assert.NoError,
		},
		{
			name: "integer from string 123",
			fields: fields{
				Name:               "param1",
				Type:               "integer",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "123",
			},
			want:    "",
			want1:   nil,
			wantErr: assert.Error,
		},
		{
			name: "float from float",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 123.5,
			},
			want:    "float",
			want1:   123.5,
			wantErr: assert.NoError,
		},
		{
			name: "float from float32",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: float32(123.5),
			},
			want:    "float",
			want1:   123.5,
			wantErr: assert.NoError,
		},
		{
			name: "float from float64",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: float64(123.5),
			},
			want:    "float",
			want1:   123.5,
			wantErr: assert.NoError,
		},
		{
			name: "float from float (no decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 123.0,
			},
			want:    "float",
			want1:   123.0,
			wantErr: assert.NoError,
		},
		{
			name: "float from integer",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 123,
			},
			want:    "float",
			want1:   123.0,
			wantErr: assert.NoError,
		},
		{
			name: "float from json number(no decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("123"), &result)
					return result
				}(),
			},
			want:    "float",
			want1:   123.0,
			wantErr: assert.NoError,
		},
		{
			name: "float from json number(zero decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("123.0"), &result)
					return result
				}(),
			},
			want:    "float",
			want1:   123.0,
			wantErr: assert.NoError,
		},
		{
			name: "float from json number(has decimal)",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("123.125"), &result)
					return result
				}(),
			},
			want:    "float",
			want1:   123.125,
			wantErr: assert.NoError,
		},
		{
			name: "float from string 123.5",
			fields: fields{
				Name:               "param1",
				Type:               "float",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "123.5",
			},
			want:    "",
			want1:   nil,
			wantErr: assert.Error,
		},
		{
			name: "bool true",
			fields: fields{
				Name:               "param1",
				Type:               "bool",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: true,
			},
			want:    "bool",
			want1:   true,
			wantErr: assert.NoError,
		},
		{
			name: "bool false",
			fields: fields{
				Name:               "param1",
				Type:               "bool",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: false,
			},
			want:    "bool",
			want1:   false,
			wantErr: assert.NoError,
		},
		{
			name: "bool from json true",
			fields: fields{
				Name:               "param1",
				Type:               "bool",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: func() interface{} {
					var result interface{}
					_ = json.Unmarshal([]byte("true"), &result)
					return result
				}(),
			},
			want:    "bool",
			want1:   true,
			wantErr: assert.NoError,
		},
		{
			name: "bool from number",
			fields: fields{
				Name:               "param1",
				Type:               "bool",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: 1,
			},
			want:    "",
			want1:   nil,
			wantErr: assert.Error,
		},
		{
			name: "bool from string true",
			fields: fields{
				Name:               "param1",
				Type:               "bool",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "true",
			},
			want:    "",
			want1:   nil,
			wantErr: assert.Error,
		},
		{
			name: "special type",
			fields: fields{
				Name:               "param1",
				Type:               "cacao_username",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "foobar",
			},
			want:    "cacao_username",
			want1:   "foobar",
			wantErr: assert.NoError,
		},
		// TODO: Add test cases.
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			param := &TemplateParameter{
				Name:               tt.fields.Name,
				Type:               tt.fields.Type,
				Description:        tt.fields.Description,
				Default:            tt.fields.Default,
				Enum:               tt.fields.Enum,
				DataValidationType: tt.fields.DataValidationType,
				Required:           tt.fields.Required,
				Editable:           tt.fields.Editable,
				Base64:             tt.fields.Base64,
			}
			got, got1, err := param.CheckValue(tt.args.value)
			if !tt.wantErr(t, err, fmt.Sprintf("CheckValue(%v)", tt.args.value)) {
				return
			}
			assert.Equalf(t, tt.want, got, "CheckValue(%v)", tt.args.value)
			assert.Equalf(t, tt.want1, got1, "CheckValue(%v)", tt.args.value)
		})
	}
}

func TestTemplateParameter_CheckValueFromString(t *testing.T) {
	type fields struct {
		Name               string
		Type               string
		Description        string
		Default            interface{}
		Enum               []interface{}
		DataValidationType string
		Required           bool
		Editable           bool
		Base64             bool
	}
	type args struct {
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		want1   interface{}
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "empty string no default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            nil,
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "",
			},
			want:    "string",
			want1:   "", // empty string is a valid string value, so we use the input parameter value as is
			wantErr: assert.NoError,
		},
		{
			name: "empty string w/ non-empty default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            "default-str",
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "",
			},
			want:    "string",
			want1:   "default-str", // empty string is treated as lack of value when there is default defined, therefore triggers default value, this differs from the behavior of CheckValue()
			wantErr: assert.NoError,
		},
		{
			name: "empty string w/ empty default",
			fields: fields{
				Name:               "param1",
				Type:               "string",
				Default:            "",
				Enum:               nil,
				DataValidationType: "",
			},
			args: args{
				value: "",
			},
			want:    "string",
			want1:   "", // use the default value
			wantErr: assert.NoError,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			param := &TemplateParameter{
				Name:               tt.fields.Name,
				Type:               tt.fields.Type,
				Description:        tt.fields.Description,
				Default:            tt.fields.Default,
				Enum:               tt.fields.Enum,
				DataValidationType: tt.fields.DataValidationType,
				Required:           tt.fields.Required,
				Editable:           tt.fields.Editable,
				Base64:             tt.fields.Base64,
			}
			got, got1, err := param.CheckValueFromString(tt.args.value)
			if !tt.wantErr(t, err, fmt.Sprintf("CheckValueFromString(%v)", tt.args.value)) {
				return
			}
			assert.Equalf(t, tt.want, got, "CheckValueFromString(%v)", tt.args.value)
			assert.Equalf(t, tt.want1, got1, "CheckValueFromString(%v)", tt.args.value)
		})
	}
}

func TestTemplateMetadata_BSON(t *testing.T) {
	jsonBytes := `{
		"schema_url": "https://gitlab.com/cyverse/cacao-common/-/raw/master/template/metadata-schemas/v3/schema.json",
		"schema_version": "3",
		"name": "single image openstack instances",
		"author": "Edwin Skidmore",
		"author_email": "edwin@cyverse.org",
		"description": "simple launch of one or more vms",
		"template_type": "openstack_terraform",
		"purpose": "openstack_compute",
		"published_versions": [],
		"cacao_pre_tasks": [],
		"cacao_post_tasks": [
		  {
			"type": "ansible",
			"location": "cacao_atmosphere_legacy"
		  }
		],
		"parameters": [
		  {
			"name": "username",
			"type": "cacao_username",
			"default": "username",
			"description": "CACAO username"
		  },
		  {
			"name": "region",
			"type": "cacao_provider_region",
			"default": "IU",
			"description": "Openstack region"
		  },
		  {
			"name": "instance_name",
			"type": "string",
			"description": "Instance name",
			"editable": true,
			"base64": false
		  },
		  {
			"name": "instance_count",
			"type": "integer",
			"default": 1,
			"description": "# of instances",
			"editable": true,
			"base64": false
		  },
		  {
			"name": "image",
			"type": "cacao_provider_image",
			"description": "Boot image id",
			"default":""
		  },
		  {
			"name": "image_name",
			"type": "cacao_provider_image_name",
			"description": "Boot image name",
			"default":""
		  },
		  {
			"name": "flavor",
			"type": "cacao_provider_flavor",
			"description": "Instance type"
		  },
		  {
			"name": "project",
			"type": "cacao_provider_project",
			"description": "OpenStack Project ID"
		  },
		  {
			"name": "keypair",
			"type": "cacao_provider_key_pair",
			"default": "cacao-ssh-key",
			"description": "Key-pair for instance access"
		  },
		  {
			"name": "power_state",
			"type": "string",
			"default": "active",
			"enum": [
			  "active",
			  "shutoff",
			  "suspend",
			  "shelved_offloaded"
			],
			"description": "Power state",
			"editable": true,
			"base64": false
		  },
		  {
			"name": "ip_pool",
			"type": "cacao_provider_external_network",
			"default": "external_network_name",
			"description": "IP pool"
		  },
		  {
			"name": "user_data",
			"type": "cacao_cloud_init",
			"description": "cloud init script"
		  },
		  {
			"name": "root_storage_source",
			"type": "string",
			"default": "image",
			"enum": [
			  "image"
			],
			"description": "Source of root disk; currently, only image is supported "
		  },
		  {
			"name": "root_storage_type",
			"type": "string",
			"default": "local",
			"enum": [
			  "local",
			  "volume"
			],
			"description": "type of root storage, either local or volume"
		  },
		  {
			"name": "root_storage_size",
			"type": "integer",
			"description": "Size of root storage in GB",
			"default" : -1
		  },
		  {
			"name": "root_storage_delete_on_termination",
			"type": "bool",
			"description": "if true, will delete the root storage when instance is deleted",
			"default": true
		  }
		]
	  }`

	var metadata TemplateMetadata

	err := json.Unmarshal([]byte(jsonBytes), &metadata)
	assert.NoError(t, err)

	// bson
	registry, err := db.GetBSONRegistry()
	assert.NoError(t, err)

	bsonBytes, err := bson.MarshalWithRegistry(registry, metadata)
	assert.NoError(t, err)

	var metadata2 TemplateMetadata
	err = bson.UnmarshalWithRegistry(registry, bsonBytes, &metadata2)
	assert.NoError(t, err)

	assert.Equal(t, "Edwin Skidmore", metadata2.Author)
	assert.Len(t, metadata2.CacaoPostTasks, 1)
	assert.Len(t, metadata2.Parameters, 16)
}
